package ch.mobi.mobitor.config;

/*-
 * §
 * mobitor-plugin-dwh
 * --
 * Copyright (C) 2019 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import io.netty.handler.ssl.SslContextBuilder;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.http.codec.ClientCodecConfigurer;
import org.springframework.http.codec.json.Jackson2JsonDecoder;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.netty.http.client.HttpClient;

import javax.net.ssl.KeyManagerFactory;
import java.io.File;
import java.io.FileInputStream;
import java.security.KeyStore;
import java.util.function.Consumer;

public class WebClients {
    private static final Consumer<ClientCodecConfigurer> jacksonConfigurer = ccc ->
            ccc.defaultCodecs().jackson2JsonDecoder(new Jackson2JsonDecoder(ObjectMappers.standard()));
    private static final WebClient standardClient = standardBuilder().build();
    private static WebClient keyStoreClient = null;

    private WebClients() {
    }

    public static WebClient standard() {
        return standardClient;
    }

    public static WebClient withKeyStore() {
        return keyStoreClient();
    }

    private static WebClient.Builder standardBuilder() {
        return WebClient.builder()
                .exchangeStrategies(ExchangeStrategies.builder().codecs(jacksonConfigurer).build());
    }

    private static WebClient keyStoreClient() {
        if (keyStoreClient == null) {
            HttpClient httpClient = HttpClient.create().secure(spec -> {
                try {
                    String keyStoreLocation = System.getProperty("javax.net.ssl.keyStore");
                    String keyStorePassword = System.getProperty("javax.net.ssl.keyStorePassword");

                    KeyManagerFactory keyManagerFactory = null;

                    if (keyStoreLocation != null && keyStorePassword != null) {
                        String keyStoreType = System.getProperty("javax.net.ssl.keyStoreType", "JKS");

                        KeyStore keyStore = KeyStore.getInstance(keyStoreType);
                        keyStore.load(new FileInputStream(new File(keyStoreLocation)), keyStorePassword.toCharArray());

                        keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
                        keyManagerFactory.init(keyStore, keyStorePassword.toCharArray());
                    }

                    spec.sslContext(SslContextBuilder.forClient()
                            .keyManager(keyManagerFactory)
                            .build());
                } catch (Exception e) {
                    throw new RuntimeException("Could not setup SSL context", e);
                }
            });
            keyStoreClient = standardBuilder()
                    .clientConnector(new ReactorClientHttpConnector(httpClient))
                    .build();
        }
        return keyStoreClient;
    }
}
