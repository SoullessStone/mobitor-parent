package ch.mobi.mobitor.service;

/*-
 * §
 * mobitor-plugins-api
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.config.EnvironmentConfigProperties;
import ch.mobi.mobitor.config.EnvironmentNetwork;
import ch.mobi.mobitor.config.EnvironmentPipeline;

import java.util.List;

public interface EnvironmentsConfigurationService {

    boolean isNetworkReachable(String environment);

    boolean isNetworkReachable(EnvironmentNetwork network);

    EnvironmentNetwork getCurrentNetwork();

    String getName(String environment);

    EnvironmentPipeline getPipeline(String environment);

    EnvironmentConfigProperties getEnvironmentConfig(String environment);

    List<EnvironmentConfigProperties> getEnvironments();

    boolean isEnvironmentReachable(String environment);
}
