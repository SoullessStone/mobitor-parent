package ch.mobi.mobitor.plugins.api.domain.screen.information;

/*-
 * §
 * mobitor-plugins-api
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ApplicationInformationLegendWrapperTest {

    private ApplicationInformation applicationInformation = mock(ApplicationInformation.class);

    @Before
    public void onStartup(){
        when(applicationInformation.getType()).thenReturn("bitbucket");
    }

    @Test
    public void getApplicationInformation() {
        ApplicationInformationLegendWrapper legendWrapper = new ApplicationInformationLegendWrapper(
                "Bitbucket Plugin",applicationInformation);

        ApplicationInformation information = legendWrapper.getApplicationInformation();

        assertNotNull(information);
        assertEquals("bitbucket", information.getType());
    }

    @Test
    public void getDescription() {
        ApplicationInformationLegendWrapper legendWrapper = new ApplicationInformationLegendWrapper(
                "Bitbucket Plugin",applicationInformation);

        String description = legendWrapper.getDescription();

        assertEquals("Bitbucket Plugin", description);
    }
}
