package ch.mobi.mobitor.plugins.api.domain.config;

/*-
 * §
 * mobitor-plugins-api
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import com.fasterxml.jackson.databind.JsonNode;
import org.junit.Test;

import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

public class ExtendableScreenConfigTest {

    @Test
    public void getPluginConfigNodeShouldReturnNode() {
        //arrange
        ExtendableScreenConfig config = new ExtendableScreenConfig();
        String property = "prop";
        JsonNode jsonNode = mock(JsonNode.class);
        config.setPluginConfig(property, jsonNode);

        //act
        JsonNode pluginConfigNode = config.getPluginConfigNode(property);

        //assert
        assertThat(pluginConfigNode).isEqualTo(jsonNode);
    }

    @Test
    public void getUnreadPluginConfigPropertiesShouldReturnUnreadProperties() {
        //arrange
        ExtendableScreenConfig config = new ExtendableScreenConfig();
        String readProperty = "prop";
        String unreadProperty = "prop2";
        String anotherUnreadProperty = "prop3";
        JsonNode jsonNode = mock(JsonNode.class);
        config.setPluginConfig(readProperty, jsonNode);
        config.setPluginConfig(unreadProperty, jsonNode);
        config.setPluginConfig(anotherUnreadProperty, jsonNode);

        config.getPluginConfigNode(readProperty);

        //act
        Set<String> unreadPluginConfigProperties = config.getUnreadPluginConfigProperties();

        //assert
        assertThat(unreadPluginConfigProperties).containsExactly(unreadProperty, anotherUnreadProperty);
    }

}
