package ch.mobi.mobitor.plugins.api.domain.config;

/*-
 * §
 * mobitor-plugins-api
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class OnDutyConfigTest {


    @Test
    public void testSprintDurationProperty() {
        OnDutyConfig config = new OnDutyConfig();
        int sprintDuration = 999;

        config.setSprintDuration(sprintDuration);

        assertEquals(config.getSprintDuration(),sprintDuration);
    }

    @Test
    public void testSprintStartProperty() {
        OnDutyConfig config = new OnDutyConfig();
        String sprintStart = "01.01.2019";

        config.setSprintStart(sprintStart);

        assertEquals(config.getSprintStart(),sprintStart);
    }


    @Test
    public void testTeamsProperty() {
        OnDutyConfig config = new OnDutyConfig();
        List<String> teams = new ArrayList<>();
        teams.add("G-One");
        teams.add("Falcon");

        config.setTeams(teams);

        assert(config.getTeams().contains("Falcon"));
        assert (config.getTeams().contains("G-One"));
    }
}
