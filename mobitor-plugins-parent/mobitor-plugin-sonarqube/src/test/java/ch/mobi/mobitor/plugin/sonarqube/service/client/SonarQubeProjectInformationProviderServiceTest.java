package ch.mobi.mobitor.plugin.sonarqube.service.client;

/*-
 * §
 * mobitor-plugin-sonarqube
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.sonarqube.domain.SonarInformation;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mockito;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.givenThat;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;


public class SonarQubeProjectInformationProviderServiceTest {

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(options().dynamicPort());

    @Test
    public void updateProjectInformation() {
        // arrange
        final String projectKey = "ch.mobi.ovn:ovn";

        givenThat(get(urlMatching("/api/measures/component?(.*)"))
                .withQueryParam("component", equalTo(projectKey))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("sonar-report-response-app2.json")
                )
        );

        String baseUrl = "http://localhost:" + wireMockRule.port();

        SonarQubeConfiguration sonarConf = new SonarQubeConfiguration();
        sonarConf.setBaseUrl(baseUrl);

        SonarQubeClient client = new SonarQubeClient(sonarConf);
        CoverageMetricService metricService = Mockito.mock(CoverageMetricService.class);

        // act
        SonarQubeProjectInformationProviderService service = new SonarQubeProjectInformationProviderService(client, metricService);
        SonarInformation sonarInfo = new SonarInformation(projectKey, false);
        service.updateProjectInformation(sonarInfo);

        // assert
        assertThat(sonarInfo, is(not(nullValue())));
        assertThat(sonarInfo.getLineCoverage(), is(not(nullValue())));
        assertThat(sonarInfo.getDuplicatedBlocks(), is(greaterThan(1f)));
    }

}
