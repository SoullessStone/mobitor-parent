package ch.mobi.mobitor.plugin.sonarqube.rule;

/*-
 * §
 * mobitor-plugin-sonarqube
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.PipelineRule;
import ch.mobi.mobitor.domain.screen.Pipeline;
import ch.mobi.mobitor.domain.screen.RuleEvaluation;
import ch.mobi.mobitor.plugin.sonarqube.domain.SonarInformation;
import ch.mobi.mobitor.plugin.test.rule.PipelineRuleTest;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import static ch.mobi.mobitor.plugin.sonarqube.domain.SonarInformation.SONAR;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@RunWith(JUnitParamsRunner.class)
public class SonarReportLimitsRuleTest extends PipelineRuleTest {

    @Override
    protected PipelineRule createNewRule() {
        return new SonarReportLimitsRule();
    }

    @Test
    public void evaluateRuleAllGoodStrict() {
        // arrange
        SonarInformation sonarInformation = new SonarInformation("junit-projectKey", true);
        sonarInformation.setCoverage(90);

        Pipeline pipeline = createPipeline();
        pipeline.addInformation(ENV, APP_NAME, sonarInformation);

        // act
        RuleEvaluation newRuleEvaluation = createNewRuleEvaluation();
        evaluateRule(pipeline, newRuleEvaluation);

        // assert
        assertThat(newRuleEvaluation, is(not(nullValue())));
        assertThat(newRuleEvaluation.hasErrors(), is(false));
    }

    @Test
    public void evaluateRuleAllGoodLazy() {
        // arrange
        SonarInformation sonarInformation = new SonarInformation("junit-projectKey", false);
        sonarInformation.setCoverage(10);
        sonarInformation.setOverallCoverage(82);

        Pipeline pipeline = createPipeline();
        pipeline.addInformation(ENV, APP_NAME, sonarInformation);

        // act
        RuleEvaluation newRuleEvaluation = createNewRuleEvaluation();
        evaluateRule(pipeline, newRuleEvaluation);

        // assert
        assertThat(newRuleEvaluation, is(not(nullValue())));
        assertThat(newRuleEvaluation.hasErrors(), is(false));
    }

    @Test
    public void evaluateRuleWithViolationsStrict() {
        // arrange
        SonarInformation sonarInformation = new SonarInformation("junit-projectKey", true);
        sonarInformation.setCoverage(69);

        Pipeline pipeline = createPipeline();
        pipeline.addInformation(ENV, APP_NAME, sonarInformation);

        // act
        RuleEvaluation newRuleEvaluation = createNewRuleEvaluation();
        evaluateRule(pipeline, newRuleEvaluation);

        // assert
        assertThat(newRuleEvaluation, is(not(nullValue())));
        assertThat(newRuleEvaluation.hasErrors(), is(true));
    }

    @Test
    public void evaluateRuleWithViolationsLazy() {
        // arrange
        SonarInformation sonarInformation = new SonarInformation("junit-projectKey", false);
        sonarInformation.setCoverage(30);
        sonarInformation.setOverallCoverage(69);
        sonarInformation.setItCoverage(66);
        sonarInformation.setLineCoverage(69);

        Pipeline pipeline = createPipeline();
        pipeline.addInformation(ENV, APP_NAME, sonarInformation);

        // act
        RuleEvaluation newRuleEvaluation = createNewRuleEvaluation();
        evaluateRule(pipeline, newRuleEvaluation);

        // assert
        assertThat(newRuleEvaluation, is(not(nullValue())));
        assertThat(newRuleEvaluation.hasErrors(), is(true));
    }

    @Test
    public void validatesType() {
        assertThat(validatesType(null), is(false));
        assertThat(validatesType(SONAR), is(true));
    }

    @Test
    @Parameters(method = "getParameterForOverriding")
    public void ruleViolated_should_override_correctly_value(float coverage,
                                                             int blockers,
                                                             int criticals,
                                                             Float coverageLimit,
                                                             Integer blockerLimit,
                                                             Integer criticalLimit,
                                                             boolean strict,
                                                             boolean expectedReturn) {
        // arrange
        SonarInformation sonarInformation = new SonarInformation("junit-projectKey", strict);
        sonarInformation.setCoverageLimit(coverageLimit);
        sonarInformation.setCriticalLimit(criticalLimit);
        sonarInformation.setBlockerLimit(blockerLimit);
        sonarInformation.setCoverage(coverage);
        sonarInformation.setBlockers(blockers);
        sonarInformation.setCriticals(criticals);
        SonarReportLimitsRule rule = new SonarReportLimitsRule();
        // act
        // assert
        assertEquals(rule.ruleViolated(sonarInformation), expectedReturn);
    }

    /** used by junit, see {@link #ruleViolated_should_override_correctly_value} */
    private Object[][] getParameterForOverriding() {
        return new Object[][]{
                {81.1f, 0, 0, null, null, null, true, false},
                {75, 0, 0, null, null, null, true, true},
                {60, 0, 0, null, null, null, true, true},
                {75, 1, 0, null, null, null, true, true},
                {75, 0, 1, null, null, null, true, true},
                {75, 0, 0, null, null, null, false, true},
                {60, 0, 0, null, null, null, false, true},
                {75, 1, 0, null, null, null, false, true},
                {75, 0, 1, null, null, null, false, true},
                {65, 0, 0, 60f, null, null, true, true},
                {65, 0, 0, 60f, null, null, false, false},
                {75, 1, 0, null, 2, null, true, true},
                {75, 1, 0, null, 2, null, false, true},
                {75, 0, 1, null, null, 2, true, true},
                {75, 0, 1, null, null, 2, false, true},
                {75.2f, 0, 1, 75.1f, null, 2, false, false},
                {75.2f, 0, 1, 75.2f, null, 2, false, false},
                {75.2f, 0, 1, 75.3f, null, 2, false, true}
        };
    }
}
