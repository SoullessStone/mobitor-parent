package ch.mobi.mobitor.plugin.sonarqube.domain;

/*-
 * §
 * mobitor-plugin-sonarqube
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(JUnitParamsRunner.class)
public class SonarInformationTest {

    @Test
    public void getLimitsValues_should_return_default_values() throws Exception {
        // Given
        SonarInformation information = new SonarInformation("Test", true);
        // When
        String limits = information.getLimitsValues();
        // Then
        assertThat(limits).isEqualToIgnoringCase(format(0, 0, 81.0));
    }

    @Test
    @Parameters(method = "getParameterForOverriding")
    public void getLimitsValues_should_return_overridden_values(Integer blockerLimit,
                                                                Integer criticalLimit,
                                                                Float coverageLimit,
                                                                String expextedResult) {
        // Given
        SonarInformation information = new SonarInformation("Test", true);
        information.setBlockerLimit(blockerLimit);
        information.setCriticalLimit(criticalLimit);
        information.setCoverageLimit(coverageLimit);
        // When
        String limits = information.getLimitsValues();
        // Then
        assertThat(limits).isEqualToIgnoringCase(expextedResult);
    }

    /**
     * used by junit, see {@link #getLimitsValues_should_return_overridden_values}
     */
    private Object[][] getParameterForOverriding() {
        return new Object[][]{
                {3, 2, 32.4f, format(3, 2, 32.4)},
                {3, 2, null, format( 3, 2, 81.0)},
                {3, null, 32.4f, format(3, 0, 32.4)},
                {null, 2, 32.4f, format(0, 2, 32.4)}
        };
    }

    private String format(int blocker, int critical, double percentage) {
        return String.format("> %d, > %d, < %.2f%s", blocker, critical, percentage, "%");
    }
}
