package ch.mobi.mobitor.plugin.sonarqube.service.client;

/*-
 * §
 * mobitor-plugin-sonarqube
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.sonarqube.service.client.domain.ProjectComponent;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.junit.Rule;
import org.junit.Test;

import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertThat;


public class SonarQubeClientTest {

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(WireMockConfiguration.options().dynamicPort());

    @Test
    public void retrieveProjectInformation() {
        // arrange
        final String projectKey = "ch.mobi.key";

        WireMock.givenThat(WireMock.get(WireMock.urlMatching("/api/measures/component?(.*)"))
                                   .withQueryParam("component", WireMock.equalTo(projectKey))
                                   .withHeader("Accept", WireMock.equalTo("application/json"))
                                   .willReturn(WireMock.aResponse()
                                                       .withStatus(200)
                                                       .withHeader("Content-Type", "application/json")
                                                       .withBodyFile("sonar-report-response-app1.json")
                )
        );

        String baseUrl = "http://localhost:" + wireMockRule.port();

        SonarQubeConfiguration sonarConfiguration = new SonarQubeConfiguration();
        sonarConfiguration.setBaseUrl(baseUrl);

        SonarQubeClient client = new SonarQubeClient(sonarConfiguration);

        // act
        ProjectComponent projectComponent = client.retrieveProjectInformation(projectKey);

        // assert
        assertThat(projectComponent, is(not(nullValue())));
        assertThat(projectComponent.getId(),is(not(nullValue())));
        assertThat(projectComponent.getKey(), is(not(nullValue())));
        assertThat(projectComponent.getName(),is(not(nullValue())));
        assertThat(projectComponent.getDescription(),is(not(nullValue())));
        assertThat(projectComponent.getLineCoverage(),is(not(0)));
        assertThat(projectComponent.getOverallCoverage(),is(not(0)));
        assertThat(projectComponent.getDuplicatedBlocks(), is(not(0)));
        assertThat(projectComponent.getCriticals(),is(not(0)));
        assertThat(projectComponent.getId(),is(not(0)));
        assertThat(projectComponent.getCoverage(),is(not(0)));
        assertThat(projectComponent.getBlockers(),is(not(0)));
        assertThat(projectComponent.getItCoverage(),is(not(0)));
    }

}
