package ch.mobi.mobitor.plugin.sonarqube.service.client;

/*-
 * §
 * mobitor-plugin-sonarqube
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.sonarqube.domain.SonarInformation;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import java.util.concurrent.atomic.AtomicInteger;

public class CoverageMetricServiceTest {

    @Test
    public void submitCoverage() {
        // arrange
        MeterRegistry meterRegistrySpy = Mockito.spy(SimpleMeterRegistry.class);
        CoverageMetricService coverageMetricService = new CoverageMetricService(meterRegistrySpy);

        SonarInformation sonarInfo = new SonarInformation("sonar:key", false);
        sonarInfo.setCoverage(80.5f);
        sonarInfo.setOverallCoverage(91.1f);

        // act, test already registered gauge as well by calling this twice
        coverageMetricService.submitCoverage(sonarInfo);
        coverageMetricService.submitCoverage(sonarInfo);

        // assert
        Mockito.verify(meterRegistrySpy).gauge(ArgumentMatchers.eq("gauge_coverage"), ArgumentMatchers.anyList(), ArgumentMatchers.any(AtomicInteger.class));
        Mockito.verify(meterRegistrySpy).gauge(ArgumentMatchers.eq("gauge_max_coverage"), ArgumentMatchers.anyList(), ArgumentMatchers.any(AtomicInteger.class));
    }

}
