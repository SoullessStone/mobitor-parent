package ch.mobi.mobitor.plugin.sonarqube;

/*-
 * §
 * mobitor-plugin-sonarqube
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */


import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.sonarqube.config.SonarProjectConfig;
import ch.mobi.mobitor.plugin.sonarqube.domain.SonarInformation;
import ch.mobi.mobitor.plugin.sonarqube.service.SonarQubeAttributeProvider;
import ch.mobi.mobitor.plugin.sonarqube.service.client.SonarQubeConfiguration;
import ch.mobi.mobitor.plugin.test.PluginTest;
import ch.mobi.mobitor.plugins.api.domain.config.ExtendableScreenConfig;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentCaptor.forClass;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

public class SonarQubePluginTest extends PluginTest<SonarQubePlugin, SonarProjectConfig, SonarInformation> {

    @Override
    public String getConfigPropertyName() {
        return "sonarProjects";
    }

    @Override
    protected SonarQubePlugin newClassUnderTestInstance() {
        SonarQubeConfiguration config = new SonarQubeConfiguration();
        SonarQubeAttributeProvider provider = new SonarQubeAttributeProvider(config);
        return new SonarQubePlugin(provider);
    }

    @Test
    public void createAndAssociateApplicationInformationBlocksShouldSetTheLimitsValues() {
        // arrange
        SonarQubeAttributeProvider sonarQubeAttributeProvider = mock(SonarQubeAttributeProvider.class);
        SonarQubePlugin plugin = new SonarQubePlugin(sonarQubeAttributeProvider);
        Screen screen = mock(Screen.class);
        ExtendableScreenConfig screenConfig = mock(ExtendableScreenConfig.class);
        SonarProjectConfig config = spy(new SonarProjectConfig());
        int blockerLimit = 2;
        int criticalLimit = 3;
        float coverageLimit = 54.3f;
        config.setBlockerLimit(blockerLimit);
        config.setCriticalLimit(criticalLimit);
        config.setCoverageLimit(coverageLimit);

        // act
        plugin.createAndAssociateApplicationInformationBlocks(screen, screenConfig, singletonList(config));

        // assert
        ArgumentCaptor<SonarInformation> sonarInfoCaptor = forClass(SonarInformation.class);
        verify(screen).addInformation(any(), any(), any(), sonarInfoCaptor.capture());
        SonarInformation capturedInformation = sonarInfoCaptor.getValue();
        assertThat(blockerLimit).isEqualTo(capturedInformation.getBlockerLimit());
        assertThat(criticalLimit).isEqualTo(capturedInformation.getCriticalLimit());
        assertThat(coverageLimit).isEqualTo(capturedInformation.getCoverageLimit());
    }
}
