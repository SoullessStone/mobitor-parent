package ch.mobi.mobitor.plugin.zaproxy;

/*-
 * §
 * mobitor-plugin-zaproxy
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.test.PluginTest;
import ch.mobi.mobitor.plugin.zaproxy.config.ZaproxyApplicationConfig;
import ch.mobi.mobitor.plugin.zaproxy.domain.ZaproxyInformation;
import ch.mobi.mobitor.plugins.api.domain.config.ExtendableScreenConfig;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.util.List;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentCaptor.forClass;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

public class ZaproxyPluginTest extends PluginTest<ZaproxyPlugin,ZaproxyApplicationConfig, ZaproxyInformation> {

    @Override
    public String getConfigPropertyName() {
        return "zaproxyApplications";
    }

    @Test
    public void createAndAssociateApplicationInformationBlocksShouldSetCorrectValuesOnInformation() {
        // arrange
        ZaproxyPlugin plugin = new ZaproxyPlugin();
        Screen screen = mock(Screen.class);
        ExtendableScreenConfig screenConfig = mock(ExtendableScreenConfig.class);
        ZaproxyApplicationConfig config = spy(new ZaproxyApplicationConfig());
        String buildId = "buildId";
        config.setBuildId(buildId);
        List<ZaproxyApplicationConfig> zaproxyConfigs = singletonList(config);

        // act
        plugin.createAndAssociateApplicationInformationBlocks(screen, screenConfig, zaproxyConfigs);

        // assert
        ArgumentCaptor<ZaproxyInformation> zapInfoCaptor = forClass(ZaproxyInformation.class);
        verify(screen).addInformation(any(), any(), any(), zapInfoCaptor.capture());
        ZaproxyInformation capturedInformation = zapInfoCaptor.getValue();
        assertThat(buildId).isEqualTo(capturedInformation.getBuildId());
    }
}
