package ch.mobi.mobitor.plugin.zaproxy.service.client;

/*-
 * §
 * mobitor-plugin-zaproxy
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.zaproxy.client.domain.Alert;
import ch.mobi.mobitor.plugin.zaproxy.client.domain.Alerts;
import ch.mobi.mobitor.plugin.zaproxy.client.domain.Instance;
import ch.mobi.mobitor.plugin.zaproxy.client.domain.SiteResponse;
import ch.mobi.mobitor.plugin.zaproxy.domain.ZaproxyInformation;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class ZaproxyProviderServiceTest {

	@Mock
	public ZaproxyTCClient zapclient;

	@Test
	public void updateProjectInformation() {
		// arrange
		/*
		 * final String projectKey = "ch.mobi.ovn:ovn";
		 * 
		 * givenThat(get(urlMatching("/sonarqube/api/resources?(.*)"))
		 * .withQueryParam("resource", equalTo(projectKey)) .withHeader("Accept",
		 * equalTo("application/json")) .willReturn(aResponse() .withStatus(200)
		 * .withHeader("Content-Type", "application/json")
		 * .withBodyFile("sonar-report-response-ovn.json") ) );
		 * 
		 * String baseUrl = "http://localhost:" + wireMockRule.port();
		 * 
		 * SonarQubeConfiguration sonarConf = new SonarQubeConfiguration();
		 * sonarConf.setBaseUrl(baseUrl);
		 * 
		 * SonarQubeClient client = new SonarQubeClient(sonarConf);
		 * CoverageMetricService metricService =
		 * Mockito.mock(CoverageMetricService.class);
		 * 
		 * // act SonarQubeProjectInformationProviderService service = new
		 * SonarQubeProjectInformationProviderService(client, metricService);
		 * SonarInformation sonarInfo = new SonarInformation(projectKey, false);
		 * service.updateProjectInformation(sonarInfo);
		 */
		ZaproxyProviderService zps = new ZaproxyProviderService(zapclient);
		ZaproxyInformation zapInfo = new ZaproxyInformation("123456");

		Mockito.when(zapclient.retrieveZaproxyReport("123456")).thenReturn(createSiteResponse());

		zps.updateZaproxyInformation(zapInfo);

		// assert
		assertThat(zapInfo, is(not(nullValue())));
		assertThat(zapInfo.getHighestIssueNumber(), is(equalTo(1)));
		assertThat(zapInfo.getHighNumber(), is(equalTo(1)));
		assertThat(zapInfo.getMediumNumber(), is(equalTo(3)));
		assertThat(zapInfo.getLowNumber(), is(equalTo(1)));
		assertThat(zapInfo.getInfoNumber(), is(equalTo(1)));
		assertThat(zapInfo.getIssueSummary(), is(equalTo("Issue numbers:    \"High\": 1    \"Medium\": 3    \"Low\": 1    \"Info\": 1")));
	}

	private SiteResponse createSiteResponse() {
		SiteResponse sr = new SiteResponse();
		List<Alerts> sites = createSiteList();
		sr.setSite(sites);
		return sr;
	}

	private List<Alerts> createSiteList() {
		List<Alerts> al = new ArrayList<>();
		Alerts a = new Alerts();
		a.setAlerts(createAlerts());
		al.add(a);
		return al;
	}

	private List<Alert> createAlerts() {
		List<Alert> al = new ArrayList<>();
		al.add(createAlert(0, 0));
		al.add(createAlert(1, 0));
		al.add(createAlert(2, 0));
		al.add(createAlert(2, 1));
		al.add(createAlert(2, 2));
		al.add(createAlert(3, 0));
		return al;
	}

	private Alert createAlert(int i, int j) {
		Alert a = new Alert();
		a.alert("alert-" + i + "-" + j).cweid("cweid-" + i + "-" + j).desc("desc-" + i + "-" + j)
				.name("name-" + i + "-" + j).pluginid("pluginid-" + i + "-" + j).reference("reference-" + i + "-" + j)
				.riskdesc("riskdesc-" + i + "-" + j);
		a.riskcode(i);
		a.setInstances(createInstances(1));
		return a;
	}

	private List<Instance> createInstances(int i) {
		List<Instance> insts = new ArrayList<>();
		Instance inst = new Instance();
		inst.evidence("evidence" + i).method("method" + i).param("param" + i).uri("http://uri.ch/" + i);
		insts.add(inst);
		return insts;
	}

}
