package ch.mobi.mobitor.plugin.zaproxy.client.domain;

/*-
 * §
 * mobitor-plugin-zaproxy
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.util.ResourceUtils;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import ch.mobi.mobitor.plugin.zaproxy.client.domain.Alert;
import ch.mobi.mobitor.plugin.zaproxy.client.domain.Alerts;
import ch.mobi.mobitor.plugin.zaproxy.client.domain.Instance;
import ch.mobi.mobitor.plugin.zaproxy.client.domain.SiteResponse;

public class SiteResponseTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void testDeserialize() throws IOException {
		// arrange
		// act
		SiteResponse response = getSiteResponseFromFile("zaproxy_issue_report.json");

		// assert
		assertEquals(2, response.getSite().size());
		assertEquals(2, response.getSite().get(0).getAlerts().size());
		assertEquals(1, response.getSite().get(0).getAlerts().get(0).getInstances().size());
	}

	@Test
	public void testSerialize() throws IOException {
		SiteResponse response = new SiteResponse();
		List<Alert> alertItems = new ArrayList<>();
		alertItems.add(new Alert().pluginid("10011").alert("Missing HTTP Header").name("Missing HTTP Header")
				.riskdesc("Medium").riskcode(1));
		Alert ai = new Alert().pluginid("30001").alert("SQL Injection").name("SQL Injection").riskdesc("High")
				.riskcode(3);
		List<Instance> instances = new ArrayList<>();
		instances.add(new Instance().evidence("evidence1").method("method1").param("param1").uri("uri1"));
		instances.add(new Instance().evidence("evidence2").method("method2").param("param2").uri("uri2"));
		instances.add(new Instance().evidence("evidence3").method("method3").param("param3").uri("uri3"));
		ai.setInstances(instances);
		alertItems.add(ai);
		List<Alerts> alerts = new ArrayList<>();
		Alerts sites = new Alerts();
		sites.setAlerts(alertItems);
		sites.name("https://blabla.ch").host("blbla.ch").port("3456").ssl("tls");
		alerts.add(sites);
		sites = new Alerts();
		sites.name("https://nexus.host.domain:8080").host("nexus.host.domain").port("8080").ssl("yes");
		sites.setAlerts(new ArrayList<>());
		alerts.add(sites);
		response.setSite(alerts);

		ObjectMapper mapper = new ObjectMapper();
		String json = mapper.writeValueAsString(response);
		mapper.enable(SerializationFeature.INDENT_OUTPUT);
		mapper.writeValue(System.out, response);

		assertEquals(818, json.length());
	}

	private SiteResponse getSiteResponseFromFile(String filename) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		File file = ResourceUtils.getFile(ResourceUtils.CLASSPATH_URL_PREFIX + "__files/" + filename);
		return mapper.readValue(file, SiteResponse.class);
	}
}
