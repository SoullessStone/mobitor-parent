package ch.mobi.mobitor.plugin.zaproxy.service.client;

/*-
 * §
 * mobitor-plugin-zaproxy
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.zaproxy.client.domain.SiteResponse;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.junit.Rule;
import org.junit.Test;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.givenThat;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertThat;


public class ZaproxyTCClientTest {

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(options().dynamicPort());
    
    @Test
    public void retrieveZaproxyReport() {
        // arrange
        givenThat(get(urlMatching("/TeamCity/httpAuth/app/rest/builds/buildType:(.*)"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("zaproxy-report-json.json")
                )
        );
        
        ZaproxyTCClient zapClient = getClient();
        
        // act
        SiteResponse response = zapClient.retrieveZaproxyReport("");

        // assert
        assertThat(response, is(not(nullValue())));
        assertThat(response.getSite(),hasSize(2));
    }
    
    private ZaproxyTCClient getClient() {
        ZapTeamCityConfiguration zapTCConfiguration = new ZapTeamCityConfiguration();
        String baseUrl = "http://localhost:" + wireMockRule.port() + "/TeamCity";
        zapTCConfiguration.setBaseUrl(baseUrl);
        zapTCConfiguration.setUsername("junit");
        zapTCConfiguration.setPassword("junit");

         return new ZaproxyTCClient(zapTCConfiguration);
    }

}
