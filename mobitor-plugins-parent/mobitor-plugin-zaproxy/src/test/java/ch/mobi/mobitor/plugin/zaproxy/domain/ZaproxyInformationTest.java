package ch.mobi.mobitor.plugin.zaproxy.domain;

/*-
 * §
 * mobitor-plugin-zaproxy
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ZaproxyInformationTest {

	private ZaproxyInformation zapInfo;

	@Before
	public void setUp() throws Exception {
		this.zapInfo = new ZaproxyInformation("");
	}

	@After
	public void tearDown() throws Exception {
		this.zapInfo = null;
	}

	@Test
	public void testReset() {
		// prepare
		zapInfo.incHigh();
		zapInfo.incMedium();

		// call reset
		zapInfo.reset();

		// check
		assertThat(zapInfo.getHighNumber(), equalTo(0));
		assertThat(zapInfo.getMediumNumber(), equalTo(0));
	}

	@Test
	public void testGetHighestIssueNumber() {
		// prepare
		zapInfo.incHigh();
		zapInfo.incMedium();
		zapInfo.incMedium();

		// call reset
		Integer highest = zapInfo.getHighestIssueNumber();

		// check
		assertThat(highest, equalTo(1));
	}

	@Test
	public void testGetHighestIssueNumber_2info() {
		// prepare
		zapInfo.incInfo();
		zapInfo.incInfo();

		// call reset
		Integer highest = zapInfo.getHighestIssueNumber();

		// check
		assertThat(highest, equalTo(2));
	}

	@Test
	public void testGetHighestIssueNumberMessage_1high() {
		// prepare
		zapInfo.reset();
		zapInfo.incHigh();
		zapInfo.incMedium();
		zapInfo.incMedium();

		// call reset
		String message = zapInfo.getHighestIssueNumberMessage();

		// check
		assertThat(message, containsString("1"));
	}

	@Test
	public void testGetHighestIssueNumberMessage_3info() {
		// prepare
		zapInfo.reset();
		zapInfo.incInfo();
		zapInfo.incInfo();
		zapInfo.incInfo();

		// call reset
		String message = zapInfo.getHighestIssueNumberMessage();

		// check
		assertThat(message, containsString("3"));
	}

	@Test
	public void testGetIssueSummary() {
		// prepare
		zapInfo.reset();
		zapInfo.incHigh();
		zapInfo.incHigh();
		zapInfo.incMedium();
		zapInfo.incLow();
		zapInfo.incLow();
		zapInfo.incLow();
		zapInfo.incLow();
		zapInfo.incInfo();
		zapInfo.incInfo();
		zapInfo.incInfo();

		// call reset
		String message = zapInfo.getIssueSummary();

		// check
		assertThat(message, containsString("\"High\": 2"));
		assertThat(message, containsString("\"Medium\": 1"));
		assertThat(message, containsString("\"Low\": 4"));
		assertThat(message, containsString("\"Info\": 3"));
	}

}
