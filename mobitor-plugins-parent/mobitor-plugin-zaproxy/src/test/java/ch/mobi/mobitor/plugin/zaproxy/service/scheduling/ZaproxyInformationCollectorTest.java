package ch.mobi.mobitor.plugin.zaproxy.service.scheduling;

/*-
 * §
 * mobitor-plugin-zaproxy
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.zaproxy.domain.ZaproxyInformation;
import ch.mobi.mobitor.plugin.zaproxy.service.client.ZaproxyProviderService;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformation;
import ch.mobi.mobitor.plugins.api.model.ScreensModel;
import ch.mobi.mobitor.plugins.api.service.RuleService;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static ch.mobi.mobitor.plugin.zaproxy.domain.ZaproxyInformation.ZAPROXY;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class ZaproxyInformationCollectorTest {

	@Test
	public void collectZaproxyProjectInformation() {
		// arrange
		RuleService ruleService = Mockito.mock(RuleService.class);
		ZaproxyProviderService zaproxyProviderService = mock(ZaproxyProviderService.class);
		ScreensModel screensModel = Mockito.mock(ScreensModel.class);
		Screen screen = Mockito.mock(Screen.class);

		ZaproxyInformation zaproxyInfoStrict = new ZaproxyInformation("123456");
		ZaproxyInformation zaproxyInfoRelax = new ZaproxyInformation("654321");
		List<ApplicationInformation> sonarList = Arrays.asList(zaproxyInfoStrict, zaproxyInfoRelax);

		List<Screen> screensList = Collections.singletonList(screen);
		Mockito.when(screensModel.getAvailableScreens()).thenReturn(screensList);
		Mockito.when(screen.getMatchingInformation(ArgumentMatchers.eq(ZAPROXY))).thenReturn(sonarList);

		ZaproxyInformationCollector zaproxyCollector = new ZaproxyInformationCollector(screensModel,
				zaproxyProviderService, ruleService);

		// act
		zaproxyCollector.collectZaproxyInformationForScreens();

		// assert
		verify(screensModel).getAvailableScreens();
		verify(ruleService).updateRuleEvaluation(screen, ZAPROXY);
		verify(screen).setRefreshDate(eq(ZAPROXY), any(Date.class));
	}

}
