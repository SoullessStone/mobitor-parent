package ch.mobi.mobitor.plugin.zaproxy.domain;

/*-
 * §
 * mobitor-plugin-zaproxy
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import java.text.MessageFormat;

import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformation;

public class ZaproxyInformation implements ApplicationInformation {

	public static final String ZAPROXY = "zaproxy";

	private final String buildId;

	private String reportUrlTemplate;
	private Integer highNumber = 0;
	private Integer mediumNumber = 0;
	private Integer lowNumber = 0;
	private Integer infoNumber = 0;
	private boolean informationUpdated = false;

	public ZaproxyInformation(String buildId) {
		this.buildId = buildId;
		this.reportUrlTemplate = "";
	}

	public String getBuildId() {
		return buildId;
	}

	public String getWebUrl() {
		return MessageFormat.format(reportUrlTemplate, buildId);
	}

	public void incHigh() {
		this.highNumber++;
	}

	public void setReportUrlTemplate(String url) {
		this.reportUrlTemplate = url;
	}

	public void incMedium() {
		this.mediumNumber++;
	}

	public void incLow() {
		this.lowNumber++;
	}

	public void incInfo() {
		this.infoNumber++;
	}

	public Integer getHighNumber() {
		return highNumber;
	}

	public Integer getMediumNumber() {
		return mediumNumber;
	}

	public Integer getLowNumber() {
		return lowNumber;
	}

	public Integer getInfoNumber() {
		return infoNumber;
	}

	public void reset() {
		this.highNumber = 0;
		this.mediumNumber = 0;
		this.lowNumber = 0;
		this.infoNumber = 0;
		this.informationUpdated = true;
	}

	public Integer getHighestIssueNumber() {
		if (highNumber > 0) {
			return highNumber;
		}
		if (mediumNumber > 0) {
			return mediumNumber;
		}
		if (lowNumber > 0) {
			return lowNumber;
		}
		if (infoNumber > 0) {
			return infoNumber;
		}
		return 0;
	}

	public String getHighestIssueNumberMessage() {
		if (!isInformationUpdated() ) {
			return "Data not available!";
		}
		if (highNumber > 0) {
			return highNumber.toString();
		}
		if (mediumNumber > 0) {
			return mediumNumber.toString();
		}
		if (lowNumber > 0) {
			return lowNumber.toString();
		}
		if (infoNumber > 0) {
			return infoNumber.toString();
		}
		return "OK";
	}

	public String getIssueSummary() {
		if (!isInformationUpdated() ) {
			return "Zaproxy data not available!";
		}
		StringBuffer sb = new StringBuffer("Issue numbers:");
		sb.append("    \"High\": ").append(highNumber);
		sb.append("    \"Medium\": ").append(mediumNumber);
		sb.append("    \"Low\": ").append(lowNumber);
		sb.append("    \"Info\": ").append(infoNumber);

		return sb.toString();
	}

	public boolean isInformationUpdated() {
		return informationUpdated;
	}

	@Override
	public String getType() {
		return ZAPROXY;
	}

	@Override
	public boolean hasInformation() {
		return true;
	}
}
