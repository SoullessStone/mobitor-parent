package ch.mobi.mobitor.plugin.zaproxy.client.domain;

/*-
 * §
 * mobitor-plugin-zaproxy
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Alert {

	@JsonProperty
	private String pluginid;
	@JsonProperty
	private String alert;
	@JsonProperty
	private String name;
	@JsonProperty
	private String riskdesc;
	@JsonProperty
	private Integer riskcode;
	@JsonProperty
	private String desc;
	@JsonProperty
	private String solution;
	@JsonProperty
	private String reference;
	@JsonProperty
	private String cweid;
	@JsonProperty
	private String wascid;

	@JsonProperty
	private List<Instance> instances;

	public Alert() {
	}

	public void setPluginid(String pluginid) {
		this.pluginid = pluginid;
	}

	public Alert pluginid(String pluginid) {
		this.pluginid = pluginid;
		return this;
	}

	public void setAlert(String alert) {
		this.alert = alert;
	}

	public Alert alert(String alert) {
		this.alert = alert;
		return this;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Alert name(String name) {
		this.name = name;
		return this;
	}

	public void setRiskdesc(String riskdesc) {
		this.riskdesc = riskdesc;
	}

	public Alert riskdesc(String riskdesc) {
		this.riskdesc = riskdesc;
		return this;
	}

	public void setRiskcode(Integer riskcode) {
		this.riskcode = riskcode;
	}

	public Alert riskcode(Integer riskcode) {
		this.riskcode = riskcode;
		return this;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public void setSolution(String solution) {
		this.solution = solution;
	}

	public void setReference(String reference) {
		this.reference = reference;
	}

	public void setCweid(String cweid) {
		this.cweid = cweid;
	}

	public void setWascid(String wascid) {
		this.wascid = wascid;
	}

	public String getPluginid() {
		return pluginid;
	}

	public String getAlert() {
		return alert;
	}

	public String getName() {
		return name;
	}

	public String getRiskdesc() {
		return riskdesc;
	}

	public Integer getRiskcode() {
		return riskcode;
	}

	public String getDesc() {
		return desc;
	}

	public Alert desc(String desc) {
		this.desc = desc;
		return this;
	}

	public String getSolution() {
		return solution;
	}

	public Alert solution(String solution) {
		this.solution = solution;
		return this;
	}

	public String getReference() {
		return reference;
	}

	public Alert reference(String reference) {
		this.reference = reference;
		return this;
	}

	public String getCweid() {
		return cweid;
	}

	public Alert cweid(String cweid) {
		this.cweid = cweid;
		return this;
	}

	public String getWascid() {
		return wascid;
	}

	public Alert wascid(String wascid) {
		this.wascid = wascid;
		return this;
	}

	public void setInstances(List<Instance> instances) {
		this.instances = instances;
	}

	public List<Instance> getInstances() {
		return this.instances;
	}

	public String toString() {

		StringBuffer sb = new StringBuffer("{");
		sb.append("  \"pluginid\": ").append(pluginid);
		sb.append("  \"alert\": ").append(alert);
		sb.append("  \"name\": ").append(name);
		sb.append("  \"riskdesc\": ").append(riskdesc);
		sb.append("  \"desc\": ").append(desc);
		sb.append("  \"solution\": ").append(solution);
		sb.append("  \"reference\": ").append(reference);
		sb.append("  \"cweid\": ").append(cweid);
		sb.append("  \"wascid\": ").append(wascid);
		sb.append("  }");
		return sb.toString();
	}
}
