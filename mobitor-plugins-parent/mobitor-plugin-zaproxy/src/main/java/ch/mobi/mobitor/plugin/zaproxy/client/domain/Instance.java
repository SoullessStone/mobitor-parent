package ch.mobi.mobitor.plugin.zaproxy.client.domain;

/*-
 * §
 * mobitor-plugin-zaproxy
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import com.fasterxml.jackson.annotation.JsonProperty;

public class Instance {
	@JsonProperty
	private String uri;
	@JsonProperty
	private String method;
	@JsonProperty
	private String param;
	@JsonProperty
	private String evidence;

	public Instance() {
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public Instance uri(String uri) {
		this.uri = uri;
		return this;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public Instance method(String method) {
		this.method = method;
		return this;
	}

	public String getParam() {
		return param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	public Instance param(String param) {
		this.param = param;
		return this;
	}

	public String getEvidence() {
		return evidence;
	}

	public void setEvidence(String evidence) {
		this.evidence = evidence;
	}

	public Instance evidence(String evidence) {
		this.evidence = evidence;
		return this;
	}
	
}
