package ch.mobi.mobitor.plugin.zaproxy.service.client;

/*-
 * §
 * mobitor-plugin-zaproxy
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import java.io.IOException;
import java.net.URI;
import java.text.MessageFormat;

import org.apache.http.HttpHost;
import org.apache.http.client.fluent.Executor;
import org.apache.http.client.fluent.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import ch.mobi.mobitor.plugin.zaproxy.client.domain.SiteResponse;

@Component
public class ZaproxyTCClient {

    private static final Logger LOG = LoggerFactory.getLogger(ZaproxyTCClient.class);

    private static final String TEAMCITY_REST_PATH = "/httpAuth/app/rest";
    private static final String TEAMCITY_REPORT_PATH = "/repository/download";

    private final ZapTeamCityConfiguration teamCityConfiguration;

    private Executor executor;

    @Autowired
    public ZaproxyTCClient(ZapTeamCityConfiguration teamCityConfiguration) {
        this.teamCityConfiguration = teamCityConfiguration;
    }

    public SiteResponse retrieveZaproxyReport(String configId) {
        final String teamCityHost = teamCityConfiguration.getBaseUrl();
        final String username = teamCityConfiguration.getUsername();
        final String password = teamCityConfiguration.getPassword();

        // avoid handshake failure due to mixed up hostnames:
        System.setProperty("jsse.enableSNIExtension", "false");

        String requestUri = MessageFormat.format("{0}{1}/builds/buildType:(id:{2})/artifacts/content/zap_reports.zip!/report.json",
                teamCityHost, TEAMCITY_REST_PATH, configId);

        LOG.debug("GET: " + requestUri);

        try {
            URI tcUri = new URI(teamCityHost);
            HttpHost host = HttpHost.create(tcUri.getHost());
            executor = Executor.newInstance().auth(host, username, password).authPreemptive(host);

            String response = executor
                .execute(Request
                    .Get(requestUri)
                    .addHeader("accept", "application/json")
                    .connectTimeout(5000)
                    .socketTimeout(5000))
                .returnContent()
                .asString();

            LOG.trace("JSON String: " + response);

            return createZaproxyResponse(response);

        } catch (Exception ex) {
            LOG.error("Could not retrieve build information for " + configId);
            LOG.error(configId + " " + ex.getMessage());
            LOG.debug("Exception", ex);
        }

        return null;
    }

    private SiteResponse createZaproxyResponse(String jsonResponse) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        SiteResponse response = mapper.readValue(jsonResponse, SiteResponse.class);
        return response;
    }

    public String getReportUrlTemplate(String buildId) {
        String requestUri = MessageFormat.format("{0}{1}/builds/buildType:(id:{2})", teamCityConfiguration.getBaseUrl(),
                TEAMCITY_REST_PATH, buildId);

        String response = null;
        try {
            response = executor
                .execute(Request
                    .Get(requestUri)
                    .addHeader("accept", "application/json")
                    .connectTimeout(5000)
                    .socketTimeout(5000))
                .returnContent()
                .asString();
        } catch (Exception e) {
            LOG.error("Could not retrieve build information for " + buildId);
            LOG.error(buildId + " " + e.getMessage());
            LOG.debug("Exception", e);
        }

        String id = extractBuildId(response);

        return teamCityConfiguration.getBaseUrl() + TEAMCITY_REPORT_PATH + "/" + buildId + "/" + id
                + ":id/zap_reports.zip!/report.html";
    }

    private String extractBuildId(String resp) {
        if (resp == null) {
            return "";
        }
        int idpos = resp.indexOf("id");
        int begIndex = resp.indexOf(':', idpos) + 1;
        int endIndex = resp.indexOf(',', begIndex);
        return resp.substring(begIndex, endIndex);
    }
}
