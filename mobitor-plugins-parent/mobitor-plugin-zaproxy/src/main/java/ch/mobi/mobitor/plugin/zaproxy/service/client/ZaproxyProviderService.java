package ch.mobi.mobitor.plugin.zaproxy.service.client;

/*-
 * §
 * mobitor-plugin-zaproxy
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ch.mobi.mobitor.plugin.zaproxy.client.domain.Alert;
import ch.mobi.mobitor.plugin.zaproxy.client.domain.SiteResponse;
import ch.mobi.mobitor.plugin.zaproxy.domain.ZaproxyInformation;

@Service
public class ZaproxyProviderService {

    private static final Logger LOG = LoggerFactory.getLogger(ZaproxyProviderService.class);

    private final ZaproxyTCClient zaproxyClient;

    @Autowired
    public ZaproxyProviderService(ZaproxyTCClient zaproxyClient) {
        this.zaproxyClient = zaproxyClient;
    }

    public void updateZaproxyInformation(ZaproxyInformation zapInfo) {
        SiteResponse siteResponse = zaproxyClient.retrieveZaproxyReport(zapInfo.getBuildId());
        if (siteResponse != null) {
            AlertProcessor aproc = new AlertProcessor(zapInfo);
            siteResponse.getSite().forEach(s -> s.getAlerts().forEach(aproc::process));

            LOG.debug("updated zaproxy information for: " + zapInfo.getBuildId());
        } else {
            LOG.warn("could not update zaproxy information for: " + zapInfo.getBuildId());
        }
    }

    private class AlertProcessor {
        private ZaproxyInformation zapInfo;

        public AlertProcessor(ZaproxyInformation zapInfo) {
            this.zapInfo = zapInfo;
            this.zapInfo.reset();
            this.zapInfo.setReportUrlTemplate(zaproxyClient.getReportUrlTemplate(zapInfo.getBuildId()));
        }

        public void process(Alert a) {
            if (a.getRiskcode() == 0) {
                zapInfo.incInfo();
            } else if (a.getRiskcode() == 1) {
                zapInfo.incLow();
            } else if (a.getRiskcode() == 2) {
                zapInfo.incMedium();
            } else if (a.getRiskcode() == 3) {
                zapInfo.incHigh();
            }
        }
    }

}
