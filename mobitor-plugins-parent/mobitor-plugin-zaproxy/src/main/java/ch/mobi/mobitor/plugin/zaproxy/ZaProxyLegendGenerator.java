package ch.mobi.mobitor.plugin.zaproxy;

/*-
 * §
 * mobitor-plugin-zaproxy
 * --
 * Copyright (C) 2018 - 2019 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.zaproxy.domain.ZaproxyInformation;
import ch.mobi.mobitor.plugins.api.MobitorPluginLegendGenerator;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformationLegendWrapper;

import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;

public class ZaProxyLegendGenerator implements MobitorPluginLegendGenerator {

    @Override
    public List<ApplicationInformationLegendWrapper> createSuccessList() {
        ZaproxyInformation zaProxySuccessInfo = new ZaproxyInformation("successBuildId");
        zaProxySuccessInfo.reset();

        ApplicationInformationLegendWrapper successDeployment = new ApplicationInformationLegendWrapper("Successful deployment.", zaProxySuccessInfo);
        return singletonList(successDeployment);
    }

    @Override
    public List<ApplicationInformationLegendWrapper> createErrorList() {
        ZaproxyInformation highZaInfo = new ZaproxyInformation("errorBuildId");
        highZaInfo.reset();
        highZaInfo.incHigh();
        highZaInfo.incHigh();
        highZaInfo.incHigh();
        ZaproxyInformation medZaInfo = new ZaproxyInformation("errorBuildId");
        medZaInfo.reset();
        medZaInfo.incMedium();
        medZaInfo.incMedium();
        ZaproxyInformation lowZaInfo = new ZaproxyInformation("errorBuildId");
        lowZaInfo.reset();
        lowZaInfo.incLow();

        ApplicationInformationLegendWrapper wrapperHigh = new ApplicationInformationLegendWrapper("High risk report", highZaInfo);
        ApplicationInformationLegendWrapper wrapperMed = new ApplicationInformationLegendWrapper("Medium risk report", medZaInfo);
        ApplicationInformationLegendWrapper wrapperLow = new ApplicationInformationLegendWrapper("Low risk report", lowZaInfo);

        return asList(wrapperHigh, wrapperMed, wrapperLow);
    }
}
