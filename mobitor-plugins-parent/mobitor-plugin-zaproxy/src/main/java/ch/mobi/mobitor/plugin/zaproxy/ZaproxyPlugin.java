package ch.mobi.mobitor.plugin.zaproxy;

/*-
 * §
 * mobitor-plugin-zaproxy
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import java.util.ArrayList;
import java.util.List;

import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformationLegendWrapper;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.zaproxy.config.ZaproxyApplicationConfig;
import ch.mobi.mobitor.plugin.zaproxy.domain.ZaproxyInformation;
import ch.mobi.mobitor.plugins.api.MobitorPlugin;
import ch.mobi.mobitor.plugins.api.domain.config.ExtendableScreenConfig;

@Component
@ConditionalOnProperty(name = "mobitor.plugins.zaproxyApplications.enabled", havingValue = "true")
public class ZaproxyPlugin implements MobitorPlugin<ZaproxyApplicationConfig> {
	@Override
	public String getConfigPropertyName() {
		return "zaproxyApplications";
	}

	@Override
	public Class<ZaproxyApplicationConfig> getConfigClass() {
		return ZaproxyApplicationConfig.class;
	}

	@Override
	public void createAndAssociateApplicationInformationBlocks(Screen screen, ExtendableScreenConfig screenConfig, List<ZaproxyApplicationConfig> configs) {
        for (ZaproxyApplicationConfig zaproxyApplicationConfig : configs) {
            String serverName = zaproxyApplicationConfig.getServerName();
            String applicationName = zaproxyApplicationConfig.getApplicationName();
            String environment = zaproxyApplicationConfig.getEnvironment();
            String buildId = zaproxyApplicationConfig.getBuildId();

            ZaproxyInformation zaproxyInformation = new ZaproxyInformation(buildId);
            screen.addInformation(serverName, applicationName, environment, zaproxyInformation);
        }
	}

	@Override
	public List<ApplicationInformationLegendWrapper> getLegendApplicationInformationList() {
		return new ZaProxyLegendGenerator().getLegendList();
	}
}
