package ch.mobi.mobitor.plugin.zaproxy.client.domain;

/*-
 * §
 * mobitor-plugin-zaproxy
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * represents the {@code <site></site>} section of a TeamCity response that queries
 * {@code app/rest/builds/buildType:(id:...)/artifacts/content/zap_reports.zip!/report.json}
 *
 * ex: {@code https://teamcity/httpAuth/app/rest/builds/buildType:(id:ConfigId)/artifacts/content/zap_reports.zip!/report.json}
 */
public class SiteResponse implements Serializable {

	private static final long serialVersionUID = 17839547856237292L;

	@JsonProperty
	private List<Alerts> site;

	@JsonCreator
	public SiteResponse() {
	}

	public List<Alerts> getSite() {
		return site;
	}

	public void setSite(List<Alerts> sites) {
		this.site = sites;
	}
	
	public String toString() {
		StringBuffer sb = new StringBuffer("{[\n");
		for (Alerts a:site) {
			sb.append(a.toString());
		}
		sb.append("]}");
		return sb.toString();
	}

}
