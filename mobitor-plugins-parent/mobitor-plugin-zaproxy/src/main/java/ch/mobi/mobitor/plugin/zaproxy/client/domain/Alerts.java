package ch.mobi.mobitor.plugin.zaproxy.client.domain;

/*-
 * §
 * mobitor-plugin-zaproxy
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import java.io.Serializable;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * represents the {@code <site></site>} section of a TeamCity response that queries
 * {@code app/rest/builds/buildType:(id:...)/artifacts/content/zap_reports.zip!/report.json}
 *
 * ex: {@code https://teamcity/httpAuth/app/rest/builds/buildType:(id:ConfigId)/artifacts/content/zap_reports.zip!/report.json}
 */
@JsonPropertyOrder({ "name", "host", "port", "ssl", "alerts" })
public class Alerts implements Serializable {

	private static final long serialVersionUID = 17839547856237292L;

	@JsonProperty("@name")
	private String name;

	@JsonProperty("@host")
	private String host;

	@JsonProperty("@port")
	private String port;

	@JsonProperty("@ssl")
	private String ssl;

	@JsonProperty
	private List<Alert> alerts;

	@JsonCreator
	public Alerts() {
	}

	public List<Alert> getAlerts() {
		return alerts;
	}

	public void setAlerts(List<Alert> alerts) {
		this.alerts = alerts;
	}

	public String getName() {
		return name;
	}

	public Alerts name(String name) {
		this.name = name;
		return this;
	}

	public String getHost() {
		return host;
	}

	public Alerts host(String host) {
		this.host = host;
		return this;
	}

	public String getPort() {
		return port;
	}

	public Alerts port(String port) {
		this.port = port;
		return this;
	}

	public String getSsl() {
		return ssl;
	}

	public Alerts ssl(String ssl) {
		this.ssl = ssl;
		return this;
	}
	
	public String toString() {

		StringBuffer sb = new StringBuffer("  {");
		sb.append("  \"host\": ").append(host);
		sb.append("  \"port\": ").append(port);
		sb.append("  \"ssl\": ").append(ssl);
		sb.append("\n    [\n      ");
			
		for (Alert a:alerts) {
			sb.append(a.toString()).append("\n      ");
		}
		int l = sb.length();
		sb.delete(l-2, l).append("]}\n");
		return sb.toString();
	}

}
