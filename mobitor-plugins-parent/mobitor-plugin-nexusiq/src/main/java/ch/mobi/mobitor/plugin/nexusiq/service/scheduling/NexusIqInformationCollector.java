package ch.mobi.mobitor.plugin.nexusiq.service.scheduling;

/*-
 * §
 * mobitor-plugin-nexusiq
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.nexusiq.NexusIqPlugin;
import ch.mobi.mobitor.plugin.nexusiq.config.Stage;
import ch.mobi.mobitor.plugin.nexusiq.domain.NexusIqInformation;
import ch.mobi.mobitor.plugin.nexusiq.service.client.NexusIqClient;
import ch.mobi.mobitor.plugin.nexusiq.service.client.domain.ApplicationViolationResponse;
import ch.mobi.mobitor.plugin.nexusiq.service.client.domain.ApplicationViolationsResponse;
import ch.mobi.mobitor.plugin.nexusiq.service.client.domain.ApplicationsResponse;
import ch.mobi.mobitor.plugin.nexusiq.service.client.domain.PolicyViolationResponse;
import ch.mobi.mobitor.plugins.api.model.ScreensModel;
import ch.mobi.mobitor.plugins.api.service.RuleService;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

import static ch.mobi.mobitor.plugin.nexusiq.domain.NexusIqInformation.NEXUS_IQ;

@Component
@ConditionalOnBean(NexusIqPlugin.class)
public class NexusIqInformationCollector {

	private final static Logger LOG = LoggerFactory.getLogger(NexusIqInformationCollector.class);

	private final ScreensModel screensModel;
	private final NexusIqClient nexusIqClient;
	private final NexusIqConfigurationService nexusIqConfigurationService;
	private final RuleService ruleService;

	@Autowired
	public NexusIqInformationCollector(NexusIqConfigurationService nexusIqConfigurationService, RuleService ruleService,
			ScreensModel screensModel, NexusIqClient nexusIqClient) {
		this.nexusIqConfigurationService = nexusIqConfigurationService;
		this.ruleService = ruleService;
		this.screensModel = screensModel;
		this.nexusIqClient = nexusIqClient;
	}

	@Scheduled(fixedDelayString = "${scheduling.pollingIntervalMs.nexusIqInformationPollingInterval}", initialDelayString = "${scheduling.pollingInitialDelayMs.second}")
	public void collectNexusIqInformation() {
		List<String> policyIds = nexusIqConfigurationService.getPolicyIds();
		ApplicationViolationsResponse applicationViolationsResponse = nexusIqClient.retrieveViolations(policyIds);
		ApplicationsResponse applicationsResponse = nexusIqClient.retrieveApplications();

		if (applicationViolationsResponse != null
				&& CollectionUtils.isNotEmpty(applicationViolationsResponse.getApplicationViolations())) {
			this.screensModel.getAvailableScreens().forEach((screen) -> populateNexusIqInformation(screen,
					applicationViolationsResponse, applicationsResponse));
		}
	}

	private void populateNexusIqInformation(Screen screen, ApplicationViolationsResponse applicationViolationsResponse,
			ApplicationsResponse applicationsResponse) {
		List<NexusIqInformation> nexusIqInfoList = screen.getMatchingInformation(NEXUS_IQ);
		if (CollectionUtils.isNotEmpty(nexusIqInfoList)) {
			LOG.debug("Number of application violations from Nexus IQ: "
					+ applicationViolationsResponse.getApplicationViolations().size());
			nexusIqInfoList.forEach(nexusIqInformation -> updateViolationCount(applicationViolationsResponse,
					applicationsResponse, nexusIqInformation));
		}

		ruleService.updateRuleEvaluation(screen, NEXUS_IQ);
		screen.setRefreshDate(NEXUS_IQ, new Date());
	}

	private void updateViolationCount(ApplicationViolationsResponse applicationViolationsResponse,
			ApplicationsResponse applicationsResponse, NexusIqInformation nexusIqInformation) {
		nexusIqInformation.resetViolationsCount();

		String publicId = nexusIqInformation.getPublicId();
		if (!applicationsResponse.hasApplication(publicId)) {
			LOG.info("Application [" + publicId + "] was not found in Nexus IQ!");
			nexusIqInformation.setHasError(true);
		}
		List<ApplicationViolationResponse> applicationViolations = applicationViolationsResponse
				.getApplicationViolations();
		boolean foundViolation = false;
		String reportUrl = "";
		for (ApplicationViolationResponse applicationViolation : applicationViolations) {
			if (publicId.equals(applicationViolation.getApplication().getPublicId())) {
				foundViolation = true;
				for (PolicyViolationResponse policyViolationResponse : applicationViolation.getPolicyViolations()) {
					String policyId = policyViolationResponse.getPolicyId();
					Stage stage = policyViolationResponse.getStage();
					if (nexusIqInformation.getStage() == stage) {
						nexusIqInformation.increaseViolationCount(policyId);
						reportUrl = policyViolationResponse.getReportUrl();
					}
				}
			}
		}
		if (foundViolation) {
			nexusIqInformation.setReportUrl(nexusIqClient.getBaseUrl() + "/" + reportUrl);
		} else {
			nexusIqInformation.setReportUrl(nexusIqClient.getBaseUrl() + "/" +nexusIqInformation.getNoIssueUrl());
			LOG.info("Could not find [" + publicId + "] in Nexus IQ!");
		}
	}

}
