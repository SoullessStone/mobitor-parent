package ch.mobi.mobitor.plugin.nexusiq.service.scheduling;

/*-
 * §
 * mobitor-plugin-nexusiq
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.nexusiq.service.scheduling.domain.NexusIqConfig;
import ch.mobi.mobitor.plugin.nexusiq.service.scheduling.domain.NexusPolicyId;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class NexusIqConfigurationService {

    private static final Logger LOG = LoggerFactory.getLogger(NexusIqConfigurationService.class);

    private final ResourceLoader resourceLoader;

    private NexusIqConfig nexusConfig;
    private List<String> policyIds;
    private Map<String, String> policyIdToLabelMap;

    @Autowired
    public NexusIqConfigurationService(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    @PostConstruct
    public void initializeNexusConfiguration() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        Resource resource = resourceLoader.getResource("classpath:mobitor/plugin/nexusiq/config/nexus-iq-configuration.json");
        try (InputStream inputStream = resource.getInputStream()) {
            this.nexusConfig = mapper.readValue(inputStream, NexusIqConfig.class);
            createPolicyIdList(this.nexusConfig);
            createPolicyToLabelMap(this.nexusConfig);

        } catch (Exception e) {
            LOG.error("Could not initialize Nexus IQ configuration.", e);
        }
    }

    private void createPolicyToLabelMap(NexusIqConfig nexusConfig) {
        this.policyIdToLabelMap = nexusConfig.getPolicyIds().stream().collect(Collectors.toMap(NexusPolicyId::getId, NexusPolicyId::getLabel));
    }

    private void createPolicyIdList(NexusIqConfig nexusConfig) {
        this.policyIds = nexusConfig.getPolicyIds().stream().map(NexusPolicyId::getId).collect(Collectors.toList());
    }

    public NexusIqConfig getNexusConfig() {
        return nexusConfig;
    }

    public List<String> getPolicyIds() {
        return Collections.unmodifiableList(policyIds);
    }

    public String getLabel(String policyId) {
        return this.policyIdToLabelMap.get(policyId);
    }
}
