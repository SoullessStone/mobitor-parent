package ch.mobi.mobitor.plugin.nexusiq.domain;

/*-
 * §
 * mobitor-plugin-nexusiq
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.nexusiq.config.Stage;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformation;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class NexusIqInformation implements ApplicationInformation {

	public static final String NEXUS_IQ = "nexus_iq";

	private final String publicId;
	private Map<String, Integer> violationsMap = new HashMap<>();
	private boolean informationUpdated = false;
	private Stage stage;
	private boolean hasError;
	private String noIssueUrl;
	private String reportUrl;

	public NexusIqInformation(String publicId, Stage stage) {
		this.publicId = publicId;
		this.stage = stage;
	}

	@Override
	public String getType() {
		return NEXUS_IQ;
	}

	@Override
	public boolean hasInformation() {
		return true;
	}

	public String getPublicId() {
		return publicId;
	}

	public Stage getStage() {
		return stage;
	}

	public void resetViolationsCount() {
		this.violationsMap = new HashMap<>();
		this.informationUpdated = true;
	}

	public void increaseViolationCount(String policyId) {
		violationsMap.merge(policyId, 1, Integer::sum);
	}

	public int getNexusIqViolations() {
		return this.violationsMap.size();
	}

	public int getTotalViolations() {
		return violationsMap.values().stream().mapToInt(Number::intValue).sum();
	}

	public Map<String, Integer> getViolationsMap() {
		return Collections.unmodifiableMap(violationsMap);
	}

	public boolean isInformationUpdated() {
		return informationUpdated;
	}

	public void setHasError(boolean hasError) {
		this.hasError = hasError;
	}

	public boolean hasError() {
		return this.hasError;
	}

	public void setNoIssueUrl(String noIssueUrl) {
		this.noIssueUrl = noIssueUrl;
		if (this.reportUrl == null) {
			this.reportUrl = noIssueUrl;
		}
	}

	public String getNoIssueUrl() {
		return noIssueUrl;
	}

	public void setReportUrl(String reportUrl) {
		this.reportUrl = reportUrl;
	}

	public String getReportUrl() {
		return this.reportUrl;
	}
}
