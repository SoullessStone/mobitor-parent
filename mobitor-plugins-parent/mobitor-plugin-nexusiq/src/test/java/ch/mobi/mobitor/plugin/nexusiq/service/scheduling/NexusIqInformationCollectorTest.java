package ch.mobi.mobitor.plugin.nexusiq.service.scheduling;

/*-
 * §
 * mobitor-plugin-nexusiq
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.nexusiq.config.Stage;
import ch.mobi.mobitor.plugin.nexusiq.domain.NexusIqInformation;
import ch.mobi.mobitor.plugin.nexusiq.service.client.NexusIqClient;
import ch.mobi.mobitor.plugin.nexusiq.service.client.domain.ApplicationResponse;
import ch.mobi.mobitor.plugin.nexusiq.service.client.domain.ApplicationViolationResponse;
import ch.mobi.mobitor.plugin.nexusiq.service.client.domain.ApplicationViolationsResponse;
import ch.mobi.mobitor.plugin.nexusiq.service.client.domain.ApplicationsResponse;
import ch.mobi.mobitor.plugin.nexusiq.service.client.domain.PolicyViolationResponse;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformation;
import ch.mobi.mobitor.plugins.api.model.ScreensModel;
import ch.mobi.mobitor.plugins.api.service.RuleService;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static ch.mobi.mobitor.plugin.nexusiq.domain.NexusIqInformation.NEXUS_IQ;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class NexusIqInformationCollectorTest {

	private static String BASE_URL = "http://nexusiq";
	private static String REPORT_URL = "ui/links/application/sylber-sylber_webgui_ear/report/13187c1959ba4bf7994596809fda198d";
	private static String REPORT_URL_PREFIX = "assets/index.html#/management/view/application/";
	private static String PUBLIC_ID = "junit-pub";

	@Test
	public void collectNexusIqInformation() {
		// arrange
		NexusIqInformation nexusIqInfo = new NexusIqInformation(PUBLIC_ID, Stage.BUILD);
		List<ApplicationInformation> nexusIqInfoList = Collections.singletonList(nexusIqInfo);
		Screen screen = createScreen(nexusIqInfoList);
		List<Screen> screensList = Collections.singletonList(screen);
		ScreensModel screensModel = createScreensModel(screensList);
		RuleService ruleServiceSpy = Mockito.mock(RuleService.class);

		NexusIqConfigurationService nexusIqConfService = mock(NexusIqConfigurationService.class);
		ApplicationViolationsResponse appViolationsResp = createApplicationViolationsResponse(PUBLIC_ID);

		ApplicationsResponse applicationsResponse = Mockito.mock(ApplicationsResponse.class);
		NexusIqClient nexusIqClient = createNexusIqClient(appViolationsResp, applicationsResponse);

		NexusIqInformationCollector nexusIqCollector = new NexusIqInformationCollector(nexusIqConfService,
				ruleServiceSpy, screensModel, nexusIqClient);

		// act
		nexusIqCollector.collectNexusIqInformation();

		// assert
		verify(screensModel).getAvailableScreens();
		verify(ruleServiceSpy).updateRuleEvaluation(any(), any());
		verify(screen).setRefreshDate(eq(NEXUS_IQ), any(Date.class));
		assertEquals(BASE_URL + "/" + REPORT_URL, ((NexusIqInformation) nexusIqInfoList.get(0)).getReportUrl());
	}

	@Test
	public void collectNexusIqInformation_NoViolationForApplication() {
		// arrange
		NexusIqInformation nexusIqInfo = new NexusIqInformation(PUBLIC_ID, Stage.BUILD);
		nexusIqInfo.setNoIssueUrl(REPORT_URL_PREFIX);
		List<ApplicationInformation> nexusIqInfoList = Collections.singletonList(nexusIqInfo);
		Screen screen = createScreen(nexusIqInfoList);
		List<Screen> screensList = Collections.singletonList(screen);
		ScreensModel screensModel = createScreensModel(screensList);
		RuleService ruleServiceSpy = Mockito.mock(RuleService.class);

		NexusIqConfigurationService nexusIqConfService = mock(NexusIqConfigurationService.class);
		ApplicationViolationsResponse appViolationsResp = createApplicationViolationsResponse(PUBLIC_ID + "1");

		ApplicationsResponse applicationsResponse = Mockito.mock(ApplicationsResponse.class);
		NexusIqClient nexusIqClient = createNexusIqClient(appViolationsResp, applicationsResponse);

		NexusIqInformationCollector nexusIqCollector = new NexusIqInformationCollector(nexusIqConfService,
				ruleServiceSpy, screensModel, nexusIqClient);

		// act
		nexusIqCollector.collectNexusIqInformation();

		// assert
		verify(screensModel).getAvailableScreens();
		verify(ruleServiceSpy).updateRuleEvaluation(any(), any());
		verify(screen).setRefreshDate(eq(NEXUS_IQ), any(Date.class));
		assertEquals(BASE_URL + "/" + REPORT_URL_PREFIX, ((NexusIqInformation) nexusIqInfoList.get(0)).getReportUrl());
	}

	private ScreensModel createScreensModel(List<Screen> screensList) {
		ScreensModel screensModel = Mockito.mock(ScreensModel.class);
		Mockito.when(screensModel.getAvailableScreens()).thenReturn(screensList);

		return screensModel;
	}

	private Screen createScreen(List<ApplicationInformation> nexusIqInfoList) {
		Screen screen = Mockito.mock(Screen.class);
		Mockito.when(screen.getMatchingInformation(NEXUS_IQ)).thenReturn(nexusIqInfoList);

		return screen;
	}

	private ApplicationViolationsResponse createApplicationViolationsResponse(String publicId) {
		ApplicationViolationsResponse appViolationsResp = new ApplicationViolationsResponse();
		List<ApplicationViolationResponse> appViolations = new ArrayList<>();
		ApplicationViolationResponse appViolation = new ApplicationViolationResponse();
		ApplicationResponse app = new ApplicationResponse();
		app.setPublicId(publicId);
		appViolation.setApplication(app);
		List<PolicyViolationResponse> policyViolations = new ArrayList<>();
		PolicyViolationResponse policyViolation = new PolicyViolationResponse();
		policyViolation.setStage(Stage.BUILD);
		policyViolation.setReportUrl(REPORT_URL);
		policyViolation.setPolicyId("junit-policy-id");
		policyViolations.add(policyViolation);
		appViolation.setPolicyViolations(policyViolations);
		appViolations.add(appViolation);
		appViolationsResp.setApplicationViolations(appViolations);

		return appViolationsResp;
	}

	private NexusIqClient createNexusIqClient(ApplicationViolationsResponse appViolationsResp,
			ApplicationsResponse applicationsResponse) {
		NexusIqClient nexusIqClient = mock(NexusIqClient.class);
		when(nexusIqClient.getBaseUrl()).thenReturn(BASE_URL);
		when(nexusIqClient.retrieveViolations(anyList())).thenReturn(appViolationsResp);
		when(nexusIqClient.retrieveApplications()).thenReturn(applicationsResponse);
		return nexusIqClient;
	}

}
