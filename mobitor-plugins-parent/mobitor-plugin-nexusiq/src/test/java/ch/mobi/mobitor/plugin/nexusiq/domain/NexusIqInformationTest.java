package ch.mobi.mobitor.plugin.nexusiq.domain;

/*-
 * §
 * mobitor-plugin-nexusiq
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.nexusiq.config.Stage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class NexusIqInformationTest {

	private NexusIqInformation info;

	@Before
	public void setUp() {
		info = new NexusIqInformation("publicId", Stage.RELEASE);
	}

	@After
	public void tearDown() {
		info = null;
	}

	@Test
	public void testResetViolationsCount() {
		info.increaseViolationCount("policyId1");

		info.resetViolationsCount();

		assertThat(info.getNexusIqViolations(), equalTo(0));
		assertThat(info.getTotalViolations(), equalTo(0));
	}

	@Test
	public void testIncreaseViolationCount() {
		info.increaseViolationCount("policyId1");
		info.increaseViolationCount("policyId1");
		info.increaseViolationCount("policyId1");

		assertThat(info.getNexusIqViolations(), equalTo(1));
		assertThat(info.getTotalViolations(), equalTo(3));
	}

}
