package ch.mobi.mobitor.plugin.nexusiq.service.client;

/*-
 * §
 * mobitor-plugin-nexusiq
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.nexusiq.NexusIqPluginConfiguration;
import ch.mobi.mobitor.plugin.nexusiq.service.client.domain.ApplicationViolationResponse;
import ch.mobi.mobitor.plugin.nexusiq.service.client.domain.ApplicationViolationsResponse;
import ch.mobi.mobitor.plugin.nexusiq.service.client.domain.ApplicationsResponse;
import ch.mobi.mobitor.plugin.nexusiq.service.client.domain.PolicyViolationResponse;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.junit.Rule;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.givenThat;
import static com.github.tomakehurst.wiremock.client.WireMock.matching;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class NexusIqClientTest {

	@Rule
	public WireMockRule wireMockRule = new WireMockRule(options().dynamicPort());

	@Test
	public void retrieveViolations() {
		// arrange
		givenThat(get(urlMatching("/api/v2/policyViolations?(.*)")).withQueryParam("p", matching("([a-z0-9]+)"))
				.withHeader("Accept", equalTo("application/json"))
				.willReturn(aResponse().withStatus(200).withHeader("Content-Type", "application/json")
						.withBodyFile("nexus-iq-response-policy-violations.json")));

		NexusIqClient client = getClient();

		List<String> violationIds = Arrays.asList("d89b440a4a65481b8109541bd1673910",
				"eb17003085e84a8cad0fea7d312f3fbc");

		// act
		ApplicationViolationsResponse applicationViolations = client.retrieveViolations(violationIds);

		// assert
		assertThat(applicationViolations, is(not(nullValue())));
		assertThat(applicationViolations.getApplicationViolations(), hasSize(greaterThan(0)));

		for (ApplicationViolationResponse applicationViolationResponse : applicationViolations
				.getApplicationViolations()) {
			for (PolicyViolationResponse policyViolationResponse : applicationViolationResponse.getPolicyViolations()) {
				assertThat(policyViolationResponse.getStage(), is(not(nullValue())));
			}
		}
	}

	@Test
	public void retrieveApplications() {
		// arrange
		givenThat(get(urlMatching("/api/v2/applications(.*)")).withHeader("Accept", equalTo("application/json"))
				.willReturn(aResponse().withStatus(200).withHeader("Content-Type", "application/json")
						.withBodyFile("nexus-iq-response-applications.json")));

		NexusIqClient client = getClient();

		// act
		ApplicationsResponse applications = client.retrieveApplications();

		// assert
		assertThat(applications, is(not(nullValue())));
		assertThat(applications.getApplications(), hasSize(greaterThan(0)));
		assertTrue(applications.hasApplication("mobitor"));
		assertFalse(applications.hasApplication("does-not_exist"));
	}

	private NexusIqClient getClient() {
		NexusIqPluginConfiguration nexusIqConfiguration = new NexusIqPluginConfiguration();
		String baseUrl = "http://localhost:" + wireMockRule.port();
		nexusIqConfiguration.setBaseUrl(baseUrl);
		nexusIqConfiguration.setUsername("junit");
		nexusIqConfiguration.setPassword("junit");

		return new NexusIqClient(nexusIqConfiguration);
	}

}
