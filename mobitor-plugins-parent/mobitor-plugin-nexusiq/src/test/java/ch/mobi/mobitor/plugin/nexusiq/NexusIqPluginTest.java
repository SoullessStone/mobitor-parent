package ch.mobi.mobitor.plugin.nexusiq;

/*-
 * §
 * mobitor-plugin-nexusiq
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.nexusiq.config.NexusIqApplicationConfig;
import ch.mobi.mobitor.plugin.nexusiq.config.Stage;
import ch.mobi.mobitor.plugin.nexusiq.domain.NexusIqInformation;
import ch.mobi.mobitor.plugin.test.PluginTest;
import ch.mobi.mobitor.plugins.api.domain.config.ExtendableScreenConfig;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentCaptor.forClass;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

public class NexusIqPluginTest extends PluginTest<NexusIqPlugin, NexusIqApplicationConfig, NexusIqInformation> {

    @Override
    public String getConfigPropertyName() {
        return "nexusIqApplications";
    }

    @Override
    protected NexusIqPlugin newClassUnderTestInstance() {
        NexusIqPluginConfiguration nexusIqPluginConfiguration = new NexusIqPluginConfiguration();
        return new NexusIqPlugin(nexusIqPluginConfiguration);
    }

    @Test
    public void createAndAssociateApplicationInformationBlocksShouldSetCorrectValuesOnInformation() {
        // arrange
        NexusIqPlugin plugin = newClassUnderTestInstance();
        Screen screen = mock(Screen.class);
        ExtendableScreenConfig screenConfig = mock(ExtendableScreenConfig.class);
        NexusIqApplicationConfig config = spy(new NexusIqApplicationConfig());
        String publicId = "publicId";
        Stage stage = Stage.OPERATE;
        config.setPublicId(publicId);
        config.setStage(stage);

        // act
        plugin.createAndAssociateApplicationInformationBlocks(screen, screenConfig, singletonList(config));

        // assert
        ArgumentCaptor<NexusIqInformation> nexusIqInfoCaptor = forClass(NexusIqInformation.class);
        verify(screen).addInformation(any(), any(), any(), nexusIqInfoCaptor.capture());
        NexusIqInformation capturedInformation = nexusIqInfoCaptor.getValue();
        assertThat(publicId).isEqualTo(capturedInformation.getPublicId());
        assertThat(stage).isEqualTo(capturedInformation.getStage());
    }
}
