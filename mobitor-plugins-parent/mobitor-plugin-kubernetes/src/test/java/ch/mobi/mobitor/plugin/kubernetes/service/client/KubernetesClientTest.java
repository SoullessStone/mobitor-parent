package ch.mobi.mobitor.plugin.kubernetes.service.client;

/*-
 * §
 * mobitor-plugin-kubernetes
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.config.EnvironmentNetwork;
import ch.mobi.mobitor.plugin.kubernetes.service.client.domain.JobList;
import ch.mobi.mobitor.plugin.kubernetes.service.client.domain.KubernetesServerConfig;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.google.common.collect.Sets;
import org.jetbrains.annotations.NotNull;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.givenThat;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.Matchers.empty;
import static org.junit.Assert.assertThat;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
public class KubernetesClientTest {

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(WireMockConfiguration.options().dynamicPort());

    @MockBean
    public KubernetesConfigurationService kubernetesConfigurationService;


    @NotNull
    private KubernetesClient setupClient() {
        String baseUrl = "http://localhost:" + wireMockRule.port();

        KubernetesServerConfig localKubeConfig = new KubernetesServerConfig();
        localKubeConfig.setNetwork(EnvironmentNetwork.DEVELOPMENT);
        localKubeConfig.setUrl(baseUrl);
        localKubeConfig.setEnvironments(Sets.newHashSet("T1", "T2"));

        given(this.kubernetesConfigurationService.getKubernetesServerConfigForNetwork(EnvironmentNetwork.DEVELOPMENT)).willReturn(localKubeConfig);
        given(this.kubernetesConfigurationService.getKubernetesServerConfigForEnvironment(ArgumentMatchers.any())).willReturn(localKubeConfig);

        KubernetesRestClient kubernetesRestClient = new KubernetesRestClient();
        return new KubernetesClient(kubernetesConfigurationService, kubernetesRestClient);
    }



    @Test
    public void testJobIsRunning() {
        // arrange
        givenThat(get(urlEqualTo("/apis/batch/v1/namespaces/pdn-b/jobs/?labelSelector=app=pdn-produktversion-job"))
                                   .withHeader("Accept", equalTo("application/json"))
                                   .willReturn(aResponse()
                                                       .withStatus(200)
                                                       .withHeader("Content-Type", "application/json")
                                                       .withBodyFile("k8s-job-running.json")
                )
        );

        KubernetesClient client = setupClient();

        // act
        JobList jobList = client.retrieveJobInformation("B", "pdn-b", "app=pdn-produktversion-job");

        // assert
        assertThat(jobList, is(not(nullValue())));
        assertThat(jobList.getItems(), is(not(empty())));
        assertThat(jobList.getItems().get(0).getStatus().getActive(), is(1));
    }

    @Test
    public void testJobIsSucceeded() {
        // arrange
        givenThat(get(urlEqualTo("/apis/batch/v1/namespaces/pdn-b/jobs/?labelSelector=app=pdn-produktversion-job"))
                                   .withHeader("Accept", equalTo("application/json"))
                                   .willReturn(aResponse()
                                                       .withStatus(200)
                                                       .withHeader("Content-Type", "application/json")
                                                       .withBodyFile("k8s-job-succesful.json")
                )
        );

        KubernetesClient client = setupClient();

        // act
        JobList jobList = client.retrieveJobInformation("B", "pdn-b", "app=pdn-produktversion-job");

        // assert
        assertThat(jobList, is(not(nullValue())));
        assertThat(jobList.getItems(), is(not(empty())));
        assertThat(jobList.getItems().get(0).getStatus().getSucceeded(), is(1));
    }

}
