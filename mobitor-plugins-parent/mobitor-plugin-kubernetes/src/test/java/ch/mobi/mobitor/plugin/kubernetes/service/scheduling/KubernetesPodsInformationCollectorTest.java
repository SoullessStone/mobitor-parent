package ch.mobi.mobitor.plugin.kubernetes.service.scheduling;

/*-
 * §
 * mobitor-plugin-kubernetes
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.config.EnvironmentNetwork;
import ch.mobi.mobitor.plugin.kubernetes.service.client.KubernetesClient;
import ch.mobi.mobitor.plugin.kubernetes.service.client.KubernetesConfigurationService;
import ch.mobi.mobitor.plugin.kubernetes.service.client.KubernetesPodInformationModel;
import ch.mobi.mobitor.plugin.kubernetes.service.client.domain.KubernetesServerConfig;
import ch.mobi.mobitor.plugin.kubernetes.service.client.domain.PodList;
import ch.mobi.mobitor.service.EnvironmentsConfigurationService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@RunWith(SpringRunner.class)
public class KubernetesPodsInformationCollectorTest {

    @MockBean
    KubernetesConfigurationService k8sConfService;

    @Test
    public void collectPodsInformation() {
        // arrange
        KubernetesServerConfig k8sServerConfig = new KubernetesServerConfig();
        when(k8sConfService.getKubernetesServerConfigForNetwork(any())).thenReturn(k8sServerConfig);
        KubernetesClient k8sClient = mock(KubernetesClient.class);

        PodList podList = new PodList();
        when(k8sClient.retrievePodInformation(any(EnvironmentNetwork.class))).thenReturn(podList);

        EnvironmentsConfigurationService environmentsConfigurationService = Mockito.mock(EnvironmentsConfigurationService.class);
        when(environmentsConfigurationService.isNetworkReachable(any(EnvironmentNetwork.class))).thenReturn(true);

        KubernetesPodInformationModel k8sPodModel = new KubernetesPodInformationModel();

        KubernetesPodsInformationCollector k8sCollector = new KubernetesPodsInformationCollector(environmentsConfigurationService, k8sClient, k8sPodModel);

        // act
        k8sCollector.collectPodsInformation();

        // assert
        verify(k8sClient, times(EnvironmentNetwork.values().length)).retrievePodInformation(any());
        verify(environmentsConfigurationService, times(EnvironmentNetwork.values().length)).isNetworkReachable(any(EnvironmentNetwork.class));
    }

}
