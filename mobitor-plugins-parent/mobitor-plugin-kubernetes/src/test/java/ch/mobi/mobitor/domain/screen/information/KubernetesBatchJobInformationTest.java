package ch.mobi.mobitor.domain.screen.information;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.kubernetes.domain.KubernetesBatchJobInformation;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;

import java.time.Duration;
import java.time.LocalDateTime;

import static java.time.temporal.ChronoUnit.SECONDS;
import static org.assertj.core.api.Assertions.assertThat;

public class KubernetesBatchJobInformationTest {

    @Test
    public void lastStartShouldReturnFormattedDate() {
        //given
        KubernetesBatchJobInformation information = aKubernetesBatchJobInformation();

        //when
        String result = information.lastStart();

        //then
        assertThat(result).isEqualTo("13.12.2017 14:15:16");
    }

    @Test
    public void lastStartShouldReturnNullIfStartIsNotSet() {
        //given
        KubernetesBatchJobInformation information = aKubernetesBatchJobInformation();
        information.setStart(null);

        //when
        String result = information.lastStart();

        //then
        assertThat(result).isNull();
    }

    @Test
    public void lastDurationShouldReturnFormattedDuration() {
        //given
        KubernetesBatchJobInformation information = aKubernetesBatchJobInformation();

        //when
        String result = information.lastDuration();

        //then
        assertThat(result).isEqualTo("1h 16min 07s");
    }

    @Test
    public void lastDurationShouldReturnNullIfDurationIsNotSet() {
        //given
        KubernetesBatchJobInformation information = aKubernetesBatchJobInformation();
        information.setDuration(null);

        //when
        String result = information.lastDuration();

        //then
        assertThat(result).isNull();
    }

    @NotNull
    public static KubernetesBatchJobInformation aKubernetesBatchJobInformation() {
        KubernetesBatchJobInformation information = new KubernetesBatchJobInformation("P", "", "app-p");
        information.updateStatus(0, 1, null);
        information.setStart(LocalDateTime.of(2017, 12,13, 14, 15, 16));
        information.setDuration(Duration.of(4567L, SECONDS));
        return information;
    }
}
