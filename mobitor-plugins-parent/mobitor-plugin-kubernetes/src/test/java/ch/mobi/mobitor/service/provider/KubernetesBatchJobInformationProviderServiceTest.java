package ch.mobi.mobitor.service.provider;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.config.EnvironmentConfigProperties;
import ch.mobi.mobitor.config.EnvironmentNetwork;
import ch.mobi.mobitor.plugin.kubernetes.domain.KubernetesBatchJobInformation;
import ch.mobi.mobitor.plugin.kubernetes.service.client.KubernetesBatchJobInformationProviderService;
import ch.mobi.mobitor.plugin.kubernetes.service.client.KubernetesClient;
import ch.mobi.mobitor.plugin.kubernetes.service.client.domain.Job;
import ch.mobi.mobitor.plugin.kubernetes.service.client.domain.JobList;
import ch.mobi.mobitor.plugin.kubernetes.service.client.domain.JobStatus;
import ch.mobi.mobitor.service.EnvironmentsConfigurationService;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import static ch.mobi.mobitor.plugin.kubernetes.domain.JobStatusPhase.RUNNING;
import static ch.mobi.mobitor.plugin.kubernetes.domain.JobStatusPhase.SUCCEEDED;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class KubernetesBatchJobInformationProviderServiceTest {

    private KubernetesClient kubernetesClient;

    private KubernetesBatchJobInformationProviderService service;

    @Before
    public void setUp() {
        kubernetesClient = mock(KubernetesClient.class);
        EnvironmentsConfigurationService environmentsConfigurationService = mock(EnvironmentsConfigurationService.class);
        service = new KubernetesBatchJobInformationProviderService(kubernetesClient, environmentsConfigurationService);
        when(environmentsConfigurationService.getEnvironmentConfig(any())).thenReturn(mock(EnvironmentConfigProperties.class));
        when(environmentsConfigurationService.isNetworkReachable(any(EnvironmentNetwork.class))).thenReturn(true);
        when(environmentsConfigurationService.isNetworkReachable(any(String.class))).thenReturn(true);
    }

    @Test
    public void updateBatchJobInformationShouldNotUpdateInformationIfKubernetesClientDoesNotReturnAResponse() {
        //given
        KubernetesBatchJobInformation information = new KubernetesBatchJobInformation("P", "app=1234", "app-p");

        //when
        service.updateBatchJobInformation(information);

        //then
        assertThat(information.getStatus()).isNull();
        assertThat(information.lastStart()).isNull();
        assertThat(information.lastDuration()).isNull();
    }

    @Test
    public void updateBatchJobInformationShouldNotUpdateInformationIfNamespaceWasNotFound() {
        //given
        KubernetesBatchJobInformation information = new KubernetesBatchJobInformation("P", "app=1234", "app-p");
        when(kubernetesClient.retrieveJobInformation("P", "pdn-p", "app=1234")).thenReturn(new JobList());

        //when
        service.updateBatchJobInformation(information);

        //then
        assertThat(information.getStatus()).isNull();
        assertThat(information.lastStart()).isNull();
        assertThat(information.lastDuration()).isNull();
    }


    @Test
    public void updateBatchJobInformationShouldSetInformation() {
        //given
        KubernetesBatchJobInformation information = new KubernetesBatchJobInformation("b", "app=1234", "pdn-b");
        JobList testJobList = new JobList();
        Job job1 = new Job();
        JobStatus jobStatus1 = new JobStatus();
        jobStatus1.setStartTime(LocalDateTime.of(2000, 8, 13, 13, 13, 13));
        jobStatus1.setCompletionTime(LocalDateTime.of(2000, 8, 13, 15, 20, 47));
        jobStatus1.setSucceeded(1);
        job1.setStatus(jobStatus1);
        List<Job> items = Collections.singletonList(job1);
        testJobList.setItems(items);

        when(kubernetesClient.retrieveJobInformation("b", "pdn-b", "app=1234")).thenReturn(testJobList);

        //when
        service.updateBatchJobInformation(information);

        //then
        assertThat(information.getStatus()).isEqualTo(SUCCEEDED);
        assertThat(information.lastStart()).isEqualTo("13.08.2000 13:13:13");
        assertThat(information.lastDuration()).isEqualTo("2h 07min 34s");
    }

    @Test
    public void updateRunningBatchJobInformationShouldSetInformationStateToRunning() {
        //given
        KubernetesBatchJobInformation information = new KubernetesBatchJobInformation("b", "app=1234", "pdn-b");
        JobList testJobList = new JobList();
        Job job1 = new Job();
        JobStatus jobStatus1 = new JobStatus();
        jobStatus1.setStartTime(LocalDateTime.of(2000, 8, 13, 13, 13, 13));
        jobStatus1.setActive(1);
        job1.setStatus(jobStatus1);
        List<Job> items = Collections.singletonList(job1);
        testJobList.setItems(items);

        when(kubernetesClient.retrieveJobInformation("b", "pdn-b", "app=1234")).thenReturn(testJobList);

        //when
        service.updateBatchJobInformation(information);

        //then
        assertThat(information.getStatus()).isEqualTo(RUNNING);
        assertThat(information.lastStart()).isEqualTo("13.08.2000 13:13:13");
        assertThat(information.lastDuration()).isEqualTo(null);
    }

}
