package ch.mobi.mobitor.plugin.kubernetes.service.scheduling;

/*-
 * §
 * mobitor-plugin-kubernetes
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.kubernetes.service.client.KubernetesBatchJobInformationProviderService;
import ch.mobi.mobitor.plugins.api.model.ScreensModel;
import ch.mobi.mobitor.plugins.api.service.RuleService;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static java.util.Arrays.asList;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class KubernetesBatchJobInformationCollectorTest {

    @Mock
    private ScreensModel screensModel;
    @Mock
    private RuleService ruleService;
    @Mock
    private KubernetesBatchJobInformationProviderService informationProviderService;

    @Test
    public void collectKubernetesBatchJobInformationShouldPopulateKubernetesBatchJobInformationForEachScreen() {
        //given
        MockitoAnnotations.initMocks(this);
        KubernetesBatchJobInformationCollector sut = new KubernetesBatchJobInformationCollector(informationProviderService, screensModel, ruleService);
        when(screensModel.getAvailableScreens()).thenReturn(asList(mock(Screen.class), mock(Screen.class), mock(Screen.class)));
        //when
        sut.collectKubernetesBatchJobInformation();

        //then
        verify(ruleService, times(3)).updateRuleEvaluation(any(), any());
    }
}
