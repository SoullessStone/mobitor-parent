package ch.mobi.mobitor.plugin.kubernetes.service.client.domain;

/*-
 * §
 * mobitor-plugin-kubernetes
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * ContainerState holds a possible state of container.
 *
 * Only one of its members may be specified. If none of them is specified, the default one is ContainerStateWaiting.
 *
 * @see <a href="https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.10/#containerstate-v1-core">ContainerState v1 core</a>
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ContainerState {

//    @JsonProperty
//    private ContainerStateRunning running;

    @JsonProperty
    private ContainerStateTerminated terminated;

//    @JsonProperty
//    private ContainerStateWaiting waiting;

    public ContainerStateTerminated getTerminated() {
        return terminated;
    }

    public void setTerminated(ContainerStateTerminated terminated) {
        this.terminated = terminated;
    }
}
