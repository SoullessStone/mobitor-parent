package ch.mobi.mobitor.plugin.kubernetes.service.client;

/*-
 * §
 * mobitor-plugin-kubernetes
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.config.EnvironmentNetwork;
import ch.mobi.mobitor.plugin.kubernetes.service.client.domain.JobList;
import ch.mobi.mobitor.plugin.kubernetes.service.client.domain.KubernetesServerConfig;
import ch.mobi.mobitor.plugin.kubernetes.service.client.domain.PodList;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;



@Component
public class KubernetesClient {

    private final KubernetesConfigurationService kubernetesConfigurationService;
    private final KubernetesRestClient kubernetesRestClient;


    @Autowired
    public KubernetesClient(KubernetesConfigurationService kubernetesConfigurationService,
                            KubernetesRestClient kubernetesRestClient) {
        this.kubernetesConfigurationService = kubernetesConfigurationService;
        this.kubernetesRestClient = kubernetesRestClient;
    }

    @Nullable
    public PodList retrievePodInformation(EnvironmentNetwork network) {
        KubernetesServerConfig serverConfig = kubernetesConfigurationService.getKubernetesServerConfigForNetwork(network);
        String url = serverConfig.getUrl() + "/api/v1/pods";
        return kubernetesRestClient.executeRequest(url, PodList.class);
    }

    @Nullable
    public JobList retrieveJobInformation(String environment, String namespace, String labelSelector) {
        KubernetesServerConfig config = kubernetesConfigurationService.getKubernetesServerConfigForEnvironment(environment);
        String url = String.format("%s/apis/batch/v1/namespaces/%s/jobs/?labelSelector=%s", config.getUrl(), namespace, labelSelector);
        return kubernetesRestClient.executeRequest(url, JobList.class);
    }

}
