package ch.mobi.mobitor.plugin.kubernetes.service.client;

/*-
 * §
 * mobitor-plugin-kubernetes
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.kubernetes.domain.KubernetesBatchJobInformation;
import ch.mobi.mobitor.plugin.kubernetes.service.client.domain.Job;
import ch.mobi.mobitor.plugin.kubernetes.service.client.domain.JobList;
import ch.mobi.mobitor.plugin.kubernetes.service.client.domain.JobStatus;
import ch.mobi.mobitor.service.EnvironmentsConfigurationService;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;

@Service
public class KubernetesBatchJobInformationProviderService {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final KubernetesClient kubernetesClient;
    private final EnvironmentsConfigurationService environmentsConfigurationService;

    @Autowired
    public KubernetesBatchJobInformationProviderService(KubernetesClient kubernetesClient,
                                                        EnvironmentsConfigurationService environmentsConfigurationService) {
        this.kubernetesClient = kubernetesClient;
        this.environmentsConfigurationService = environmentsConfigurationService;
    }

    public void updateBatchJobInformation(KubernetesBatchJobInformation information) {
        String labelSelector = information.getLabelSelector();
        String environment = information.getEnvironment();

//        EnvironmentConfig environmentConfig = environmentsConfigurationService.getEnvironmentConfig(environment);
        boolean networkIsReachable = environmentsConfigurationService.isNetworkReachable(environment);
        if (networkIsReachable) {
            updateInformation(information, labelSelector, environment);
        } else {
            information.setEnabled(false);
        }
    }

    private void updateInformation(KubernetesBatchJobInformation information, String labelSelector, String environment) {
        JobList jobList = kubernetesClient.retrieveJobInformation(environment, information.getNamespace(), labelSelector);
        try {
            if (jobList != null && CollectionUtils.size(jobList.getItems()) > 0) {
                List<Job> jobs = jobList.getItems();
                jobs.sort(Comparator.comparing(j -> j.getStatus().getStartTime()));

                Job job = jobs.get(jobs.size()-1);
                JobStatus status = job.getStatus();
                if (status != null) {
                    information.updateStatus(status.getActive(), status.getSucceeded(), status.getConditions());
                    information.setStart(status.getStartTime());
                    information.setCompletion(status.getCompletionTime());
                    information.calculateDuration();
                    logger.debug("Updated Kubernetes batch job information for label {} in environment {}", labelSelector, environment);
                    return;
                }
            }
        } catch (IllegalStateException e) {
            logger.error(e.getMessage());
        }
        logger.error("Could not update Kubernetes batch job information for label {} in environment {}", labelSelector, environment);
    }

}
