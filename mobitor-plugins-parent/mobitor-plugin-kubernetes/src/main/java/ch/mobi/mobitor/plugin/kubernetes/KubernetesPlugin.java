package ch.mobi.mobitor.plugin.kubernetes;

/*-
 * §
 * mobitor-plugin-kubernetes
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.kubernetes.config.KubernetesBatchJobConfig;
import ch.mobi.mobitor.plugin.kubernetes.domain.KubernetesBatchJobInformation;
import ch.mobi.mobitor.plugins.api.MobitorPlugin;
import ch.mobi.mobitor.plugins.api.domain.config.ExtendableScreenConfig;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformationLegendWrapper;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ConditionalOnProperty(name = "mobitor.plugins.kubernetesBatchJobs.enabled", havingValue = "true")
public class KubernetesPlugin implements MobitorPlugin<KubernetesBatchJobConfig> {

    @Override
    public String getConfigPropertyName() {
        return "kubernetesBatchJobs";
    }

    @Override
    public Class<KubernetesBatchJobConfig> getConfigClass() {
        return KubernetesBatchJobConfig.class;
    }

    @Override
    public void createAndAssociateApplicationInformationBlocks(Screen screen, ExtendableScreenConfig screenConfig, List<KubernetesBatchJobConfig> configs) {
        for (KubernetesBatchJobConfig config : configs) {
            String serverName = config.getServerName();
            String applicationName = config.getApplicationName();
            String environment = config.getEnvironment();
            String labelSelector = config.getLabelSelector();
            String namespace = config.getNamespace();

            KubernetesBatchJobInformation information = new KubernetesBatchJobInformation(environment, labelSelector, namespace);
            screen.addInformation(serverName, applicationName, environment, information);
        }
    }

    @Override
    public List<ApplicationInformationLegendWrapper> getLegendApplicationInformationList() {
        return new KubernetesBatchJobLegendGenerator().getLegendList();
    }

}
