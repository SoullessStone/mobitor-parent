package ch.mobi.mobitor.plugin.dwh.service.scheduling;

/*-
 * §
 * mobitor-plugin-dwh
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.dwh.DwhDeploymentsPlugin;
import ch.mobi.mobitor.plugin.dwh.domain.DwhDeploymentInformation;
import ch.mobi.mobitor.plugin.dwh.domain.DwhDeploymentsInformation;
import ch.mobi.mobitor.plugin.dwh.service.client.DwhDeployment;
import ch.mobi.mobitor.plugin.dwh.service.client.DwhDeploymentsService;
import ch.mobi.mobitor.plugins.api.model.ScreensModel;
import ch.mobi.mobitor.plugins.api.service.RuleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Date;
import java.util.List;

import static ch.mobi.mobitor.plugin.dwh.domain.DwhDeploymentsInformation.DWH_DEPLOYMENT;
import static java.time.temporal.ChronoField.*;
import static java.util.stream.Collectors.toList;

@Component
@ConditionalOnBean(DwhDeploymentsPlugin.class)
public class DwhDeploymentsInformationCollector {
    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final DateTimeFormatter SHORT_DATE = new DateTimeFormatterBuilder()
            .appendValueReduced(YEAR, 2, 2, 2000).appendLiteral('-')
            .appendValue(MONTH_OF_YEAR, 2).appendLiteral('-')
            .appendValue(DAY_OF_MONTH, 2).toFormatter();
    private final ScreensModel screensModel;
    private final RuleService ruleService;
    private final DwhDeploymentsService dwhDeploymentsService;

    @Autowired
    public DwhDeploymentsInformationCollector(ScreensModel screensModel, RuleService ruleService, DwhDeploymentsService dwhDeploymentsService) {
        this.screensModel = screensModel;
        this.ruleService = ruleService;
        this.dwhDeploymentsService = dwhDeploymentsService;
    }

    @Scheduled(fixedDelayString = "${scheduling.pollingIntervalMs.dwhDeploymentInformationPollingInterval}", initialDelayString = "${scheduling.pollingInitialDelayMs.second}")
    public void collectDwhDeploymentsInformation() {
        long start = System.currentTimeMillis();
        logger.info("Started retrieving DWH deployments information...");
        this.screensModel.getAvailableScreens().parallelStream().forEach(this::populateDwhDeploymentsInformation);
        long stop = System.currentTimeMillis();
        logger.info("Reading DWH deployment information took: " + (stop - start) + "ms");
    }

    private void populateDwhDeploymentsInformation(Screen screen) {
        screen.<DwhDeploymentsInformation>getMatchingInformation(DWH_DEPLOYMENT).forEach(this::updateInformation);
        ruleService.updateRuleEvaluation(screen, DWH_DEPLOYMENT);
        screen.setRefreshDate(DWH_DEPLOYMENT, new Date());
    }

    private void updateInformation(DwhDeploymentsInformation information) {
        List<DwhDeployment> dwhDeployments = dwhDeploymentsService.retrieveDeploymentInformation(
                information.getServerName(), information.getPlugin(), information.getEnvironment());
        if (dwhDeployments != null) {
            information.setDeployments(dwhDeployments.stream()
                    .map(d -> new DwhDeploymentInformation(
                            information.getApplicationName(), information.getEnvironment(),
                            d.getReleaseTimestamp(), d.getJiraId(), d.getBranchName(), d.getDeployUser(), d.getCiLink(),
                            d.getApplication(), LocalDate.ofInstant(d.getReleaseTimestamp(), ZoneId.of("CET")).format(SHORT_DATE), d.getState()))
                    .collect(toList()));
        }
    }

}
