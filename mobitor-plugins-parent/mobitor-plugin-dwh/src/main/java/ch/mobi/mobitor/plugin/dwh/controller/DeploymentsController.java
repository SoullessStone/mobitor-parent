package ch.mobi.mobitor.plugin.dwh.controller;

/*-
 * §
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.dwh.domain.DwhDeploymentInformation;
import ch.mobi.mobitor.plugin.dwh.domain.DwhDeploymentsInformation;
import ch.mobi.mobitor.plugins.api.model.ScreensModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.function.Predicate;

import static ch.mobi.mobitor.plugin.dwh.domain.DwhDeploymentsInformation.DWH_DEPLOYMENT;
import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;

@Controller
public class DeploymentsController {

    private final ScreensModel screensModel;

    @Autowired
    public DeploymentsController(ScreensModel screensModel) {
        this.screensModel = screensModel;
    }

    @RequestMapping("/plugin/dwh/deployments")
    public String overview(@RequestParam("screen") String screenConfigKey,
                           @RequestParam("env") String env,
                           @RequestParam("server") String server,
                           @RequestParam("application") String application,
                           Model model) {

        Screen screen = screensModel.getScreen(screenConfigKey);

        List<DwhDeploymentInformation> deployments = filterDeployments(
                screen.getMatchingInformation(DWH_DEPLOYMENT, env, server, application),
                deployInfo -> true);

        model.addAttribute("deployments", deployments);
        model.addAttribute("screenConfigKey", screenConfigKey);
        model.addAttribute("environment", env);
        model.addAttribute("serverName", server);
        model.addAttribute("applicationName", application);
        model.addAttribute("screen", screen);

        return "deployments";
    }

    @RequestMapping("/plugin/dwh/byBuildId")
    public String buildId(@RequestParam("screen") String screenConfigKey,
                          @RequestParam("buildId") String buildId,
                          Model model) {

        Screen screen = screensModel.getScreen(screenConfigKey);

        List<DwhDeploymentInformation> deployments = filterDeployments(
                screen.getMatchingInformation(DWH_DEPLOYMENT),
                deployInfo -> deployInfo.getCiLink() != null && deployInfo.getCiLink().contains(buildId));

        model.addAttribute("deployments", deployments);
        model.addAttribute("ciLink", deployments.isEmpty() ? "" : deployments.get(0).getCiLink());
        model.addAttribute("buildId", buildId);
        model.addAttribute("screenConfigKey", screenConfigKey);
        model.addAttribute("screen", screen);

        return "byBuildId";
    }

    @RequestMapping("/plugin/dwh/byEnv")
    public String env(@RequestParam("screen") String screenConfigKey,
                      @RequestParam("env") String environment,
                      Model model) {

        Screen screen = screensModel.getScreen(screenConfigKey);

        List<DwhDeploymentInformation> deployments = filterDeployments(
                screen.getMatchingInformation(DWH_DEPLOYMENT),
                deployInfo -> environment.equals(deployInfo.getEnvironment()));

        model.addAttribute("deployments", deployments);
        model.addAttribute("ciLink", deployments.isEmpty() ? "" : deployments.get(0).getCiLink());
        model.addAttribute("env", environment);
        model.addAttribute("screenConfigKey", screenConfigKey);
        model.addAttribute("screen", screen);

        return "byEnv";
    }

    private List<DwhDeploymentInformation> filterDeployments(List<DwhDeploymentsInformation> deploysInfos, Predicate<DwhDeploymentInformation> predicate) {
        return deploysInfos
                .stream()
                .flatMap(deploysInfo -> deploysInfo.getDeployments().stream().filter(predicate))
                .sorted(comparing(DwhDeploymentInformation::getTimestamp).reversed())
                .collect(toList());
    }
}