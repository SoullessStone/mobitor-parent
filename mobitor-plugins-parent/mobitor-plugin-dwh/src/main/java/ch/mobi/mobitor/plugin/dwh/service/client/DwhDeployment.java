package ch.mobi.mobitor.plugin.dwh.service.client;

/*-
 * §
 * mobitor-plugin-dwh
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import java.time.Instant;
import java.util.Objects;

public class DwhDeployment {
    private final String application;
    private final String deployObject;
    private final Instant releaseTimestamp;
    private final String jiraId;
    private final String branchName;
    private final String deployUser;
    private final String ciLink;
    private final String state;

    public DwhDeployment(String application, String deployObject,
                         Instant releaseTimestamp, String jiraId,
                         String branchName, String deployUser,
                         String ciLink, String state) {
        this.application = application;
        this.deployObject = deployObject;
        this.releaseTimestamp = releaseTimestamp;
        this.jiraId = jiraId;
        this.branchName = branchName;
        this.deployUser = deployUser;
        this.ciLink = ciLink;
        this.state = state;
    }

    public String getApplication() {
        return application;
    }

    public String getDeployObject() {
        return deployObject;
    }

    public Instant getReleaseTimestamp() {
        return releaseTimestamp;
    }

    public String getJiraId() {
        return jiraId;
    }

    public String getBranchName() {
        return branchName;
    }

    public String getDeployUser() {
        return deployUser;
    }

    public String getCiLink() {
        return ciLink;
    }

    public String getState() {
        return state;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DwhDeployment that = (DwhDeployment) o;
        return application.equals(that.application) &&
                deployObject.equals(that.deployObject) &&
                releaseTimestamp.equals(that.releaseTimestamp) &&
                Objects.equals(jiraId, that.jiraId) &&
                Objects.equals(branchName, that.branchName) &&
                Objects.equals(deployUser, that.deployUser) &&
                Objects.equals(ciLink, that.ciLink) &&
                state.equals(that.state);
    }

    @Override
    public int hashCode() {
        return Objects.hash(application, deployObject, releaseTimestamp, jiraId, branchName, deployUser, ciLink, state);
    }

    @Override
    public String toString() {
        return "DwhDeployment{" +
                "application='" + application + '\'' +
                ", deployObject='" + deployObject + '\'' +
                ", releaseTimestamp=" + releaseTimestamp +
                ", jiraId='" + jiraId + '\'' +
                ", branchName='" + branchName + '\'' +
                ", deployUser='" + deployUser + '\'' +
                ", ciLink='" + ciLink + '\'' +
                ", state='" + state + '\'' +
                '}';
    }
}
