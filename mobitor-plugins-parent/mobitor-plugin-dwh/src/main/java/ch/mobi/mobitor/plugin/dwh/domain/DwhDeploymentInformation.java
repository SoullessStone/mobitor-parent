package ch.mobi.mobitor.plugin.dwh.domain;

/*-
 * §
 * mobitor-plugin-dwh
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import java.time.Instant;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;

import static java.time.temporal.ChronoField.HOUR_OF_DAY;
import static java.time.temporal.ChronoField.MINUTE_OF_HOUR;

public class DwhDeploymentInformation {
    private final DateTimeFormatter SIMPLE_TIME = new DateTimeFormatterBuilder()
            .appendValue(HOUR_OF_DAY, 2).appendLiteral(':')
            .appendValue(MINUTE_OF_HOUR, 2).toFormatter();

    private final String applicationName;
    private final String environment;

    private final Instant timestamp;
    private final String jiraId;
    private final String branch;
    private final String deployUser;
    private final String ciLink;
    private final String serverName;
    private final String version;
    private final String state;

    public DwhDeploymentInformation(String applicationName, String environment,
                                    Instant timestamp, String jiraId, String branch, String deployUser, String ciLink, String serverName, String version, String state) {
        this.applicationName = applicationName;
        this.environment = environment;
        this.timestamp = timestamp;
        this.jiraId = jiraId;
        this.branch = branch;
        this.deployUser = deployUser;
        this.ciLink = ciLink;
        this.serverName = serverName;
        this.version = version;
        this.state = state;
    }

    public boolean isSuccessful() {
        return "successful".equals(state);
    }

    public String getApplicationName() {
        return applicationName;
    }

    public String getEnvironment() {
        return environment;
    }

    public String getTime() {
        return LocalTime.ofInstant(timestamp, ZoneId.of("CET")).format(SIMPLE_TIME);
    }

    public Instant getTimestamp() {
        return timestamp;
    }

    public String getJiraId() {
        return jiraId;
    }

    public String getBranch() {
        return branch;
    }

    public String getDeployUser() {
        return deployUser;
    }

    public String getCiLink() {
        return ciLink;
    }

    public String getServerName() {
        return serverName;
    }

    public String getVersion() {
        return version;
    }

    public String getState() {
        return state;
    }
}
