package ch.mobi.mobitor.plugin.dwh.rule;

/*-
 * §
 * mobitor-plugin-dwh
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Pipeline;
import ch.mobi.mobitor.domain.screen.PipelineRule;
import ch.mobi.mobitor.domain.screen.RuleEvaluation;
import ch.mobi.mobitor.plugin.dwh.domain.DwhDeploymentInformation;
import ch.mobi.mobitor.plugin.dwh.domain.DwhDeploymentsInformation;
import ch.mobi.mobitor.plugin.test.rule.PipelineRuleTest;
import org.junit.Test;

import java.time.Instant;

import static ch.mobi.mobitor.plugin.dwh.domain.DwhDeploymentsInformation.DWH_DEPLOYMENT;
import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class DwhDeploymentSuccessRuleTest extends PipelineRuleTest {
    private final String PLUGIN_NAME = "junit-plugin";

    @Test
    public void validateRuleCaresAboutCorrectType() {
        // arrange
        DwhDeploymentSuccessRule rule = new DwhDeploymentSuccessRule();

        // act
        boolean validatesType = rule.validatesType(DWH_DEPLOYMENT);

        // assert
        assertThat(validatesType, is(true));
    }

    @Test
    public void validateRuleHasFailuresWhenDeploymentFailed() {
        // arrange
        DwhDeploymentSuccessRule rule = new DwhDeploymentSuccessRule();
        Pipeline pipeline = createPipeline();
        DwhDeploymentsInformation dwhInfo = new DwhDeploymentsInformation(SERVER_NAME, APP_NAME, PLUGIN_NAME, ENV);
        dwhInfo.setDeployments(asList(new DwhDeploymentInformation(APP_NAME, ENV, Instant.now(), "jiraId", "branch", "deployUser", "http://link", "application", "1", "faalsch")));

        pipeline.addInformation(ENV, APP_NAME, dwhInfo);
        RuleEvaluation newRuleEvaluation = createNewRuleEvaluation();

        // act
        rule.evaluateRule(pipeline, newRuleEvaluation);

        // assert
        assertThat(newRuleEvaluation.hasErrors(), is(true));
    }

    @Test
    public void validateRuleSuccessfulWhenDeploymentOk() {
        // arrange
        DwhDeploymentSuccessRule rule = new DwhDeploymentSuccessRule();
        Pipeline pipeline = createPipeline();
        DwhDeploymentsInformation dwhInfo = new DwhDeploymentsInformation(SERVER_NAME, APP_NAME, PLUGIN_NAME, ENV);
        dwhInfo.setDeployments(asList(new DwhDeploymentInformation(APP_NAME, ENV, Instant.now(), "jiraId", "branch", "deployUser", "http://link", "application", "1", "successful")));

        pipeline.addInformation(ENV, APP_NAME, dwhInfo);
        RuleEvaluation newRuleEvaluation = createNewRuleEvaluation();

        // act
        rule.evaluateRule(pipeline, newRuleEvaluation);

        // assert
        assertThat(newRuleEvaluation.hasErrors(), is(false));
    }

    @Override
    protected PipelineRule createNewRule() {
        return new DwhDeploymentSuccessRule();
    }
}
