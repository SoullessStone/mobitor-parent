package ch.mobi.mobitor.plugin.dwh.service.scheduling;

/*-
 * §
 * mobitor-plugin-dwh
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.dwh.domain.DwhDeploymentsInformation;
import ch.mobi.mobitor.plugin.dwh.service.client.DwhDeployment;
import ch.mobi.mobitor.plugin.dwh.service.client.DwhDeploymentsService;
import ch.mobi.mobitor.plugins.api.model.ScreensModel;
import ch.mobi.mobitor.plugins.api.service.RuleService;
import org.junit.Test;

import java.time.Instant;
import java.util.Date;
import java.util.List;

import static ch.mobi.mobitor.plugin.dwh.domain.DwhDeploymentsInformation.DWH_DEPLOYMENT;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

public class DwhDeploymentsInformationCollectorTest {

    @Test
    public void collectDwhDeploymentsInformation() {
        // arrange
        RuleService ruleService = mock(RuleService.class);
        ScreensModel screensModel = mock(ScreensModel.class);
        DwhDeploymentsService dwhDeploymentsService = mock(DwhDeploymentsService.class);
        Screen screen = mock(Screen.class);
        List<Screen> screensList = singletonList(screen);
        DwhDeploymentsInformation info1 = new DwhDeploymentsInformation("serverName", "application", "plugin", "environment");

        when(screensModel.getAvailableScreens()).thenReturn(screensList);
        when(screen.getMatchingInformation(eq(DWH_DEPLOYMENT))).thenReturn(asList(info1));
        Instant timestamp = Instant.ofEpochMilli(1234567890);
        String state = "successful";
        DwhDeployment ed = new DwhDeployment("", "", timestamp, "", "", "", "", state);
        when(dwhDeploymentsService.retrieveDeploymentInformation(any(), any(), any())).thenReturn(asList(ed));

        DwhDeploymentsInformationCollector collector = new DwhDeploymentsInformationCollector(screensModel, ruleService, dwhDeploymentsService);

        // act
        collector.collectDwhDeploymentsInformation();

        // assert
        verify(ruleService).updateRuleEvaluation(any(), eq(DWH_DEPLOYMENT));
        verify(screen).setRefreshDate(eq(DWH_DEPLOYMENT), any(Date.class));
        assertThat(info1.getVersion()).isEqualTo("70-01-15");
        assertThat(info1.isSuccessful()).isEqualTo(true);
    }

}
