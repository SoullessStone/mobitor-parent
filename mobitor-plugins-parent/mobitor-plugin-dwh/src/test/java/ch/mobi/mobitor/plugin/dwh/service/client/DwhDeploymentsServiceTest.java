package ch.mobi.mobitor.plugin.dwh.service.client;

/*-
 * §
 * mobitor-plugin-dwh
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.dwh.service.client.dto.DwhServerConfig;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.junit.Rule;
import org.junit.Test;

import java.time.Instant;
import java.time.LocalDateTime;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static java.time.ZoneOffset.UTC;
import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DwhDeploymentsServiceTest {

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(options().dynamicPort());

    private final static String SERVICE_BASE_PATH = "/dwh/etl/rest";

    private void configureReleasesUrl() {
        givenThat(get(urlMatching(SERVICE_BASE_PATH + "/versions/.*"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("dwh-version-response.json")
                )
        );
    }


    @Test
    public void retrieveDeploymentInformation() {
        // arrange
        String baseUrl = "http://localhost:" + wireMockRule.port() + SERVICE_BASE_PATH;
        configureReleasesUrl();
        DwhConfigurationService configService = mock(DwhConfigurationService.class);
        DwhServerConfig dwhServerConfig = new DwhServerConfig(baseUrl, "BB");
        when(configService.getDwhServerConfig(anyString())).thenReturn(dwhServerConfig);
        DwhDeploymentsService service = new DwhDeploymentsService(configService);

        // act & assert
        Instant timestamp = Instant.from(LocalDateTime.of(2019, 5, 5, 15, 47, 47, 631868000).atZone(UTC));

        assertEquals(asList(
                new DwhDeployment("EDWHC", "Query Surge", timestamp, "LUETSCHINE-707", "feature/LUETSCHINE-707", "u665", null, "successful")
                ),
                service.retrieveDeploymentInformation("EDWHC", "Query Surge", "BB"));

        assertEquals(asList(
                new DwhDeployment("EDWHC", "Data Stage", timestamp, "LUETSCHINE-709", "feature/LUETSCHINE-709", "u667", null, "successful")
                ),
                service.retrieveDeploymentInformation("EDWHC", "Data Stage", "BB"));

        assertEquals(asList(
                new DwhDeployment("EDWHC", "SQL", timestamp, "LUETSCHINE-708", "feature/LUETSCHINE-708", "u666", null, "successful"),
                new DwhDeployment("EDWHC", "SQL", timestamp, "", "heads/master", "u666", "http://link", "successful")
                ),
                service.retrieveDeploymentInformation("EDWHC", "SQL", "BB"));
    }
}
