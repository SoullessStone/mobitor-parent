package ch.mobi.mobitor.plugin.bitbucket.service.scheduling;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.deployment.Deployment;
import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.bitbucket.domain.BitBucketInformation;
import ch.mobi.mobitor.plugin.bitbucket.service.client.BitBucketClient;
import ch.mobi.mobitor.plugin.bitbucket.service.client.domain.BitBucketAuthorResponse;
import ch.mobi.mobitor.plugin.bitbucket.service.client.domain.BitBucketCommitResponse;
import ch.mobi.mobitor.plugin.bitbucket.service.client.domain.BitBucketTagResponse;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformation;
import ch.mobi.mobitor.plugins.api.model.ScreensModel;
import ch.mobi.mobitor.plugins.api.service.RuleService;
import ch.mobi.mobitor.service.DeploymentInformationService;
import io.micrometer.core.instrument.MeterRegistry;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.boot.test.rule.OutputCapture;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static ch.mobi.mobitor.plugin.bitbucket.domain.BitBucketInformation.BITBUCKET;
import static java.util.Collections.singletonList;
import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anySet;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.matches;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class BitBucketInformationCollectorTest {

    @Rule
    public OutputCapture capture = new OutputCapture();

    private BitBucketInformationCollector collector;
    private Screen bitbucketScreen;
    private RuleService ruleService;
    private CommitUntilDeployMetricService commitService;

    @Before
    public void setUp(){
        ScreensModel screens = mock(ScreensModel.class);
        bitbucketScreen = mock(Screen.class);
        List<Screen> screensList = new ArrayList<>();
        screensList.add(bitbucketScreen);

        when(screens.getAvailableScreens()).thenReturn(screensList);

        BitBucketInformation bitBucketInformation = new BitBucketInformation();
        bitBucketInformation.setProject("ARO");
        bitBucketInformation.setRepository("aro-anliegenfall-service");
        bitBucketInformation.setServerName("aro_anliegenfall_service");
        bitBucketInformation.setApplicationName("ch_mobi_aro_anliegenfall_service");
        bitBucketInformation.setEnvironment("P");
        List<ApplicationInformation> applicationInformations = new ArrayList<>();
        applicationInformations.add(bitBucketInformation);

        when(bitbucketScreen.getMatchingInformation(BITBUCKET)).thenReturn(applicationInformations);

        DeploymentInformationService deploymentInformationService = mock(DeploymentInformationService.class);
        Deployment deployment = new Deployment();
        deployment.setServerName("aro_anliegenfall_service");
        deployment.setEnvironment("P");
        deployment.setState("success");
        deployment.setId("459477");
        deployment.setDeploymentDate(123456L);
        deployment.setRequestUser("exampleusername");
        List<Deployment> deployments = new ArrayList<>();
        deployments.add(deployment);

        when(deploymentInformationService.retrieveDeployments("aro_anliegenfall_service")).thenReturn(deployments);
        when(deploymentInformationService.filterDeployments(any(),any())).thenReturn(deployments);
        when(deploymentInformationService.getVersion(any(),any())).thenReturn("2.0.1");



        BitBucketClient bitBucketClient = mock(BitBucketClient.class);
        BitBucketTagResponse tagResponse = new BitBucketTagResponse();
        tagResponse.setDisplayId("aro-2.0.1");
        tagResponse.setLatestCommit("Yesterday");
        List<BitBucketTagResponse> tagList = new ArrayList<>();
        tagList.add(tagResponse);
        BitBucketCommitResponse response = new BitBucketCommitResponse();
        BitBucketAuthorResponse authorResponse = new BitBucketAuthorResponse();
        authorResponse.setDisplayName("exampleusername");
        response.setAuthor(authorResponse);
        response.setAuthorTimestamp("12-12-12 08:00:00");
        response.setMessage("First commit");

        when(bitBucketClient.retrieveTags(any(),any(),any())).thenReturn(tagList);
        when(bitBucketClient.retrieveCommit(any(),any(),any())).thenReturn(response);

        ruleService = mock(RuleService.class);
        commitService = mock(CommitUntilDeployMetricService.class);

        collector = new BitBucketInformationCollector(bitBucketClient, screens, ruleService, deploymentInformationService, commitService);
    }

    @Test
    public void collectBitBucketInformation() {
        // arrange

        // act
        collector.collectBitBucketInformation();

        // assert
        assertThat(capture.toString(), containsString("reading BitBucket information took:"));
        // ruleService is called
        verify(ruleService).updateRuleEvaluation(bitbucketScreen, BITBUCKET);

        // bitbucket refresh date is updated
        verify(bitbucketScreen).setRefreshDate(eq(BITBUCKET), any(Date.class));

        // Metric refresh (no BitBucketConfig in this test Screen)
        verify(commitService, atLeastOnce()).submitCommitUntilDeploymentDuration(any());
    }

    @Test
    public void testCollectorRetrievesTagsAndCommits() {
        // arrange
        ScreensModel screensModel = mock(ScreensModel.class);
        Screen testScreen = mock(Screen.class);
        when(screensModel.getAvailableScreens()).thenReturn(singletonList(testScreen));
        List<ApplicationInformation> bitBucketInfosList = new ArrayList<>();
        BitBucketInformation bitInfo = new BitBucketInformation();
        bitInfo.setServerName("server_name");
        bitInfo.setApplicationName("application_name_6");
        bitInfo.setEnvironment("TESTENV");
        bitInfo.setProject("TESTPROJ");
        bitInfo.setRepository("TESTREPO");
        bitBucketInfosList.add(bitInfo);

        when(testScreen.getMatchingInformation(anyString())).thenReturn(bitBucketInfosList);

        DeploymentInformationService deploymentInformationService = mock(DeploymentInformationService.class);

        List<ch.mobi.mobitor.domain.deployment.Deployment> deployments = new ArrayList<>();
        Deployment depl = new Deployment();
        depl.setDeploymentDate(System.currentTimeMillis());
        deployments.add(depl);

        when(deploymentInformationService.retrieveDeployments(anyString())).thenReturn(deployments);
        when(deploymentInformationService.filterDeployments(anyList(), anySet())).thenReturn(deployments);
        when(deploymentInformationService.getVersion(eq("application_name_6"), anyList())).thenReturn("0.0.0-JUNIT");

        BitBucketClient bbClientMock = mock(BitBucketClient.class);
        List<BitBucketTagResponse> bitBucketTags = new ArrayList<>();
        BitBucketTagResponse tag = new BitBucketTagResponse();
        tag.setLatestCommit("JUNITCOMMITID");
        tag.setDisplayId("0.0.0-JUNIT");
        bitBucketTags.add(tag);

        BitBucketCommitResponse bitBucketCommitResponse = new BitBucketCommitResponse();
        BitBucketAuthorResponse author = new BitBucketAuthorResponse();
        author.setDisplayName("JUNIT Name");
        bitBucketCommitResponse.setAuthor(author);
        bitBucketCommitResponse.setAuthorTimestamp(Long.toString(System.currentTimeMillis()));

        when(bbClientMock.retrieveTags(anyString(), anyString(), anyString())).thenReturn(bitBucketTags);
        when(bbClientMock.retrieveCommit(anyString(), anyString(), matches("JUNITCOMMITID"))).thenReturn(bitBucketCommitResponse);

        RuleService ruleService = mock(RuleService.class);

        MeterRegistry meterRegistryMock = mock(MeterRegistry.class);
        CommitUntilDeployMetricService commit2deployService = new CommitUntilDeployMetricService(meterRegistryMock);

        BitBucketInformationCollector bbCollector = new BitBucketInformationCollector(bbClientMock, screensModel, ruleService, deploymentInformationService, commit2deployService);

        // act
        bbCollector.collectBitBucketInformation();

        // assert
        verify(bbClientMock).retrieveCommit(anyString(), anyString(), anyString());
        verify(bbClientMock).retrieveTags(anyString(), anyString(), anyString());
    }
}
