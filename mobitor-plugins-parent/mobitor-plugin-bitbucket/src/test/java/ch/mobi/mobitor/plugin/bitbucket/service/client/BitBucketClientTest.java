package ch.mobi.mobitor.plugin.bitbucket.service.client;

/*-
 * §
 * mobitor-plugin-bitbucket
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.bitbucket.BitBucketPluginConfiguration;
import ch.mobi.mobitor.plugin.bitbucket.service.client.domain.BitBucketCommitInfoResponse;
import ch.mobi.mobitor.plugin.bitbucket.service.client.domain.BitBucketCommitResponse;
import ch.mobi.mobitor.plugin.bitbucket.service.client.domain.BitBucketTagResponse;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.github.tomakehurst.wiremock.matching.AnythingPattern;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.givenThat;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static com.github.tomakehurst.wiremock.client.WireMock.verify;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.hamcrest.collection.IsEmptyCollection.empty;


public class BitBucketClientTest {

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(options().dynamicPort());
    private BitBucketPluginConfiguration bitBucketPluginConfiguration = new BitBucketPluginConfiguration();

    @Before
    public void setup() {
        bitBucketPluginConfiguration.setUsername("bitbucket-junit");
        bitBucketPluginConfiguration.setPassword("bitbucket-junit");
    }

    private void configureTagsUrl() {
        givenThat(get(urlMatching("/rest/api/1.0/projects/([a-zA-Z0-9_\\-]+)/repos/([a-zA-Z0-9_\\-]+)/tags/.*"))
                .withHeader("Accept", equalTo("application/json"))
                .withQueryParam("filterText", new AnythingPattern())
                .withBasicAuth(bitBucketPluginConfiguration.getUsername(), bitBucketPluginConfiguration.getPassword())
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("bitbucket-tags-list.json")
                )
        );
    }

    private void configureCommitIdUrl() {
        givenThat(get(urlMatching("/rest/api/1.0/projects/([a-zA-Z0-9_\\-]+)/repos/([a-zA-Z0-9_\\-]+)/commits/([a-zA-Z0-9_\\-]{40})"))
                .withHeader("Accept", equalTo("application/json"))
                .withBasicAuth(bitBucketPluginConfiguration.getUsername(), bitBucketPluginConfiguration.getPassword())
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("bitbucket-commit-9e4a22.json")
                )
        );
    }

    private void configureCommitIdsUrl() {
        givenThat(get(urlMatching("/rest/api/1.0/projects/([a-zA-Z0-9_\\-]+)/repos/([a-zA-Z0-9_\\-]+)/commits/(.*)"))
                .withHeader("Accept", equalTo("application/json"))
                .withBasicAuth(bitBucketPluginConfiguration.getUsername(), bitBucketPluginConfiguration.getPassword())
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("bitbucket-commits-list.json")
                )
        );
    }


    @Test
    public void retrieveTags() {
        // arrange
        configureTagsUrl();

        String baseUrl = "http://localhost:" + wireMockRule.port();
        bitBucketPluginConfiguration.setBaseUrl(baseUrl);

        BitBucketClient client = new BitBucketClient(bitBucketPluginConfiguration);

        // act
        List<BitBucketTagResponse> tags = client.retrieveTags("vvn", "vvn-angebot-jeeservice", "");

        // assert
        verify(getRequestedFor(urlMatching("/rest/api/1.0/projects/([a-zA-Z0-9_\\-]+)/repos/([a-zA-Z0-9_\\-]+)/tags/.*")).withQueryParam("filterText", new AnythingPattern()));

        assertThat(tags, is(not(nullValue())));
        assertThat(tags, is(not(empty())));
    }

    @Test
    public void retrieveCommit() {
        // arrange
        configureCommitIdUrl();

        String baseUrl = "http://localhost:" + wireMockRule.port();
        bitBucketPluginConfiguration.setBaseUrl(baseUrl);

        BitBucketClient client = new BitBucketClient(bitBucketPluginConfiguration);

        // act
        BitBucketCommitResponse commitInfo = client.retrieveCommit("vvn", "vvn-angebot-jeeservice", "72fc2749b789b2083e9de11392ae9499a24a8c9d");

        // assert
        verify(getRequestedFor(urlMatching("/rest/api/1.0/projects/([a-zA-Z0-9_\\-]+)/repos/([a-zA-Z0-9_\\-]+)/commits/([a-zA-Z0-9_\\-]{40})")));

        assertThat(commitInfo, is(not(nullValue())));
        assertThat(commitInfo.getAuthorTimestamp(), not(isEmptyOrNullString()));

        assertThat(commitInfo.getAuthor(), is(not(nullValue())));
        assertThat(commitInfo.getAuthor().getDisplayName(), is(not(nullValue())));
    }

    @Test
    public void retrieveCommits() {
        // arrange
        configureCommitIdsUrl();

        String baseUrl = "http://localhost:" + wireMockRule.port();
        bitBucketPluginConfiguration.setBaseUrl(baseUrl);

        BitBucketClient client = new BitBucketClient(bitBucketPluginConfiguration);

        // act
        List<BitBucketCommitInfoResponse> commitInfos = client.retrieveCommits("mcs", "mcs-jeeservice", "");


        // assert
        verify(getRequestedFor(urlMatching("/rest/api/1.0/projects/([a-zA-Z0-9_\\-]+)/repos/([a-zA-Z0-9_\\-]+)/commits/(.*)")));

        assertThat(commitInfos, is(not(nullValue())));
        assertThat(commitInfos, is(not(empty())));
        for (BitBucketCommitInfoResponse commitInfo : commitInfos) {
            assertThat(commitInfo.getJiraKeys(), is(not(nullValue())));
            if ("bc87ed15b90cb2764a3cf26e79016f4a30840f27".equals(commitInfo.getId())) {
                assertThat(commitInfo.getJiraKeys(), hasSize(2));
            }
        }


    }

}
