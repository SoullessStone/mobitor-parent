package ch.mobi.mobitor.plugin.bitbucket.domain;

/*-
 * §
 * mobitor-plugin-bitbucket
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;


public class BitBucketInformationTest {

    private BitBucketInformation information;

    @Before
    public void setUp() {
        information = new BitBucketInformation();
    }

    @Test
    public void getCommitDateShouldReturnCommitDate() {
        //arrange
        long date = new Date().getTime();
        information.setCommitTimestamp(String.valueOf(date));

        //act
        Date commitDate = information.getCommitDate();

        //assert
        assertThat(commitDate).isToday();
    }

    @Test
    public void getCommitDateShouldReturnNullIfNotInitialized() {
        //arrange

        //act
        Date commitDate = information.getCommitDate();

        //assert
        assertThat(commitDate).isNull();
    }

    @Test
    public void getDaysSinceCommitShouldReturnCorrectDays() {
        //arrange
        int days = 7;
        information.setCommitTimestamp(String.valueOf(epochBeforeDays(days)));

        //act
        long daysSinceCommit = information.getDaysSinceCommit();

        //assert
        assertThat(daysSinceCommit).isEqualTo(days);
    }

    @Test
    public void getDaysSinceCommitShouldReturnNegativeValueIfNotInitialized() {
        //arrange

        //act
        long daysSinceCommit = information.getDaysSinceCommit();

        //assert
        assertThat(daysSinceCommit).isEqualTo(-1L);
    }

    @Test
    public void getDaysSinceDeploymentShouldReturnCorrectDays() {
        //arrange
        int commit = 7;
        information.setCommitTimestamp(String.valueOf(epochBeforeDays(commit)));
        int deployment = 4;
        information.setDeploymentDate(epochBeforeDays(deployment));

        //act
        long daysSinceDeployment = information.getDaysSinceDeployment();

        //assert
        assertThat(daysSinceDeployment).isEqualTo(-(deployment - commit));
    }

    @Test
    public void getDaysSinceDeploymentShouldReturnNegativeValueIfNotInitialized() {
        //arrange

        //act
        long daysSinceCommit = information.getDaysSinceDeployment();

        //assert
        assertThat(daysSinceCommit).isEqualTo(-1L);
    }

    private long epochBeforeDays(int days) {
        long diffOfDaysInMillis = TimeUnit.MILLISECONDS.convert(days, TimeUnit.DAYS);
        return System.currentTimeMillis() - diffOfDaysInMillis;
    }

}
