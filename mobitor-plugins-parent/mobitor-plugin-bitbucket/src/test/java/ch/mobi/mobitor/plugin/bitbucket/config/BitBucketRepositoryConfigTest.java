package ch.mobi.mobitor.plugin.bitbucket.config;

/*-
 * §
 * mobitor-plugin-bitbucket
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import org.junit.Test;
import static org.junit.Assert.*;

public class BitBucketRepositoryConfigTest {

    @Test
    public void projectPropertyTest() {
        BitBucketRepositoryConfig config = new BitBucketRepositoryConfig();
        String projectName = "Mobitor";

        config.setProject(projectName);

        assertEquals(config.getProject(),projectName);
    }

    @Test
    public void repositoryPropertyTest() {
        BitBucketRepositoryConfig config = new BitBucketRepositoryConfig();
        String repositoryName = "jap-mobitor-parent";

        config.setRepository(repositoryName);

        assertEquals(config.getRepository(),repositoryName);
    }

    @Test
    public void serverNamePropertyTest() {
        BitBucketRepositoryConfig config = new BitBucketRepositoryConfig();
        String serverName = "jap_mobitor";

        config.setServerName(serverName);

        assertEquals(config.getServerName(),serverName);
    }

    @Test
    public void applicationNamePropertyTest() {
        BitBucketRepositoryConfig config = new BitBucketRepositoryConfig();
        String applicationName = "ch_mobi_jap_mobitor";

        config.setApplicationName(applicationName);

        assertEquals(config.getApplicationName(),applicationName);
    }

    @Test
    public void environmentPropertyTest() {
        BitBucketRepositoryConfig config = new BitBucketRepositoryConfig();
        String environmentName = "build";

        config.setEnvironment(environmentName);

        assertEquals(config.getEnvironment(),environmentName);
    }
}
