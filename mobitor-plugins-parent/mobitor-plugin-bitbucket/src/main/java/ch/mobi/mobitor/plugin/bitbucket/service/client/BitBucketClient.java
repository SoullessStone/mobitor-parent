package ch.mobi.mobitor.plugin.bitbucket.service.client;

/*-
 * §
 * mobitor-plugin-bitbucket
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.bitbucket.BitBucketPluginConfiguration;
import ch.mobi.mobitor.plugin.bitbucket.service.client.domain.BitBucketCommitInfoResponse;
import ch.mobi.mobitor.plugin.bitbucket.service.client.domain.BitBucketCommitResponse;
import ch.mobi.mobitor.plugin.bitbucket.service.client.domain.BitBucketCommitsResponse;
import ch.mobi.mobitor.plugin.bitbucket.service.client.domain.BitBucketTagResponse;
import ch.mobi.mobitor.plugin.bitbucket.service.client.domain.BitBucketTagsResponse;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpHost;
import org.apache.http.client.fluent.Executor;
import org.apache.http.client.fluent.Request;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static java.nio.charset.StandardCharsets.UTF_8;

@Component
public class BitBucketClient {

    private static final Logger LOG = LoggerFactory.getLogger(BitBucketClient.class);

    private final BitBucketPluginConfiguration bitBucketPluginConfiguration;

    @Autowired
    public BitBucketClient(BitBucketPluginConfiguration bitBucketPluginConfiguration) {
        this.bitBucketPluginConfiguration = bitBucketPluginConfiguration;
    }

    /**
     * https://bitbucket.server/rest/api/1.0/projects/vvn/repos/vvn-angebot-jeeservice/tags/
     *
     * This method does not support personal repositories for now.
     *
     * @param project project key in BitBucket
     * @param repository the git repository name in bitbucket
     * @param filterText the text to filter tags, must not be null (may be empty)
     */
    public List<BitBucketTagResponse> retrieveTags(@NotNull String project, @NotNull String repository, String filterText) {
        String restUrlPath = "/rest/api/1.0/projects/%1$s/repos/%2$s/tags/?filterText=%3$s&limit=200";
        String url = bitBucketPluginConfiguration.getBaseUrl() + String.format(restUrlPath, project, repository, filterText);

        Executor executor = Executor.newInstance();
        HttpHost host = HttpHost.create(bitBucketPluginConfiguration.getBaseUrl());
        executor.auth(bitBucketPluginConfiguration.getUsername(), bitBucketPluginConfiguration.getPassword()).authPreemptive(host);
        try {
            String response = executor
                    .execute(Request.Get(url)
                            .addHeader("Accept", "application/json")
                            .connectTimeout(3000)
                            .socketTimeout(3000))
                    .returnContent()
                    .asString();

            return createTagsResponse(response);

        } catch (Exception e) {
            String tagsFrom = String.format("Project:%1$s // Repository:%2$s // Filter:%3$s", project, repository, filterText);
            LOG.error("Could not determine Tags for: " + tagsFrom, e);
        }

        return new ArrayList<>();
    }

    private List<BitBucketTagResponse> createTagsResponse(String response) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        BitBucketTagsResponse bitBucketTagsResponse = mapper.readValue(response, BitBucketTagsResponse.class);
        return bitBucketTagsResponse.getTags();
    }

    public BitBucketCommitResponse retrieveCommit(@NotNull String project, @NotNull String repository, @NotNull String commitId) {
        String restUrlPath = "/rest/api/1.0/projects/%1$s/repos/%2$s/commits/%3$s";
        String url = bitBucketPluginConfiguration.getBaseUrl() + String.format(restUrlPath, project, repository, commitId);

        HttpHost host = HttpHost.create(bitBucketPluginConfiguration.getBaseUrl());
        Executor executor = Executor.newInstance();
        executor.auth(bitBucketPluginConfiguration.getUsername(), bitBucketPluginConfiguration.getPassword()).authPreemptive(host);
        try {
            String response = executor
                    .execute(Request.Get(url)
                            .addHeader("Accept", "application/json")
                            .connectTimeout(3000)
                            .socketTimeout(3000))
                    .returnContent()
                    .asString();

            return createCommitResponse(response);

        } catch (Exception e) {
            LOG.error("Could not retrieve commit information from BitBucket:" + e.getMessage(), e);
        }

        return null;
    }


    private BitBucketCommitResponse createCommitResponse(String response) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        BitBucketCommitResponse commitResponse = mapper.readValue(response, BitBucketCommitResponse.class);
        return commitResponse;
    }

    public List<BitBucketCommitInfoResponse> retrieveCommits(@NotNull String project, @NotNull String repository, String until) {
        String untilParam = StringUtils.isBlank(until) ? "" : until;
        String restUrlPath = "/rest/api/1.0/projects/%1$s/repos/%2$s/commits/?limit=500&until=%3$s";

        HttpHost host = HttpHost.create(bitBucketPluginConfiguration.getBaseUrl());
        Executor executor = Executor.newInstance();
        executor.auth(bitBucketPluginConfiguration.getUsername(), bitBucketPluginConfiguration.getPassword()).authPreemptive(host);
        String url = "";
        try {
            url = bitBucketPluginConfiguration.getBaseUrl() + String.format(
                    restUrlPath,
                    project,
                    repository,
                    URLEncoder.encode(untilParam, UTF_8));
            String response = executor
                    .execute(Request.Get(url)
                            .addHeader("Accept", "application/json")
                            .connectTimeout(20000)
                            .socketTimeout(20000))
                    .returnContent()
                    .asString();

            return createCommitsResponse(response).getCommits();

        } catch (Exception e) {
            LOG.error("Could not retrieve commits from BitBucket: " + url);
            LOG.error("Message from BitBucket:" + e.getMessage());
        }

        return Collections.emptyList();
    }

    private BitBucketCommitsResponse createCommitsResponse(String response) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        BitBucketCommitsResponse commitsResponse = mapper.readValue(response, BitBucketCommitsResponse.class);
        return commitsResponse;
    }
}
