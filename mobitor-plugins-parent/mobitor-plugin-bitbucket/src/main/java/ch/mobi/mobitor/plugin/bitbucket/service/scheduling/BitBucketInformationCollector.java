package ch.mobi.mobitor.plugin.bitbucket.service.scheduling;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.deployment.Deployment;
import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.bitbucket.BitBucketRepositoriesPlugin;
import ch.mobi.mobitor.plugin.bitbucket.domain.BitBucketInformation;
import ch.mobi.mobitor.plugin.bitbucket.service.client.BitBucketClient;
import ch.mobi.mobitor.plugin.bitbucket.service.client.domain.BitBucketCommitResponse;
import ch.mobi.mobitor.plugin.bitbucket.service.client.domain.BitBucketTagResponse;
import ch.mobi.mobitor.plugins.api.model.ScreensModel;
import ch.mobi.mobitor.plugins.api.service.RuleService;
import ch.mobi.mobitor.service.DeploymentInformationService;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

import static ch.mobi.mobitor.plugin.bitbucket.domain.BitBucketInformation.BITBUCKET;

@Component
@ConditionalOnBean(BitBucketRepositoriesPlugin.class)
public class BitBucketInformationCollector {

    private final static Logger LOG = LoggerFactory.getLogger(BitBucketInformationCollector.class);

    private final ScreensModel screensModel;
    private final BitBucketClient bitBucketClient;
    private final RuleService ruleService;
    private final DeploymentInformationService deploymentInformationService;
    private final CommitUntilDeployMetricService commitUntilDeployMetricService;

    @Autowired
    public BitBucketInformationCollector(BitBucketClient bitBucketClient,
                                         ScreensModel screensModel,
                                         RuleService ruleService,
                                         DeploymentInformationService deploymentInformationService,
                                         CommitUntilDeployMetricService commitUntilDeployMetricService) {
        this.bitBucketClient = bitBucketClient;
        this.screensModel = screensModel;
        this.ruleService = ruleService;
        this.deploymentInformationService = deploymentInformationService;
        this.commitUntilDeployMetricService = commitUntilDeployMetricService;
    }

    @Scheduled(fixedDelayString = "${scheduling.pollingIntervalMs.bitBucketPollingInterval}", initialDelayString = "${scheduling.pollingInitialDelayMs.second}")
    public void collectBitBucketInformation() {
        long start = System.currentTimeMillis();
        this.screensModel.getAvailableScreens().parallelStream().forEach(this::populateBitBucketInformation);
        long stop = System.currentTimeMillis();
        LOG.info("reading BitBucket information took: " + (stop-start) + "ms");
    }

    private void populateBitBucketInformation(Screen screen) {
        List<BitBucketInformation> bitBucketInformationList = screen.getMatchingInformation(BITBUCKET);
        bitBucketInformationList.forEach(this::updateCommitDate);

        ruleService.updateRuleEvaluation(screen, BITBUCKET);
        screen.setRefreshDate(BITBUCKET, new Date());
    }

    private void updateCommitDate(BitBucketInformation bitBucketInformation) {
        String project = bitBucketInformation.getProject();
        String repository = bitBucketInformation.getRepository();
        String server = bitBucketInformation.getServerName();
        String application = bitBucketInformation.getApplicationName();
        String env = bitBucketInformation.getEnvironment();

        String version = "";
        long deploymentDate = 0;
        try {
            List<Deployment> deployments = deploymentInformationService.retrieveDeployments(server);
            List<Deployment> filteredDeployments = deploymentInformationService.filterDeployments(deployments, Set.of(env));
            if (filteredDeployments != null && filteredDeployments.size() == 1) {
                Deployment deployment = filteredDeployments.get(0);
                version = deploymentInformationService.getVersion(application, Collections.singletonList(deployment));
                deploymentDate = deployment.getDeploymentDate();
            }

        } catch (Exception e) {
            LOG.error("Could not read version from LIIMA.");
        }

        String releaseTag = "release/" + version;

        List<BitBucketTagResponse> bitBucketTags = bitBucketClient.retrieveTags(project, repository, releaseTag);
        String commitId = findCommitId(version, releaseTag, bitBucketTags);
        if (StringUtils.isBlank(commitId)) {
            LOG.warn("Could not find tag for version: " + version);

        } else {
            BitBucketCommitResponse commitResponse = bitBucketClient.retrieveCommit(project, repository, commitId);

            bitBucketInformation.setCommitTimestamp(commitResponse.getAuthorTimestamp());
            bitBucketInformation.setCommitMessage(commitResponse.getMessage());
            bitBucketInformation.setAuthor(commitResponse.getAuthor().getDisplayName());
            bitBucketInformation.setDeploymentDate(deploymentDate);

            updateMetric(bitBucketInformation);
        }
    }

    private void updateMetric(BitBucketInformation bitBucketInformation) {
        commitUntilDeployMetricService.submitCommitUntilDeploymentDuration(bitBucketInformation);
    }

    /**
     * try to match version from LIIMA or the extracted build counter with the git tag name
     */
    private String findCommitId(@NotNull String version, @NotNull String buildCount, List<BitBucketTagResponse> bitBucketTags) {
        for (BitBucketTagResponse bitBucketTag : bitBucketTags) {
            if (bitBucketTag.getDisplayId().endsWith(version) || bitBucketTag.getDisplayId().contains(buildCount)) {
                return bitBucketTag.getLatestCommit();
            }
        }
        return null;
    }

}
