package ch.mobi.mobitor.plugin.bitbucket.service.client.domain;

/*-
 * §
 * mobitor-plugin-bitbucket
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import com.fasterxml.jackson.annotation.JsonProperty;

public class BitBucketCommitResponse {

    @JsonProperty private String id;
    @JsonProperty private String displayId;
    @JsonProperty private BitBucketAuthorResponse author;
    @JsonProperty private String authorTimestamp;
    @JsonProperty private String message;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDisplayId() {
        return displayId;
    }

    public void setDisplayId(String displayId) {
        this.displayId = displayId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public BitBucketAuthorResponse getAuthor() {
        return author;
    }

    public void setAuthor(BitBucketAuthorResponse author) {
        this.author = author;
    }

    public String getAuthorTimestamp() {
        return authorTimestamp;
    }

    public void setAuthorTimestamp(String authorTimestamp) {
        this.authorTimestamp = authorTimestamp;
    }
}
