package ch.mobi.mobitor.plugin.bitbucket.service.client.domain;

/*-
 * §
 * mobitor-plugin-bitbucket
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */


import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class BitBucketCommitsResponse {

    @JsonProperty private long start;
    @JsonProperty private long nextPageStart;
    @JsonProperty private long size;
    @JsonProperty private long limit;
    @JsonProperty private boolean isLastPage;

    @JsonProperty("values") private List<BitBucketCommitInfoResponse> commits;

    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public long getNextPageStart() {
        return nextPageStart;
    }

    public void setNextPageStart(long nextPageStart) {
        this.nextPageStart = nextPageStart;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public long getLimit() {
        return limit;
    }

    public void setLimit(long limit) {
        this.limit = limit;
    }

    public boolean isLastPage() {
        return isLastPage;
    }

    public void setLastPage(boolean lastPage) {
        isLastPage = lastPage;
    }

    public List<BitBucketCommitInfoResponse> getCommits() {
        return commits;
    }

    public void setCommits(List<BitBucketCommitInfoResponse> commits) {
        this.commits = commits;
    }
}
