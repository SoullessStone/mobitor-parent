package ch.mobi.mobitor.plugin.rest.config;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */


import ch.mobi.mobitor.plugins.api.PluginConfig;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class RestServiceConfig extends PluginConfig {

    public final static String ENV_VARIABLE = "${env}";

    @JsonProperty private String swaggerUri;
    @JsonProperty private List<AdditionalRestUri> additionalUris = new ArrayList<>();

    public String getSwaggerUri() {
        return swaggerUri;
    }

    public void setSwaggerUri(String swaggerUri) {
        this.swaggerUri = swaggerUri;
    }

    public List<AdditionalRestUri> getAdditionalUris() {
        return additionalUris;
    }

    public void setAdditionalUris(List<AdditionalRestUri> additionalUris) {
        this.additionalUris = additionalUris;
    }
}
