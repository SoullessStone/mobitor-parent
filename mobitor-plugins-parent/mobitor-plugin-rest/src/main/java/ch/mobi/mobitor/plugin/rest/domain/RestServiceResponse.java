package ch.mobi.mobitor.plugin.rest.domain;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static ch.mobi.mobitor.plugin.rest.domain.ResponseInterpretation.FAILURE;
import static ch.mobi.mobitor.plugin.rest.domain.ResponseInterpretation.SUCCESS;

public class RestServiceResponse implements Serializable {

    private final String path;
    private final int statusCode;

    private long durationSlowLimit = 10_000;
    private final long durationMs;

    private final ResponseInterpretation interpretation;
    private final List<String> messages;

    private final Map<String, String> properties = new HashMap<>();

    public RestServiceResponse(String path, int statusCode, ResponseInterpretation interpretation, List<String> messages, long durationMs) {
        this.path = path;
        this.statusCode = statusCode;
        this.interpretation = interpretation;
        this.messages = messages;
        this.durationMs = durationMs;
    }

    public boolean isSuccess() {
        return RestInformationUtils.success(statusCode) && SUCCESS.equals(interpretation);
    }

    public int getStatusCode() {
        return statusCode;
    }

    public List<String> getMessages() {
        return messages;
    }

    public void addMessage(String message) {
        messages.add(message);
    }

    public long getDurationMs() {
        return durationMs;
    }

    public boolean durationTooSlow() {
        return durationMs > durationSlowLimit;
    }

    public long getDurationSlowLimit() {
        return durationSlowLimit;
    }

    public String getPath() {
        return path;
    }

    public void addProperty(String key, String value) {
        this.properties.put(key, value);
    }

    public String getProperty(String key) {
        return properties.get(key);
    }

    public static RestServiceResponse createSuccessForFastRestServiceResponse(String path, int statusCode, long durationMs) {
        RestServiceResponse restServiceResponse = new RestServiceResponse(path, statusCode, SUCCESS, new ArrayList<>(), durationMs);
        restServiceResponse.durationSlowLimit = 200;
        return restServiceResponse;
    }

    public static RestServiceResponse createSuccessRestServiceResponse(String path, int statusCode, long durationMs) {
        return new RestServiceResponse(path, statusCode, SUCCESS, new ArrayList<>(), durationMs);
    }

    public static RestServiceResponse createErrorRestServiceResponse(String path, int statusCode, String... messages) {
        List<String> messageList = new ArrayList<>();
        if (messages != null && messages.length > 0) {
            messageList.addAll(Arrays.asList(messages));
        }
        return new RestServiceResponse(path, statusCode, FAILURE, messageList, -1);
    }

}
