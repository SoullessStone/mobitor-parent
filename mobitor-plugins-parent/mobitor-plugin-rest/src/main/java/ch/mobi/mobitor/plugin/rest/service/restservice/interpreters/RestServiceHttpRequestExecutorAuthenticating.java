package ch.mobi.mobitor.plugin.rest.service.restservice.interpreters;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import org.apache.http.HttpResponse;
import org.apache.http.client.fluent.Executor;
import org.apache.http.client.fluent.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;

import static ch.mobi.mobitor.plugin.rest.service.restservice.RequestHeaderConstants.HEADER_X_USER_ID;

@Component
@Qualifier("authenticating")
public class RestServiceHttpRequestExecutorAuthenticating implements RestServiceHttpRequestExecutor {

    private final DefaultRestServiceHttpRequestConfiguration defaultRestServiceHttpRequestConfiguration;

    private final String username;

    @Autowired
    public RestServiceHttpRequestExecutorAuthenticating(DefaultRestServiceHttpRequestConfiguration defaultRestServiceHttpRequestConfiguration,
                                                        @Value("${restService.authentification.username}") String username) {
        this.defaultRestServiceHttpRequestConfiguration = defaultRestServiceHttpRequestConfiguration;
        this.username = username;
    }

    @Override
    public HttpResponse execute(String uri) throws IOException {
        Executor executor = Executor.newInstance();
        Request request = defaultRestServiceHttpRequestConfiguration.request(uri);
        request.addHeader(HEADER_X_USER_ID, username);
        return executor.execute(request).returnResponse();
    }

}
