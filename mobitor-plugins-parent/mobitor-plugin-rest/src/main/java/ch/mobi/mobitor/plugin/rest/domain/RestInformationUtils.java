package ch.mobi.mobitor.plugin.rest.domain;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.rest.domain.swagger.PathHttpMethods;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class RestInformationUtils {

    private static final Logger LOG = LoggerFactory.getLogger(RestInformationUtils.class);

    /**
     * @return a list of fully qualified rest paths that match the pathMatchingPredicate of the interpreter, do not contain curly braces (parameters in swagger.json)
     *         and are a normalized uri (no duplicate // and so on)
     */
    public static List<String> getMatchingPaths(String restBasePath, Predicate<String> pathMatchingPredicate, Map<String, PathHttpMethods> paths) {
        Predicate<Map.Entry<String, PathHttpMethods>> doesNotContainCurlyBraces = entry -> !entry.getKey().matches(".*\\{.*}.*");
        Predicate<Map.Entry<String, PathHttpMethods>> isGetResource = entry -> entry.getValue().supportsGet();

        // create map instead of "path" -> "pathHttpMethods" a map with a normalized full path: "http://fullpath"

        List<String> matchingPaths = paths.entrySet()
                                          .stream()
                                          .filter(isGetResource)
                                          .filter(doesNotContainCurlyBraces)
                                          .map(entry -> normalizeUri(restBasePath + entry.getKey()))
                                          .filter(pathMatchingPredicate)
                                          .filter(Objects::nonNull)
                                          .collect(Collectors.toList());

        LOG.debug(String.format("Found %s matching paths for predicate %s", matchingPaths.size(), pathMatchingPredicate.getClass().getName() ));
        return matchingPaths;
    }

    public static String normalizeUri(String uri) {
        try {
            String normalizedRestUri = new URI(uri).normalize().toString();
            return normalizedRestUri;

        } catch (URISyntaxException ex) {
            LOG.warn("Could no normalize url: " + uri);
            return null;
        }
    }

    public static boolean success(int statusCode) {
        return (statusCode > 199) && (statusCode < 300);
    }

}
