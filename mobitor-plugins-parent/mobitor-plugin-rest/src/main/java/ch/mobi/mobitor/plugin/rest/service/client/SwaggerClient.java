package ch.mobi.mobitor.plugin.rest.service.client;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.rest.RestPluginConfiguration;
import ch.mobi.mobitor.plugin.rest.domain.swagger.SwaggerResponse;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.client.fluent.Executor;
import org.apache.http.client.fluent.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.UUID;

import static ch.mobi.mobitor.plugin.rest.service.restservice.RequestHeaderConstants.HEADER_CALLER;
import static ch.mobi.mobitor.plugin.rest.service.restservice.RequestHeaderConstants.HEADER_CALLER_VALUE;
import static ch.mobi.mobitor.plugin.rest.service.restservice.RequestHeaderConstants.HEADER_CALLER_VERSION;
import static ch.mobi.mobitor.plugin.rest.service.restservice.RequestHeaderConstants.HEADER_CORR_ID;
import static ch.mobi.mobitor.plugin.rest.service.restservice.RequestHeaderConstants.HEADER_X_USER_ID;

@Component
public class SwaggerClient {

    private static final Logger LOG = LoggerFactory.getLogger(SwaggerClient.class);

    private final RestPluginConfiguration restPluginConfiguration;
    private final String username;

    @Autowired
    public SwaggerClient(RestPluginConfiguration restPluginConfiguration,
                        @Value("${restService.authentification.username}") String username) {
        this.restPluginConfiguration = restPluginConfiguration;
        this.username = username;
    }

    public SwaggerResponse retrieveSwaggerJson(String swaggerJsonUri) {

        // https://teamcity/httpAuth/app/rest/builds?locator=buildType:ConfigId,running:any&count=1
        //
        // avoid handshake failure due to mixed up hostnames: -Djsse.enableSNIExtension=false
        // TODO verify this is still an issue
        System.setProperty("jsse.enableSNIExtension", "false");

        try {
            LOG.debug("GET: " + swaggerJsonUri);
            URI swaggerUri = new URI(swaggerJsonUri);
            Executor executor = Executor.newInstance();
            String response = executor.execute(
                    Request.Get(swaggerUri)
                           .addHeader("accept", "application/json")
                           .addHeader(HEADER_CALLER, HEADER_CALLER_VALUE)
                           .addHeader(HEADER_CALLER_VERSION, restPluginConfiguration.getVersion())
                           .addHeader(HEADER_CORR_ID, String.valueOf(UUID.randomUUID()))
                           .addHeader(HEADER_X_USER_ID, username)
                           .connectTimeout(3000)
                           .socketTimeout(3000)
            )
            .returnContent()
            .asString();

            LOG.trace("JSON String: " + response);

            return createSwaggerResponse(response);

        } catch (IOException ex) {
            LOG.error("Could not retrieve swagger json from uri: " + swaggerJsonUri);
            LOG.error("Reason: " + ex.getMessage());
        } catch (URISyntaxException ex) {
            LOG.error("Could not parse swagger json uri: " + ex.getMessage());
        }

        return null;
    }

    private SwaggerResponse createSwaggerResponse(String jsonContent) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        // act
        return mapper.readValue(jsonContent, SwaggerResponse.class);
    }


    /**
     * @param swaggerJsonUrl full URL to the swagger.json ("https://app.host.domain/ovn/swagger.json")
     * @param swaggerBasePath the basePath from the swagger.json ("/ovn/rest")
     * @return the URL pointing to the /rest/ endpoints ("https://app.host.domain/ovn/rest") or null
     *         if there is a parsing issue
     */
    public String buildBaseUrl(String swaggerJsonUrl, String swaggerBasePath) {
        try {
            URI uri = new URI(swaggerJsonUrl);
            String host = uri.getHost();
            String scheme = uri.getScheme();

            String restBasePath = scheme + "://" + host + swaggerBasePath;
            return restBasePath;

        } catch (URISyntaxException e) {
            return null;
        }
    }
}
