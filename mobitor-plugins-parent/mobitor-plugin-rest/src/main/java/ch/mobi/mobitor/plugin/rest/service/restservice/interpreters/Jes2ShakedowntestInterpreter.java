package ch.mobi.mobitor.plugin.rest.service.restservice.interpreters;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.rest.domain.ResponseInterpretation;
import ch.mobi.mobitor.plugin.rest.domain.RestServiceResponse;
import ch.mobi.mobitor.plugin.rest.domain.shakedowntest.ShakedowntestResponse;
import ch.mobi.mobitor.plugin.rest.service.restservice.SwaggerEndpointInterpreter;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

import static ch.mobi.mobitor.plugin.rest.domain.ResponseInterpretation.FAILURE;
import static ch.mobi.mobitor.plugin.rest.service.RestPluginConstants.CACHE_NAME_REST_RESPONSES;

@Component
public class Jes2ShakedowntestInterpreter implements SwaggerEndpointInterpreter {

    private static final Logger LOG = LoggerFactory.getLogger(Jes2ShakedowntestInterpreter.class);

    private final RestServiceHttpRequestExecutor restServiceHttpRequestExecutor;

    @Autowired
    public Jes2ShakedowntestInterpreter(@Qualifier("authenticating") RestServiceHttpRequestExecutor restServiceHttpRequestExecutor) {
        this.restServiceHttpRequestExecutor = restServiceHttpRequestExecutor;
    }

    @Override
    public Predicate<String> getMatchPredicate() {
        Predicate<String> containsPathElement = path -> path.contains("/shakedowntest");
        return containsPathElement;
    }

    @Override
    @Cacheable(cacheNames = CACHE_NAME_REST_RESPONSES)
    public RestServiceResponse fetchResponse(String uri) {
        try {
            HttpResponse httpResponse = restServiceHttpRequestExecutor.execute(uri);
            int statusCode = httpResponse.getStatusLine().getStatusCode();
            String json = EntityUtils.toString(httpResponse.getEntity(), "UTF-8");
            ShakedowntestResponse shakedowntestResponse = createShakedowntestResponse(json);
            if (shakedowntestResponse == null) {
                throw new Exception("json not de-serialized");

            } else {
                ResponseInterpretation interpretation = ResponseInterpretation.SUCCESS;

                List<String> messages = new ArrayList<>();
                if (shakedowntestResponse.hasAnyErrors()) {
                    messages.addAll(shakedowntestResponse.getTechnicalMessages());
                    interpretation = FAILURE;
                }

                RestServiceResponse restServiceResponse = new RestServiceResponse(uri, statusCode, interpretation, messages, -1);
                return restServiceResponse;
            }



        } catch (Exception e) {
            LOG.error("Could not query uri: " + uri, e);

            List<String> messages = new ArrayList<>();
            messages.add("Shakedowntest-Response could not be interpreted: " + uri);
            messages.add(Arrays.toString(e.getStackTrace()));

            RestServiceResponse errorRestServiceResponse = new RestServiceResponse(uri, -10, FAILURE, messages, -1);
            return errorRestServiceResponse;
        }
    }

    private ShakedowntestResponse createShakedowntestResponse(String json) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        try {
            ShakedowntestResponse shakedowntestResponse = mapper.readValue(json, ShakedowntestResponse.class);
            return shakedowntestResponse;

        } catch (IOException e) {
            LOG.error("Could not read shakedowntest response", e);
            return null;
        }
    }

}
