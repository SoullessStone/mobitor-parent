package ch.mobi.mobitor.plugin.rest.service.configservice;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */


import ch.mobi.mobitor.config.EnvironmentConfigProperties;
import ch.mobi.mobitor.config.EnvironmentNetwork;
import ch.mobi.mobitor.config.EnvironmentPipeline;
import ch.mobi.mobitor.plugin.rest.RestPluginConfiguration;
import ch.mobi.mobitor.plugin.rest.config.environment.EnvironmentConfig;
import ch.mobi.mobitor.service.EnvironmentsConfigurationService;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.TestOnly;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.InputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static ch.mobi.mobitor.config.EnvironmentNetwork.CI;
import static ch.mobi.mobitor.config.EnvironmentNetwork.DEVELOPMENT;
import static ch.mobi.mobitor.config.EnvironmentNetwork.LOCALHOST;
import static ch.mobi.mobitor.config.EnvironmentNetwork.PREPROD;
import static ch.mobi.mobitor.config.EnvironmentNetwork.PRODUCTION;


@Service
@ConditionalOnMissingBean(EnvironmentsConfigurationService.class)
public class DefaultEnvironmentsConfigurationService implements EnvironmentsConfigurationService {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultEnvironmentsConfigurationService.class);

    private final ResourceLoader resourceLoader;
    private final RestPluginConfiguration restPluginConfiguration;

    private List<EnvironmentConfig> environments;
    private Map<String, EnvironmentConfig> envConfigMap = new HashMap<>();

    @Autowired
    public DefaultEnvironmentsConfigurationService(ResourceLoader resourceLoader, RestPluginConfiguration restPluginConfiguration) {
        this.resourceLoader = resourceLoader;
        this.restPluginConfiguration = restPluginConfiguration;
    }

    @PostConstruct
    public void initializeEnvironments() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        Resource resource = resourceLoader.getResource("classpath:mobitor/plugin/rest/environment-mappings.json");
        try (InputStream inputStream = resource.getInputStream()) {
            this.environments = mapper.readValue(inputStream, mapper.getTypeFactory().constructCollectionType(List.class, EnvironmentConfig.class));

            populateEnvironmentConfigs();

        } catch (Exception e) {
            LOG.error("Could not initialize application servers.", e);
        }
    }

    private void populateEnvironmentConfigs() {
        for (EnvironmentConfig environment : this.environments) {
            this.envConfigMap.put(environment.getEnvironment(), environment);
        }
    }

    @TestOnly
    public void updateEnvironmentConfigs(List<EnvironmentConfig> configs) {
        this.environments = configs;
        populateEnvironmentConfigs();
    }

    public List<EnvironmentConfigProperties> getEnvironments() {
        return Collections.unmodifiableList(this.environments);
    }

    public EnvironmentConfig getEnvironmentConfig(String environment) {
        return this.envConfigMap.get(environment);
    }

    public boolean isEnvironmentReachable(String targetEnvironment) {
        EnvironmentConfig targetEnvConfig = getEnvironmentConfig(targetEnvironment);
        EnvironmentNetwork targetNetwork = targetEnvConfig.getNetwork();

        EnvironmentNetwork currentNetwork = restPluginConfiguration.getNetwork();

        if (LOCALHOST.equals(currentNetwork)) {
            return targetEnvConfig.isReachableFromLocalhost();
        }
        return isNetworkReachable(targetNetwork);
    }

    @Override
    public boolean isNetworkReachable(EnvironmentNetwork targetNetwork) {
        EnvironmentNetwork currentNetwork = restPluginConfiguration.getNetwork();

        if(CI.equals(targetNetwork)) {
            return false;
        } else if (currentNetwork.equals(targetNetwork)) {
            return true;
        } else if (LOCALHOST.equals(currentNetwork) && DEVELOPMENT.equals(targetNetwork)) {
            return true;
        } else if (PREPROD.equals(currentNetwork) && (DEVELOPMENT.equals(targetNetwork) || LOCALHOST.equals(targetNetwork))) {
            return true;
        } else return PRODUCTION.equals(currentNetwork);

    }

    @Override
    public EnvironmentNetwork getCurrentNetwork() {
        return restPluginConfiguration.getNetwork();
    }

    public String getName(String env) {
        return this.envConfigMap.get(env).getName();
    }

    @Override
    public boolean isNetworkReachable(String environment) {
        EnvironmentConfig environmentConfig = getEnvironmentConfig(environment);
        return this.isNetworkReachable(environmentConfig.getNetwork());
    }

    @Override
    public EnvironmentPipeline getPipeline(String environment) {
        return getEnvironmentConfig(environment).getPipeline();
    }
}
