package ch.mobi.mobitor.plugin.rest;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.rest.config.AdditionalRestUri;
import ch.mobi.mobitor.plugin.rest.config.RestServiceConfig;
import ch.mobi.mobitor.plugin.rest.domain.RestCallAdditionalUri;
import ch.mobi.mobitor.plugin.rest.domain.RestCallInformation;
import ch.mobi.mobitor.plugin.rest.service.configservice.RestServiceByReachableEnvironmentFilter;
import ch.mobi.mobitor.plugin.rest.service.configservice.RestServiceFixUrlByEnvironmentProcessor;
import ch.mobi.mobitor.plugins.api.MobitorPlugin;
import ch.mobi.mobitor.plugins.api.domain.config.ExtendableScreenConfig;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformationLegendWrapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static ch.mobi.mobitor.plugin.rest.ScreenConfigRestServiceHelper.getParsedRestServiceConfigs;

@Component
@ConditionalOnProperty(name = "mobitor.plugins.restServices.enabled", havingValue = "true")
public class RestResourcePlugin implements MobitorPlugin<RestServiceConfig> {

    private static final Logger LOG = LoggerFactory.getLogger(RestResourcePlugin.class);

    private final RestServiceByReachableEnvironmentFilter restServiceEnvFilter;
    private final RestServiceFixUrlByEnvironmentProcessor restServiceUrlProcessor;

    @Autowired
    public RestResourcePlugin(RestServiceByReachableEnvironmentFilter restServiceEnvFilter,
                              RestServiceFixUrlByEnvironmentProcessor restServiceUrlProcessor) {
        this.restServiceEnvFilter = restServiceEnvFilter;
        this.restServiceUrlProcessor = restServiceUrlProcessor;
    }

    @Override
    public String getConfigPropertyName() {
        return "restServices";
    }

    @Override
    public Class<RestServiceConfig> getConfigClass() {
        return RestServiceConfig.class;
    }

    @Override
    public void createAndAssociateApplicationInformationBlocks(Screen screen, ExtendableScreenConfig screenConfig, List<RestServiceConfig> configs) {
        List<String> environments = screenConfig.getEnvironments();
        List<RestServiceConfig> restServiceConfigs = getParsedRestServiceConfigs(environments, configs);

        // filter by environment
        List<RestServiceConfig> filteredRestServices = restServiceConfigs.stream().filter(restServiceEnvFilter).collect(Collectors.toList());

        LOG.info("For screenConfig: " + screenConfig.getConfigKey() + " there where " + restServiceConfigs.size() + " rest services configured.");
        LOG.info("For screenConfig: " + screenConfig.getConfigKey() + " there are " + filteredRestServices.size() + " reachable rest services.");

        // fix urls
        filteredRestServices.forEach(restServiceUrlProcessor::replaceUrlPerEnvironment);

        for (RestServiceConfig restServiceConfig : filteredRestServices) {
            String serverName = restServiceConfig.getServerName();
            String applicationName = restServiceConfig.getApplicationName();
            String environment = restServiceConfig.getEnvironment();
            String swaggerUri = restServiceConfig.getSwaggerUri();

            RestCallInformation restCallInformation = new RestCallInformation();
            restCallInformation.setSwaggerUri(swaggerUri);
            restCallInformation.setServerName(serverName);
            restCallInformation.setEnvironment(environment);

            List<AdditionalRestUri> additionalConfigUris = restServiceConfig.getAdditionalUris();
            List<RestCallAdditionalUri> additionalUris = new ArrayList<>();
            for (AdditionalRestUri addConfigUri : additionalConfigUris) {
                RestCallAdditionalUri additionalUri = new RestCallAdditionalUri(addConfigUri.getUri(), addConfigUri.getValidationRegex());
                additionalUris.add(additionalUri);
            }
            restCallInformation.setAdditionalUris(additionalUris);

            screen.addInformation(serverName, applicationName, environment, restCallInformation);
        }

    }

    @Override
    public List<ApplicationInformationLegendWrapper> getLegendApplicationInformationList() {
        return new RestResourceLegendGenerator().getLegendList();
    }

}
