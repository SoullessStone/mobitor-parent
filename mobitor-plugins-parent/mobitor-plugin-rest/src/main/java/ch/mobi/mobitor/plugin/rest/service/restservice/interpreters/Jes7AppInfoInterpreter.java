package ch.mobi.mobitor.plugin.rest.service.restservice.interpreters;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.rest.domain.RestServiceResponse;
import ch.mobi.mobitor.plugin.rest.domain.appinfo.AppInfoResponse;
import ch.mobi.mobitor.plugin.rest.service.restservice.SwaggerEndpointInterpreter;
import ch.mobi.mobitor.plugin.rest.service.restservice.TkNameIdExtractor;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import static ch.mobi.mobitor.plugin.rest.domain.ResponseInterpretation.SUCCESS;
import static ch.mobi.mobitor.plugin.rest.service.RestPluginConstants.CACHE_NAME_REST_RESPONSES;

@Component
public class Jes7AppInfoInterpreter implements SwaggerEndpointInterpreter, TkNameIdExtractor {

    private static final Logger LOG = LoggerFactory.getLogger(Jes7AppInfoInterpreter.class);

    private final RestServiceHttpRequestExecutor restServiceHttpRequestExecutor;

    @Autowired
    public Jes7AppInfoInterpreter(@Qualifier("authenticating") RestServiceHttpRequestExecutor restServiceHttpRequestExecutor) {
        this.restServiceHttpRequestExecutor = restServiceHttpRequestExecutor;
    }

    @Override
    public Predicate<String> getMatchPredicate() {
        Predicate<String> containsPathElement = path -> path.contains("/jap/appinfo");
        return containsPathElement;
    }

    @Override
    @Cacheable(cacheNames = CACHE_NAME_REST_RESPONSES)
    public RestServiceResponse fetchResponse(String uri) {
        try {
            HttpResponse httpResponse = restServiceHttpRequestExecutor.execute(uri);
            int statusCode = httpResponse.getStatusLine().getStatusCode();
            String json = EntityUtils.toString(httpResponse.getEntity(), "UTF-8");
            AppInfoResponse appInfoResponse = createAppInfoResponse(json);
            if (appInfoResponse == null) {
                throw new Exception("could not de-serialize appinfo");

            } else {
                List<String> messages = new ArrayList<>();
                String tkNameId = appInfoResponse.getTkNameId();

                RestServiceResponse restServiceResponse = new RestServiceResponse(uri, statusCode, SUCCESS, messages, -1);
                restServiceResponse.addProperty("tkNameId", tkNameId);
                restServiceResponse.addProperty("stackName", appInfoResponse.getStackName());
                restServiceResponse.addProperty("stackVersion", appInfoResponse.getStackVersion());

                return restServiceResponse;
            }

        } catch (Exception e) {
            String actMsg = "Could not query: " + uri;
            String exMsg = ExceptionUtils.getStackTrace(e);
            LOG.error(actMsg);
            return RestServiceResponse.createErrorRestServiceResponse(uri, -20, actMsg, exMsg);
        }
    }

    private AppInfoResponse createAppInfoResponse(String json) {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        try {
            AppInfoResponse appInfoResponse = mapper.readValue(json, AppInfoResponse.class);
            return appInfoResponse;

        } catch (Exception e) {
            return null;
        }
    }
}
