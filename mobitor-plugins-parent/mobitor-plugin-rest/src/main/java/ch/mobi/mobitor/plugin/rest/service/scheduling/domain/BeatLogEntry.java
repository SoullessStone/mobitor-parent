package ch.mobi.mobitor.plugin.rest.service.scheduling.domain;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */


import com.fasterxml.jackson.annotation.JsonProperty;

public class BeatLogEntry {

    @JsonProperty private String beatType;
    @JsonProperty private String serverName;
    @JsonProperty private String status;
    @JsonProperty private String stage;
    @JsonProperty private String eventTime;
    @JsonProperty private double errorCount;
    @JsonProperty private String healthStatusType;
    private String tkNameId;

    public String getBeatType() {
        return beatType;
    }

    public void setBeatType(String beatType) {
        this.beatType = beatType;
    }

    public String getServerName() {
        return serverName;
    }

    public void setServerName(String serverName) {
        this.serverName = serverName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStage() {
        return stage;
    }

    public void setStage(String stage) {
        this.stage = stage;
    }

    public String getEventTime() {
        return eventTime;
    }

    public void setEventTime(String eventTime) {
        this.eventTime = eventTime;
    }

    public void setErrorCount(double errorCount) {
        this.errorCount = errorCount;
    }

    public double getErrorCount() {
        return errorCount;
    }

    public void setHealthStatusType(String healthStatusType) {
        this.healthStatusType = healthStatusType;
    }

    public String getHealthStatusType() {
        return healthStatusType;
    }

    public void setTkNameId(String tkNameId) {
        this.tkNameId = tkNameId;
    }

    public String getTkNameId() {
        return tkNameId;
    }
}
