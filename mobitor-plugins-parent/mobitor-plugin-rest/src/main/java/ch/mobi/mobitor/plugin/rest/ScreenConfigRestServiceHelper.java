package ch.mobi.mobitor.plugin.rest;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.rest.config.AdditionalRestUri;
import ch.mobi.mobitor.plugin.rest.config.RestServiceConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

import static ch.mobi.mobitor.plugin.rest.config.RestServiceConfig.ENV_VARIABLE;

public class ScreenConfigRestServiceHelper {

    private static final Logger LOG = LoggerFactory.getLogger(ScreenConfigRestServiceHelper.class);

    public static List<RestServiceConfig> getParsedRestServiceConfigs(List<String> environments, List<RestServiceConfig> restServiceConfigs) {
        List<RestServiceConfig> restConfigs = new ArrayList<>();

        for (RestServiceConfig restServiceConfig : restServiceConfigs) {
            if (isEmpty(restServiceConfig.getEnvironment())) {
                // environment is empty, create on config per environments replacing the placeholder, if it exists or not
                String swaggerUrl = restServiceConfig.getSwaggerUri();
                for (String environment : environments) {
                    if(isNotEmpty(restServiceConfig.getEnvironment())) {
                        LOG.warn("The REST configuration for: " + swaggerUrl + " has a placeholder but also an environment. ignoring environment.");
                    }

                    RestServiceConfig confPerEnv = new RestServiceConfig();
                    confPerEnv.setEnvironment(environment);

                    confPerEnv.setServerName(restServiceConfig.getServerName());
                    confPerEnv.setApplicationName(restServiceConfig.getApplicationName());

                    String swaggerUrlPerEnv = swaggerUrl != null ? swaggerUrl.replace(ENV_VARIABLE, environment.toLowerCase()) : null;
                    confPerEnv.setSwaggerUri(swaggerUrlPerEnv);

                    // copy / replace additionalUris:
                    if (isNotEmpty(restServiceConfig.getAdditionalUris())) {
                        List<AdditionalRestUri> addUrisPerEnv = new ArrayList<>();
                        restServiceConfig.getAdditionalUris().forEach(addUri -> {
                            AdditionalRestUri addUriPerEnv = new AdditionalRestUri();
                            addUriPerEnv.setUri(addUri.getUri().replace(ENV_VARIABLE, environment.toLowerCase()));
                            addUriPerEnv.setValidationRegex(addUri.getValidationRegex());

                            addUrisPerEnv.add(addUriPerEnv);
                        });

                        confPerEnv.setAdditionalUris(addUrisPerEnv);
                    }

                    restConfigs.add(confPerEnv);
                }

            } else {
                // environment variable is set, use this config:
                restConfigs.add(restServiceConfig);
            }
        }
        return restConfigs;
    }

    private static boolean isNotEmpty(List<AdditionalRestUri> additionalUris) {
        return additionalUris != null && additionalUris.size() > 0;
    }

    private static boolean isNotEmpty(String environment) {
        return !isEmpty(environment);
    }

    private static boolean isEmpty(String environment) {
        return environment == null || "".equals(environment.trim());
    }

}
