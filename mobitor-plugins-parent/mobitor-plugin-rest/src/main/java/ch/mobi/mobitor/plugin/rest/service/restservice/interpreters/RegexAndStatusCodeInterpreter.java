package ch.mobi.mobitor.plugin.rest.service.restservice.interpreters;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.rest.domain.ResponseInterpretation;
import ch.mobi.mobitor.plugin.rest.domain.RestServiceResponse;
import ch.mobi.mobitor.plugin.rest.service.restservice.AdditionalEndpointInterpreter;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import static ch.mobi.mobitor.plugin.rest.domain.ResponseInterpretation.FAILURE;
import static ch.mobi.mobitor.plugin.rest.domain.ResponseInterpretation.SUCCESS;
import static ch.mobi.mobitor.plugin.rest.domain.RestInformationUtils.success;
import static ch.mobi.mobitor.plugin.rest.domain.RestServiceResponse.createErrorRestServiceResponse;
import static ch.mobi.mobitor.plugin.rest.service.RestPluginConstants.CACHE_NAME_REST_RESPONSES;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

@Component
public class RegexAndStatusCodeInterpreter implements AdditionalEndpointInterpreter {

    private static final Logger LOG = LoggerFactory.getLogger(RegexAndStatusCodeInterpreter.class);

    private final RestServiceHttpRequestExecutor restServiceHttpRequestExecutor;

    @Autowired
    public RegexAndStatusCodeInterpreter(@Qualifier("anonymous") RestServiceHttpRequestExecutor restServiceHttpRequestExecutor) {
        this.restServiceHttpRequestExecutor = restServiceHttpRequestExecutor;
    }

    @Override
    @Cacheable(cacheNames = CACHE_NAME_REST_RESPONSES, key = "#uri")
    public RestServiceResponse fetchResponse(String uri, String validationRegex) {
        try {
            List<String> messages = new ArrayList<>();

            HttpResponse httpResponse = restServiceHttpRequestExecutor.execute(uri);
            int statusCode = httpResponse.getStatusLine().getStatusCode();
            String responseString = EntityUtils.toString(httpResponse.getEntity());

            boolean patternMatches = true;
            if (isNotEmpty(validationRegex)) {
                patternMatches = Pattern.compile(validationRegex).matcher(responseString).find();
                if (!patternMatches) {
                    messages.add("validation regex does not match: " + validationRegex + "; response: " + responseString);
                }
            }

            ResponseInterpretation interpretation = (success(statusCode) && patternMatches) ? SUCCESS : FAILURE;

            RestServiceResponse restServiceResponse = new RestServiceResponse(uri, statusCode, interpretation,  messages, -1);
            return restServiceResponse;

        } catch (Exception e) {
            String actMsg = "Could not query: " + uri;
            String exMsg = ExceptionUtils.getStackTrace(e);
            LOG.error(actMsg, e);
            return createErrorRestServiceResponse(uri, -90, actMsg, exMsg);
        }
    }

}
