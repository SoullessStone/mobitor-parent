package ch.mobi.mobitor.plugin.rest.config.environment;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.config.ConfigDomainMapping;
import ch.mobi.mobitor.config.EnvironmentConfigProperties;
import ch.mobi.mobitor.config.EnvironmentNetwork;
import ch.mobi.mobitor.config.EnvironmentPipeline;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class EnvironmentConfig implements EnvironmentConfigProperties {

    @JsonProperty private String environment;
    @JsonProperty private boolean buildEnvironment = false;
    @JsonProperty private String name;
    @JsonProperty private String label;
    @JsonProperty private String description;
    @JsonProperty private String descriptionUrl;
    @JsonProperty private EnvironmentNetwork network;
    @JsonProperty private boolean reachableFromLocalhost = false;
    @JsonProperty private List<ConfigDomainMapping> configDomainMappings = new ArrayList<>();
    @JsonProperty private EnvironmentPipeline pipeline;
    @JsonProperty private String timestampUrl;

    @JsonCreator
    public EnvironmentConfig() {
    }

    public String getEnvironment() {
        return environment;
    }

    public void setEnvironment(String environment) {
        this.environment = environment;
    }

    public boolean isBuildEnvironment() {
        return buildEnvironment;
    }

    public void setBuildEnvironment(boolean buildEnvironment) {
        this.buildEnvironment = buildEnvironment;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionUrl() {
        return descriptionUrl;
    }

    public void setDescriptionUrl(String descriptionUrl) {
        this.descriptionUrl = descriptionUrl;
    }

    public EnvironmentNetwork getNetwork() {
        return network;
    }

    public void setNetwork(EnvironmentNetwork network) {
        this.network = network;
    }

    public boolean isReachableFromLocalhost() {
        return reachableFromLocalhost;
    }

    public void setReachableFromLocalhost(boolean reachableFromLocalhost) {
        this.reachableFromLocalhost = reachableFromLocalhost;
    }

    public EnvironmentPipeline getPipeline() {
        return pipeline;
    }

    public void setPipeline(EnvironmentPipeline pipeline) {
        this.pipeline = pipeline;
    }

    public String getTimestampUrl() {
        return timestampUrl;
    }

    public void setTimestampUrl(String timestampUrl) {
        this.timestampUrl = timestampUrl;
    }

    public List<ConfigDomainMapping> getConfigDomainMappings() {
        return configDomainMappings;
    }

    public void setConfigDomainMappings(List<ConfigDomainMapping> configDomainMappings) {
        this.configDomainMappings = configDomainMappings;
    }
}
