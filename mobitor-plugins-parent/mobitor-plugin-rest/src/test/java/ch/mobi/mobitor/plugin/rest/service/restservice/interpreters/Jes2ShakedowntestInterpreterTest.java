package ch.mobi.mobitor.plugin.rest.service.restservice.interpreters;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.rest.domain.RestServiceResponse;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;

public class Jes2ShakedowntestInterpreterTest extends SwaggerEndpointInterpreterTest {

    private Jes2ShakedowntestInterpreter createInterpreterWithResponseFromFile(String file, int statusCode) {
        RestServiceHttpRequestExecutor executor = createExecutorWithResponseFromFile(file, statusCode);
        return new Jes2ShakedowntestInterpreter(executor);
    }

    private Jes2ShakedowntestInterpreter createInterpreterWithErrorResponse(String json) {
        MockRestServiceHttpRequestExecutor executor = new MockRestServiceHttpRequestExecutor(json, 505);
        return new Jes2ShakedowntestInterpreter(executor);
    }

    @Test
    public void testInterpretSuccessRequestButShakedownError() {
        Jes2ShakedowntestInterpreter interpreter = createInterpreterWithResponseFromFile("__files/shakedowntest-response-error.json", 200);

        RestServiceResponse restServiceResponse = interpreter.fetchResponse("path");

        assertThat(restServiceResponse, is(not(nullValue())));
        assertThat(restServiceResponse.isSuccess(), is(false));
        assertThat(restServiceResponse.getMessages(), hasSize(3));
    }

    @Override
    public void testInterpretSuccess() {
        Jes2ShakedowntestInterpreter interpreter = createInterpreterWithResponseFromFile("interpreter/jes2_shakedowntest.json", 200);

        RestServiceResponse restServiceResponse = interpreter.fetchResponse("path");

        assertThat(restServiceResponse, is(not(nullValue())));
        assertThat(restServiceResponse.isSuccess(), is(true));
        assertThat(restServiceResponse.getMessages(), hasSize(0));
    }

    @Override
    public void testInterpretFailure() {
        Jes2ShakedowntestInterpreter interpreter = createInterpreterWithErrorResponse("");
        RestServiceResponse restServiceResponse = interpreter.fetchResponse("path");

        assertThat(restServiceResponse, is(not(nullValue())));
        assertThat(restServiceResponse.isSuccess(), is(false));
        assertThat(restServiceResponse.getMessages(), is(not(empty())));
    }

    @Override
    public void testMatchPredicate() {
        Jes2ShakedowntestInterpreter interpreter = new Jes2ShakedowntestInterpreter(null);
        testMatchPredicateMatchesNumber(interpreter, 1);
    }

}

