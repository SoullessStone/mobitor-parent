package ch.mobi.mobitor.plugin.rest.service.configservice;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.config.EnvironmentConfigProperties;
import ch.mobi.mobitor.config.EnvironmentNetwork;
import ch.mobi.mobitor.plugin.rest.RestPluginConfiguration;
import ch.mobi.mobitor.config.ConfigDomainMapping;
import ch.mobi.mobitor.plugin.rest.service.configservice.DefaultEnvironmentsConfigurationService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.core.io.DefaultResourceLoader;

import java.util.List;

import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.assertThat;

public class DefaultEnvironmentsConfigurationServiceTest {

    private DefaultEnvironmentsConfigurationService environmentsConfigurationService;
    private RestPluginConfiguration restPluginConfiguration;

    @Before
    public void setup() {
        restPluginConfiguration = new RestPluginConfiguration();
        environmentsConfigurationService = new DefaultEnvironmentsConfigurationService(new DefaultResourceLoader(), restPluginConfiguration);

        // act
        environmentsConfigurationService.initializeEnvironments();
    }

    @Test
    public void appServersAreNotEmpty() {
        // assert
        List<EnvironmentConfigProperties> environmentConfigs = environmentsConfigurationService.getEnvironments();
        assertThat(environmentConfigs, is(not(empty())));
    }

    @Test
    public void testEnvironmentNetworkIsNeverNull() {
        List<EnvironmentConfigProperties> environmentConfigs = environmentsConfigurationService.getEnvironments();
        environmentConfigs.forEach(config -> assertThat(config.getNetwork(), is(not(nullValue()))));
    }

    @Test
    public void testConfigDomainMappingsAreNeverNullForNonBuildEnvironments() {
        List<EnvironmentConfigProperties> environmentConfigs = environmentsConfigurationService.getEnvironments();

        environmentConfigs.stream().filter(config -> !config.isBuildEnvironment()).forEach(config -> {

            List<ConfigDomainMapping> configDomainMappings = config.getConfigDomainMappings();

            assertThat(configDomainMappings, is(not(nullValue())));
            assertThat(configDomainMappings, is(not(empty())));
        });
    }

    @Test
    public void testNamesAreNeverNullAndEndWithEnvLetter() {
        List<EnvironmentConfigProperties> environmentConfigs = environmentsConfigurationService.getEnvironments();
        environmentConfigs.forEach(config -> assertThat(config.getEnvironment(), is(not(nullValue()))));
        environmentConfigs.forEach(config -> assertThat(config.getName(), is(not(nullValue()))));
        environmentConfigs.forEach(config -> assertThat(config.getEnvironment(), config.getName().endsWith(config.getEnvironment().toLowerCase()), is(true)));
    }

    @Test
    public void testIsEnvironmentReachableWhenInLocalhost() {
        restPluginConfiguration.setNetwork(EnvironmentNetwork.LOCALHOST);

        assertThat(environmentsConfigurationService.isEnvironmentReachable("build"), is(false));

        assertThat(environmentsConfigurationService.isEnvironmentReachable("Y"), is(true)); // for local development, to see some rest calls
        assertThat(environmentsConfigurationService.isEnvironmentReachable("W"), is(false));
        assertThat(environmentsConfigurationService.isEnvironmentReachable("V"), is(false));

        assertThat(environmentsConfigurationService.isEnvironmentReachable("I"), is(false));
        assertThat(environmentsConfigurationService.isEnvironmentReachable("T"), is(false));

        assertThat(environmentsConfigurationService.isEnvironmentReachable("P"), is(false));
    }

    @Test
    public void testIsEnvironmentReachableWhenInCi() {
        restPluginConfiguration.setNetwork(EnvironmentNetwork.CI);

        assertThat(environmentsConfigurationService.isEnvironmentReachable("build"), is(false));

        assertThat(environmentsConfigurationService.isEnvironmentReachable("Y"), is(false));
        assertThat(environmentsConfigurationService.isEnvironmentReachable("W"), is(false));
        assertThat(environmentsConfigurationService.isEnvironmentReachable("V"), is(false));

        assertThat(environmentsConfigurationService.isEnvironmentReachable("I"), is(false));
        assertThat(environmentsConfigurationService.isEnvironmentReachable("T"), is(false));

        assertThat(environmentsConfigurationService.isEnvironmentReachable("P"), is(false));
    }

    @Test
    public void testIsEnvironmentReachableWhenInDevelopment() {
        restPluginConfiguration.setNetwork(EnvironmentNetwork.DEVELOPMENT);

        assertThat(environmentsConfigurationService.isEnvironmentReachable("build"), is(false));

        assertThat(environmentsConfigurationService.isEnvironmentReachable("W"), is(true));
        assertThat(environmentsConfigurationService.isEnvironmentReachable("Y"), is(true));
        assertThat(environmentsConfigurationService.isEnvironmentReachable("V"), is(true));

        assertThat(environmentsConfigurationService.isEnvironmentReachable("I"), is(false));
        assertThat(environmentsConfigurationService.isEnvironmentReachable("T"), is(false));

        assertThat(environmentsConfigurationService.isEnvironmentReachable("P"), is(false));
    }

    @Test
    public void testIsEnvironmentReachableWhenInPreProd() {
        restPluginConfiguration.setNetwork(EnvironmentNetwork.PREPROD);

        assertThat(environmentsConfigurationService.isEnvironmentReachable("build"), is(false));

        assertThat(environmentsConfigurationService.isEnvironmentReachable("V"), is(true));
        assertThat(environmentsConfigurationService.isEnvironmentReachable("W"), is(true));
        assertThat(environmentsConfigurationService.isEnvironmentReachable("Y"), is(true));

        assertThat(environmentsConfigurationService.isEnvironmentReachable("I"), is(true));
        assertThat(environmentsConfigurationService.isEnvironmentReachable("T"), is(true));

        assertThat(environmentsConfigurationService.isEnvironmentReachable("P"), is(false));
    }

    @Test
    public void testIsEnvironmentReachableWhenInProduction() {
        restPluginConfiguration.setNetwork(EnvironmentNetwork.PRODUCTION);

        assertThat(environmentsConfigurationService.isEnvironmentReachable("build"), is(false));

        assertThat(environmentsConfigurationService.isEnvironmentReachable("V"), is(true));
        assertThat(environmentsConfigurationService.isEnvironmentReachable("W"), is(true));
        assertThat(environmentsConfigurationService.isEnvironmentReachable("Y"), is(true));

        assertThat(environmentsConfigurationService.isEnvironmentReachable("I"), is(true));
        assertThat(environmentsConfigurationService.isEnvironmentReachable("T"), is(true));

        assertThat(environmentsConfigurationService.isEnvironmentReachable("P"), is(true));
    }

    @Test
    public void testIsNetworkReachableWhenInLocalhost() {
        restPluginConfiguration.setNetwork(EnvironmentNetwork.LOCALHOST);

        assertThat(environmentsConfigurationService.isNetworkReachable(EnvironmentNetwork.CI), is(false));
        assertThat(environmentsConfigurationService.isNetworkReachable(EnvironmentNetwork.DEVELOPMENT), is(true));
        assertThat(environmentsConfigurationService.isNetworkReachable(EnvironmentNetwork.PREPROD), is(false));
        assertThat(environmentsConfigurationService.isNetworkReachable(EnvironmentNetwork.PRODUCTION), is(false));
    }

    @Test
    public void testIsNetworkReachableWhenInCi() {
        restPluginConfiguration.setNetwork(EnvironmentNetwork.CI);

        assertThat(environmentsConfigurationService.isNetworkReachable(EnvironmentNetwork.CI), is(false));
        assertThat(environmentsConfigurationService.isNetworkReachable(EnvironmentNetwork.DEVELOPMENT), is(false));
        assertThat(environmentsConfigurationService.isNetworkReachable(EnvironmentNetwork.PREPROD), is(false));
        assertThat(environmentsConfigurationService.isNetworkReachable(EnvironmentNetwork.PRODUCTION), is(false));
    }

    @Test
    public void testIsNetworkReachableWhenInDevelopment() {
        restPluginConfiguration.setNetwork(EnvironmentNetwork.DEVELOPMENT);

        assertThat(environmentsConfigurationService.isNetworkReachable(EnvironmentNetwork.CI), is(false));
        assertThat(environmentsConfigurationService.isNetworkReachable(EnvironmentNetwork.DEVELOPMENT), is(true));
        assertThat(environmentsConfigurationService.isNetworkReachable(EnvironmentNetwork.PREPROD), is(false));
        assertThat(environmentsConfigurationService.isNetworkReachable(EnvironmentNetwork.PRODUCTION), is(false));
    }

    @Test
    public void testIsNetworkReachableWhenInPreProd() {
        restPluginConfiguration.setNetwork(EnvironmentNetwork.PREPROD);

        assertThat(environmentsConfigurationService.isNetworkReachable(EnvironmentNetwork.CI), is(false));
        assertThat(environmentsConfigurationService.isNetworkReachable(EnvironmentNetwork.DEVELOPMENT), is(true));
        assertThat(environmentsConfigurationService.isNetworkReachable(EnvironmentNetwork.PREPROD), is(true));
        assertThat(environmentsConfigurationService.isNetworkReachable(EnvironmentNetwork.PRODUCTION), is(false));
    }
    @Test
    public void testIsNetworkReachableWhenInProduction() {
        restPluginConfiguration.setNetwork(EnvironmentNetwork.PRODUCTION);

        assertThat(environmentsConfigurationService.isNetworkReachable(EnvironmentNetwork.CI), is(false));
        assertThat(environmentsConfigurationService.isNetworkReachable(EnvironmentNetwork.DEVELOPMENT), is(true));
        assertThat(environmentsConfigurationService.isNetworkReachable(EnvironmentNetwork.PREPROD), is(true));
        assertThat(environmentsConfigurationService.isNetworkReachable(EnvironmentNetwork.PRODUCTION), is(true));
    }

    @Test
    public void testEnvPipelineIsNotEmpty() {
        environmentsConfigurationService.getEnvironments().forEach(env -> assertThat(env.getPipeline(), is(not(nullValue()))));
    }

}
