package ch.mobi.mobitor.plugin.rest.service.configservice;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.rest.config.AdditionalRestUri;
import ch.mobi.mobitor.plugin.rest.config.RestServiceConfig;
import org.junit.Test;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class RestServiceFixUrlByEnvironmentProcessorTest {

    @Test
    public void replaceUrlPerEnvironmentShouldUpdateAllUrls() {
        //arrange
        String newUrl = "http://new.url";
        LoadBalancerFilterService loadBalancerFilterService = mock(LoadBalancerFilterService.class);
        when(loadBalancerFilterService.filterUri(any(), any())).thenReturn(newUrl);
        RestServiceFixUrlByEnvironmentProcessor sut = new RestServiceFixUrlByEnvironmentProcessor(loadBalancerFilterService);
        RestServiceConfig config = new RestServiceConfig();
        config.setSwaggerUri("::1");
        AdditionalRestUri additionalRestUri1 = new AdditionalRestUri();
        additionalRestUri1.setUri("http://gibts.net");
        AdditionalRestUri additionalRestUri2 = new AdditionalRestUri();
        additionalRestUri2.setUri("https://url.ch");
        config.setAdditionalUris(asList(additionalRestUri1, additionalRestUri2));

        //act
        sut.replaceUrlPerEnvironment(config);

        //assert
        assertThat(config.getSwaggerUri()).isEqualTo(newUrl);
        assertThat(config.getAdditionalUris()).hasSize(2)
                                              .extracting(AdditionalRestUri::getUri)
                                              .containsExactly(newUrl, newUrl);
    }

}
