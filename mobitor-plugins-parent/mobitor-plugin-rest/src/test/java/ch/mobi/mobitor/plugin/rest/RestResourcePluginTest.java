package ch.mobi.mobitor.plugin.rest;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.rest.config.RestServiceConfig;
import ch.mobi.mobitor.plugin.rest.domain.RestCallInformation;
import ch.mobi.mobitor.plugin.rest.service.configservice.RestServiceByReachableEnvironmentFilter;
import ch.mobi.mobitor.plugin.rest.service.configservice.RestServiceFixUrlByEnvironmentProcessor;
import ch.mobi.mobitor.plugin.test.PluginTest;
import ch.mobi.mobitor.plugins.api.domain.config.ExtendableScreenConfig;
import ch.mobi.mobitor.service.EnvironmentsConfigurationService;
import org.junit.Test;

import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class RestResourcePluginTest extends PluginTest<RestResourcePlugin, RestServiceConfig, RestCallInformation> {

    @Override
    public String getConfigPropertyName() {
        return "restServices";
    }

    @Override
    protected RestResourcePlugin newClassUnderTestInstance() {
        EnvironmentsConfigurationService environmentsConfigurationService = mock(EnvironmentsConfigurationService.class);
        when(environmentsConfigurationService.isEnvironmentReachable(any())).thenReturn(true);
        RestServiceByReachableEnvironmentFilter restServiceEnvFilter = new RestServiceByReachableEnvironmentFilter(environmentsConfigurationService);
        RestServiceFixUrlByEnvironmentProcessor restServiceUrlProcessor = mock(RestServiceFixUrlByEnvironmentProcessor.class);
        return new RestResourcePlugin(restServiceEnvFilter, restServiceUrlProcessor);
    }

    @Test
    public void createAndAssociateApplicationInformationBlocksShouldAddInformationBlocks() {
        //arrange
        EnvironmentsConfigurationService environmentsConfigurationService = mock(EnvironmentsConfigurationService.class);
        when(environmentsConfigurationService.isEnvironmentReachable(any())).thenReturn(true);
        RestServiceByReachableEnvironmentFilter restServiceEnvFilter = new RestServiceByReachableEnvironmentFilter(environmentsConfigurationService);
        RestServiceFixUrlByEnvironmentProcessor restServiceUrlProcessor = mock(RestServiceFixUrlByEnvironmentProcessor.class);
        RestResourcePlugin plugin = new RestResourcePlugin(restServiceEnvFilter, restServiceUrlProcessor);

        Screen screen = mock(Screen.class);
        ExtendableScreenConfig screenConfig = mock(ExtendableScreenConfig.class);
        List<String> environments = asList("B", "T", "P");
        when(screenConfig.getEnvironments()).thenReturn(environments);
        RestServiceConfig config = mock(RestServiceConfig.class);

        //act
        plugin.createAndAssociateApplicationInformationBlocks(screen, screenConfig, singletonList(config));

        //assert
        verify(restServiceUrlProcessor, times(environments.size())).replaceUrlPerEnvironment(any());
        verify(screen, times(environments.size())).addInformation(any(), any(), any(), any());
    }

}
