package ch.mobi.mobitor.plugin.rest.service.configservice;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.rest.RestPluginConfiguration;
import org.junit.Before;
import org.junit.Test;
import org.springframework.core.io.DefaultResourceLoader;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

public class LoadBalancerFilterServiceTest {

    private LoadBalancerFilterService loadBalancerFilterService;

    @Before
    public void setup() {
        RestPluginConfiguration restPluginConf = new RestPluginConfiguration();
        DefaultEnvironmentsConfigurationService environmentsConfigurationService = new DefaultEnvironmentsConfigurationService(new DefaultResourceLoader(), restPluginConf);
        environmentsConfigurationService.initializeEnvironments();

        loadBalancerFilterService = new LoadBalancerFilterService(environmentsConfigurationService);
    }

    @Test
    public void testDevelopmentNetworkUriReplacementForK8s() {
        List<String> envs = Arrays.asList("Y", "V", "W");

        for (String targetEnvironment : envs) {
            String configSwaggerUri = "http://vvn-${env}.kubernetes.host.domain/vvn/antragserstellung/rest/swagger.json";
            String newSwaggerUri = loadBalancerFilterService.filterUri(targetEnvironment, configSwaggerUri);

            assertNotNull(newSwaggerUri);
            assertThat(newSwaggerUri, containsString(".kubernetes.host.domain"));
        }
    }

    @Test
    public void testDevelopmentNetworkUriReplacementForLb() {
        List<String> envs = Arrays.asList("Y", "V", "W");

        for (String targetEnvironment : envs) {
            String config = "https://jsplb01${env}.loadbalancer.host.domain/ovn/swagger.json";
            String newSwaggerUri = loadBalancerFilterService.filterUri(targetEnvironment, config);

            assertNotNull(newSwaggerUri);
            assertThat(newSwaggerUri, containsString(".loadbalancer.host.domain"));
        }
    }

    @Test
    public void testIntegrationNetworkUriReplacementForK8s() {
        List<String> envs = Arrays.asList("I", "T");

        for (String targetEnvironment : envs) {
            String configSwaggerUri = "http://vvn-${env}.k8s.domain/vvn/antragserstellung/rest/swagger.json";
            String newSwaggerUri = loadBalancerFilterService.filterUri(targetEnvironment, configSwaggerUri);

            assertNotNull(newSwaggerUri);
            assertThat(newSwaggerUri, containsString(".kubernetes.int.host.domain"));
        }
    }

    @Test
    public void testIntegrationNetworkUriReplacementForLb() {
        List<String> envs = Arrays.asList("I", "T");

        for (String targetEnvironment : envs) {
            String configSwaggerUri = "https://jsplb01${env}.lb.domain/ovn/swagger.json";
            String newSwaggerUri = loadBalancerFilterService.filterUri(targetEnvironment, configSwaggerUri);

            assertNotNull(newSwaggerUri);
            assertThat(newSwaggerUri, containsString(".loadbalancer.int.host.domain"));
        }
    }

    @Test
    public void testProdNetworkUriReplacementForK8s() {
        String configSwaggerUri = "http://vvn-${env}.k8s.domain/vvn/antragserstellung/rest/swagger.json";
        String newSwaggerUri = loadBalancerFilterService.filterUri("P", configSwaggerUri);

        assertNotNull(newSwaggerUri);
        assertThat(newSwaggerUri, containsString(".kubernetes.prod.host.domain"));
    }

    @Test
    public void testProdNetworkUriReplacementForLb() {
        String configSwaggerUri = "https://jsplb01${env}.lb.domain/ovn/swagger.json";
        String newSwaggerUri = loadBalancerFilterService.filterUri("P", configSwaggerUri);

        assertNotNull(newSwaggerUri);
        assertThat(newSwaggerUri, containsString(".loadbalancer.prod.host.domain"));
    }

}
