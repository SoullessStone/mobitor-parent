package ch.mobi.mobitor.plugin.rest.service.restservice.interpreters;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.rest.domain.RestServiceResponse;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;


public class Jes7EndpointSecuredInterpreterTest extends SwaggerEndpointInterpreterTest {

    private Jes7EndpointSecuredInterpreter createInterpreterWithSecuredStatusCode() {
        MockRestServiceHttpRequestExecutor executor = new MockRestServiceHttpRequestExecutor("", 401);
        return new Jes7EndpointSecuredInterpreter(executor);
    }

    private Jes7EndpointSecuredInterpreter createInterpreterWithErrorResponse(String json) {
        MockRestServiceHttpRequestExecutor executor = new MockRestServiceHttpRequestExecutor(json, 505);
        return new Jes7EndpointSecuredInterpreter(executor);
    }

    @Override
    public void testInterpretSuccess() {
        Jes7EndpointSecuredInterpreter interpreter = createInterpreterWithSecuredStatusCode();
        RestServiceResponse restServiceResponse = interpreter.fetchResponse("path");

        assertThat(restServiceResponse, is(not(nullValue())));
        assertThat(restServiceResponse.isSuccess(), is(true));
    }

    @Override
    public void testInterpretFailure() {
        Jes7EndpointSecuredInterpreter interpreter = createInterpreterWithErrorResponse("error!");
        RestServiceResponse restServiceResponse = interpreter.fetchResponse("path");

        assertThat(restServiceResponse, is(not(nullValue())));
        assertThat(restServiceResponse.isSuccess(), is(false));
    }

    @Override
    public void testMatchPredicate() {
        Jes7EndpointSecuredInterpreter interpreter = new Jes7EndpointSecuredInterpreter(null);
        testMatchPredicateMatchesNumber(interpreter, 1);
    }

    @Test
    public void testCreateSwaggerResponseWithExecutorException() throws IOException {
        RestServiceHttpRequestExecutor executor = Mockito.mock(RestServiceHttpRequestExecutor.class);
        when(executor.execute(anyString())).thenThrow(new RuntimeException());

        Jes7EndpointSecuredInterpreter interpreter = new Jes7EndpointSecuredInterpreter(executor);
        RestServiceResponse restServiceResponse = interpreter.fetchResponse(null);

        assertThat(restServiceResponse, is(not(nullValue())));
        assertThat(restServiceResponse.isSuccess(), is(false));
    }
}

