package ch.mobi.mobitor.plugin.rest.domain.shakedowntest;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.junit.Assert.assertThat;

public class ShakedowntestResponseTest {

    @Test
    public void emptyShakedownResponseHasNoErrors() {
        ShakedowntestResponse shakedowntestResponse = new ShakedowntestResponse();

        assertThat(shakedowntestResponse.hasAnyErrors(), is(false));
    }

    @Test
    public void testSuccessShakedownResult() throws IOException {
        ShakedowntestResponse response = createShakedowntestResponse("shakedowntest-response-success.json");

        assertThat(response, is(not(nullValue())));
        assertThat(response.hasAnyErrors(), is(false));
        assertThat(response.getTechnicalMessages(), hasSize(0));
    }

    @Test
    public void testErrorShakedownResult() throws IOException {
        ShakedowntestResponse response = createShakedowntestResponse("shakedowntest-response-error.json");

        assertThat(response, is(not(nullValue())));
        assertThat(response.hasAnyErrors(), is(true));
        assertThat(response.getTechnicalMessages(), not(empty()));
    }

    private ShakedowntestResponse createShakedowntestResponse(String filename) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        File screenConfigFile = ResourceUtils.getFile(ResourceUtils.CLASSPATH_URL_PREFIX + "__files/" + filename);

        return mapper.readValue(screenConfigFile, ShakedowntestResponse.class);
    }
}
