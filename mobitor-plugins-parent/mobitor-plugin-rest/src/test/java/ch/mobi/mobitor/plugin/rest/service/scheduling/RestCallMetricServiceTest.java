package ch.mobi.mobitor.plugin.rest.service.scheduling;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.rest.domain.RestCallInformation;
import ch.mobi.mobitor.plugin.rest.domain.RestServiceResponse;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.rule.OutputCapture;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;


public class RestCallMetricServiceTest {

    @Rule
    public OutputCapture capture = new OutputCapture();

    @Test
    public void submitRestCallFailures() {
        // arrange
        MeterRegistry meterRegistrySpy = Mockito.spy(SimpleMeterRegistry.class);
        RestCallMetricService restMetricService = new RestCallMetricService(meterRegistrySpy);

        RestCallInformation restCallInfo = new RestCallInformation();
        restCallInfo.setSwaggerUri("http://swagger.url");
        restCallInfo.setRefreshDate(new Date());
        restCallInfo.setServerName("server_junit");
        restCallInfo.setEnvironment("env_junit");

        // act
        restMetricService.submitRestCallFailures(restCallInfo);

        // assert
        verify(meterRegistrySpy).gauge(eq("gauge_rest_status_failures"), anyList(), any(AtomicLong.class));
    }

    @Test
    public void writeHeartBeatLogNoErrors() {
        // arrange
        MeterRegistry meterRegistrySpy = Mockito.spy(SimpleMeterRegistry.class);
        RestCallMetricService restMetricService = new RestCallMetricService(meterRegistrySpy);
        RestCallInformation restCallInformation = new RestCallInformation();
        restCallInformation.setSwaggerUri("http://swagger.url/swagger.json");
        restCallInformation.setTkNameId("junit-tkNameId");
        restCallInformation.setRefreshDate(new Date());

        // act
        restMetricService.writeHeartBeatLog(restCallInformation);

        // assert
        assertThat(capture.toString(), containsString("INFO"));
        assertThat(capture.toString(), containsString("RestCallMetricService"));
        assertThat(capture.toString(), containsString("HealthStatusBeat: "));
        assertThat(capture.toString(), containsString("\"status\":\"success\""));
        assertThat(capture.toString(), containsString("\"errorCount\":0.0"));
        assertThat(capture.toString(), containsString("\"tkNameId\":\"junit-tkNameId\""));
        assertThat(capture.toString(), not(containsString("\"tkNameId\":null")));
    }

    @Test
    public void writeHeartBeatLogWithErrors() {
        // arrange
        MeterRegistry meterRegistrySpy = Mockito.spy(SimpleMeterRegistry.class);
        RestCallMetricService restMetricService = new RestCallMetricService(meterRegistrySpy);
        RestCallInformation restCallInformation = new RestCallInformation();
        restCallInformation.setSwaggerUri("http://swagger.url/swagger.json");
        restCallInformation.setRefreshDate(new Date());
        restCallInformation.setTkNameId("junit-tkNameId");
        List<RestServiceResponse> restServiceResponses = new ArrayList<>();
        restServiceResponses.add(RestServiceResponse.createErrorRestServiceResponse("http://path1/", 409));
        restServiceResponses.add(RestServiceResponse.createErrorRestServiceResponse("http://path2/", 412));
        restServiceResponses.add(RestServiceResponse.createSuccessRestServiceResponse("http://path3/",200, 550));
        restCallInformation.setRestServiceResponses(restServiceResponses);

        // act
        restMetricService.writeHeartBeatLog(restCallInformation);

        // assert
        assertThat(capture.toString(), containsString("INFO"));
        assertThat(capture.toString(), containsString("RestCallMetricService"));
        assertThat(capture.toString(), containsString("HealthStatusBeat: "));
        assertThat(capture.toString(), containsString("\"status\":\"error\""));
        assertThat(capture.toString(), containsString("\"errorCount\":2.0"));
        assertThat(capture.toString(), containsString("\"tkNameId\":\"junit-tkNameId\""));
        assertThat(capture.toString(), not(containsString("\"tkNameId\":null")));
    }

}
