package ch.mobi.mobitor.plugin.rest.service.restservice.interpreters;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.rest.domain.RestServiceResponse;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;


public class Jes7AppInfoInterpreterTest extends SwaggerEndpointInterpreterTest {

    private Jes7AppInfoInterpreter createInterpreterWithHttpResponseFromFile(String file, int statusCode) {
        RestServiceHttpRequestExecutor executor = createExecutorWithResponseFromFile(file, statusCode);
        return new Jes7AppInfoInterpreter(executor);
    }

    private Jes7AppInfoInterpreter createInterpreterWithErrorResponse(String json) {
        MockRestServiceHttpRequestExecutor executor = new MockRestServiceHttpRequestExecutor(json, 505);
        return new Jes7AppInfoInterpreter(executor);
    }

    @Test
    public void testTkNameIdExtractionSuccess() {
        Jes7AppInfoInterpreter interpreter = createInterpreterWithHttpResponseFromFile("interpreter/jes7_appinfo.json", 200);

        RestServiceResponse response = interpreter.fetchResponse("path");
        String tkNameId = response.getProperty("tkNameId");

        assertThat(tkNameId, is(equalTo("app1-comp1-service")));
    }

    @Override
    public void testInterpretSuccess() {
        Jes7AppInfoInterpreter interpreter = createInterpreterWithHttpResponseFromFile("interpreter/jes7_appinfo.json", 200);

        RestServiceResponse response = interpreter.fetchResponse("path");

        assertThat(response, is(not(nullValue())));
        assertThat(response.isSuccess(), is(true));
    }

    @Override
    public void testInterpretFailure() {
        Jes7AppInfoInterpreter interpreter = createInterpreterWithErrorResponse("error!");

        RestServiceResponse response = interpreter.fetchResponse("path");

        assertThat(response, is(not(nullValue())));
        assertThat(response.isSuccess(), is(false));
    }

    @Override
    public void testMatchPredicate() {
        Jes7AppInfoInterpreter interpreter = new Jes7AppInfoInterpreter(null);

        testMatchPredicateMatchesNumber(interpreter, 1);
    }

    @Test
    public void testCreateSwaggerResponseWithExecutorException() throws IOException {

        RestServiceHttpRequestExecutor executor = Mockito.mock(RestServiceHttpRequestExecutor.class);
        when(executor.execute(anyString())).thenThrow(new RuntimeException());

        Jes7AppInfoInterpreter interpreter = new Jes7AppInfoInterpreter(executor);
        RestServiceResponse restServiceResponse = interpreter.fetchResponse(null);

        assertThat(restServiceResponse, is(not(nullValue())));
        assertThat(restServiceResponse.isSuccess(), is(false));
    }

}

