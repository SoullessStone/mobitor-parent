package ch.mobi.mobitor.plugin.rest.service.restservice.interpreters;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.rest.domain.RestServiceResponse;
import ch.mobi.mobitor.plugin.rest.domain.interpreters.healthstatus.HealthStatusResponse;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

public class Jes2HealthStatusInterpreterTest extends SwaggerEndpointInterpreterTest {

    private Jes2HealthStatusInterpreter createInterpreterWithHttpResponseFromFile(String file, int statusCode) {
        RestServiceHttpRequestExecutor executor = createExecutorWithResponseFromFile(file, statusCode);
        return new Jes2HealthStatusInterpreter(executor);
    }

    private Jes2HealthStatusInterpreter createInterpreterWithErrorResponse(String json) {
        MockRestServiceHttpRequestExecutor executor = new MockRestServiceHttpRequestExecutor(json, 505);
        return new Jes2HealthStatusInterpreter(executor);
    }

    @Test
    public void testTkNameIdExtractionSuccess() {
        // arrange
        Jes2HealthStatusInterpreter interpreter = createInterpreterWithHttpResponseFromFile("interpreter/jes2_health_status.json", 200);

        // act
        RestServiceResponse restServiceResponse = interpreter.fetchResponse("http://junit/");

        // assert
        String tkNameId = restServiceResponse.getProperty("tkNameId");
        assertThat(tkNameId, equalTo("app1-comp1"));
    }

    @Override
    public void testInterpretSuccess() {
        // arrange
        Jes2HealthStatusInterpreter interpreter = createInterpreterWithHttpResponseFromFile("interpreter/jes2_health_status.json", 200);
        // Jes2HealthStatusInterpreter interpreter = createInterpreterWithHttpResponseFromFile(, 200);

        // act
        RestServiceResponse restServiceResponse = interpreter.fetchResponse("http://junit/");

        // assert
        assertThat(restServiceResponse, is(not(nullValue())));
        assertThat(restServiceResponse.isSuccess(), is(true));
    }

    @Test
    public void testInterpretFailureWhenTelemetrieIsDisabled() {
        Jes2HealthStatusInterpreter interpreter = createInterpreterWithHttpResponseFromFile("interpreter/jes2_health_status_mon_disabled.json", 200);

        RestServiceResponse restServiceResponse = interpreter.fetchResponse("http://junit/");

        assertThat(restServiceResponse, is(not(nullValue())));
        assertThat(restServiceResponse.isSuccess(), is(false));
        assertThat(restServiceResponse.getMessages(), hasSize(1));
    }

    @Test
    public void testInterpretEmptyHealthStatus() {
        Jes2HealthStatusInterpreter interpreter = createInterpreterWithHttpResponseFromFile("interpreter/jes2_health_status_invalid_empty.json", 200);

        RestServiceResponse restServiceResponse = interpreter.fetchResponse("http://junit/");
        String tkNameId = restServiceResponse.getProperty("tkNameId");

        assertThat(restServiceResponse, is(not(nullValue())));
        assertThat(restServiceResponse.isSuccess(), is(false));
        assertThat(restServiceResponse.getMessages(), hasSize(1));

        assertThat(tkNameId, is(nullValue()));
    }

    @Override
    public void testInterpretFailure() {
        Jes2HealthStatusInterpreter interpreter = createInterpreterWithErrorResponse("");
        RestServiceResponse restServiceResponse = interpreter.fetchResponse("http://junit");

        assertThat(restServiceResponse, is(not(nullValue())));
        assertThat(restServiceResponse.isSuccess(), is(false));
    }

    @Override
    public void testMatchPredicate() {
        Jes2HealthStatusInterpreter interpreter = new Jes2HealthStatusInterpreter(null);
        testMatchPredicateMatchesNumber(interpreter, 1);
    }

    @Test
    public void testCreateSwaggerResponseWithNull() {
        Jes2HealthStatusInterpreter interpreter = new Jes2HealthStatusInterpreter(null);
        HealthStatusResponse healthStatusResponse = interpreter.createHealthStatusResponse(null);

        assertThat(healthStatusResponse, is(nullValue()));
    }

    @Test
    public void testCreateSwaggerResponseWithExecutorException() throws IOException {
        RestServiceHttpRequestExecutor executor = Mockito.mock(RestServiceHttpRequestExecutor.class);
        when(executor.execute(anyString())).thenThrow(new RuntimeException());

        Jes2HealthStatusInterpreter interpreter = new Jes2HealthStatusInterpreter(executor);
        RestServiceResponse restServiceResponse = interpreter.fetchResponse(null);

        assertThat(restServiceResponse, is(not(nullValue())));
        assertThat(restServiceResponse.isSuccess(), is(false));
    }

    @Test
    public void testInvalidJson() {
        Jes2HealthStatusInterpreter interpreter = createInterpreterWithErrorResponse("alive!");
        HealthStatusResponse healthStatusResponse = interpreter.createHealthStatusResponse("alive!");

        assertThat(healthStatusResponse, is(nullValue()));
    }

}
