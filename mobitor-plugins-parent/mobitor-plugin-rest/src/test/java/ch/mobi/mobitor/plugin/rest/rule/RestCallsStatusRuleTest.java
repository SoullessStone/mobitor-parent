package ch.mobi.mobitor.plugin.rest.rule;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Pipeline;
import ch.mobi.mobitor.domain.screen.RuleEvaluation;
import ch.mobi.mobitor.plugin.rest.domain.ResponseInterpretation;
import ch.mobi.mobitor.plugin.rest.domain.RestCallInformation;
import ch.mobi.mobitor.plugin.rest.domain.RestServiceResponse;
import ch.mobi.mobitor.plugin.test.rule.PipelineRuleTest;
import org.junit.Test;

import static ch.mobi.mobitor.plugin.rest.domain.RestCallInformation.REST;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

public class RestCallsStatusRuleTest extends PipelineRuleTest<RestCallsStatusRule> {

    @Override
    protected RestCallsStatusRule createNewRule() {
        return new RestCallsStatusRule();
    }

    private RestServiceResponse createResponse(int statusCode) {
        return new RestServiceResponse("path", statusCode, ResponseInterpretation.SUCCESS, null, 0);
    }

    @Test
    public void evaluateRuleAllGood() {
        // arrange
        Pipeline pipeline = createPipeline();
        RestCallInformation restInformation = new RestCallInformation();
        restInformation.setSwaggerUri("junit-swaggerUri");
        restInformation.getRestServiceResponses().add(createResponse(200));

        pipeline.addInformation(ENV, APP_NAME, restInformation);


        // act
        RuleEvaluation ruleEvaluation = createNewRuleEvaluation();
        evaluateRule(pipeline, ruleEvaluation);

        // assert
        assertThat(ruleEvaluation, is(not(nullValue())));
        assertThat(ruleEvaluation.hasErrors(), is(false));
    }

    @Test
    public void evaluateRuleWithViolations() {
        // arrange
        Pipeline pipeline = createPipeline();
        RestCallInformation restInformation = new RestCallInformation();
        restInformation.setSwaggerUri("junit-swaggerUri");
        restInformation.getRestServiceResponses().add(createResponse(200));
        restInformation.getRestServiceResponses().add(createResponse(555));

        pipeline.addInformation(ENV, APP_NAME, restInformation);

        // act
        RuleEvaluation ruleEvaluation = createNewRuleEvaluation();
        evaluateRule(pipeline, ruleEvaluation);

        // assert
        assertThat(ruleEvaluation, is(not(nullValue())));
        assertThat(ruleEvaluation.hasErrors(), is(true));
    }

    @Test
    public void validatesType() {
        assertThat(validatesType(null), is(false));
        assertThat(validatesType(REST), is(true));
    }
}
