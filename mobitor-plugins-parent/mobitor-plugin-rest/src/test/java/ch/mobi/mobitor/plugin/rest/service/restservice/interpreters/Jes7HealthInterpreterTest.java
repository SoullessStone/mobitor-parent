package ch.mobi.mobitor.plugin.rest.service.restservice.interpreters;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.rest.domain.RestServiceResponse;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;


public class Jes7HealthInterpreterTest extends SwaggerEndpointInterpreterTest {

    private Jes7HealthInterpreter createInterpreterWithHttpResponseFromFile(String file, int statusCode) {
        RestServiceHttpRequestExecutor executor = createExecutorWithResponseFromFile(file, statusCode);
        return new Jes7HealthInterpreter(executor);
    }

    private Jes7HealthInterpreter createInterpreterWithErrorResponse(String json) {
        MockRestServiceHttpRequestExecutor executor = new MockRestServiceHttpRequestExecutor(json, 505);
        return new Jes7HealthInterpreter(executor);
    }

    @Override
    public void testInterpretSuccess() {
        Jes7HealthInterpreter interpreter = createInterpreterWithHttpResponseFromFile("interpreter/jes7_health.json", 200);

        RestServiceResponse restServiceResponse = interpreter.fetchResponse("path");

        assertThat(restServiceResponse, is(not(nullValue())));
        assertThat(restServiceResponse.isSuccess(), is(true));
    }

    @Test
    public void testInterpretFailureResponse() {
        Jes7HealthInterpreter interpreter = createInterpreterWithHttpResponseFromFile("interpreter/jes7_health_failure.json", 200);

        RestServiceResponse restServiceResponse = interpreter.fetchResponse("path");

        assertThat(restServiceResponse, is(not(nullValue())));
        assertThat(restServiceResponse.isSuccess(), is(false));
        assertThat(restServiceResponse.getMessages(), hasSize(1));
    }

    @Override
    public void testInterpretFailure() {
        Jes7HealthInterpreter interpreter = createInterpreterWithErrorResponse("error");
        RestServiceResponse restServiceResponse = interpreter.fetchResponse("path");

        assertThat(restServiceResponse, is(not(nullValue())));
        assertThat(restServiceResponse.isSuccess(), is(false));
    }

    @Override
    public void testMatchPredicate() {
        Jes7HealthInterpreter interpreter = new Jes7HealthInterpreter(null);
        testMatchPredicateMatchesNumber(interpreter, 1);
    }

    @Test
    public void testCreateSwaggerResponseWithExecutorException() throws IOException {
        RestServiceHttpRequestExecutor executor = Mockito.mock(RestServiceHttpRequestExecutor.class);
        when(executor.execute(anyString())).thenThrow(new RuntimeException("junit exception thrown via mock"));

        Jes7HealthInterpreter interpreter = new Jes7HealthInterpreter(executor);
        RestServiceResponse restServiceResponse = interpreter.fetchResponse("http://health");

        assertThat(restServiceResponse, is(not(nullValue())));
        assertThat(restServiceResponse.isSuccess(), is(false));
    }

    @Test
    public void testInterpretFailureWithNoReasonContainsHealthCheckName() {
        Jes7HealthInterpreter interpreter = createInterpreterWithHttpResponseFromFile("interpreter/jes7_health_faild_with_no_reason.json", 200);

        RestServiceResponse restServiceResponse = interpreter.fetchResponse("path");

        assertThat(restServiceResponse, is(not(nullValue())));
        assertThat(restServiceResponse.isSuccess(), is(false));
        assertThat(restServiceResponse.getMessages(), hasSize(1));

        String reasonMessage = restServiceResponse.getMessages().get(0);
        assertThat(reasonMessage, containsString("pingVerification"));
    }

}

