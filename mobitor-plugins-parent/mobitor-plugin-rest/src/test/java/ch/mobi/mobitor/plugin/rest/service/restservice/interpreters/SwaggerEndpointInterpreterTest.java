package ch.mobi.mobitor.plugin.rest.service.restservice.interpreters;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.rest.RestPluginConfiguration;
import ch.mobi.mobitor.plugin.rest.domain.swagger.PathHttpMethods;
import ch.mobi.mobitor.plugin.rest.domain.swagger.SwaggerResponse;
import ch.mobi.mobitor.plugin.rest.service.restservice.SwaggerEndpointInterpreter;
import org.apache.commons.io.FileUtils;
import org.hamcrest.collection.IsCollectionWithSize;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static ch.mobi.mobitor.plugin.rest.domain.RestInformationUtils.getMatchingPaths;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

public abstract class SwaggerEndpointInterpreterTest {

    private final List<String> REST_PATHS = Arrays.asList(
            "/health/flyway/current",
            "/jap/flyway/schema/version/current",
            "/jap/appinfo",
            "/appinfo",
            "/jap/health",
            "/jap/properties",
            "/health",
            "/partnerkurzinformationen/shakedown",
            "/partnersuche/shakedown",
            "/mcs/partneradapter/shakedowntest",
            "/personen/{partnerFachnummer}/kontaktadress-praeferenzen/shakedown",
            "/personen/{partnerFachnummer}/kontaktadress-praeferenzen/shakedowntest",
            "https://app.testhost.domain/pdv/partner/rest/adresse/ping",
            "http://app-test.host.domain/vvn/antragserstellung/rest/health/status",
            "https://app.host.domain/pdv/partner/rest/beziehungspartnerdaten/ping",
            "https://app.host.domain/pdv/partner/rest/oe-korrespondenzdaten/ping",
            "https://app.host.domain/pdv/partner/rest/akquisiteurkonto-beziehungen/ping",
            "https://app.host.domain/pdv/partner/rest/formatiertePartneranschriften/ping",
            "https://app.host.domain/pdv/partner/rest/akquisiteurkontobeziehungen/ping",
            "https://app.host.domain/pdv/partner/rest/ping",
            "https://app.host.domain/pdv/partner/rest/mcs/partneradapter/ping",
            "https://app.host.domain/pdv/partner/rest/partner-kurzinformationen/ping",
            "https://app.host.domain/pdv/partner/rest/partnersuche/ping",
            "https://app.host.domain/pdv/partner/rest/vertragspartnerbeziehungen/ping",
            "https://app.host.domain/pdv/partner/rest/partner-beziehungen/ping",
            "https://app.host.domain/pdv/partner/rest/partnerbeziehungen/ping",
            "https://app.host.domain/pdv/partner/rest/partnerkurzinformationen/ping",
            "https://app.host.domain/pdv/partner/rest/personen/{partnerFachnummer}/kontaktadresspraeferenzen/ping",
            "https://app.host.domain/pdv/partner/rest/partnerdaten/ping",
            "https://app.host.domain/pdv/partner/rest/personen/ping",
            "https://app.host.domain/pdv/partner/rest/personen/{partnerFachnummer}/zahlungsverbindungen/ping");

    private SwaggerResponse createSwaggerResponse() {
        SwaggerResponse sr = new SwaggerResponse();
        Map<String, PathHttpMethods> paths = new HashMap<>();
        for (String restPath : REST_PATHS) {
            PathHttpMethods pathHttpMethods = new PathHttpMethods();
            pathHttpMethods.add("get", null);
            paths.put(restPath, pathHttpMethods);
        }

        sr.setPaths(paths);
        return sr;
    }

    protected RestServiceHttpRequestExecutor createExecutorWithResponseFromFile(String file, int statusCode) {
        String json = readFromExampleFile(file);
        MockRestServiceHttpRequestExecutor executor = new MockRestServiceHttpRequestExecutor(json, statusCode);

        return executor;
    }

    String readFromExampleFile(String classpathFile) {
        try {
            File jsonFile = ResourceUtils.getFile(ResourceUtils.CLASSPATH_URL_PREFIX + classpathFile);
            return FileUtils.readFileToString(jsonFile, StandardCharsets.UTF_8);

        } catch (Exception ignored) {
        }

        return null;
    }

    void testMatchPredicateMatchesNumber(SwaggerEndpointInterpreter interpreter, int expectedMatches) {
        SwaggerResponse sr = createSwaggerResponse();
        List<String> matchingPaths = getMatchingPaths("", interpreter.getMatchPredicate(), sr.getPaths());

        assertThat(matchingPaths, is(not(nullValue())));
        assertThat(matchingPaths, IsCollectionWithSize.hasSize(expectedMatches));
    }

    @NotNull
    protected RestServiceHttpRequestExecutor anonymousRequestExecutor() {
        return new RestServiceHttpRequestExecutorAnonymous(new DefaultRestServiceHttpRequestConfiguration(restPluginConfigurationMock()));
    }

    @NotNull
    protected RestServiceHttpRequestExecutor authenticatingRequestExecutor() {
        return new RestServiceHttpRequestExecutorAuthenticating(new DefaultRestServiceHttpRequestConfiguration(restPluginConfigurationMock()),
                "EXAMPLEUSERNAME");
    }

/*
    @NotNull
    protected MobitorApplicationConfiguration mobitorApplicationConfigurationMock() {
        MobitorApplicationConfiguration mock = Mockito.mock(MobitorApplicationConfiguration.class);
        Mockito.when(mock.getVersion()).thenReturn("1.2.3");
        return mock;
    }
*/

    protected RestPluginConfiguration restPluginConfigurationMock() {
        return new RestPluginConfiguration();
    }

    @Test
    public abstract void testMatchPredicate();

    @Test
    public abstract void testInterpretSuccess();

    @Test
    public abstract void testInterpretFailure();

}
