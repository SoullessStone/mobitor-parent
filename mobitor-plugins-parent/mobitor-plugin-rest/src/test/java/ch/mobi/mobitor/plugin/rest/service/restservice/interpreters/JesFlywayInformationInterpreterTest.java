package ch.mobi.mobitor.plugin.rest.service.restservice.interpreters;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.rest.domain.RestServiceResponse;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;


public class JesFlywayInformationInterpreterTest  extends SwaggerEndpointInterpreterTest {

    private JesFlywayInformationInterpreter createInterpreterWithHttpResponseFromFile(String file, int statusCode) {
        RestServiceHttpRequestExecutor executor = createExecutorWithResponseFromFile(file, statusCode);
        return new JesFlywayInformationInterpreter(executor);
    }

    private JesFlywayInformationInterpreter createInterpreterWithErrorResponse(String json) {
        MockRestServiceHttpRequestExecutor executor = new MockRestServiceHttpRequestExecutor(json, 505);
        return new JesFlywayInformationInterpreter(executor);
    }

    @Override
    public void testInterpretSuccess() {
        JesFlywayInformationInterpreter interpreter = createInterpreterWithHttpResponseFromFile("interpreter/jes7_flyway_current.json", 200);

        RestServiceResponse restServiceResponse = interpreter.fetchResponse("path");

        assertThat(restServiceResponse, is(not(nullValue())));
        assertThat(restServiceResponse.isSuccess(), is(true));
    }

    @Override
    public void testInterpretFailure() {
        JesFlywayInformationInterpreter interpreter = createInterpreterWithErrorResponse("error");
        RestServiceResponse restServiceResponse = interpreter.fetchResponse("path");

        assertThat(restServiceResponse, is(not(nullValue())));
        assertThat(restServiceResponse.isSuccess(), is(false));
    }

    @Override
    public void testMatchPredicate() {
        JesFlywayInformationInterpreter interpreter = new JesFlywayInformationInterpreter(null);
        testMatchPredicateMatchesNumber(interpreter, 2);
    }

    @Test
    public void testCreateSwaggerResponseWithExecutorException() throws IOException {
        RestServiceHttpRequestExecutor executor = Mockito.mock(RestServiceHttpRequestExecutor.class);
        when(executor.execute(anyString())).thenThrow(new RuntimeException());

        JesFlywayInformationInterpreter interpreter = new JesFlywayInformationInterpreter(executor);
        RestServiceResponse restServiceResponse = interpreter.fetchResponse(null);

        assertThat(restServiceResponse, is(not(nullValue())));
        assertThat(restServiceResponse.isSuccess(), is(false));
    }

}

