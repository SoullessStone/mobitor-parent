package ch.mobi.mobitor.plugin.rest.rule;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Pipeline;
import ch.mobi.mobitor.domain.screen.RuleEvaluation;
import ch.mobi.mobitor.plugin.rest.domain.FlywayRestServiceResponse;
import ch.mobi.mobitor.plugin.rest.domain.RestCallInformation;
import ch.mobi.mobitor.plugin.rest.domain.RestServiceResponse;
import ch.mobi.mobitor.plugin.test.rule.PipelineRuleTest;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static ch.mobi.mobitor.plugin.rest.domain.RestCallInformation.REST;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;


public class FlywayStatusRuleTest extends PipelineRuleTest<FlywayStatusRule> {

    @Test
    public void evaluateRuleWithEmptyInformation() {
        // arrange
        RestCallInformation restInfo = new RestCallInformation();
        restInfo.setSwaggerUri("http://junit.swagger.local");
        Pipeline pipeline = createPipeline();
        pipeline.addInformation(ENV, APP_NAME, restInfo);

        // act
        RuleEvaluation ruleEvaluation = createNewRuleEvaluation();
        evaluateRule(pipeline, ruleEvaluation);

        // assert
        assertThat(ruleEvaluation.hasErrors(), is(false));
    }

    @Test
    public void evaluateRuleWithGoodInformation() {
        // arrange
        RestCallInformation restInfo = new RestCallInformation();
        restInfo.setSwaggerUri("http://junit.swagger.local");

        List<RestServiceResponse> responses = new ArrayList<>();
        responses.add(FlywayRestServiceResponse.createSuccessRestServiceResponse("path", 200, 50));
        restInfo.setRestServiceResponses(responses);

        Pipeline pipeline = createPipeline();
        pipeline.addInformation(ENV, APP_NAME, restInfo);

        // act
        RuleEvaluation ruleEvaluation = createNewRuleEvaluation();
        evaluateRule(pipeline, ruleEvaluation);

        // assert
        assertThat(ruleEvaluation.hasErrors(), is(false));
    }

    @Test
    public void evaluateRuleWithBadInformation() {
        // arrange
        RestCallInformation restInfo = new RestCallInformation();
        restInfo.setSwaggerUri("http://junit.swagger.local");

        List<RestServiceResponse> responses = new ArrayList<>();
        responses.add(FlywayRestServiceResponse.createErrorRestServiceResponse("path", 500));
        restInfo.setRestServiceResponses(responses);

        Pipeline pipeline = createPipeline();
        pipeline.addInformation(ENV, APP_NAME, restInfo);

        // act
        RuleEvaluation ruleEvaluation = createNewRuleEvaluation();
        evaluateRule(pipeline, ruleEvaluation);

        // assert
        assertThat(ruleEvaluation.hasErrors(), is(true));
    }

    @Test
    public void validatesType() {
        FlywayStatusRule fr = new FlywayStatusRule();

        assertThat(fr.validatesType(REST), is(true));
    }

    @Override
    protected FlywayStatusRule createNewRule() {
        return new FlywayStatusRule();
    }

}
