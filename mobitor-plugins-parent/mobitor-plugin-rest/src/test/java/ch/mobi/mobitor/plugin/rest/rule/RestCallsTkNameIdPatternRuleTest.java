package ch.mobi.mobitor.plugin.rest.rule;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Pipeline;
import ch.mobi.mobitor.domain.screen.RuleEvaluation;
import ch.mobi.mobitor.plugin.rest.domain.RestCallInformation;
import ch.mobi.mobitor.plugin.test.rule.PipelineRuleTest;
import org.junit.Test;

import static ch.mobi.mobitor.plugin.rest.domain.RestCallInformation.REST;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class RestCallsTkNameIdPatternRuleTest extends PipelineRuleTest<RestCallsTkNameIdPatternRule> {

    @Test
    public void evaluateRuleWithValidTkNameId() {
        // arrange
        RestCallInformation restInfo = new RestCallInformation();
        restInfo.setSwaggerUri("http://junit.swagger.local");
        restInfo.setTkNameId("aid-fknameid-service");
        Pipeline pipeline = createPipeline();
        pipeline.addInformation(ENV, APP_NAME, restInfo);

        // act
        RuleEvaluation ruleEvaluation = createNewRuleEvaluation();
        evaluateRule(pipeline, ruleEvaluation);

        // assert
        assertThat(ruleEvaluation.hasErrors(), is(false));
    }

    @Test
    public void evaluateRuleWithInvalidTkNameId() {
        // arrange
        RestCallInformation restInfo = new RestCallInformation();
        restInfo.setSwaggerUri("http://junit.swagger.local");
        restInfo.setTkNameId("name");
        Pipeline pipeline = createPipeline();
        pipeline.addInformation(ENV, APP_NAME, restInfo);

        // act
        RuleEvaluation ruleEvaluation = createNewRuleEvaluation();
        evaluateRule(pipeline, ruleEvaluation);

        // assert
        assertThat(ruleEvaluation.hasErrors(), is(true));
    }

    @Test
    public void validatesType() {
        RestCallsTkNameIdPatternRule rule = new RestCallsTkNameIdPatternRule();

        assertThat(rule.validatesType(REST), is(true));
    }

    @Test
    public void ruleViolated() {
        // arrange
        // act
        // assert
        assertThat(isValidTkNameId("diu-elan2insign-service"), is(true));
        assertThat(isValidTkNameId("b2e-profil-service"), is(true));
        assertThat(isValidTkNameId("vvn-baustein-service"), is(true));
        assertThat(isValidTkNameId("vvn-baustein-rwc"), is(true));
        assertThat(isValidTkNameId("vvn-baustein-postgresql"), is(true));
        assertThat(isValidTkNameId("abc-def-ghij"), is(true));
        assertThat(isValidTkNameId("abc-sdfsdgasdgfasfd-asdfasghij"), is(true));
        assertThat(isValidTkNameId("abc-d-e"), is(true));
        assertThat(isValidTkNameId("pdv-partnersuche-loader-service"), is(true));
        assertThat(isValidTkNameId("pdv-partnersuche-loader2db-service"), is(true));
        assertThat(isValidTkNameId("pdv-partner4suche-loader4spoud-webapp"), is(true));
        assertThat(isValidTkNameId("peddv-partner4suche-loader4spoud-webapp"), is(true));
        assertThat(isValidTkNameId("peddv-partner4suche-webapp"), is(true));
        assertThat(isValidTkNameId("pddv-partner4suche-webapp"), is(true));

        assertThat(isValidTkNameId("abcdef-cd-service"), is(false));
        assertThat(isValidTkNameId("ab-cd"), is(false));
        assertThat(isValidTkNameId("ab-cd-cd-cd"), is(false));
        assertThat(isValidTkNameId("a"), is(false));
        assertThat(isValidTkNameId("a_b_c"), is(false));
        assertThat(isValidTkNameId("!/ovn/flotte"), is(false));
        assertThat(isValidTkNameId(""), is(false));
        assertThat(isValidTkNameId(null), is(false));

        assertThat(isValidTkNameId("app-name-jeeservice"), is(true));

        assertThat(isValidTkNameId("mps-core-jeeservice"), is(true));
        assertThat(isValidTkNameId("mps-core-b2b-jsfwebclient"), is(true));
        assertThat(isValidTkNameId("mps-core-b2e-jsfwebclient"), is(true));
        assertThat(isValidTkNameId("mps-core-standalone-jsfwebclient"), is(true));
        assertThat(isValidTkNameId("mps-core-siebel-jsfwebclient"), is(true));
        assertThat(isValidTkNameId("mps-core-uid-javabatch"), is(true));

    }

    private boolean isValidTkNameId(String tkNameId) {
        RestCallsTkNameIdPatternRule tkNameIdRule = new RestCallsTkNameIdPatternRule();
        RestCallInformation restCallInfo = new RestCallInformation();
        restCallInfo.setSwaggerUri("http://swagger.local");
        restCallInfo.setTkNameId(tkNameId);

        return !tkNameIdRule.ruleViolated(restCallInfo);
    }


    @Override
    protected RestCallsTkNameIdPatternRule createNewRule() {
        return new RestCallsTkNameIdPatternRule();
    }
}
