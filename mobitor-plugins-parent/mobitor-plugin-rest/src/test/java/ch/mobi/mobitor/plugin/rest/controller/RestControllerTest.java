package ch.mobi.mobitor.plugin.rest.controller;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */


import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugins.api.model.ScreensModel;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static ch.mobi.mobitor.it.TestViewResolver.viewResolver;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class RestControllerTest {

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        ScreensModel model = mock(ScreensModel.class);
        when(model.getScreen(any())).thenReturn(mock(Screen.class));
        this.mockMvc = MockMvcBuilders.standaloneSetup(new RestController(model))
                                      .setViewResolvers(viewResolver())
                                      .build();
    }

    @Test
    public void requestShouldReturnStatusOk() throws Exception {
        mockMvc.perform(get("/rest")
                .param("screen", "2")
                .param("env", "B")
                .param("server", "server")
                .param("application", "application"))
               .andDo(print())
               .andExpect(status().isOk());
    }

    @Test
    public void requestShouldForwardToRestView() throws Exception {
        mockMvc.perform(get("/rest")
                .param("screen", "2")
                .param("env", "B")
                .param("server", "server")
                .param("application", "application"))
               .andExpect(forwardedUrl("/templates/rest.html"));
    }
}
