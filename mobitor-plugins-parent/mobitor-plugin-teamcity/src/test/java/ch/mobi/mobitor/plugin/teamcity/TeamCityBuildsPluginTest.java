package ch.mobi.mobitor.plugin.teamcity;

/*-
 * §
 * mobitor-plugin-teamcity
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.teamcity.config.TeamCityBuildConfig;
import ch.mobi.mobitor.plugin.teamcity.domain.TeamCityBuildInformation;
import ch.mobi.mobitor.plugin.teamcity.domain.TeamCityBuildWithVersionInformation;
import ch.mobi.mobitor.plugin.test.PluginTest;
import ch.mobi.mobitor.plugins.api.domain.config.ExtendableScreenConfig;
import org.junit.Test;

import static java.util.Collections.singletonList;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class TeamCityBuildsPluginTest extends PluginTest<TeamCityBuildsPlugin, TeamCityBuildConfig, TeamCityBuildInformation> {

    @Override
    public String getConfigPropertyName() {
        return "teamCityBuilds";
    }

    @Test
    public void createAndAssociateApplicationInformationBlocksShouldAddInformationWithVersionToScreen() throws Exception {
        //arrange
        TeamCityBuildsPlugin sut = newClassUnderTestInstance();
        Screen screen = mock(Screen.class);
        ExtendableScreenConfig screenConfig = mock(ExtendableScreenConfig.class);
        TeamCityBuildConfig config = new TeamCityBuildConfig();
        config.setBuildNumberIsVersion(true);

        //act
        sut.createAndAssociateApplicationInformationBlocks(screen, screenConfig, singletonList(config));

        //assert
        verify(screen).addInformation(any(), any(), any(), any(TeamCityBuildWithVersionInformation.class));
    }
}
