package ch.mobi.mobitor.plugin.teamcity.service.client;

/*-
 * §
 * mobitor-plugin-teamcity
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.teamcity.domain.TeamCityBuildInformation;
import ch.mobi.mobitor.plugin.teamcity.domain.TeamCityCoverageInformation;
import ch.mobi.mobitor.plugin.teamcity.domain.TeamCityProjectInformation;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.hamcrest.CoreMatchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.math.BigDecimal;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.givenThat;
import static com.github.tomakehurst.wiremock.client.WireMock.matching;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;


public class TeamCityInformationProviderServiceTest {

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(options().dynamicPort());

    private TeamCityInformationProviderService teamCityService;

    @Before
    public void setup() {
        TeamCityConfiguration configuration = new TeamCityConfiguration();
        String baseUrl = "http://localhost:" + wireMockRule.port() + "/TeamCity";
        configuration.setBaseUrl(baseUrl);
        configuration.setUsername("junit");
        configuration.setPassword("junit");

        TeamCityClient client = new TeamCityClient(configuration);
        teamCityService = new TeamCityInformationProviderService(client);
    }

    @Test
    public void updateSuccessfulBuildInformation() {
        // arrange
        TeamCityBuildInformation build = new TeamCityBuildInformation("tcConfigId", "junit");

        givenThat(get(urlMatching("/TeamCity/httpAuth/app/rest/builds?(.*)"))
                .withQueryParam("count", equalTo("1"))
                .withQueryParam("locator", matching("([buildType:][a-zA-Z0-9_:,]+)"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("teamcity-buildType-ags-success.json")
                )
        );

        // act
        teamCityService.updateBuildInformation(build);

        // assert
        assertThat(build.getConfigId(), CoreMatchers.equalTo("tcConfigId"));
        assertThat(build.isSuccessful(), is(true));
        assertThat(build.isRunning(), is(false));
    }

    @Test
    public void updateFailureBuildInformation() {
        // arrange
        TeamCityBuildInformation build = new TeamCityBuildInformation("tcConfigId", "junit");

        givenThat(get(urlMatching("/TeamCity/httpAuth/app/rest/builds?(.*)"))
                .withQueryParam("count", equalTo("1"))
                .withQueryParam("locator", matching("([buildType:][a-zA-Z0-9_:,]+)"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("teamcity-buildType-ags-failure.json")
                )
        );

        givenThat(get(urlMatching("/TeamCity/httpAuth/app/rest/builds/id:(.*)/statistics"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("teamcity-buildType-ags-failure-stats.json")
                )
        );

        // act
        teamCityService.updateBuildInformation(build);

        // assert
        assertThat(build.getConfigId(), CoreMatchers.equalTo("tcConfigId"));
        assertThat(build.isSuccessful(), is(false));
        assertThat(build.isRunning(), is(false));

        assertThat(build.getTestsFailed(), is(not(nullValue())));
    }

    @Test
    public void updateProjectInformation() {
        // arrange
        // create buildSuccess response
        givenThat(get(urlMatching("/TeamCity/httpAuth/app/rest/builds?(.*)"))
                .withQueryParam("count", equalTo("1"))
                .withQueryParam("locator", matching("(buildType:Zug_V2_E2e_v_Chrome_einfacher_fall[a-zA-Z0-9:,&=]+)"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("teamcity-project-buildType-success.json")
                )
        );

        //create failureResponse
        givenThat(get(urlMatching("/TeamCity/httpAuth/app/rest/builds?(.*)"))
                .withQueryParam("count", equalTo("1"))
                .withQueryParam("locator", matching("(buildType:Zug_V2_E2e_v_Chrome_Szenario01[a-zA-Z0-9:,&=]+)"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("teamcity-project-buildType-failure.json")
                )
        );

        // act
        TeamCityProjectInformation teamCityProjectInformation = new TeamCityProjectInformation("Zug_V2_E2e_V_Chrome", "junit");

        givenThat(get(urlMatching("/TeamCity/httpAuth/app/rest/projects/Zug_V2_E2e_V_Chrome/buildTypes"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("teamcity-projects.json")
                )
        );
        teamCityService.updateProjectInformation(teamCityProjectInformation);

        // assert
        assertThat(teamCityProjectInformation.getStatus(),is("FAILURE"));
        assertThat(teamCityProjectInformation.getJobsPassed(),is(1));
        assertThat(teamCityProjectInformation.getJobsTotal(),is(2));
    }

    @Test
    public void updateCoverageInformation() {

        // arrange
        givenThat(get(urlMatching("/TeamCity/httpAuth/app/rest/builds?(.*)"))
                .withQueryParam("count", equalTo("1"))
                .withQueryParam("locator", matching("(buildType:Jap_JapEnvchecker_CommitRc[a-zA-Z0-9:,&=]+)"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("teamcity-project-buildType-envchecker-success.json")
                )
        );

        // act
        TeamCityCoverageInformation teamCityCoverageInformation = new TeamCityCoverageInformation("Jap_JapEnvchecker_CommitRc", "junit");

        givenThat(get(urlMatching("/TeamCity/httpAuth/app/rest/builds/id:(.*)/statistics"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("teamcity-buildType-envchecker-success-stats.json")
                )
        );

        teamCityService.updateCoverageInformation(teamCityCoverageInformation);

        // assert
        assertThat(teamCityCoverageInformation.getStatus(),is("SUCCESS"));
        assertThat(teamCityCoverageInformation.getCoverage(),is(new BigDecimal("98.8")));
    }


}
