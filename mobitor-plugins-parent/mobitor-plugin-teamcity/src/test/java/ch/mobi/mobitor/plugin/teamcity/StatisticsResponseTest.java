package ch.mobi.mobitor.plugin.teamcity;

/*-
 * §
 * mobitor-plugin-teamcity
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.teamcity.service.client.domain.StatisticsResponse;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

public class StatisticsResponseTest {

    @Test
    public void testsPassedShouldReturnFoundProperty() throws IOException {
        //given
        StatisticsResponse response = getStatisticsResponseFromFile("teamcity-statistics-without-coverage.json");

        //when
        int result = response.testsPassed();

        //then
        assertThat(result).isEqualTo(90);
    }

    @Test
    public void testsTotalShouldReturnZeroIfPropertyWasNotFound() throws IOException {
        //given
        StatisticsResponse response = getStatisticsResponseFromFile("teamcity-statistics-without-coverage.json");

        //when
        int result = response.testsTotal();

        //then
        assertThat(result).isEqualTo(0);
    }

    @Test
    public void coverageShouldReturnLineCoverageIfItIsNotNull() throws IOException {
        //given
        StatisticsResponse response = getStatisticsResponseFromFile("teamcity-statistics-with-line-coverage.json");

        //when
        BigDecimal coverage = response.coverage();

        //then
        assertThat(coverage).isEqualTo("98.803827");
    }

    @Test
    public void coverageShouldReturnStatementCoverageIfLineCoverageIsNull() throws IOException {
        //given
        StatisticsResponse response = getStatisticsResponseFromFile("teamcity-statistics-with-statement-coverage.json");

        //when
        BigDecimal coverage = response.coverage();

        //then
        assertThat(coverage).isEqualTo("85.5");
    }

    @Test
    public void coverageShouldReturnNullIfStatementCoverageAndLineCoverageIsNull() throws IOException {
        //given
        StatisticsResponse response = getStatisticsResponseFromFile("teamcity-statistics-without-coverage.json");

        //when
        BigDecimal coverage = response.coverage();

        //then
        assertThat(coverage).isNull();
    }

    private StatisticsResponse getStatisticsResponseFromFile(String filename) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        File file = ResourceUtils.getFile(ResourceUtils.CLASSPATH_URL_PREFIX + filename);
        return mapper.readValue(file, StatisticsResponse.class);
    }
}
