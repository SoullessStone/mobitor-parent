package ch.mobi.mobitor.plugin.teamcity.service.scheduling;

/*-
 * §
 * mobitor-plugin-teamcity
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.teamcity.domain.TeamCityBuildInformation;
import ch.mobi.mobitor.plugin.teamcity.service.client.TeamCityInformationProviderService;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformation;
import ch.mobi.mobitor.plugins.api.model.ScreensModel;
import ch.mobi.mobitor.plugins.api.service.CollectorMetricService;
import ch.mobi.mobitor.plugins.api.service.RuleService;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import static ch.mobi.mobitor.plugin.teamcity.domain.TeamCityBuildInformation.TEAM_CITY_BUILD;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TeamCityBuildStatusCollectorTest {

    @Test
    public void collectTeamCityInformationForScreens() {
        // arrange
        RuleService ruleService = Mockito.mock(RuleService.class);
        ScreensModel screensModel = mock(ScreensModel.class);
        TeamCityInformationProviderService tcInfProviderService = mock(TeamCityInformationProviderService.class);
        CollectorMetricService collectorMetricService = mock(CollectorMetricService.class);

        TeamCityBuildInformation tcBuildInfo = new TeamCityBuildInformation("tcJunitConfigId", "Label for JUnit");
        List<ApplicationInformation> tcBuildInfos = Collections.singletonList(tcBuildInfo);

        Screen screen = mock(Screen.class);
        List<Screen> screensList = Collections.singletonList(screen);

        when(screensModel.getAvailableScreens()).thenReturn(screensList);
        when(screen.getMatchingInformation(eq(TEAM_CITY_BUILD))).thenReturn(tcBuildInfos);

        TeamCityBuildStatusCollector tcCollector = new TeamCityBuildStatusCollector(screensModel, tcInfProviderService, ruleService, collectorMetricService);

        // act
        tcCollector.collectTeamCityBuildInformationForScreens();

        // assert
        Mockito.verify(screensModel).getAvailableScreens();
        Mockito.verify(ruleService).updateRuleEvaluation(ArgumentMatchers.any(), ArgumentMatchers.any());
        Mockito.verify(screen).setRefreshDate(ArgumentMatchers.eq(TEAM_CITY_BUILD), ArgumentMatchers.any(Date.class));
    }

}
