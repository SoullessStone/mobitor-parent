package ch.mobi.mobitor.plugin.teamcity;

/*-
 * §
 * mobitor-plugin-teamcity
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.teamcity.service.client.domain.BuildsResponse;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.IOException;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class BuildsResponseDeserializeTest {

    @Test
    public void testBuildResponseFinishedDeserialize() throws IOException {
        // arrange
        BuildsResponse br = createBuildResponse("build-response-example-finished.json");

        // assert
        assertNotNull(br);
        assertNotNull(br.getBuilds());
        assertFalse(br.getBuilds().get(0).isRunning());
    }

    private BuildsResponse createBuildResponse(String filename) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        File screenConfigFile = ResourceUtils.getFile(ResourceUtils.CLASSPATH_URL_PREFIX + filename);
        // act
        return mapper.readValue(screenConfigFile, BuildsResponse.class);
    }

    @Test
    public void testBuildResponseRunningDeserialize() throws IOException {
        // arrange
        BuildsResponse br = createBuildResponse("build-response-example-running.json");

        // assert
        assertNotNull(br);
        assertNotNull(br.getBuilds());
        assertTrue(br.getBuilds().get(0).isRunning());
    }

}
