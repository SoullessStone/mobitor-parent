package ch.mobi.mobitor.plugin.teamcity.service.client;

/*-
 * §
 * mobitor-plugin-teamcity
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.teamcity.domain.TeamCityBuildInformation;
import ch.mobi.mobitor.plugin.teamcity.domain.TeamCityCoverageInformation;
import ch.mobi.mobitor.plugin.teamcity.domain.TeamCityProjectInformation;
import ch.mobi.mobitor.plugin.teamcity.service.client.domain.BuildResponse;
import ch.mobi.mobitor.plugin.teamcity.service.client.domain.BuildsResponse;
import ch.mobi.mobitor.plugin.teamcity.service.client.domain.StatisticsResponse;
import ch.mobi.mobitor.plugin.teamcity.service.client.domain.project.ProjectBuildTypeResponse;
import ch.mobi.mobitor.plugin.teamcity.service.client.domain.project.ProjectBuildTypesResponse;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;

import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class TeamCityInformationProviderServiceSuggestTest {

    @Mock
    private TeamCityClient teamCityClient;

    @InjectMocks
    private TeamCityInformationProviderService service;

    @Test
    public void updateProjectInformation_should_call_client_with_false() {
        // arrange
        String projectId = "Project ID";
        String projectLabel = "Project Label";
        TeamCityProjectInformation project = new TeamCityProjectInformation(projectId, projectLabel);
        ProjectBuildTypeResponse firstResponse = new ProjectBuildTypeResponse();
        firstResponse.setId(projectId);
        ProjectBuildTypesResponse projectInformation = new ProjectBuildTypesResponse();
        projectInformation.setProjectBuildTypeResponses(Collections.singletonList(firstResponse));
        given(teamCityClient.retrieveProjectInformation(projectId)).willReturn(projectInformation);
        // act
        service.updateProjectInformation(project);
        // assert
        verify(teamCityClient).retrieveBuildInformation(projectId, false);
        Assertions.assertThat(project.isShowAnyBranch()).isFalse();
    }

    @Test
    public void updateProjectInformation_should_call_client_with_true() {
        // arrange
        String projectId = "Project ID";
        String projectLabel = "Project Label";
        TeamCityProjectInformation project = new TeamCityProjectInformation(projectId, projectLabel);
        project.setShowAnyBranch(true);
        ProjectBuildTypeResponse firstResponse = new ProjectBuildTypeResponse();
        firstResponse.setId(projectId);
        ProjectBuildTypesResponse projectInformation = new ProjectBuildTypesResponse();
        projectInformation.setProjectBuildTypeResponses(Collections.singletonList(firstResponse));
        given(teamCityClient.retrieveProjectInformation(projectId)).willReturn(projectInformation);
        // act
        service.updateProjectInformation(project);
        // assert
        verify(teamCityClient).retrieveBuildInformation(projectId, true);
        Assertions.assertThat(project.isShowAnyBranch()).isTrue();
    }

    @Test
    public void updateBuildInformation_should_call_client_with_false() {
        // arrange
        String buildId = "Build Config ID";
        String buildLabel = "Build Label";
        TeamCityBuildInformation build = new TeamCityBuildInformation(buildId, buildLabel);
        build.setShowAnyBranch(false);
        // act
        service.updateBuildInformation(build);
        // assert
        verify(teamCityClient).retrieveBuildInformation(buildId, false);
    }

    @Test
    public void updateBuildInformation_should_call_client_with_true() {
        // arrange
        String buildId = "Build Config ID";
        String buildLabel = "Build Label";
        TeamCityBuildInformation build = new TeamCityBuildInformation(buildId, buildLabel);
        build.setShowAnyBranch(true);
        // act
        service.updateBuildInformation(build);
        // assert
        verify(teamCityClient).retrieveBuildInformation(buildId, true);
    }

    @Test
    public void updateCoverageInformation_should_call_client_with_false() {
        // arrange
        String buildConfigId = "Build Config ID";
        String buildLabel = "Build Label";
        String buildId = "Build Id";
        String status = "SUCCESS";

        TeamCityCoverageInformation coverage = new TeamCityCoverageInformation(buildConfigId, buildLabel);
        coverage.setStatus(status);

        BuildsResponse buildsResponse = Mockito.mock(BuildsResponse.class);
        BuildResponse buildResponse = Mockito.mock(BuildResponse.class);
        StatisticsResponse statisticsResponse = Mockito.mock(StatisticsResponse.class);

        given(buildResponse.getStatus()).willReturn("SUCCESS");
        given(buildResponse.getId()).willReturn(buildId);
        given(buildsResponse.getFirstBuild()).willReturn(buildResponse);
        given(teamCityClient.retrieveBuildInformation(buildConfigId, false)).willReturn(buildsResponse);
        given(teamCityClient.retrieveBuildStatistics(buildId)).willReturn(statisticsResponse);

        // act
        service.updateCoverageInformation(coverage);

        // assert
        verify(teamCityClient).retrieveBuildStatistics(buildId);

    }
}
