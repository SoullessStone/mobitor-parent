package ch.mobi.mobitor.plugin.teamcity.service.client.domain.project;

/*-
 * §
 * mobitor-plugin-teamcity
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.junit.Assert.assertThat;

public class ProjectBuildTypesResponseDeserializeTest {

    @Test
    public void testProjectBuildTypesResponseDeserialize() throws IOException {
        // arrange
        ProjectBuildTypesResponse pr = createBuildResponse("project-build-types-response-example.json");

        // assert
        assertNotNull(pr);
        assertThat(pr.getCount(), is(2L));
        assertThat(pr.getProjectBuildTypeResponses(), is(notNullValue()));
        assertThat(pr.getProjectBuildTypeResponses().size(), is(2));

        ProjectBuildTypeResponse firstProjectBuildTypeResponse = pr.getProjectBuildTypeResponses().get(0);
        assertThat(firstProjectBuildTypeResponse.getId(), is("Zug_V2_E2e_v_Chrome_einfacher_fall"));
        assertThat(firstProjectBuildTypeResponse.getHref(), is("/httpAuth/app/rest/buildTypes/id:Zug_V2_E2e_v_Chrome_einfacher_fall"));
        assertThat(firstProjectBuildTypeResponse.getName(), is("einfacher_fall"));

        ProjectBuildTypeResponse secondProjectBuildTypeResponse = pr.getProjectBuildTypeResponses().get(1);
        assertThat(secondProjectBuildTypeResponse.getId(), is("Zug_V2_E2e_v_Chrome_Szenario01"));
        assertThat(secondProjectBuildTypeResponse.getHref(), is("/httpAuth/app/rest/buildTypes/id:Zug_V2_E2e_v_Chrome_Szenario01"));
        assertThat(secondProjectBuildTypeResponse.getName(), is("Szenario01"));

    }

    private ProjectBuildTypesResponse createBuildResponse(String filename) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        File screenConfigFile = ResourceUtils.getFile(ResourceUtils.CLASSPATH_URL_PREFIX + filename);
        // act
        return mapper.readValue(screenConfigFile, ProjectBuildTypesResponse.class);
    }

}
