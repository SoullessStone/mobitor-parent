package ch.mobi.mobitor.plugin.teamcity.service.client;

/*-
 * §
 * mobitor-plugin-teamcity
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.containing;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.givenThat;
import static com.github.tomakehurst.wiremock.client.WireMock.matching;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static com.github.tomakehurst.wiremock.client.WireMock.verify;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;

@RunWith(MockitoJUnitRunner.class)
public class TeamCityClientTest {

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(options().dynamicPort());
    private TeamCityConfiguration configuration = new TeamCityConfiguration();
    private TeamCityClient client;

    @Before
    public void setUp() {
        configuration.setUsername("user");
        configuration.setPassword("password");
        configuration.setBaseUrl("http://localhost:" + wireMockRule.port() + "/TeamCity");
        client = new TeamCityClient(configuration);
    }

    @Test
    public void retrieveBuildInformation_should_ask_only_defaultbranch() {
        // arrange
        givenThat(
                get(urlMatching("/TeamCity/httpAuth/app/rest/builds.*"))
                .withQueryParam("locator", matching("buildType:([a-zA-Z0-9_\\-]+),running:any"))
                .withQueryParam("count", equalTo("1"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody("teamcity-project-buildType-success.json")));
        // act
        client.retrieveBuildInformation("JUnitConfigId", false);
        // assert
        verify(getRequestedFor(urlMatching("/TeamCity/httpAuth/app/rest/builds.*"))
                .withQueryParam("locator", equalTo("buildType:JUnitConfigId,running:any"))
                .withQueryParam("count", containing("1")));
    }

    @Test
    public void retrieveBuildInformation_should_ask_anybranches() {
        // arrange
        givenThat(
                get(urlMatching("/TeamCity/httpAuth/app/rest/builds.*"))
                        .withQueryParam("locator", matching("buildType:([a-zA-Z0-9_\\-\\,\\:]+)"))
                        .withQueryParam("count", equalTo("1"))
                        .willReturn(aResponse()
                                .withStatus(200)
                                .withHeader("Content-Type", "application/json")
                                .withBody("teamcity-project-buildType-success.json")));
        // act
        client.retrieveBuildInformation("JUnitConfigId", true);
        // assert
        verify(getRequestedFor(urlMatching("/TeamCity/httpAuth/app/rest/builds.*"))
                .withQueryParam("locator", equalTo("buildType:JUnitConfigId,running:any,branch:default:any"))
                .withQueryParam("count", containing("1")));
    }
}
