package ch.mobi.mobitor.plugin.teamcity.service.client;

/*-
 * §
 * mobitor-plugin-teamcity
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.teamcity.domain.TeamCityBuildInformation;
import ch.mobi.mobitor.plugin.teamcity.domain.TeamCityCoverageInformation;
import ch.mobi.mobitor.plugin.teamcity.domain.TeamCityProjectInformation;
import ch.mobi.mobitor.plugin.teamcity.service.client.domain.BuildResponse;
import ch.mobi.mobitor.plugin.teamcity.service.client.domain.BuildsResponse;
import ch.mobi.mobitor.plugin.teamcity.service.client.domain.StatisticsResponse;
import ch.mobi.mobitor.plugin.teamcity.service.client.domain.project.ProjectBuildTypeResponse;
import ch.mobi.mobitor.plugin.teamcity.service.client.domain.project.ProjectBuildTypesResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import static java.lang.Math.min;


@Service
public class TeamCityInformationProviderService {

    private static final Logger LOG = LoggerFactory.getLogger(TeamCityInformationProviderService.class);

    private final TeamCityClient teamCityClient;

    @Autowired
    public TeamCityInformationProviderService(TeamCityClient teamCityClient) {
        this.teamCityClient = teamCityClient;
    }

    public void updateProjectInformation(TeamCityProjectInformation project) {
        ProjectBuildTypesResponse projectBuildTypesResponse = teamCityClient.retrieveProjectInformation(project.getProjectId());
        if (projectBuildTypesResponse == null) {
            LOG.error("TeamCity Project " + project.getId() + " cannot be found!");
            return;
        }
        List<ProjectBuildTypeResponse> projectBuildTypeResponses = projectBuildTypesResponse.getProjectBuildTypeResponses();

        List<BuildsResponse> buildsResponses = new ArrayList<>();

        for (ProjectBuildTypeResponse projectBuildTypeResponse : projectBuildTypeResponses) {
            buildsResponses.add(teamCityClient.retrieveBuildInformation(projectBuildTypeResponse.getId(), project.isShowAnyBranch()));
        }

        AtomicInteger numberOfSuccess = new AtomicInteger();
        AtomicInteger numberOfFailure = new AtomicInteger();
        AtomicBoolean isRunning = new AtomicBoolean(false);
        AtomicInteger percentageComplete = new AtomicInteger(100);
        buildsResponses.stream()
                       .filter(Objects::nonNull)
                       .forEach(buildsResponse -> {
            if (buildsResponse.isFirstBuildSuccessfully()) {
                numberOfSuccess.getAndIncrement();
            } else {
                numberOfFailure.getAndIncrement();
            }

            isRunning.set(isRunning.get() || buildsResponse.getFirstBuild().isRunning());

            if (buildsResponse.getFirstBuild().isRunning()) {
                int percentageOfBuild = buildsResponse.getFirstBuild().getPercentageComplete();
                percentageComplete.set(min(percentageOfBuild, percentageComplete.get()));
           }
        });

        project.setStatus(numberOfFailure.get() == 0 ? "SUCCESS" : "FAILURE");
        project.setJobsPassed(numberOfSuccess.get());
        project.setJobsFailed(numberOfFailure.get());
        project.setJobsTotal(buildsResponses.size());
        project.setWebUrl(teamCityClient.getProjectWebUrl(project.getProjectId()));
        project.setRunning(isRunning.get());
        project.setPercentageComplete(percentageComplete.get());
    }

    public void updateBuildInformation(TeamCityBuildInformation build) {
        BuildsResponse buildsResponse = teamCityClient.retrieveBuildInformation(build.getConfigId(), build.isShowAnyBranch());
        if (buildsResponse != null) {
            build.setUpdated(new Date());
            BuildResponse firstBuild = buildsResponse.getFirstBuild();
            if (firstBuild != null) {
                build.setState(firstBuild.getState()); // finished,running
                build.setNumber(firstBuild.getNumber()); // 1.20.0-xxx
                build.setStatus(firstBuild.getStatus()); // SUCCESS
                build.setWebUrl(firstBuild.getWebUrl());
                build.setPercentageComplete(firstBuild.getPercentageComplete());
                build.setId(firstBuild.getId());

                if (!build.isSuccessful()) {
                    LOG.debug("Broken build, retrieving statistics for: " + firstBuild.getBuildTypeId());
                    // retrieve statistics:
                    StatisticsResponse statsResponse = teamCityClient.retrieveBuildStatistics(build.getId());
                    if (statsResponse != null) {
                        build.setTestsTotal(statsResponse.testsTotal());
                        build.setTestsPassed(statsResponse.testsPassed());
                        build.setTestsFailed(statsResponse.testsFailed());
                        build.setTestsIgnored(statsResponse.testsIgnored());
                    }
                }
            }

            LOG.debug("updated teamcity build information for: " + build.getConfigId());
        } else {
            LOG.warn("could not update teamcity build information for: " + build.getConfigId());
        }
    }

    public void updateCoverageInformation(TeamCityCoverageInformation coverage) {
        BuildsResponse buildsResponse = teamCityClient.retrieveBuildInformation(coverage.getConfigId(), false);
        if (buildsResponse != null) {
            coverage.setUpdated(new Date());
            BuildResponse firstBuild = buildsResponse.getFirstBuild();

            if (firstBuild != null) {
                coverage.setId(firstBuild.getId());
                coverage.setStatus(firstBuild.getStatus());
                coverage.setWebUrl(firstBuild.getWebUrl());

                if (coverage.isSuccessful()) {
                    LOG.debug("Retrieving coverage for: " + firstBuild.getBuildTypeId());

                    // retrieve coverage:
                    StatisticsResponse statsResponse = teamCityClient.retrieveBuildStatistics(coverage.getId());
                    if (statsResponse != null) {
                        coverage.setCoverage(statsResponse.coverage());
                    }
                }
                LOG.debug("updated teamcity coverage information for: " + coverage.getConfigId());
            }

        } else {
            LOG.warn("could not update teamcity build information for: " + coverage.getConfigId());
        }
    }
}
