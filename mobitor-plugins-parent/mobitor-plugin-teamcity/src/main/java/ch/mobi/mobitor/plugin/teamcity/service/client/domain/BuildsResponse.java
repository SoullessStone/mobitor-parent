package ch.mobi.mobitor.plugin.teamcity.service.client.domain;

/*-
 * §
 * mobitor-plugin-teamcity
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

/**
 * represents the {@code <builds></builds>} section of a TeamCity response that queries {@code /app/rest/builds}
 *
 * ex: {@code https://TeamCity/httpAuth/app/rest/builds?locator=buildType:Jap_JavaBatch_Pom_Lib,running:any&count=1}
 *
 * @see BuildResponse
 */
public class BuildsResponse implements Serializable {

    @JsonProperty private long id;
    @JsonProperty private long count;
    @JsonProperty private String href;
    @JsonProperty private String nextHref;

    @JsonProperty("build") private List<BuildResponse> builds;

    @JsonCreator
    public BuildsResponse() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCount() {
        return count;
    }

    public void setCount(long count) {
        this.count = count;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String getNextHref() {
        return nextHref;
    }

    public void setNextHref(String nextHref) {
        this.nextHref = nextHref;
    }

    public List<BuildResponse> getBuilds() {
        return builds;
    }

    public void setBuilds(List<BuildResponse> builds) {
        this.builds = builds;
    }

    @JsonIgnore
    public BuildResponse getFirstBuild() {
        if (this.builds != null && this.builds.size() > 0) {
            return this.builds.get(0);
        }
        return null;
    }

    @JsonIgnore
    public boolean isFirstBuildSuccessfully() {
        if (this.builds != null && this.builds.size() > 0) {
            return this.builds.get(0).isSuccessful();
        }
        return false;
    }
}
