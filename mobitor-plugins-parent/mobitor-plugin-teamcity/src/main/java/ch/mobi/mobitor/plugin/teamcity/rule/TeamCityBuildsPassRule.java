package ch.mobi.mobitor.plugin.teamcity.rule;

/*-
 * §
 * mobitor-plugin-teamcity
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.PipelineRule;
import ch.mobi.mobitor.domain.screen.Pipeline;
import ch.mobi.mobitor.domain.screen.RuleEvaluation;
import ch.mobi.mobitor.domain.screen.RuleViolationSeverity;
import ch.mobi.mobitor.domain.screen.ServerContext;
import ch.mobi.mobitor.plugin.teamcity.domain.TeamCityBuildInformation;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

import static ch.mobi.mobitor.plugin.teamcity.domain.TeamCityBuildInformation.TEAM_CITY_BUILD;

/**
 * Verify that TeamCity builds are always passing.
 */
@Component
public class TeamCityBuildsPassRule implements PipelineRule {

    @Override
    public void evaluateRule(Pipeline pipeline, RuleEvaluation re) {
        Map<String, ServerContext> serverContextMap = pipeline.getServerContextMap();
        for (Map.Entry<String, ServerContext> entry : serverContextMap.entrySet()) {
            String env = entry.getKey();
            ServerContext serverContext = entry.getValue();

            List<TeamCityBuildInformation> buildInfos = serverContext.getMatchingInformation(TEAM_CITY_BUILD);

            buildInfos.stream().filter(this::ruleViolated).forEach(tcInfo -> re.addViolation(env, tcInfo, RuleViolationSeverity.ERROR, "Build not successful: " + tcInfo.getConfigId()));
        }
    }

    @Override
    public boolean validatesType(String type) {
        return TEAM_CITY_BUILD.equals(type);
    }

    private boolean ruleViolated(TeamCityBuildInformation teamCityBuildInformation) {
        return !teamCityBuildInformation.isSuccessful();
    }

}
