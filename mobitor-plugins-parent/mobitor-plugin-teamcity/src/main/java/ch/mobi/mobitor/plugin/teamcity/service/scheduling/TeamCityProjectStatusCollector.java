package ch.mobi.mobitor.plugin.teamcity.service.scheduling;

/*-
 * §
 * mobitor-plugin-teamcity
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */


import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.teamcity.TeamCityProjectsPlugin;
import ch.mobi.mobitor.plugin.teamcity.domain.TeamCityProjectInformation;
import ch.mobi.mobitor.plugin.teamcity.service.client.TeamCityInformationProviderService;
import ch.mobi.mobitor.plugins.api.model.ScreensModel;
import ch.mobi.mobitor.plugins.api.service.CollectorMetricService;
import ch.mobi.mobitor.plugins.api.service.RuleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

import static ch.mobi.mobitor.plugin.teamcity.domain.TeamCityProjectInformation.TEAM_CITY_PROJECT;
import static ch.mobi.mobitor.plugin.teamcity.service.scheduling.TeamCityBuildStatusCollector.CACHE_NAME_TEAMCITY_BUILDS;
import static ch.mobi.mobitor.plugin.teamcity.service.scheduling.TeamCityBuildStatusCollector.CACHE_NAME_TEAMCITY_PROJECTS;

@Component
@ConditionalOnBean(TeamCityProjectsPlugin.class)
public class TeamCityProjectStatusCollector {

    private static final Logger LOG = LoggerFactory.getLogger(TeamCityProjectStatusCollector.class);

    private final TeamCityInformationProviderService teamCityInformationProviderService;
    private final ScreensModel screensModel;
    private final RuleService ruleService;
    private final CollectorMetricService collectorMetricService;

    @Autowired
    public TeamCityProjectStatusCollector(ScreensModel screensModel,
                                          TeamCityInformationProviderService teamCityInformationProviderService,
                                          RuleService ruleService,
                                          CollectorMetricService collectorMetricService) {
        this.screensModel = screensModel;
        this.teamCityInformationProviderService = teamCityInformationProviderService;
        this.ruleService = ruleService;
        this.collectorMetricService = collectorMetricService;
    }

    @Scheduled(fixedDelayString = "${scheduling.pollingIntervalMs.teamCityProjectStatusInformationPollingInterval}", initialDelayString = "${scheduling.pollingInitialDelayMs.second}")
    @CacheEvict(cacheNames = {CACHE_NAME_TEAMCITY_PROJECTS, CACHE_NAME_TEAMCITY_BUILDS}, allEntries = true)
    public void collectTeamCityProjectInformationForScreens() {
        long start = System.currentTimeMillis();
        try {
            this.screensModel.getAvailableScreens().parallelStream().forEach(this::populateProjectInformation);
        } catch (Exception e) {
            LOG.error("Could not load TeamCity Project Status!", e);
            LOG.error("ScreensModel" + screensModel);
            LOG.error("Available Screens" + (screensModel == null ? "no screensModel" : screensModel.getAvailableScreens()));
        }
        long stop = System.currentTimeMillis();
        long duration = stop - start;

        LOG.info("reading TC project status took: " + (duration) + "ms");
        collectorMetricService.submitCollectorDuration("tc.project", duration);
        collectorMetricService.updateLastRunCompleted(TEAM_CITY_PROJECT);
    }

    private void populateProjectInformation(Screen screen) {
        try {
            List<TeamCityProjectInformation> teamCityProjectInformationList = screen.getMatchingInformation(TEAM_CITY_PROJECT);
            teamCityProjectInformationList.forEach(teamCityInformationProviderService::updateProjectInformation);

            ruleService.updateRuleEvaluation(screen, TEAM_CITY_PROJECT);

            screen.setRefreshDate(TEAM_CITY_PROJECT, new Date());
        } catch (Exception e) {
            LOG.error("Could not populate TeamCity project information!", e);
        }
    }

}
