package ch.mobi.mobitor.plugin.swd.service.client.domain;

/*-
 * §
 * mobitor-plugin-swd
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

import static ch.mobi.mobitor.plugin.swd.service.client.domain.SwdDeploymentTest.aDeployment;
import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;

public class SwdDeploymentListTest {

    public static final String P = "P";
    public static final String T = "T";
    private SwdDeployment deploymentP;

    private SwdDeploymentList deploymentList;

    @Before
    public void setUp() {
        deploymentP = aDeployment(P);
        SwdDeployment deploymentT = aDeployment(T);
        SwdDeployment deploymentT2 = aDeployment(T);
        deploymentList = new SwdDeploymentList(asList(deploymentP, deploymentT, deploymentT2));
    }

    @Test
    public void findDeploymentByEnvironmentShouldReturnPatch() {
        //when
        Optional<SwdDeployment> result = deploymentList.findDeploymentByEnvironment(P);

        //then
        assertThat(result).isPresent()
                          .hasValue(deploymentP);
    }

    @Test
    public void findDeploymentByEnvironmentShouldNotFindPatch() {
        //when
        Optional<SwdDeployment> result = deploymentList.findDeploymentByEnvironment("non-existing");

        //then
        assertThat(result).isEmpty();
    }

    @Test
    public void findDeploymentByEnvironmentShouldThrowExceptionIfMultiplePatchesWereFound() {
        //when
        Optional<SwdDeployment> deploymentByEnvironment = deploymentList.findDeploymentByEnvironment(T);

        //then
        assertThat(deploymentByEnvironment).isNotEmpty();
    }

    @Test(expected = NullPointerException.class)
    public void findDeploymentByEnvironmentShouldThrowExceptionForNullParameter() {
        //when
        deploymentList.findDeploymentByEnvironment(null);

        //then exception
    }
}
