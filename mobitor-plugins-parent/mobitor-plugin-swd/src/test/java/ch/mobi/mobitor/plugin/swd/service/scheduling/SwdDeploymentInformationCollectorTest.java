package ch.mobi.mobitor.plugin.swd.service.scheduling;

/*-
 * §
 * mobitor-plugin-swd
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.swd.domain.SwdDeploymentInformation;
import ch.mobi.mobitor.plugin.swd.service.client.SwdDeploymentInformationProviderService;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformation;
import ch.mobi.mobitor.plugins.api.model.ScreensModel;
import ch.mobi.mobitor.plugins.api.service.RuleService;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static java.util.Arrays.asList;

public class SwdDeploymentInformationCollectorTest {

    @Mock
    private ScreensModel screensModel;

    @Mock
    private RuleService ruleService;

    @Mock
    private SwdDeploymentInformationProviderService providerService;

    @Test
    public void collectSwdDeploymentInformationShouldNotUpdateInformationIfNoInformationOnScreenWasFound() {
        //given
        MockitoAnnotations.initMocks(this);
        SwdDeploymentInformationCollector sut = new SwdDeploymentInformationCollector(providerService, screensModel, ruleService);
        Mockito.when(screensModel.getAvailableScreens()).thenReturn(asList(Mockito.mock(Screen.class), Mockito.mock(Screen.class), Mockito.mock(Screen.class)));

        //when
        sut.collectSwdDeploymentInformation();

        //then
        Mockito.verify(providerService, Mockito.never()).updateSwdDeploymentInformation(ArgumentMatchers.any());
        Mockito.verify(ruleService, Mockito.never()).updateRuleEvaluation(ArgumentMatchers.any(), ArgumentMatchers.any());
    }

    @Test
    public void collectSwdDeploymentInformationShouldUpdateInformationForAllFoundScreens() {
        //given
        MockitoAnnotations.initMocks(this);
        SwdDeploymentInformationCollector sut = new SwdDeploymentInformationCollector(providerService, screensModel, ruleService);
        List<Screen> screenList = asList(aScreenMock(), aScreenMock(), aScreenMock());
        Mockito.when(screensModel.getAvailableScreens()).thenReturn(screenList);

        //when
        sut.collectSwdDeploymentInformation();

        //then
        Mockito.verify(providerService, Mockito.times(screenList.size())).updateSwdDeploymentInformation(ArgumentMatchers.any());
        Mockito.verify(ruleService, Mockito.times(screenList.size())).updateRuleEvaluation(ArgumentMatchers.any(), ArgumentMatchers.any());
    }

    private Screen aScreenMock() {
        Screen screen = Mockito.mock(Screen.class);
        ApplicationInformation applicationInformation = new SwdDeploymentInformation("foo.zip", "P");
        Mockito.when(screen.getMatchingInformation(ArgumentMatchers.any())).thenReturn(asList(applicationInformation, applicationInformation));
        return screen;
    }
}
