package ch.mobi.mobitor.plugin.swd.service.client;

/*-
 * §
 * mobitor-plugin-swd
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.swd.service.client.domain.SwdDeployment;
import ch.mobi.mobitor.plugin.swd.service.client.domain.SwdDeploymentList;
import ch.mobi.mobitor.plugin.swd.service.client.domain.SwdPatch;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.jetbrains.annotations.NotNull;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.givenThat;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.groups.Tuple.tuple;
import static org.mockito.BDDMockito.given;

@RunWith(SpringRunner.class)
public class SwdClientTest {

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(options().dynamicPort());

    @MockBean
    private SwdConfigurationService configurationService;

    @Test
    public void retrieveSwdDeploymentInformationShouldReturnSwdDeployments() {
        //given
        SwdClient client = setupClient();

        //when
        SwdDeploymentList result = client.retrieveSwdDeploymentInformation();

        //then
        assertThat(result.getDeployments()).extracting(SwdDeployment::getEnvironment).containsExactly("P", "B");
        List<SwdPatch> patches = result.findDeploymentByEnvironment("B")
                                       .map(SwdDeployment::getPatches)
                                       .orElse(emptyList());
        assertThat(patches).extracting(SwdPatch::getFileName, SwdPatch::getRevision)
                           .containsExactlyInAnyOrder(
                                   tuple("FileName1.zip", "")
                                   , tuple("FileName1804Path.zip", "15308.")
                                   , tuple("FileName1804NewestPath.zip", "15308.")
                                   , tuple("FileName1804MassivePath.zip", "15308."));
    }

    @NotNull
    private SwdClient setupClient() {
        String baseUrl = "http://localhost:" + wireMockRule.port();
        given(configurationService.getBaseUrl()).willReturn(baseUrl);

        givenThat(get(urlMatching("/ELAN/ELAN-Patch/GetStatus"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("swd-deployments.json")
                )
        );
        return new SwdClient(configurationService);
    }
}
