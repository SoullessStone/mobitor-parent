package ch.mobi.mobitor.plugin.swd.service.client;

/*-
 * §
 * mobitor-plugin-swd
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.swd.domain.SwdDeploymentInformation;
import ch.mobi.mobitor.plugin.swd.service.client.domain.SwdDeployment;
import ch.mobi.mobitor.plugin.swd.service.client.domain.SwdDeploymentList;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDateTime;

import static ch.mobi.mobitor.plugin.swd.service.client.domain.SwdDeploymentTest.aDeployment;
import static ch.mobi.mobitor.plugin.swd.service.client.domain.SwdPatchTest.aPatch;
import static java.time.LocalDateTime.of;
import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SwdDeploymentInformationProviderServiceTest {

    private SwdClient client;
    private SwdDeploymentInformationProviderService service;

    private SwdDeploymentInformation informationP;
    private SwdDeploymentInformation informationT;
    private SwdDeploymentInformation informationX;

    @Before
    public void setUp() {
        client = mock(SwdClient.class);
        service = new SwdDeploymentInformationProviderService(client);

        informationP = new SwdDeploymentInformation("foo.zip", "P");
        informationT = new SwdDeploymentInformation("test.zip", "T");
        informationX = new SwdDeploymentInformation("x.zip", "X");
    }

    @Test
    public void updateSwdDeploymentInformationShouldNotUpdateInformationIfClientDoesNotReturnAResponse() {
        //given
        when(client.retrieveSwdDeploymentInformation()).thenReturn(new SwdDeploymentList(emptyList()));

        //when
        service.updateSwdDeploymentInformation(asList(informationP, informationT, informationX));

        //then
        assertThat(informationP.getRevision()).isNull();
        assertThat(informationP.getPatchDate()).isNull();
        assertThat(informationT.getRevision()).isNull();
        assertThat(informationT.getPatchDate()).isNull();
        assertThat(informationX.getRevision()).isNull();
        assertThat(informationX.getPatchDate()).isNull();
    }

    @Test
    public void updateSwdDeploymentInformationShouldUpdateInformationMatchingEnvironmentAndFileName() {
        //given
        LocalDateTime patchDate = of(2011, 12, 13, 14, 15, 16);
        SwdDeployment deploymentP = aDeployment("P",
                aPatch("foo.zip", "123456.", patchDate),
                aPatch("bar.zip", "234567.", patchDate));
        SwdDeployment deploymentT = aDeployment("T",
                aPatch("test.zip", "345678.", patchDate),
                aPatch("test.tar.gz", "456789.", patchDate));
        SwdDeployment deploymentY = aDeployment("Y",
                aPatch("y.zip", "111.", patchDate),
                aPatch("y.tar.gz", "222.", patchDate));
        when(client.retrieveSwdDeploymentInformation()).thenReturn(new SwdDeploymentList(asList(deploymentP, deploymentT, deploymentY)));

        //when
        service.updateSwdDeploymentInformation(asList(informationP, informationT, informationX));

        //then
        assertThat(informationP.getRevision()).isEqualTo("123456.");
        assertThat(informationP.getPatchDate()).isEqualTo(patchDate);
        assertThat(informationT.getRevision()).isEqualTo("345678.");
        assertThat(informationT.getPatchDate()).isEqualTo(patchDate);
        assertThat(informationX.getRevision()).isNull();
        assertThat(informationX.getPatchDate()).isNull();
    }

}
