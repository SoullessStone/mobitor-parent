package ch.mobi.mobitor.plugin.swd.service.client.domain;

/*-
 * §
 * mobitor-plugin-swd
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

import static ch.mobi.mobitor.plugin.swd.service.client.domain.SwdPatchTest.aPatch;
import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;

public class SwdDeploymentTest {

    private static final String FILE_NAME1 = "file1.zip";
    private static final String FILE_NAME2 = "file2.zip";

    private SwdDeployment deployment;
    private SwdPatch patch1;

    @Before
    public void setUp() {
        patch1 = aPatch(FILE_NAME1);
        SwdPatch patch2 = aPatch(FILE_NAME2);
        SwdPatch patch3 = aPatch(FILE_NAME2);
        deployment = new SwdDeployment();
        asList(patch1, patch2, patch3).forEach(patch -> deployment.addPatch(patch));
    }

    @Test
    public void findPatchByFileNameShouldReturnPatch() {
        //when
        Optional<SwdPatch> result = deployment.findPatchByFileName(FILE_NAME1);

        //then
        assertThat(result).isPresent().hasValue(patch1);
    }

    @Test
    public void findPatchByFileNameShouldNotFindPatch() {
        //when
        Optional<SwdPatch> result = deployment.findPatchByFileName("non-existing");

        //then
        assertThat(result).isEmpty();
    }

    @Test(expected = IllegalStateException.class)
    public void findPatchByFileNameShouldThrowExceptionIfMultiplePatchesWereFound() {
        //when
        deployment.findPatchByFileName(FILE_NAME2);

        //then exception
    }

    @Test(expected = NullPointerException.class)
    public void findPatchByFileNameShouldThrowExceptionForNullParameter() {
        //when
        deployment.findPatchByFileName(null);

        //then exception
    }

    @NotNull
    public static SwdDeployment aDeployment(String environment, SwdPatch... patches) {
        SwdDeployment deployment = new SwdDeployment();
        deployment.setEnvironment(environment);
        for (SwdPatch patch : patches) {
            deployment.addPatch(patch);
        }
        return deployment;
    }
}
