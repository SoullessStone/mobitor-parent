package ch.mobi.mobitor.plugin.swd.service.client;

/*-
 * §
 * mobitor-plugin-swd
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.swd.domain.SwdDeploymentInformation;
import ch.mobi.mobitor.plugin.swd.service.client.domain.SwdDeploymentList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class SwdDeploymentInformationProviderService {

    private final SwdClient swdClient;

    @Autowired
    public SwdDeploymentInformationProviderService(SwdClient swdClient) {
        this.swdClient = swdClient;
    }

    public void updateSwdDeploymentInformation(List<SwdDeploymentInformation> information) {
        SwdDeploymentList deployments = swdClient.retrieveSwdDeploymentInformation();
        information.forEach(i -> deployments.findDeploymentByEnvironment(i.getEnvironment())
                                            .flatMap(deployment -> deployment.findPatchByFileName(i.getApplicationName()))
                                            .ifPresent(patch -> {
                                                i.setRevision(patch.getRevision());
                                                i.setPatchDate(LocalDateTime.of(patch.getDate(), patch.getTime()));
                                            }));
    }
}
