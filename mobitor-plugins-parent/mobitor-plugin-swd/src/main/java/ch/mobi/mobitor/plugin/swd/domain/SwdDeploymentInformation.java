package ch.mobi.mobitor.plugin.swd.domain;

/*-
 * §
 * mobitor-plugin-swd
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.information.VersionInformation;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformation;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class SwdDeploymentInformation implements ApplicationInformation, VersionInformation {

    public static final String SWD_DEPLOYMENT = "swd_deployment";

    private final String applicationName;
    private final String environment;

    private LocalDateTime patchDate;
    private String revision;

    public SwdDeploymentInformation(String applicationName, String environment) {
        this.applicationName = applicationName;
        this.environment = environment;
    }


    @Override
    public String getType() {
        return SWD_DEPLOYMENT;
    }

    @Override
    public boolean hasInformation() {
        return true;
    }

    @Override
    public String getVersion() {
        return revision;
    }

    public String patchDate() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss");
        return (patchDate == null) ? null : patchDate.format(formatter);
    }

    public String getApplicationName() {
        return applicationName;
    }

    public String getEnvironment() {
        return environment;
    }

    public LocalDateTime getPatchDate() {
        return patchDate;
    }

    public void setPatchDate(LocalDateTime patchDate) {
        this.patchDate = patchDate;
    }

    public String getRevision() {
        return revision;
    }

    public void setRevision(String revision) {
        this.revision = revision;
    }
}
