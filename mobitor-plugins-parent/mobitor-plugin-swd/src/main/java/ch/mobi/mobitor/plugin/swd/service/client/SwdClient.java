package ch.mobi.mobitor.plugin.swd.service.client;

/*-
 * §
 * mobitor-plugin-swd
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.swd.service.client.domain.SwdDeployment;
import ch.mobi.mobitor.plugin.swd.service.client.domain.SwdDeploymentList;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;

@Component
public class SwdClient {

    private final SwdConfigurationService configurationService;

    @Autowired
    public SwdClient(SwdConfigurationService configurationService) {
        this.configurationService = configurationService;
    }

    @NotNull
    public SwdDeploymentList retrieveSwdDeploymentInformation() {
        String url = configurationService.getBaseUrl() + "/ELAN/ELAN-Patch/GetStatus";

        WebClient client = WebClient.create();

        Mono<SwdDeployment[]> swdDeploymentsMono = client.get()
                                           .uri(url)
                                           .accept(MediaType.APPLICATION_JSON)
                                           .retrieve()
                                           .bodyToMono(SwdDeployment[].class);

        SwdDeployment[] swdDeployments = swdDeploymentsMono.block();

        return new SwdDeploymentList(swdDeployments == null ? emptyList() : asList(swdDeployments));
    }

}
