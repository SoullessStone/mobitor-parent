package ch.mobi.mobitor.plugin.jira.service.client;

/*-
 * §
 * mobitor-plugin-jira
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.jira.service.client.domain.FilterResponse;
import ch.mobi.mobitor.plugin.jira.service.client.domain.FilterResultResponse;
import ch.mobi.mobitor.plugin.jira.service.client.domain.IssueResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.hamcrest.collection.IsCollectionWithSize;
import org.hamcrest.collection.IsEmptyCollection;
import org.junit.Rule;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.givenThat;
import static com.github.tomakehurst.wiremock.client.WireMock.verify;
import static com.jayway.jsonpath.matchers.JsonPathMatchers.hasJsonPath;
import static com.jayway.jsonpath.matchers.JsonPathMatchers.isJson;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;


public class JiraClientTest {

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(WireMockConfiguration.options().dynamicPort());

    @Test
    public void testRetrieveFilter() {
        // arrange
        givenThat(get(WireMock.urlMatching("/rest/api/2/filter/[0-9]+"))
                                   .withHeader("Accept", WireMock.equalTo("application/json"))
                                   .willReturn(WireMock.aResponse()
                                                       .withStatus(200)
                                                       .withHeader("Content-Type", "application/json")
                                                       .withBodyFile("jira-filter-11121-response.json")
                )
        );

        String baseUrl = "http://localhost:" + wireMockRule.port();

        JiraConfiguration jiraConfiguration = createJiraConfiguration(baseUrl);

        JiraClient jiraClient = new JiraClient(jiraConfiguration);
        String filterId = "11121";

        // act
        FilterResponse filterResponse = jiraClient.retrieveFilter(filterId);

        // assert
        assertThat(filterResponse, is(not(nullValue())));
        assertThat(filterResponse.getId(), is(not(nullValue())));
        assertThat(filterResponse.getName(), is(not(nullValue())));
        assertThat(filterResponse.getViewUrl(), is(not(nullValue())));
        assertThat(filterResponse.getSearchUrl(), is(not(nullValue())));
    }

    @Test
    public void testRetrieveFilterResult() {
        // arrange
        String baseUrl = "http://localhost:" + wireMockRule.port();
        JiraConfiguration jiraConfiguration = createJiraConfiguration(baseUrl);

        givenThat(get(WireMock.urlMatching("/rest/api/2/search?(.*)"))
                                   .withHeader("Accept", WireMock.equalTo("application/json"))
                                   .withQueryParam("jql", WireMock.matching("(.+)"))
                                   .withBasicAuth(jiraConfiguration.getUsername(), jiraConfiguration.getPassword())
                                   .willReturn(WireMock.aResponse()
                                                       .withStatus(200)
                                                       .withHeader("Content-Type", "application/json")
                                                       .withBodyFile("jira-filter-11121-search-response.json")
                )
        );

        JiraClient jiraClient = new JiraClient(jiraConfiguration);
        String searchUrl = baseUrl + "/rest/api/2/search?jql=test-jql-pseudo-query";

        // act
        FilterResultResponse filterResultResponse = jiraClient.retrieveFilterResult(searchUrl);

        // assert
        assertThat(filterResultResponse, is(not(nullValue())));
        assertThat(filterResultResponse.getTotal(), is(7L));
        assertThat(filterResultResponse.getIssues(), is(not(IsEmptyCollection.empty())));
        assertThat(filterResultResponse.getIssues().get(0).getFields().getPriority().getName(), is("Critical"));

        assertThat(filterResultResponse.countCriticalIssues(), is(1L));
        assertThat(filterResultResponse.countMajorIssues(), is(6L));
    }

    private JiraConfiguration createJiraConfiguration(String baseUrl) {
        JiraConfiguration jiraConfiguration = new JiraConfiguration();
        jiraConfiguration.setBaseUrl(baseUrl);
        jiraConfiguration.setUsername("test-username");
        jiraConfiguration.setPassword("test-password");
        return jiraConfiguration;
    }

    @Test
    public void testRetrieveIssue() {
        // arrange
        String baseUrl = "http://localhost:" + wireMockRule.port();
        JiraConfiguration jiraConfiguration = createJiraConfiguration(baseUrl);

        givenThat(get(WireMock.urlMatching("/rest/api/2/issue/JIRA-5671"))
                                   .withHeader("Accept", WireMock.equalTo("application/json"))
                                   .withBasicAuth(jiraConfiguration.getUsername(), jiraConfiguration.getPassword())
                                   .willReturn(WireMock.aResponse()
                                                       .withStatus(200)
                                                       .withHeader("Content-Type", "application/json")
                                                       .withBodyFile("jira-issue-jiraprj-5671.json")
                )
        );

        JiraClient jiraClient = new JiraClient(jiraConfiguration);

        // act
        IssueResponse issueResponse = jiraClient.retrieveIssue("JIRA-5671");

        // assert
        verify(WireMock.getRequestedFor(WireMock.urlMatching("/rest/api/2/issue/(.+)")));

        assertThat(issueResponse, is(not(nullValue())));
        assertThat(issueResponse.getFields(), is(not(nullValue())));


        assertThat(issueResponse.getFields().getUpdated(), is(not(nullValue())));
        assertThat(issueResponse.getFields().getResolutiondate(), is(not(nullValue())));
        assertThat(issueResponse.getFields().getCreated(), is(not(nullValue())));
    }

    @Test
    public void testRetrieveNonExistingIssue() {
        // arrange
        String baseUrl = "http://localhost:" + wireMockRule.port();
        JiraConfiguration jiraConfiguration = createJiraConfiguration(baseUrl);

        givenThat(get(WireMock.urlMatching("/rest/api/2/issue/JIRA-6666"))
                                   .withHeader("Accept", WireMock.equalTo("application/json"))
                                   .withBasicAuth(jiraConfiguration.getUsername(), jiraConfiguration.getPassword())
                                   .willReturn(WireMock.aResponse().withStatus(204))
        );

        JiraClient jiraClient = new JiraClient(jiraConfiguration);

        // act
        IssueResponse issueResponse = jiraClient.retrieveIssue("JIRA-6666");

        // assert
        verify(WireMock.getRequestedFor(WireMock.urlMatching("/rest/api/2/issue/(.+)")));

        assertThat(issueResponse, is(nullValue()));
    }

    @Test
    public void testCreateCustomFieldJson() throws JsonProcessingException {
        JiraClient client = new JiraClient(null);

        Set<String> set = new HashSet<>();
        set.add("env-a");
        set.add("env-b");
        set.add("env-c");
        String customFieldName = "customfield_junit_000";
        String json = client.createCustomFieldsJson(customFieldName, set);

        assertThat(json, isJson());
        assertThat(json, hasJsonPath("$.fields"));
        assertThat(json, hasJsonPath("$.fields." + customFieldName));
        assertThat(json, hasJsonPath("$.fields." + customFieldName + ".*", IsCollectionWithSize.hasSize(set.size())));
    }

}
