package ch.mobi.mobitor.plugin.jira.service.client;

/*-
 * §
 * mobitor-plugin-jira
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.jira.service.client.domain.IssueResponse;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

public class IssueFieldsDeserializerTest {

    @Test
    public void testJiraResponseDeserializer() throws IOException, URISyntaxException {
        // arrange
        String deploymentEnvsKey = "customfield_10700";

        String json = new String(Files.readAllBytes(Paths.get(getClass().getClassLoader().getResource("jira-example-issue-with-customfield.json").toURI())));

        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        // act
        IssueResponse issue = mapper.readValue(json, IssueResponse.class);


        // assert
        assertThat(issue.getFields(), is(not(nullValue())));
        assertThat(issue.getFields().getDeploymentEnvironments(deploymentEnvsKey), is(not(nullValue())));
        assertThat(issue.getFields().getDeploymentEnvironments(deploymentEnvsKey), is(not(empty())));
        assertThat(issue.getFields().getDeploymentEnvironments(deploymentEnvsKey), hasSize(1));
    }

}
