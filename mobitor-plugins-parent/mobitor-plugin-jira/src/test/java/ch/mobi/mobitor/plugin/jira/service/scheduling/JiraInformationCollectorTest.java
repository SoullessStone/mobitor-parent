package ch.mobi.mobitor.plugin.jira.service.scheduling;

/*-
 * §
 * mobitor-plugin-jira
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.jira.domain.JiraInformation;
import ch.mobi.mobitor.plugin.jira.service.client.JiraClient;
import ch.mobi.mobitor.plugin.jira.service.client.domain.FilterResponse;
import ch.mobi.mobitor.plugin.jira.service.client.domain.FilterResultResponse;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformation;
import ch.mobi.mobitor.plugins.api.model.ScreensModel;
import ch.mobi.mobitor.plugins.api.service.RuleService;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import static ch.mobi.mobitor.plugin.jira.domain.JiraInformation.JIRA;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


public class JiraInformationCollectorTest {

    @Test
    public void collectJiraFilterInformation() {
        // arrange
        RuleService ruleService = Mockito.mock(RuleService.class);
        ScreensModel screensModel = Mockito.mock(ScreensModel.class);
        Screen screen = Mockito.mock(Screen.class);

        JiraInformation jiraInfo = new JiraInformation();
        List<ApplicationInformation> jiraInfos = Collections.singletonList(jiraInfo);

        List<Screen> screenList = Collections.singletonList(screen);

        when(screensModel.getAvailableScreens()).thenReturn(screenList);
        when(screen.getMatchingInformation(ArgumentMatchers.eq(JIRA))).thenReturn(jiraInfos);

        FilterResponse filterResponse = new FilterResponse();
        filterResponse.setSearchUrl("http://localhost/junit/jira/searchFilter");
        FilterResultResponse filterResultResponse = new FilterResultResponse();

        JiraClient jiraClient = mock(JiraClient.class);
        when(jiraClient.retrieveFilter(any())).thenReturn(filterResponse);
        when(jiraClient.retrieveFilterResult(any())).thenReturn(filterResultResponse);

        JiraInformationCollector jiraCollector = new JiraInformationCollector(jiraClient, screensModel, ruleService);

        // act
        jiraCollector.collectJiraFilterInformation();

        // assert
        verify(screensModel).getAvailableScreens();
        verify(ruleService).updateRuleEvaluation(screen, JIRA);
        verify(screen).setRefreshDate(eq(JIRA), any(Date.class));
    }

}
