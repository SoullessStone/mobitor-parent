= Mobitor Jira Plugin

link:../../index.html[Back Mobitor Overview]

== Features

This plugin allows to show an information block that shows the result of a Jira filter.


== Configuration

.screen.json
[source,json]
----
"jiraFilters": [
  {
    "filterId": "123456",
    "serverName": "server_name_1",
    "applicationName": "application_name_1",
    "environment": "preprod"
  }
---

.Parameters
|===
| Name | Description | Required | Default

| filterId
| The Jira filter id (can be seen in the Url or the filter edit dialog in Jira). Will be a number (not a JQL string)
| yes
| empty

| serverName
| The server name that refers to the serverNames within the same screen, used to reference the position of this information block in the screen
| yes
| empty

| applicationName
| The application name that refers to the sub-row of the server name in the same screen. Used to reference the position if this information block in the screen
| yes
| empty

| environment
| Environment of this information block. Refers to the environments in the same screen. Used to reference the position in the screen (the column).
| yes
| empty
|===


== Result

image::jira-filter-information-block.png[]

Shows the number of issues of this Jira filter. Turns red for critical issues.



