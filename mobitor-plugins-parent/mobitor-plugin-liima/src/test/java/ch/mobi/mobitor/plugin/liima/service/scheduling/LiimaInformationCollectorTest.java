package ch.mobi.mobitor.plugin.liima.service.scheduling;

/*-
 * §
 * mobitor-plugin-liima
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.deployment.Deployment;
import ch.mobi.mobitor.domain.deployment.DeploymentApplication;
import ch.mobi.mobitor.domain.screen.Pipeline;
import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.domain.screen.ServerContext;
import ch.mobi.mobitor.plugin.liima.domain.LiimaInformation;
import ch.mobi.mobitor.plugin.test.domain.TestPipeline;
import ch.mobi.mobitor.plugin.test.domain.TestServerContext;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformation;
import ch.mobi.mobitor.plugins.api.model.ScreensModel;
import ch.mobi.mobitor.plugins.api.service.CollectorMetricService;
import ch.mobi.mobitor.plugins.api.service.RuleService;
import ch.mobi.mobitor.service.DeploymentInformationService;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static ch.mobi.mobitor.plugin.liima.domain.LiimaInformation.LIIMA;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anySet;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


public class LiimaInformationCollectorTest {

    private TestPipeline createPipeline(String serverName, String applicationName, String environment) {
        Map<String, ServerContext> serverContextMap = new HashMap<>();
        ApplicationInformation liimaInfo = new LiimaInformation(serverName, applicationName, environment);

        ServerContext testServerContext = new TestServerContext();
        testServerContext.addInformation(applicationName, liimaInfo);

        serverContextMap.put(applicationName, testServerContext);

        TestPipeline testPipeline = new TestPipeline(serverName, serverContextMap);
        return testPipeline;
    }

    @Test
    public void collectLiimaInformationForScreens() {
        // arrange
        RuleService ruleService = Mockito.mock(RuleService.class);
        ScreensModel screensModel = mock(ScreensModel.class);
        CollectorMetricService collectorMetricService = mock(CollectorMetricService.class);

        LiimaInformation liimaInformation = new LiimaInformation("server1", "application1", "Y");
        List<ApplicationInformation> liimaInfos = singletonList(liimaInformation);

        Screen screen = mock(Screen.class);

        TestPipeline pipeline1 = createPipeline("server1", "application1", "Y");
        TestPipeline pipeline2 = createPipeline("server2", "application2", "Y");

        List<Pipeline> pipelines = asList(pipeline1, pipeline2);
        when(screen.getPipelines()).thenReturn(pipelines);

        List<Screen> screensList = singletonList(screen);

        when(screensModel.getAvailableScreens()).thenReturn(screensList);
        when(screen.getMatchingInformation(eq(LIIMA), eq("Y"), eq("server1"), eq("application1"))).thenReturn(liimaInfos);

        DeploymentInformationService deploymentInformationService = Mockito.mock(DeploymentInformationService.class);
        Deployment deployment = new Deployment();
        deployment.setEnvironment("Y");
        deployment.setServerName("server1");
        DeploymentApplication deplApp1 = new DeploymentApplication();
        deplApp1.setVersion("1.1.1");
        deplApp1.setApplicationName("application1");
        deployment.addDeploymentApplication(deplApp1);
        List<Deployment> liimaDeployments = singletonList(deployment);

        when(deploymentInformationService.retrieveDeployments(ArgumentMatchers.eq("app1"))).thenReturn(liimaDeployments);
        when(deploymentInformationService.filterDeployments(anyList(), anySet())).thenReturn(liimaDeployments);

        LiimaInformationCollector liimaInformationCollector = new LiimaInformationCollector(ruleService, screensModel, deploymentInformationService, collectorMetricService);

        // act
        liimaInformationCollector.collectLiimaInformationForScreens();

        // assert
        verify(screensModel).getAvailableScreens();
        verify(ruleService).updateRuleEvaluation(any(), any());
        verify(screen).setRefreshDate(ArgumentMatchers.eq(LIIMA), any(Date.class));

        verify(deploymentInformationService, times(2)).retrieveDeployments(anyString());
        verify(deploymentInformationService, times(2)).filterDeployments(anyList(), anySet());

        verify(collectorMetricService).submitCollectorDuration(anyString(), anyLong());
    }

}
