package ch.mobi.mobitor.plugin.liima;

/*-
 * §
 * mobitor-plugin-liima
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.liima.config.AppServerConfig;
import ch.mobi.mobitor.plugin.liima.service.ServersConfigurationService;
import ch.mobi.mobitor.plugins.api.domain.config.ExtendableScreenConfig;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformationLegendWrapper;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class LiimaDeploymentsPluginTest {
    private ServersConfigurationService serversConfigurationService;
    private Screen screen;
    private ExtendableScreenConfig screenConfig;
    private List<String> configurations = new ArrayList<>();
    private AppServerConfig appServerConfig = new AppServerConfig();
    private Map<String,AppServerConfig> serverMap = new HashMap<>();

    @Before
    public void setUp(){
        serversConfigurationService = mock(ServersConfigurationService.class);
        screen = mock(Screen.class);
        screenConfig = mock(ExtendableScreenConfig.class);
        List<String> environments = new ArrayList<>();
        environments.add("P");

        when(screenConfig.getEnvironments()).thenReturn(environments);

        appServerConfig.setAppServerName("ch_mobi_mobitor");
        List<String> appserverNames = new ArrayList<>();
        appserverNames.add("mobitor");
        appServerConfig.setApplicationNames(appserverNames);
        serverMap.put("mobitor",appServerConfig);

        when(serversConfigurationService.getAppServerNameToConfigMap()).thenReturn(serverMap);

        configurations.add("mobitor");
    }

    @Test
    public void getConfigPropertyName() {
        LiimaDeploymentsPlugin liimaPlugin = new LiimaDeploymentsPlugin(serversConfigurationService);
        String expected = "liimaServers";

        String actual = liimaPlugin.getConfigPropertyName();

        assertEquals(expected,actual);
    }

    @Test
    public void getConfigClass() {
        LiimaDeploymentsPlugin liimaPlugin = new LiimaDeploymentsPlugin(serversConfigurationService);

        Class<String> actual = liimaPlugin.getConfigClass();

        assertEquals(String.class, actual);
    }

    @Test
    public void createAndAssociateApplicationInformationBlocks() {
        LiimaDeploymentsPlugin liimaPlugin = new LiimaDeploymentsPlugin(serversConfigurationService);
        liimaPlugin.createAndAssociateApplicationInformationBlocks(screen,screenConfig,configurations);

        verify(screen).addInformation(eq("mobitor"),eq("mobitor"),eq("P"),any());
    }

    @Test
    public void getLegendApplicationInformationList() {
        LiimaDeploymentsPlugin liimaPlugin = new LiimaDeploymentsPlugin(serversConfigurationService);
        List<ApplicationInformationLegendWrapper> legend = liimaPlugin.getLegendApplicationInformationList();

        assertNotNull(legend);
        assertEquals(3,legend.size());
    }
}
