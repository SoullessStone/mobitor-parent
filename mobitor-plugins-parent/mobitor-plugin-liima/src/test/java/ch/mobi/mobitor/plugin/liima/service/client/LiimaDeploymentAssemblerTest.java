package ch.mobi.mobitor.plugin.liima.service.client;

/*-
 * §
 * mobitor-plugin-liima
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.liima.client.dto.AppWithVersion;
import ch.mobi.liima.client.dto.Deployment;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.Assert.assertThat;

public class LiimaDeploymentAssemblerTest {

    @Test
    public void getDeploymentWithEmptyApplicationsAndEmptyParameters() {
        // arrange
        String serverName = "junit-ServerName";
        String environment = "J";
        Deployment liimaDeployment = new Deployment(serverName, environment);
        LiimaDeploymentAssembler assembler = new LiimaDeploymentAssembler(liimaDeployment);


        // act
        ch.mobi.mobitor.domain.deployment.Deployment deployment = assembler.getDeployment();

        // assert
        assertThat(deployment, is(not(nullValue())));
        assertThat(deployment.getServerName(), is(equalTo(serverName)));
        assertThat(deployment.getEnvironment(), is(equalTo(environment)));
    }

    @Test
    public void getDeploymentWithApplicationsAndParameters() {
        // arrange
        String serverName = "junit-ServerName";
        String environment = "J";
        Deployment liimaDeployment = new Deployment(serverName, environment);
        liimaDeployment.addAppWithVersion(new AppWithVersion("app1", "1"));
        liimaDeployment.addAppWithVersion(new AppWithVersion("app2", "2"));

        liimaDeployment.addDeploymentParameter("key1", "val1");
        liimaDeployment.addDeploymentParameter("key2", "val2");

        LiimaDeploymentAssembler assembler = new LiimaDeploymentAssembler(liimaDeployment);


        // act
        ch.mobi.mobitor.domain.deployment.Deployment deployment = assembler.getDeployment();

        // assert
        assertThat(deployment, is(not(nullValue())));
        assertThat(deployment.getServerName(), is(equalTo(serverName)));
        assertThat(deployment.getEnvironment(), is(equalTo(environment)));
        assertThat(deployment.getApplications(), hasSize(2));
        assertThat(deployment.getParameters().keySet(), hasSize(2));
    }
}
