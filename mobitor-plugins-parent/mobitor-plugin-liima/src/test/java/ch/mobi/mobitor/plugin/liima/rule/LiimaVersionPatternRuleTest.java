package ch.mobi.mobitor.plugin.liima.rule;

/*-
 * §
 * mobitor-plugin-liima
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Pipeline;
import ch.mobi.mobitor.domain.screen.RuleEvaluation;
import ch.mobi.mobitor.plugin.liima.domain.LiimaInformation;
import ch.mobi.mobitor.plugin.liima.service.ServersConfigurationService;
import ch.mobi.mobitor.plugin.test.rule.PipelineRuleTest;
import org.junit.Test;
import org.mockito.ArgumentMatchers;

import static ch.mobi.mobitor.plugin.liima.domain.LiimaInformation.LIIMA;
import static org.hamcrest.CoreMatchers.any;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class LiimaVersionPatternRuleTest extends PipelineRuleTest<LiimaVersionPatternRule> {

    @Test
    public void evaluateRuleWithValidVersions() {
        // arrange
        Pipeline pipeline = createPipeline();

        LiimaInformation liimaInfo = new LiimaInformation(SERVER_NAME, APP_NAME, ENV);
        liimaInfo.setVersion("1.2.3");

        pipeline.addInformation(ENV, APP_NAME, liimaInfo);

        // act
        RuleEvaluation ruleEvaluation = createNewRuleEvaluation();
        evaluateRule(pipeline, ruleEvaluation);

        // assert
        assertThat(ruleEvaluation.hasErrors(), is(false));
    }

    @Test
    public void testRuleViolated() {
        // arrange
        ServersConfigurationService serversConfigurationService = mock(ServersConfigurationService.class);
        LiimaVersionPatternRule rule = new LiimaVersionPatternRule(serversConfigurationService);

        // act

        // assert
        // no violations of the version format
        assertThat(rule.ruleViolated("1.0.0"), is(false));
        assertThat(rule.ruleViolated("1.2.3"), is(false));
        assertThat(rule.ruleViolated("100.21.332"), is(false));
        assertThat(rule.ruleViolated("0.0.1"), is(false));
        assertThat(rule.ruleViolated("0.0.0"), is(false));
        assertThat(rule.ruleViolated("10.0.0"), is(false));

        // version format is invalid, violates the rule
        assertThat(rule.ruleViolated("00.0.0"), is(true));
        assertThat(rule.ruleViolated("0.00.0"), is(true));
        assertThat(rule.ruleViolated("0.0.00"), is(true));
        assertThat(rule.ruleViolated("0.0.01"), is(true));
        assertThat(rule.ruleViolated("1.02.3"), is(true));
        assertThat(rule.ruleViolated("01.02.03"), is(true));
        assertThat(rule.ruleViolated("1.2.3-4"), is(true));
        assertThat(rule.ruleViolated("a.b.c"), is(true));

        assertThat(rule.ruleViolated(null), is(true));
        assertThat(rule.ruleViolated(""), is(true));
        assertThat(rule.ruleViolated("  "), is(true));
    }

    @Test
    public void validatesType() {
        // arrange
        ServersConfigurationService serversConfigurationService = mock(ServersConfigurationService.class);
        LiimaVersionPatternRule rule = new LiimaVersionPatternRule(serversConfigurationService);

        // act

        // assert
        assertThat(rule.validatesType(LIIMA), is(true));
    }

    @Override
    protected LiimaVersionPatternRule createNewRule() {
        ServersConfigurationService serversConfigurationService = mock(ServersConfigurationService.class);

        return new LiimaVersionPatternRule(serversConfigurationService);
    }

}
