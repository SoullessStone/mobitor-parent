package ch.mobi.mobitor.plugin.liima;

/*-
 * §
 * mobitor-plugin-liima
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.liima.client.LiimaClient;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class LiimaPluginConfigurationTest {

    @Test
    public void liimaClientShouldReturnALiimaClient() {
        //arrange
        LiimaPluginConfiguration sut = new LiimaPluginConfiguration();
        MobitorLiimaConfiguration mobitorLiimaConfiguration = new MobitorLiimaConfiguration();

        //act
        LiimaClient result = sut.liimaClient(mobitorLiimaConfiguration);

        //assert
        assertThat(result).isNotNull();
    }

}
