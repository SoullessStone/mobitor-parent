package ch.mobi.mobitor.plugin.liima.service.client;

/*-
 * §
 * mobitor-plugin-liima
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.liima.client.LiimaClient;
import ch.mobi.liima.client.dto.AppWithVersion;
import ch.mobi.mobitor.domain.deployment.Deployment;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class LiimaInformationProviderServiceTest {
    private LiimaClient liimaClient;
    private List<Deployment> liimaDeployments;
    private List<ch.mobi.liima.client.dto.Deployment> originalDeployments;
    private Set<String> environments;

    @Before
    public void setUp(){
        liimaClient = mock(LiimaClient.class);
        liimaDeployments = new ArrayList<>();
        originalDeployments = new ArrayList<>();
        LiimaDeploymentAssembler liimaDeploymentAssembler;
        ch.mobi.liima.client.dto.Deployment liimaDeployment = new ch.mobi.liima.client.dto.Deployment();
        AppWithVersion appWithVersion = new AppWithVersion("ch_mobi_mobitor","1.0.0");

        liimaDeployment.addAppWithVersion(appWithVersion);
        liimaDeployment.setState("success");
        originalDeployments.add(liimaDeployment);
        liimaDeploymentAssembler = new LiimaDeploymentAssembler(liimaDeployment);
        liimaDeployments.add(liimaDeploymentAssembler.getDeployment());

        environments = new HashSet<>();
        environments.add("B");
        environments.add("T");
        environments.add("P");

        when(liimaClient.retrieveDeployments(any())).thenReturn(originalDeployments);
        when(liimaClient.createDeploymentsWebUri(any(),any(),any())).thenReturn("http://deploments.io/mobitor");
    }

    @Test
    public void filterDeploymentsTest() {
        LiimaInformationProviderService service = new LiimaInformationProviderService(liimaClient);

        List<Deployment> filteredList = service.filterDeployments(liimaDeployments, environments);

        assertNotNull(filteredList);
    }

    @Test
    public void getVersionTest() {
        LiimaInformationProviderService service = new LiimaInformationProviderService(liimaClient);

        String version = service.getVersion("ch_mobi_mobitor",liimaDeployments);

        assertNotNull(version);
        assertEquals("1.0.0",version);
    }

    @Test
    public void getVersionReturnsNullTest (){
        LiimaInformationProviderService service = new LiimaInformationProviderService(liimaClient);
        List<Deployment> emptyList = new ArrayList<>();

        String version = service.getVersion("ch_mobi_mobitor",emptyList);

        assertNull(version);
    }

    @Test
    public void retrieveDeploymentsTest() {
        LiimaInformationProviderService service = new LiimaInformationProviderService(liimaClient);

        List<Deployment> deployments = service.retrieveDeployments("mobitor");
        assertNotNull(deployments);
        assertEquals(1,deployments.size());
        assertEquals("ch_mobi_mobitor",deployments.get(0).getApplications().get(0).getApplicationName());
    }

    @Test
    public void createDeploymentWebUriTest() {
        LiimaInformationProviderService service = new LiimaInformationProviderService(liimaClient);

        String deplyomentUri = service.createDeploymentWebUri("mobitor","ch_mobi_mobitor","P");

        assertNotNull(deplyomentUri);
    }
}
