package ch.mobi.mobitor.plugin.liima.rule;

/*-
 * §
 * mobitor-plugin-liima
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.PipelineRule;
import ch.mobi.mobitor.domain.screen.Pipeline;
import ch.mobi.mobitor.domain.screen.RuleEvaluation;
import ch.mobi.mobitor.plugin.liima.domain.LiimaInformation;
import ch.mobi.mobitor.plugin.test.rule.PipelineRuleTest;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformation;
import org.junit.Test;

import static ch.mobi.mobitor.plugin.liima.domain.LiimaInformation.LIIMA;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

public class AmwDeploymentRuleTest extends PipelineRuleTest {

    @Override
    protected PipelineRule createNewRule() {
        return new AmwDeploymentRule();
    }

    private LiimaInformation createAmwInformation() {
        LiimaInformation liimaInformation = new LiimaInformation(SERVER_NAME, APP_NAME, ENV);

        return liimaInformation;
    }

    @Test
    public void testEvaluateRuleAllGood() {
        // arrange
        ApplicationInformation amwInformation = createAmwInformation();
        Pipeline pipeline = createPipeline();
        pipeline.addInformation(ENV, APP_NAME, amwInformation);

        // act
        RuleEvaluation ruleEvaluation = createNewRuleEvaluation();
        this.evaluateRule(pipeline, ruleEvaluation);

        // assert
        assertThat(ruleEvaluation, is(not(nullValue())));
        assertThat(ruleEvaluation.hasErrors(), is(false));
    }

    @Test
    public void testEvaluateRuleWithViolations() {
        // arrange
        LiimaInformation liimaInformation = createAmwInformation();
        liimaInformation.setState("failed");

        Pipeline pipeline = createPipeline();
        pipeline.addInformation(ENV, APP_NAME, liimaInformation);

        // act
        RuleEvaluation ruleEvaluation = createNewRuleEvaluation();
        this.evaluateRule(pipeline, ruleEvaluation);

        // assert
        assertThat(ruleEvaluation, is(not(nullValue())));
        assertThat(ruleEvaluation.hasErrors(), is(true));
    }

    @Test
    public void validatesType() {
        assertThat(this.validatesType(null), is(false));
        assertThat(this.validatesType(LIIMA), is(true));
    }

}
