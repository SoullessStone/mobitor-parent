package ch.mobi.mobitor.plugin.liima.service;

/*-
 * §
 * mobitor-plugin-liima
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */


import ch.mobi.mobitor.plugin.liima.config.AppServerConfig;
import ch.mobi.mobitor.plugin.liima.config.ApplicationNameMapping;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.TestOnly;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ServersConfigurationService {

    private static final Logger LOG = LoggerFactory.getLogger(ServersConfigurationService.class);

    private final ResourceLoader resourceLoader;
    private String appServersConfigFile = "classpath:screen/application-servers.json";

    private List<AppServerConfig> applicationServers;
    private Map<String, String> applicationNameMappings = new HashMap<>();
    private Map<String, AppServerConfig> appServerNameToConfigMap = new HashMap<>();

    @Autowired
    public ServersConfigurationService(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    @PostConstruct
    public void initializeAmwDeployments() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        Resource resource = resourceLoader.getResource(appServersConfigFile);
        try (InputStream inputStream = resource.getInputStream()) {
            this.applicationServers = mapper.readValue(inputStream, mapper.getTypeFactory().constructCollectionType(List.class, AppServerConfig.class));

        } catch (Exception e) {
            LOG.error("Could not initialize application servers.", e);
        }

        this.appServerNameToConfigMap.putAll(createServerToConfigMap(this.applicationServers));
    }

    @TestOnly
    void setAppServersConfigFile(String appServersConfigFile) {
        this.appServersConfigFile = appServersConfigFile;
    }

    public List<AppServerConfig> getApplicationServers() {
        return Collections.unmodifiableList(applicationServers);
    }

    @PostConstruct
    public void initializeApplicationNameMappings() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        Resource resource = resourceLoader.getResource("classpath:screen/application-name-mappings.json");
        try (InputStream inputStream = resource.getInputStream()) {
            List<ApplicationNameMapping> nameMappings = mapper.readValue(inputStream, mapper.getTypeFactory().constructCollectionType(List.class, ApplicationNameMapping.class));

            for (ApplicationNameMapping nameMapping : nameMappings) {
                String amwAppName = nameMapping.getApplicationName();
                String screenName = nameMapping.getScreenName();
                if (StringUtils.isNotBlank(screenName)) {
                    this.applicationNameMappings.put(amwAppName, screenName);
                    LOG.debug(MessageFormat.format("added application name mapping: {0} = {1}", amwAppName, screenName));
                } else {
                    throw new IllegalArgumentException("screenName must not be empty: " + amwAppName);
                }
            }

        } catch (IOException e) {
            LOG.error("Could not initialize application name mappings.", e);
        }
    }

    public String getApplicationScreenName(String amwApplicationName) {
        return this.applicationNameMappings.get(amwApplicationName);
    }

    private Map<String, AppServerConfig> createServerToConfigMap(List<AppServerConfig> allServers) {
        Map<String, AppServerConfig> appServerNameToConfigMap = new HashMap<>();
        for (AppServerConfig server : allServers) {
            appServerNameToConfigMap.put(server.getAppServerName(), server);
        }
        return appServerNameToConfigMap;
    }

    public Map<String, AppServerConfig> getAppServerNameToConfigMap() {
        return Collections.unmodifiableMap(appServerNameToConfigMap);
    }

    public void setAppServerNameToConfigMap(Map<String, AppServerConfig> appServerNameToConfigMap) {
        this.appServerNameToConfigMap = appServerNameToConfigMap;
    }

}
