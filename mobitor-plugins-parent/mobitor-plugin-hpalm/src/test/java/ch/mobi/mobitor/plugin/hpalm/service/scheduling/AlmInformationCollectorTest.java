package ch.mobi.mobitor.plugin.hpalm.service.scheduling;

/*-
 * §
 * mobitor-plugin-hpalm
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.hpalm.domain.AlmInformation;
import ch.mobi.mobitor.plugin.hpalm.service.client.AlmClient;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformation;
import ch.mobi.mobitor.plugins.api.model.ScreensModel;
import ch.mobi.mobitor.plugins.api.service.RuleService;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import static ch.mobi.mobitor.plugin.hpalm.domain.AlmInformation.ALM;

public class AlmInformationCollectorTest {

    @Test
    public void collectAlmTestRunsInformation() {
        // arrange
        RuleService ruleService = Mockito.mock(RuleService.class);
        AlmClient almClient = Mockito.mock(AlmClient.class);
        ScreensModel screensModel = Mockito.mock(ScreensModel.class);
        Screen screen = Mockito.mock(Screen.class);
        List<ApplicationInformation> almList = new ArrayList<>();
        AlmInformation almInfo = new AlmInformation("domain", "project", "folder", "http://url.com");
        almList.add(almInfo);

        Mockito.when(screen.getMatchingInformation(ArgumentMatchers.eq(ALM))).thenReturn(almList);
        List<Screen> screensList = Collections.singletonList(screen);
        Mockito.when(screensModel.getAvailableScreens()).thenReturn(screensList);

        AlmInformationCollector almInfoCollector = new AlmInformationCollector(screensModel, ruleService, almClient);

        // act
        almInfoCollector.collectAlmTestRunsInformation();

        // assert
        Mockito.verify(screensModel).getAvailableScreens();
        Mockito.verify(ruleService).updateRuleEvaluation(screen, ALM);
        Mockito.verify(screen).setRefreshDate(ArgumentMatchers.eq(ALM), ArgumentMatchers.any(Date.class));

        Mockito.verify(almClient).startSession();
        Mockito.verify(almClient).login();
        Mockito.verify(almClient).logout();

        Mockito.verify(almClient).retrieveTestSetFolder(ArgumentMatchers.anyString(), ArgumentMatchers.anyString(), ArgumentMatchers.anyString());
        Mockito.verify(almClient).retrieveTestSets(ArgumentMatchers.anyString(), ArgumentMatchers.anyString(), ArgumentMatchers.anyList());
        Mockito.verify(almClient).retrieveTestInstances(ArgumentMatchers.anyString(), ArgumentMatchers.anyString(), ArgumentMatchers.anyList());
        Mockito.verify(almClient).retrieveRuns(ArgumentMatchers.anyString(), ArgumentMatchers.anyString(), ArgumentMatchers.anyList());
    }

}
