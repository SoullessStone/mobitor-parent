package ch.mobi.mobitor.plugin.hpalm.domain;

/*-
 * §
 * mobitor-plugin-hpalm
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import org.jetbrains.annotations.NotNull;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class TestRunComparatorTest {

    @Test
    public void compareShouldCompareIds1() {
        //arrange
        TestRunComparator sut = new TestRunComparator();
        TestRun run1 = newTestRunWithId(1);
        TestRun run2 = newTestRunWithId(2);

        //act
        int result = sut.compare(run1, run2);

        //assert
        assertThat(result).isPositive();
    }

    @Test
    public void compareShouldCompareIds2() {
        //arrange
        TestRunComparator sut = new TestRunComparator();
        TestRun run1 = newTestRunWithId(2);
        TestRun run2 = newTestRunWithId(1);

        //act
        int result = sut.compare(run1, run2);

        //assert
        assertThat(result).isNegative();
    }

    @Test
    public void compareShouldReturnZeroIfATestRunIsNull1() {
        //arrange
        TestRunComparator sut = new TestRunComparator();
        TestRun run = newTestRunWithId(1);

        //act
        int result = sut.compare(run, null);

        //assert
        assertThat(result).isZero();
    }

    @Test
    public void compareShouldReturnZeroIfATestRunIsNull2() {
        //arrange
        TestRunComparator sut = new TestRunComparator();
        TestRun run = newTestRunWithId(1);

        //act
        int result = sut.compare(null, run);

        //assert
        assertThat(result).isZero();
    }

    @NotNull
    private TestRun newTestRunWithId(long id) {
        return new TestRun(0, 0, "", "", 0, "", "", "", id, "", "");
    }

}
