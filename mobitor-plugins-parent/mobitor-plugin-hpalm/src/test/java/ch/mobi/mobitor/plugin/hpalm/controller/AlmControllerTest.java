package ch.mobi.mobitor.plugin.hpalm.controller;

/*-
 * §
 * mobitor-plugin-hpalm
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugins.api.model.ScreensModel;
import org.junit.Test;
import org.springframework.ui.Model;

import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class AlmControllerTest {

    @Test
    public void overviewShouldAddAttributesToModel() {
        //arrange
        Screen screen = mock(Screen.class);
        ScreensModel screensModel = mock(ScreensModel.class);
        when(screensModel.getScreen(any())).thenReturn(screen);
        AlmController sut = new AlmController(screensModel);
        Model model = mock(Model.class);

        //act
        String result = sut.overview("1", "PROD", "server", "application", "userId", model);

        //assert
        verify(model).addAttribute("almInfos", emptyList());
        verify(model).addAttribute(eq("screenConfigKey"), any());
        verify(model).addAttribute(eq("environment"), any());
        verify(model).addAttribute(eq("serverName"), any());
        verify(model).addAttribute(eq("applicationName"), any());
        verify(model).addAttribute(eq("screen"), any());
        verify(model).addAttribute(eq("userId"), any());
        assertThat(result).isEqualTo("alm");
    }
}
