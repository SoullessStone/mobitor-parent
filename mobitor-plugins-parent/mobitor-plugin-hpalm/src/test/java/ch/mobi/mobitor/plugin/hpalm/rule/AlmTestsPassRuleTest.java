package ch.mobi.mobitor.plugin.hpalm.rule;

/*-
 * §
 * mobitor-plugin-hpalm
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.PipelineRule;
import ch.mobi.mobitor.domain.screen.Pipeline;
import ch.mobi.mobitor.domain.screen.RuleEvaluation;
import ch.mobi.mobitor.plugin.hpalm.domain.AlmInformation;
import ch.mobi.mobitor.plugin.hpalm.domain.TestRun;
import ch.mobi.mobitor.plugin.test.rule.PipelineRuleTest;
import org.junit.Test;

import java.util.Collections;
import java.util.List;

import static ch.mobi.mobitor.plugin.hpalm.domain.AlmInformation.ALM;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

public class AlmTestsPassRuleTest extends PipelineRuleTest {

    @Test
    public void evaluateRulePassed() {
        // arrange
        Pipeline pipeline = createPipeline();
        AlmInformation almInformation = new AlmInformation("DOMAIN", "PROJEKTE", "foldername", null);

        TestRun tr1 = new TestRun(1, 11, "2017-01-20", "SDT-testName", 111, "Name", "10:18:00", "Cycle Name", 1111, "Finished", "Passed");
        List<TestRun> testRuns = Collections.singletonList(tr1);
        almInformation.updateTestRuns(testRuns);
        pipeline.addInformation(ENV, APP_NAME, almInformation);

        // act
        RuleEvaluation ruleEvaluation = createNewRuleEvaluation();
        evaluateRule(pipeline, ruleEvaluation);

        // assert
        assertThat(ruleEvaluation, is(not(nullValue())));
        assertThat(ruleEvaluation.hasErrors(), is(false));
    }

    @Test
    public void evaluateRuleFailed() {
        // arrange
        Pipeline pipeline = createPipeline();
        AlmInformation almInformation = new AlmInformation("DOMAIN", "PROJEKTE", "foldername", null);

        TestRun tr1 = new TestRun(1, 11, "2017-01-20", "SDT-testName", 111, "Name", "10:18:00", "Cycle Name", 1111, "Finished", "Failed");
        List<TestRun> testRuns = Collections.singletonList(tr1);
        almInformation.updateTestRuns(testRuns);
        pipeline.addInformation(ENV, APP_NAME, almInformation);

        // act
        RuleEvaluation ruleEvaluation = createNewRuleEvaluation();
        evaluateRule(pipeline, ruleEvaluation);

        // assert
        assertThat(ruleEvaluation, is(not(nullValue())));
        assertThat(ruleEvaluation.hasErrors(), is(true));
    }

    @Test
    public void validatesType() {
        assertThat(validatesType(null), is(false));
        assertThat(validatesType(ALM), is(true));
    }

    @Override
    protected PipelineRule createNewRule() {
        return new AlmTestsPassRule();
    }
}
