package ch.mobi.mobitor.plugin.hpalm.service.client;

/*-
 * §
 * mobitor-plugin-hpalm
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.hpalm.domain.TestRun;
import ch.mobi.mobitor.plugin.hpalm.service.client.domain.TestInstance;
import ch.mobi.mobitor.plugin.hpalm.service.client.domain.TestSet;
import ch.mobi.mobitor.plugin.hpalm.service.client.domain.TestSetFolder;
import com.github.tomakehurst.wiremock.client.BasicCredentials;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.github.tomakehurst.wiremock.http.HttpHeader;
import com.github.tomakehurst.wiremock.http.HttpHeaders;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.github.tomakehurst.wiremock.matching.AnythingPattern;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.isEmptyOrNullString;
import static org.hamcrest.collection.IsEmptyCollection.empty;
import static org.junit.Assert.assertThat;

public class AlmClientTest {

    private static final String ALM_USERNAME = "almusername";
    private static final String ALM_PASSWORD = "almpassword";

    private AlmConfiguration almConfiguration;

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(WireMockConfiguration.options().dynamicPort());

    @Before
    public void setup() {
        almConfiguration = new AlmConfiguration();
        almConfiguration.setUsername(ALM_USERNAME);
        almConfiguration.setPassword(ALM_PASSWORD);
    }

    private void configureLoginUrl() {
        HttpHeader contentType = new HttpHeader("Content-Type", "text/plain");
        HttpHeader ssoCookie = new HttpHeader("Set-Cookie", "LWSSO_COOKIE_KEY=encryptedexamplekey.");
        HttpHeaders httpHeaders = new HttpHeaders(contentType, ssoCookie);

        WireMock.givenThat(WireMock.get(WireMock.urlMatching("/qcbin/authentication-point/authenticate"))
                                   .withBasicAuth(ALM_USERNAME, ALM_PASSWORD)
                                   .willReturn(WireMock.aResponse()
                                                       .withStatus(200)
                                                       .withHeaders(httpHeaders)
                                                       .withBody("")
                )
        );
    }

    private void configureLogoutUrl() {
        WireMock.givenThat(WireMock.get(WireMock.urlMatching("/qcbin/authentication-point/logout"))
                                   .willReturn(WireMock.aResponse()
                                                       .withStatus(200)
                                                       .withHeader("Content-Type", "text/plain")
                                                       .withBody("")
                )
        );
    }

    private void configureSessionUrl() {
        HttpHeader contentType = new HttpHeader("Content-Type", "text/plain");
        HttpHeader ssoCookie = new HttpHeader("Set-Cookie", "JSESSIONID=v482eykrgueugfj0d8e9g2rg");
        HttpHeaders httpHeaders = new HttpHeaders(contentType, ssoCookie);

        WireMock.givenThat(WireMock.post(WireMock.urlMatching("/qcbin/rest/site-session"))
                                   .willReturn(WireMock.aResponse()
                                                       .withStatus(200)
                                                       .withHeaders(httpHeaders)
                                                       .withBody("")
                )
        );
    }

    private void configureTestSetFoldersUrl() {
        WireMock.givenThat(WireMock.get(WireMock.urlMatching("/qcbin/rest/domains/([a-zA-Z0-9_]+)/projects/([a-zA-Z0-9_]+)/test-set-folders.*"))
                                   .withQueryParam("query", new AnythingPattern())
                                   .withQueryParam("fields", new AnythingPattern())
                                   .withHeader("Accept", WireMock.equalTo("application/json"))
                                   .willReturn(WireMock.aResponse()
                                                       .withStatus(200)
                                                       .withBodyFile("alm-test-set-folders.json")
                )
        );
    }

    private void configureTestSetsUrl() {
        WireMock.givenThat(WireMock.get(WireMock.urlMatching("/qcbin/rest/domains/([a-zA-Z0-9_]+)/projects/([a-zA-Z0-9_]+)/test-sets.*"))
                                   .withQueryParam("query", new AnythingPattern())
                                   .withQueryParam("fields", new AnythingPattern())
                                   .withHeader("Accept", WireMock.equalTo("application/json"))
                                   .willReturn(WireMock.aResponse()
                                                       .withStatus(200)
                                                       .withBodyFile("alm-test-sets-940-942.json")
                )
        );
    }

    private void configureTestInstancesUrl() {
        WireMock.givenThat(WireMock.get(WireMock.urlMatching("/qcbin/rest/domains/([a-zA-Z0-9_]+)/projects/([a-zA-Z0-9_]+)/test-instances.*"))
                                   .withQueryParam("query", new AnythingPattern())
                                   .withQueryParam("fields", new AnythingPattern())
                                   .withHeader("Accept", WireMock.equalTo("application/json"))
                                   .willReturn(WireMock.aResponse()
                                                       .withStatus(200)
                                                       .withBodyFile("alm-test-instances.json")
                )
        );
    }

    private void configureRunsUrls() {
        WireMock.givenThat(WireMock.get(WireMock.urlMatching("/qcbin/rest/domains/([a-zA-Z0-9_]+)/projects/([a-zA-Z0-9_]+)/runs.*"))
                                   .withQueryParam("query", WireMock.matching(".*6507.*"))
                                   .withQueryParam("fields", new AnythingPattern())
                                   .withHeader("Accept", WireMock.equalTo("application/json"))
                                   .willReturn(WireMock.aResponse()
                                                       .withStatus(200)
                                                       .withBodyFile("alm-run-cycle_617-test_6507.json")
                )
        );

        WireMock.givenThat(WireMock.get(WireMock.urlMatching("/qcbin/rest/domains/([a-zA-Z0-9_]+)/projects/([a-zA-Z0-9_]+)/runs.*"))
                                   .withQueryParam("query", WireMock.matching(".*6508.*"))
                                   .withQueryParam("fields", new AnythingPattern())
                                   .withHeader("Accept", WireMock.equalTo("application/json"))
                                   .willReturn(WireMock.aResponse()
                                                       .withStatus(200)
                                                       .withBodyFile("alm-run-cycle_617-test_6508.json")
                )
        );
    }

    @Test
    public void testLogin() {
        // arrange
        configureLoginUrl();

        String baseUrl = "http://localhost:" + wireMockRule.port();
        almConfiguration.setBaseUrl(baseUrl);

        AlmClient almClient = new AlmClient(almConfiguration);

        // act
        almClient.login();

        // assert
        assertThat(almClient.hasAuthenticationCookie(), is(true));

        WireMock.verify(WireMock.getRequestedFor(
                WireMock.urlEqualTo("/qcbin/authentication-point/authenticate"))
                                .withBasicAuth(new BasicCredentials(ALM_USERNAME, ALM_PASSWORD))
        );
    }

    @Test
    public void testLoginAndStartSession() {
        // arrange
        configureLoginUrl();
        configureSessionUrl();

        String baseUrl = "http://localhost:" + wireMockRule.port();
        almConfiguration.setBaseUrl(baseUrl);

        AlmClient almClient = new AlmClient(almConfiguration);
        almClient.login();

        // act
        almClient.startSession();

        // assert
        WireMock.verify(WireMock.postRequestedFor(WireMock.urlEqualTo("/qcbin/rest/site-session")));

        assertThat(almClient.hasSessionCookie(), is(true));
    }

    @Test
    public void testRetrieveTestSetFolder() {
        // arrange
        configureLoginUrl();
        configureSessionUrl();
        configureTestSetFoldersUrl();

        String baseUrl = "http://localhost:" + wireMockRule.port();
        almConfiguration.setBaseUrl(baseUrl);

        AlmClient almClient = new AlmClient(almConfiguration);
        almClient.login();
        almClient.startSession();

        String testSetFolderName = "Umgebung T (PreProd)";
        String domain = "RELEASETEST";
        String project = "RL_17_04";

        // act
        List<TestSetFolder> folders = almClient.retrieveTestSetFolder(domain, project, testSetFolderName);

        // assert
        WireMock.verify(WireMock.getRequestedFor(WireMock.urlMatching("/qcbin/rest/domains/" + domain + "/projects/" + project + "/test-set-folders.*")));
        assertThat(folders, is(not(nullValue())));
        assertThat(folders, is(not(empty())));
        for (TestSetFolder testSetFolder : folders) {
            assertThat(testSetFolder.getName(), is(not(isEmptyOrNullString())));
            assertThat(testSetFolder.getId(), greaterThan(0L));
        }
    }

    @Test
    public void testRetrieveTestSets() {
        // arrange
        configureLoginUrl();
        configureSessionUrl();
        configureTestSetsUrl();

        String baseUrl = "http://localhost:" + wireMockRule.port();
        almConfiguration.setBaseUrl(baseUrl);

        AlmClient almClient = new AlmClient(almConfiguration);
        almClient.login();
        almClient.startSession();

        String domain = "RELEASETEST";
        String project = "RL_17_04";

        // act
        TestSetFolder tf1 = new TestSetFolder("Umgebung T", 1);
        TestSetFolder tf2 = new TestSetFolder("Umgebung P", 2);
        List<TestSetFolder> testSetFolders = new ArrayList<>();
        testSetFolders.add(tf1);
        testSetFolders.add(tf2);

        List<TestSet> testSets = almClient.retrieveTestSets(domain, project, testSetFolders);

        // assert
        WireMock.verify(WireMock.getRequestedFor(WireMock.urlMatching("/qcbin/rest/domains/" + domain + "/projects/" + project + "/test-sets.*")));
        assertThat(testSets, is(not(nullValue())));
        assertThat(testSets, is(not(empty())));
        assertThat(testSets, hasSize(5));
        for (TestSet testSet : testSets) {
            assertThat(testSet.getName(), is(not(isEmptyOrNullString())));
            assertThat(testSet.getId(), greaterThan(0L));
        }
    }

    @Test
    public void testRetrieveTestInstances() {
        // arrange
        configureLoginUrl();
        configureSessionUrl();
        configureTestInstancesUrl();

        String baseUrl = "http://localhost:" + wireMockRule.port();
        almConfiguration.setBaseUrl(baseUrl);

        AlmClient almClient = new AlmClient(almConfiguration);
        almClient.login();
        almClient.startSession();

        String domain = "RELEASETEST";
        String project = "RL_17_04";

        // act
        List<TestSet> testSets = new ArrayList<>();
        TestSet ts1 = new TestSet("SDT - VVN - Base - cd9c007f", 617);
        TestSet ts2 = new TestSet("SDT - Post - cd9c006e", 613);
        testSets.add(ts1);
        testSets.add(ts2);


        List<TestInstance> testInstances = almClient.retrieveTestInstances(domain, project, testSets);

        // assert
        WireMock.verify(WireMock.getRequestedFor(WireMock.urlMatching("/qcbin/rest/domains/" + domain + "/projects/" + project + "/test-instances.*")));
        assertThat(testInstances, is(not(nullValue())));
        assertThat(testInstances, is(not(empty())));
        assertThat(testInstances, hasSize(14));
        for (TestInstance testInstance: testInstances) {
            assertThat(testInstance.getName(), is(not(isEmptyOrNullString())));
            assertThat(testInstance.getStatus(), is(not(isEmptyOrNullString())));
            assertThat(testInstance.getId(), greaterThan(0L));
            assertThat(testInstance.getCycleId(), greaterThan(0L));
            assertThat(testInstance.getTestId(), greaterThan(0L));
        }
    }

    @Test
    public void testLogout() {
        // arrange
        configureLoginUrl();
        configureSessionUrl();
        configureLogoutUrl();

        String baseUrl = "http://localhost:" + wireMockRule.port();
        almConfiguration.setBaseUrl(baseUrl);

        AlmClient almClient = new AlmClient(almConfiguration);
        almClient.login();
        almClient.startSession();

        // act
        almClient.logout();

        // assert
        WireMock.verify(WireMock.getRequestedFor(WireMock.urlMatching("/qcbin/authentication-point/logout")));
        assertThat(almClient.hasSessionCookie(), is(false));
    }


    @Test
    public void testRetrieveTestRuns() {
        // arrange
        configureLoginUrl();
        configureSessionUrl();
        configureRunsUrls();

        String baseUrl = "http://localhost:" + wireMockRule.port();
        almConfiguration.setBaseUrl(baseUrl);

        AlmClient almClient = new AlmClient(almConfiguration);
        almClient.login();
        almClient.startSession();

        String domain = "RELEASETEST";
        String project = "RL_17_04";

        // act
        TestInstance ti1 = new TestInstance(6507, 617, "0100_BASE-VVN-Login [1]", 1492, "Passed");
        TestInstance ti2 = new TestInstance(6508, 617, "0300_BASE-VVN-Neu_Beratung_Erstellen [1]", 1493, "Failed");
        List<TestInstance> testInstances = asList(ti1, ti2);

        List<TestRun> testRuns = almClient.retrieveRuns(domain, project, testInstances);

        // assert
        WireMock.verify(2, WireMock.getRequestedFor(WireMock.urlMatching("/qcbin/rest/domains/" + domain + "/projects/" + project + "/runs.*")));
        assertThat(testRuns, is(not(nullValue())));
        assertThat(testRuns, is(not(empty())));
        assertThat(testRuns, hasSize(2));
        for (TestRun testRun: testRuns) {
            assertThat(testRun.getName(), is(not(isEmptyOrNullString())));
            assertThat(testRun.getStatus(), is(not(isEmptyOrNullString())));
            assertThat(testRun.getState(), is(not(isEmptyOrNullString())));
            assertThat(testRun.getId(), greaterThan(0L));
            assertThat(testRun.getCycleId(), greaterThan(0L));
            assertThat(testRun.getTestId(), greaterThan(0L));
            assertThat(testRun.getExecutionDate(), is(not(isEmptyOrNullString())));
            assertThat(testRun.getExecutionTime(), is(not(isEmptyOrNullString())));
        }
    }

}
