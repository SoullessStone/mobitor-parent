package ch.mobi.mobitor.plugin.hpalm;

/*-
 * §
 * mobitor-plugin-hpalm
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.hpalm.config.AlmTestsConfig;
import ch.mobi.mobitor.plugin.hpalm.domain.AlmInformation;
import ch.mobi.mobitor.plugins.api.MobitorPlugin;
import ch.mobi.mobitor.plugins.api.domain.config.ExtendableScreenConfig;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformationLegendWrapper;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.util.List;

import static java.util.Collections.emptyList;

@Component
@ConditionalOnProperty(name = "mobitor.plugins.hpalm.enabled", havingValue = "true")
public class HpAlmTestsPlugin implements MobitorPlugin<AlmTestsConfig> {

    @Override
    public String getConfigPropertyName() {
        return "almTests";
    }

    @Override
    public Class<AlmTestsConfig> getConfigClass() {
        return AlmTestsConfig.class;
    }

    @Override
    public void createAndAssociateApplicationInformationBlocks(Screen screen, ExtendableScreenConfig screenConfig, List<AlmTestsConfig> configs) {
        for (AlmTestsConfig almTestConfig : configs) {
            String serverName = almTestConfig.getServerName();
            String applicationName = almTestConfig.getApplicationName();
            String env = almTestConfig.getEnvironment();

            String domain = almTestConfig.getDomain();
            String project = almTestConfig.getProject();
            String foldername = almTestConfig.getFoldername();
            String url = almTestConfig.getUrl();

            AlmInformation almInfo = new AlmInformation(domain, project, foldername, url);

            screen.addInformation(serverName, applicationName, env, almInfo);
        }
    }

    @Override
    public List<ApplicationInformationLegendWrapper> getLegendApplicationInformationList() {
        // Plugin to be removed?
        return emptyList();
    }
}
