package ch.mobi.mobitor.plugin.hpalm.domain;

/*-
 * §
 * mobitor-plugin-hpalm
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonDeserialize(using = TestRunDeserializer.class)
public class TestRun {

    private long duration;
    private long testId;
    private String executionDate;
    private String testName;
    private long cycleId;
    private String name;
    private String executionTime;
    private String cycleName;
    private long id;
    private String state;
    private String status;

    public TestRun(long duration, long testId, String executionDate, String testName, long cycleId, String name, String executionTime, String cycleName, long id, String state, String status) {
        this.duration = duration;
        this.testId = testId;
        this.executionDate = executionDate;
        this.testName = testName;
        this.cycleId = cycleId;
        this.name = name;
        this.executionTime = executionTime;
        this.cycleName = cycleName;
        this.id = id;
        this.state = state;
        this.status = status;
    }

    public long getDuration() {
        return duration;
    }

    public long getTestId() {
        return testId;
    }

    public String getExecutionDate() {
        return executionDate;
    }

    public String getTestName() {
        return testName;
    }

    public long getCycleId() {
        return cycleId;
    }

    public String getName() {
        return name;
    }

    public String getExecutionTime() {
        return executionTime;
    }

    public String getCycleName() {
        return cycleName;
    }

    public long getId() {
        return id;
    }

    public String getState() {
        return state;
    }

    public String getStatus() {
        return status;
    }

    @JsonIgnore
    public boolean hasPassed() {
        return "Passed".equals(this.status);
    }

    @JsonIgnore
    public boolean hasFailed() {
        return "Failed".equals(this.status);
    }
}
