package ch.mobi.mobitor.plugin.hpalm.domain;

/*-
 * §
 * mobitor-plugin-hpalm
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;

import java.io.IOException;

public class TestRunDeserializer extends JsonDeserializer<TestRun> {

    @Override
    public TestRun deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        TreeNode node = p.getCodec().readTree(p);
        TreeNode fields = node.get("Fields");

        long duration = -1;
        long testId = -1;
        String executionDate ="";
        String testName = "";
        long cycleId = -1;
        String name = "";
        String executionTime ="";
        String cycleName = "";
        long id = -1;
        String state = "";
        String status = "";

        for(int i = 0; i < fields.size(); i++) {
            ObjectNode fieldProps = (ObjectNode) fields.get(i);
            String fieldName = fieldProps.get("Name").asText();
            ArrayNode fieldValues = (ArrayNode) fieldProps.get("values");

            if ("duration".equals(fieldName)) {
                duration = extractLongValue(fieldValues);
            } else if ("test-id".equals(fieldName)) {
                testId = extractLongValue(fieldValues);
            } else if ("execution-date".equals(fieldName)) {
                executionDate = extractStringValue(fieldValues);
            } else if ("test-name".equals(fieldName)) {
                testName = extractStringValue(fieldValues);
            } else if ("cycle-id".equals(fieldName)) {
                cycleId = extractLongValue(fieldValues);
            } else if ("name".equals(fieldName)) {
                name = extractStringValue(fieldValues);
            } else if ("execution-time".equals(fieldName)) {
                executionTime = extractStringValue(fieldValues);
            } else if ("cycle-name".equals(fieldName)) {
                cycleName = extractStringValue(fieldValues);
            } else if ("id".equals(fieldName)) {
                id = extractLongValue(fieldValues);
            } else if ("state".equals(fieldName)) {
                state = extractStringValue(fieldValues);
            } else if ("status".equals(fieldName)) {
                status = extractStringValue(fieldValues);
            }
        }

        return new TestRun(duration, testId, executionDate, testName, cycleId, name, executionTime, cycleName, id, state, status);
    }

    /**
     * extractStringValue(fieldValues)
     */
    private String extractStringValue(ArrayNode fieldValues) {
        ObjectNode valueNode = (ObjectNode) fieldValues.get(0);
        TextNode fieldValue = (TextNode) valueNode.get("value");
        return fieldValue != null ? fieldValue.asText() : "";
    }

    private long extractLongValue(ArrayNode fieldValues) {
        String value = extractStringValue(fieldValues);
        return Long.parseLong(value);
    }
}
