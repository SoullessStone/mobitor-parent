package ch.mobi.mobitor.plugin.hpalm.service.client;

/*-
 * §
 * mobitor-plugin-hpalm
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.hpalm.domain.TestRun;
import ch.mobi.mobitor.plugin.hpalm.service.client.domain.TestInstance;
import ch.mobi.mobitor.plugin.hpalm.service.client.domain.TestInstances;
import ch.mobi.mobitor.plugin.hpalm.service.client.domain.TestRuns;
import ch.mobi.mobitor.plugin.hpalm.service.client.domain.TestSet;
import ch.mobi.mobitor.plugin.hpalm.service.client.domain.TestSetFolder;
import ch.mobi.mobitor.plugin.hpalm.service.client.domain.TestSetFolders;
import ch.mobi.mobitor.plugin.hpalm.service.client.domain.TestSets;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.fluent.Executor;
import org.apache.http.client.fluent.Request;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.cookie.Cookie;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * The HP ALM client cannot authenticate every single call using Basic Auth - it has to login and maintain a
 * session cookie - so it maintains a state.
 *
 * @see #login()
 * @see #startSession()
 * @see #logout()
 */
@Component
@Scope("prototype")
public class AlmClient {

    private static final Logger LOG = LoggerFactory.getLogger(AlmClient.class);

    private static final String LOGIN_URL = "/qcbin/authentication-point/authenticate";
    private static final String LOGOUT_URL = "/qcbin/authentication-point/logout";
    private static final String SESSION_URL = "/qcbin/rest/site-session";
    private static final String TEST_SET_FOLDERS_URL = "/qcbin/rest/domains/%1$s/projects/%2$s/test-set-folders?query=%3$s&fields=id,name";
    private static final String TEST_SETS_URL = "/qcbin/rest/domains/%1$s/projects/%2$s/test-sets?query=%3$s&page-size=5000&fields=id,name";
    private static final String TEST_INSTANCES_URL = "/qcbin/rest/domains/%1$s/projects/%2$s/test-instances?query=%3$s&fields=test-id,name,status";
    private static final String TEST_RUN_URL = "/qcbin/rest/domains/%1$s/projects/%2$s/runs?query=%3$s&fields=id,test-id,test-name,state,name,status,execution-date,duration,cycle-name,execution-time,cycle-id&page-size=1&order-by=%4$s";

    private static final String SSO_COOKIE_NAME = "LWSSO_COOKIE_KEY";
    private static final String SESSION_COOKIE_NAME = "JSESSIONID";

    private final AlmConfiguration almConfiguration;

    private CookieStore cookieStore;
    private CloseableHttpClient httpClient;

    @Autowired
    public AlmClient(AlmConfiguration almConfiguration) {
        this.almConfiguration = almConfiguration;
    }

    public void login() {
        String url = almConfiguration.getBaseUrl() + LOGIN_URL;

        // disable host name verification
        cookieStore = new BasicCookieStore();
        httpClient = HttpClients.custom()
                .setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
                .setDefaultCookieStore(cookieStore)
                .build();

        Executor executor = Executor.newInstance(httpClient);
        HttpHost host = HttpHost.create(almConfiguration.getBaseUrl());
        executor.auth(almConfiguration.getUsername(), almConfiguration.getPassword()).authPreemptive(host);
        try {
            HttpResponse response = executor
                    .execute(Request.Get(url)
                            .connectTimeout(3000)
                            .socketTimeout(3000))
                    .returnResponse();

            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode < 200 || statusCode > 299) {
                throw new RuntimeException("Login to ALM did not succeed.");
            }

        } catch (Exception e) {
            LOG.error("Could not login to ALM:" + e.getMessage(), e);
        }

    }

    public boolean hasAuthenticationCookie() {
        return hasCookie(SSO_COOKIE_NAME);
    }

    private boolean hasCookie(String cookieName) {
        if (this.httpClient != null && this.cookieStore != null) {
            List<Cookie> cookies = cookieStore.getCookies();
            for (Cookie cookie : cookies) {
                if (cookieName.equals(cookie.getName())) {
                    return true;
                }
            }
        }
        return false;
    }

    public void startSession() {
        if (httpClient == null || cookieStore == null) {
            throw new RuntimeException("You must login() prior of starting a session.");
        }

        String url = almConfiguration.getBaseUrl() + SESSION_URL;
        Executor executor = Executor.newInstance(httpClient);
        try {
            HttpResponse response = executor
                    .execute(Request.Post(url)
                            .connectTimeout(3000)
                            .socketTimeout(3000))
                    .returnResponse();

            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode < 200 || statusCode > 299) {
                throw new RuntimeException("Start Session in ALM did not succeed.");
            }

        } catch (Exception e) {
            LOG.error("Could not start session in ALM:" + e.getMessage(), e);
        }
    }

    public boolean hasSessionCookie() {
        return hasCookie(SESSION_COOKIE_NAME);
    }

    public List<TestSetFolder> retrieveTestSetFolder(String domain, String project, String testSetFolderName) {
        if (httpClient == null || cookieStore == null) {
            throw new RuntimeException("You must login() prior of sending requests.");
        }

        Executor executor = Executor.newInstance(httpClient);
        try {
            String url = almConfiguration.getBaseUrl() + String.format(TEST_SET_FOLDERS_URL,
                    domain,
                    project,
                    URLEncoder.encode("{name['" + testSetFolderName + "']}", UTF_8));
            String response = executor
                    .execute(Request.Get(url)
                            .addHeader("Accept", "application/json")
                            .connectTimeout(3000)
                            .socketTimeout(3000))
                    .returnContent()
                    .asString();

            return createTestSetFolderNames(response);

        } catch (Exception e) {
            LOG.error("Could not read test-set-folders from ALM:" + e.getMessage(), e);
        }

        return Collections.emptyList();
    }

    private List<TestSetFolder> createTestSetFolderNames(String response) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        TestSetFolders testSetFolders = mapper.readValue(response, TestSetFolders.class);
        return testSetFolders.getTestSetFolders();
    }

    public List<TestSet> retrieveTestSets(String domain, String project, List<TestSetFolder> testSetFolders) {
        if (httpClient == null || cookieStore == null) {
            throw new RuntimeException("You must login() prior of sending requests.");
        }

        List<Long> parentFolderIds = testSetFolders.stream().map(TestSetFolder::getId).collect(Collectors.toList());
        String parentIdFilter = StringUtils.join(parentFolderIds, " OR ");
        String query = "{parent-id[" + parentIdFilter + "]}";

        Executor executor = Executor.newInstance(httpClient);
        try {
            String url = almConfiguration.getBaseUrl() + String.format(TEST_SETS_URL,
                    domain,
                    project,
                    URLEncoder.encode(query, UTF_8));
            String response = executor
                    .execute(Request.Get(url)
                            .addHeader("Accept", "application/json")
                            .connectTimeout(3000)
                            .socketTimeout(3000))
                    .returnContent()
                    .asString();

            return createTestSets(response);

        } catch (Exception e) {
            LOG.error("Could not read test-set-folders from ALM:" + e.getMessage(), e);
        }

        return Collections.emptyList();
    }

    private List<TestSet> createTestSets(String response) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        TestSets testSets = mapper.readValue(response, TestSets.class);
        return testSets.getTestSets();
    }

    public List<TestInstance> retrieveTestInstances(String domain, String project, List<TestSet> testSets) {
        if (httpClient == null || cookieStore == null) {
            throw new RuntimeException("You must login() prior of sending requests.");
        }

        List<Long> testSetIds = testSets.stream().map(TestSet::getId).collect(Collectors.toList());
        String cycleIds = StringUtils.join(testSetIds, " OR ");
        String query = "{cycle-id[" + cycleIds  + "]}";

        Executor executor = Executor.newInstance(httpClient);
        try {
            String url = almConfiguration.getBaseUrl() + String.format(TEST_INSTANCES_URL,
                    domain,
                    project,
                    URLEncoder.encode(query, UTF_8));
            String response = executor
                    .execute(Request.Get(url)
                            .addHeader("Accept", "application/json")
                            .connectTimeout(3000)
                            .socketTimeout(3000))
                    .returnContent()
                    .asString();

            return createTestInstances(response);

        } catch (Exception e) {
            LOG.error("Could not read test-set-folders from ALM:" + e.getMessage(), e);
        }

        return Collections.emptyList();
    }

    private List<TestInstance> createTestInstances(String response) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        TestInstances testInstances = mapper.readValue(response, TestInstances.class);
        return testInstances.getTestInstances();
    }

    public List<TestRun> retrieveRuns(String domain, String project, List<TestInstance> testInstances) {
        List<TestRun> testRuns = new ArrayList<>();
        // to ensure we retrieve the latest test-instance run we must query one by one:
        for (TestInstance ti : testInstances) {
            TestRun run = retrieveRun(domain, project, ti);
            if (run != null) {
                testRuns.add(run);
            } else {
                LOG.info("Test-Instance has no /run. id: " + ti.getId());
            }
        }

        return testRuns;
    }

    private TestRun retrieveRun(String domain, String project, TestInstance testInstance) {
        if (httpClient == null || cookieStore == null) {
            throw new RuntimeException("You must login() prior of sending requests.");
        }

        String query = "{cycle-id[" + testInstance.getCycleId() + "];test-id[" +  testInstance.getTestId() + "]}";
        String orderBy = "{execution-date[DESC];execution-time[DESC]}";

        Executor executor = Executor.newInstance(httpClient);
        try {
            String url = almConfiguration.getBaseUrl() + String.format(TEST_RUN_URL,
                    domain,
                    project,
                    URLEncoder.encode(query, UTF_8),
                    URLEncoder.encode(orderBy, UTF_8));
            String response = executor
                    .execute(Request.Get(url)
                            .addHeader("Accept", "application/json")
                            .connectTimeout(3000)
                            .socketTimeout(3000))
                    .returnContent()
                    .asString(Charset.forName("UTF-8"));

            return createTestRunInstances(response);

        } catch (Exception e) {
            LOG.error("Could not read run from ALM:" + e.getMessage(), e);
        }

        return null;
    }

    private TestRun createTestRunInstances(String response) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        TestRuns testRuns = mapper.readValue(response, TestRuns.class);
        if (testRuns == null) {
            LOG.warn("No run found");
            return null;
        }

        List<TestRun> runs = testRuns.getRuns();
        if (CollectionUtils.isNotEmpty(runs) && runs.size() == 1) {
            return runs.get(0);

        } else if (CollectionUtils.isNotEmpty(runs) && runs.size() > 1) {
            LOG.warn("Multiple Runs found!");
            return runs.get(0);

        } else {
            LOG.warn("No runs found for given test-id!");
            return null;
        }
    }

    public void logout() {
        if (httpClient == null || cookieStore == null) {
            return;
        }

        String url = almConfiguration.getBaseUrl() + LOGOUT_URL;

        Executor executor = Executor.newInstance(httpClient);
        try {
            HttpResponse response = executor
                    .execute(Request.Get(url)
                            .connectTimeout(3000)
                            .socketTimeout(3000))
                    .returnResponse();

            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode < 200 || statusCode > 299) {
                LOG.warn("Logout did not succeed: " + statusCode);
            }

            this.cookieStore.clear();
            httpClient.close();
            this.httpClient = null;
            this.cookieStore = null;

        } catch (Exception e) {
            LOG.error("Could not send logout to ALM:" + e.getMessage(), e);
        }
    }
}
