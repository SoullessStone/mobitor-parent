package ch.mobi.mobitor.plugin.hpalm.domain;

/*-
 * §
 * mobitor-plugin-hpalm
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformation;

import java.util.List;

public class AlmInformation implements ApplicationInformation {

    public static final String ALM = "alm";

    private TestResults testResults = new TestResults();

    private final String domain;
    private final String project;
    private final String foldername;
    private final String url;

    public AlmInformation(String domain, String project, String foldername, String url) {
        this.domain = domain;
        this.project = project;
        this.foldername = foldername;
        this.url = url;
    }

    @Override
    public String getType() {
        return ALM;
    }

    @Override
    public boolean hasInformation() {
        return true;
    }


    public String getDomain() {
        return domain;
    }

    public String getProject() {
        return project;
    }

    public String getFoldername() {
        return foldername;
    }

    public String getUrl() {
        return url;
    }

    public void updateTestRuns(List<TestRun> testRuns) {
        testResults.updateTestRuns(testRuns);
    }

    public long getNumberOfRuns() {
        return testResults.getTestRuns().size();
    }

    public long getPassedRuns() {
        return testResults.getTestRuns().stream().filter(TestRun::hasPassed).count();
    }

    public long getFailedRuns() {
        return testResults.getTestRuns().stream().filter(TestRun::hasFailed).count();
    }

    public TestResults getTestResults() {
        return testResults;
    }

    public boolean hasFailedRuns() {
        return getFailedRuns() > 0;
    }

    public boolean hasRuns() {
        return getNumberOfRuns() > 0;
    }
}
