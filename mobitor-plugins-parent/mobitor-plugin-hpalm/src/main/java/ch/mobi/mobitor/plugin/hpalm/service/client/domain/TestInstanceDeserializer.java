package ch.mobi.mobitor.plugin.hpalm.service.client.domain;

/*-
 * §
 * mobitor-plugin-hpalm
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;

import java.io.IOException;

public class TestInstanceDeserializer extends JsonDeserializer {

    @Override
    public Object deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        TreeNode node = p.getCodec().readTree(p);
        TreeNode fields = node.get("Fields");

        String name = "";
        String status = "";
        long id = -1;
        long testId = -1;
        long cycleId = -1;
        for(int i = 0; i < fields.size(); i++) {
            ObjectNode fieldProps = (ObjectNode) fields.get(i);
            TextNode fieldName = (TextNode) fieldProps.get("Name");
            ArrayNode fieldValues = (ArrayNode) fieldProps.get("values");

            if ("name".equals(fieldName.asText())) {
                ObjectNode valueNode = (ObjectNode) fieldValues.get(0);
                TextNode fieldValue = (TextNode) valueNode.get("value");
                name = fieldValue.asText();

            } else if ("id".equals(fieldName.asText())) {
                ObjectNode valueNode = (ObjectNode) fieldValues.get(0);
                TextNode fieldValue = (TextNode) valueNode.get("value");
                id = Long.parseLong(fieldValue.asText());

            } else if ("test-id".equals(fieldName.asText())) {
                ObjectNode valueNode = (ObjectNode) fieldValues.get(0);
                TextNode fieldValue = (TextNode) valueNode.get("value");
                testId = Long.parseLong(fieldValue.asText());

            } else if ("cycle-id".equals(fieldName.asText())) {
                ObjectNode valueNode = (ObjectNode) fieldValues.get(0);
                TextNode fieldValue = (TextNode) valueNode.get("value");
                cycleId = Long.parseLong(fieldValue.asText());

            } else if ("status".equals(fieldName.asText())) {
                ObjectNode valueNode = (ObjectNode) fieldValues.get(0);
                TextNode fieldValue = (TextNode) valueNode.get("value");
                status = fieldValue.asText();
            }
        }

        return new TestInstance(testId, cycleId, name, id, status);
    }
}
