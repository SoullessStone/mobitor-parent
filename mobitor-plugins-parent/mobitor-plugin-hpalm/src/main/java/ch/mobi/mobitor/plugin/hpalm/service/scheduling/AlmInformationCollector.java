package ch.mobi.mobitor.plugin.hpalm.service.scheduling;

/*-
 * §
 * mobitor-plugin-hpalm
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.hpalm.HpAlmTestsPlugin;
import ch.mobi.mobitor.plugin.hpalm.domain.AlmInformation;
import ch.mobi.mobitor.plugin.hpalm.domain.TestRun;
import ch.mobi.mobitor.plugin.hpalm.service.client.AlmClient;
import ch.mobi.mobitor.plugin.hpalm.service.client.domain.TestInstance;
import ch.mobi.mobitor.plugin.hpalm.service.client.domain.TestSet;
import ch.mobi.mobitor.plugin.hpalm.service.client.domain.TestSetFolder;
import ch.mobi.mobitor.plugins.api.model.ScreensModel;
import ch.mobi.mobitor.plugins.api.service.RuleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

import static ch.mobi.mobitor.plugin.hpalm.domain.AlmInformation.ALM;

@Component
@ConditionalOnBean(HpAlmTestsPlugin.class)
public class AlmInformationCollector {

    private final static Logger LOG = LoggerFactory.getLogger(AlmInformationCollector.class);

    private final ScreensModel screensModel;
    private final RuleService ruleService;
    private final AlmClient almClient;

    @Autowired
    public AlmInformationCollector(ScreensModel screensModel,
                                   RuleService ruleService,
                                   AlmClient almClient) {
        this.screensModel = screensModel;
        this.ruleService = ruleService;
        this.almClient = almClient;
    }

    @Scheduled(fixedDelayString = "${scheduling.pollingIntervalMs.almPollingInterval}", initialDelayString = "${scheduling.pollingInitialDelayMs.second}")
    public void collectAlmTestRunsInformation() {
        long start = System.currentTimeMillis();
        almClient.login();
        almClient.startSession();

        this.screensModel.getAvailableScreens().forEach(this::populateAlmInformation);

        almClient.logout();
        long stop = System.currentTimeMillis();
        LOG.info("Read HP ALM Information. Took: " + (stop - start) + "ms");
    }

    private void populateAlmInformation(Screen screen) {
        List<AlmInformation> almInformationList = screen.getMatchingInformation(ALM);
        almInformationList.forEach(this::updateTestRuns);

        ruleService.updateRuleEvaluation(screen, ALM);
        screen.setRefreshDate(ALM, new Date());
    }

    private void updateTestRuns(AlmInformation almInformation) {
        String domain = almInformation.getDomain();
        String project = almInformation.getProject();
        String foldername = almInformation.getFoldername();

        List<TestSetFolder> testSetFolders = almClient.retrieveTestSetFolder(domain, project, foldername);
        List<TestSet> testSets = almClient.retrieveTestSets(domain, project, testSetFolders);
        List<TestInstance> testInstances = almClient.retrieveTestInstances(domain, project, testSets);
        List<TestRun> testRuns = almClient.retrieveRuns(domain, project, testInstances);

        almInformation.updateTestRuns(testRuns);
        LOG.debug("Updated runs for folder: " + foldername);
    }

}
