package ch.mobi.mobitor.plugin.hpalm.domain;

/*-
 * §
 * mobitor-plugin-hpalm
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

// test-set-folders
// test-sets

// test-instances
//   runs (one latest run per instance)
public class TestResults {

    private List<TestRun> testRuns = Collections.emptyList();

    public void updateTestRuns(List<TestRun> testRuns) {
        this.testRuns = testRuns;
    }

    public List<TestRun> getTestRuns() {
        return Collections.unmodifiableList(testRuns);
    }

    public List<TestRun> getTestRunsOrderedById() {
        List<TestRun> sortedList = new ArrayList<>(testRuns);
        sortedList.sort(new TestRunComparator());

        return sortedList;
    }

}
