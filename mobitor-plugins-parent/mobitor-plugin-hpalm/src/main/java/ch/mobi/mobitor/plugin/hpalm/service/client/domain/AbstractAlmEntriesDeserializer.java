package ch.mobi.mobitor.plugin.hpalm.service.client.domain;

/*-
 * §
 * mobitor-plugin-hpalm
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;

import java.io.IOException;


/*
http://wiki.fasterxml.com/JacksonHowToCustomDeserializers
https://github.com/eugenp/tutorials/tree/master/jackson#readme
http://www.baeldung.com/jackson-deserialization
*/
public abstract class AbstractAlmEntriesDeserializer<T> extends JsonDeserializer<T> {

    @Override
    public T deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        TreeNode node = p.getCodec().readTree(p);
        TreeNode fields = node.get("Fields");

        String folderName = "";
        long id = -1;
        for(int i = 0; i < fields.size(); i++) {
            ObjectNode fieldProps = (ObjectNode) fields.get(i);
            TextNode fieldName = (TextNode) fieldProps.get("Name");
            ArrayNode fieldValues = (ArrayNode) fieldProps.get("values");

            if ("name".equals(fieldName.asText())) {
                ObjectNode valueNode = (ObjectNode) fieldValues.get(0);
                TextNode fieldValue = (TextNode) valueNode.get("value");
                folderName = fieldValue.asText();

            } else if ("id".equals(fieldName.asText())) {
                ObjectNode valueNode = (ObjectNode) fieldValues.get(0);
                TextNode fieldValue = (TextNode) valueNode.get("value");
                id = Long.parseLong(fieldValue.asText());
            }
        }

        return createNewEntry(folderName, id);
    }

    protected abstract T createNewEntry(String name, long id);
}
