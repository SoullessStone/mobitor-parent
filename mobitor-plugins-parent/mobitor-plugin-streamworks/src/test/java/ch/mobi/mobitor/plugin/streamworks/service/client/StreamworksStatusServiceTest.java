package ch.mobi.mobitor.plugin.streamworks.service.client;

/*-
 * §
 * mobitor-plugin-streamworks
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.streamworks.service.client.dto.StreamworksServerConfig;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.junit.Rule;
import org.junit.Test;

import java.time.LocalDate;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class StreamworksStatusServiceTest {

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(options().dynamicPort());

    private final static String SERVICE_BASE_PATH = "/sworkswww/data/verarbeitungen/W";

    private void configureReleasesUrl() {
        givenThat(get(urlMatching("/.*")).willReturn(aResponse().withStatus(404)));
        givenThat(get(urlMatching(SERVICE_BASE_PATH + "/EDWH/.*"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("streamworks-status-response.json")
                )
        );
    }

    @Test
    public void retrieveStatusInformation() {
        // arrange
        String baseUrl = "http://localhost:" + wireMockRule.port() + SERVICE_BASE_PATH;
        configureReleasesUrl();
        StreamworksConfigurationService configService = mock(StreamworksConfigurationService.class);
        StreamworksServerConfig serverConfig = new StreamworksServerConfig(baseUrl, "BB");
        when(configService.getStreamworksServerConfig(anyString())).thenReturn(serverConfig);
        StreamworksStatusService service = new StreamworksStatusService(configService);

        // act & assert
        assertEquals(new StreamworksStatus("EDWH", LocalDate.of(2019, 5, 15), "Prepared"),
                service.retrieveStreamworksInformation("EDWH", "BB"));

        assertNull(service.retrieveStreamworksInformation("EDWHC", "BB"));
    }
}
