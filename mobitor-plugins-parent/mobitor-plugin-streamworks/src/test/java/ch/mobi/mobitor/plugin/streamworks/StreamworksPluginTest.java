package ch.mobi.mobitor.plugin.streamworks;

/*-
 * §
 * mobitor-plugin-streamworks
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.streamworks.config.StreamworksConfig;
import ch.mobi.mobitor.plugin.streamworks.domain.StreamworksStatusInformation;
import ch.mobi.mobitor.plugins.api.domain.config.ExtendableScreenConfig;
import ch.mobi.mobitor.service.EnvironmentsConfigurationService;
import org.junit.Test;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class StreamworksPluginTest {

    @Test
    public void createAndAssociateApplicationInformationBlocksShouldCreateStreamworksStatusInformation() {
        //given
        String serverName = "serverName";
        String applicationName = "applicationName";
        String env = "Prod";
        EnvironmentsConfigurationService environmentsConfigurationService = mock(EnvironmentsConfigurationService.class);
        StreamworksPlugin plugin = new StreamworksPlugin(environmentsConfigurationService);
        Screen screen = mock(Screen.class);
        ExtendableScreenConfig screenConfig = mock(ExtendableScreenConfig.class);
        StreamworksConfig config = new StreamworksConfig(serverName, null, applicationName);
        given(screen.getEnvironments()).willReturn(singletonList(env));
        given(environmentsConfigurationService.isNetworkReachable(anyString())).willReturn(true);

        //when
        plugin.createAndAssociateApplicationInformationBlocks(screen, screenConfig, singletonList(config));

        //then
        verify(screen).addInformation(eq(serverName), eq(applicationName), eq(env), any(StreamworksStatusInformation.class));
    }

    @Test
    public void createStreamworksInformationShouldContainAllRequiredProperties() {
        //given
        EnvironmentsConfigurationService environmentsConfigurationService = mock(EnvironmentsConfigurationService.class);
        given(environmentsConfigurationService.isNetworkReachable(anyString())).willReturn(true);
        StreamworksPlugin plugin = new StreamworksPlugin(environmentsConfigurationService);
        String serverName = "junitServer";
        String environment = "junitEnv";

        //when
        StreamworksStatusInformation information = plugin.createStreamworksStatusInformation(serverName, environment);

        //then
        assertThat(information.getProcess()).isEqualTo(serverName);
        assertThat(information.getEnvironment()).isEqualTo(environment);
    }

    @Test
    public void getConfigClass() {
        // arrange
        EnvironmentsConfigurationService environmentsConfigurationService = mock(EnvironmentsConfigurationService.class);
        given(environmentsConfigurationService.isNetworkReachable(anyString())).willReturn(true);
        StreamworksPlugin plugin = new StreamworksPlugin(environmentsConfigurationService);

        // act
        Class<StreamworksConfig> configClass = plugin.getConfigClass();

        // assert
        assertThat(configClass).isEqualTo(StreamworksConfig.class);
    }

}
