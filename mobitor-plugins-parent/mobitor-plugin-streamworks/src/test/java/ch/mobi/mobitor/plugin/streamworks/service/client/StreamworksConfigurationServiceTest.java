package ch.mobi.mobitor.plugin.streamworks.service.client;

/*-
 * §
 * mobitor-plugin-streamworks
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.streamworks.service.client.dto.StreamworksServerConfig;
import org.junit.Test;
import org.springframework.core.io.DefaultResourceLoader;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

public class StreamworksConfigurationServiceTest {

    @Test
    public void getStreamworksServerConfig() {
        // arrange
        StreamworksConfigurationService service = new StreamworksConfigurationService(new DefaultResourceLoader());
        service.initializeStreamworksServers();

        // act
        StreamworksServerConfig serverConfig = service.getStreamworksServerConfig("W");

        // assert
        assertThat(serverConfig, is(not(nullValue())));
        assertThat(serverConfig.getEnvironment(), is(not(nullValue())));
        assertThat(serverConfig.getUrl(), is(not(nullValue())));

    }
}
