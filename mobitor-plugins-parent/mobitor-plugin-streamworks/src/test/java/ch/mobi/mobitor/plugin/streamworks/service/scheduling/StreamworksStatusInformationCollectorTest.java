package ch.mobi.mobitor.plugin.streamworks.service.scheduling;

/*-
 * §
 * mobitor-plugin-streamworks
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.streamworks.domain.StreamworksStatusInformation;
import ch.mobi.mobitor.plugin.streamworks.service.client.StreamworksStatus;
import ch.mobi.mobitor.plugin.streamworks.service.client.StreamworksStatusService;
import ch.mobi.mobitor.plugins.api.model.ScreensModel;
import ch.mobi.mobitor.plugins.api.service.CollectorMetricService;
import ch.mobi.mobitor.plugins.api.service.RuleService;
import org.junit.Test;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import static ch.mobi.mobitor.plugin.streamworks.domain.StreamworksState.COMPLETED;
import static ch.mobi.mobitor.plugin.streamworks.domain.StreamworksStatusInformation.STREAMWORKS_STATUS;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

public class StreamworksStatusInformationCollectorTest {

    @Test
    public void collectStreamworksStatusInformation() {
        // arrange
        RuleService ruleService = mock(RuleService.class);
        ScreensModel screensModel = mock(ScreensModel.class);
        StreamworksStatusService streamworksStatusService = mock(StreamworksStatusService.class);
        Screen screen = mock(Screen.class);
        List<Screen> screensList = singletonList(screen);
        StreamworksStatusInformation info1 = new StreamworksStatusInformation("server", "env");

        when(screensModel.getAvailableScreens()).thenReturn(screensList);
        when(screen.getMatchingInformation(eq(STREAMWORKS_STATUS))).thenReturn(asList(info1));
        LocalDate version = LocalDate.of(1, 2, 3);
        String state = "Completed";
        StreamworksStatus ed = new StreamworksStatus("edwhc", version, state);
        when(streamworksStatusService.retrieveStreamworksInformation(any(), any())).thenReturn(ed);

        CollectorMetricService collectorMetricService = mock(CollectorMetricService.class);

        StreamworksStatusInformationCollector collector = new StreamworksStatusInformationCollector(screensModel, ruleService, streamworksStatusService, collectorMetricService);

        // act
        collector.collectStreamworksStatusInformation();

        // assert
        verify(ruleService).updateRuleEvaluation(any(), eq(STREAMWORKS_STATUS));
        verify(screen).setRefreshDate(eq(STREAMWORKS_STATUS), any(Date.class));
        verify(collectorMetricService).updateLastRunCompleted(eq(STREAMWORKS_STATUS));
        verify(collectorMetricService).submitCollectorDuration(anyString(), anyLong());
        assertThat(info1.getVersion()).isEqualTo(version.toString());
        assertThat(info1.getState()).isEqualTo(COMPLETED);
    }

}
