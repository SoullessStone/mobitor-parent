package ch.mobi.mobitor.plugin.streamworks.service.client;

/*-
 * §
 * mobitor-plugin-streamworks
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.config.ObjectMappers;
import ch.mobi.mobitor.plugin.streamworks.service.client.dto.StreamworksServerConfig;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class StreamworksConfigurationService {

    private static final Logger LOG = LoggerFactory.getLogger(StreamworksConfigurationService.class);

    private final ResourceLoader resourceLoader;
    private Map<String, StreamworksServerConfig> streamworksServerConfigMap;

    @Autowired
    public StreamworksConfigurationService(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    @PostConstruct
    public void initializeStreamworksServers() {
        ObjectMapper mapper = ObjectMappers.standard();

        Resource resource = resourceLoader.getResource("classpath:mobitor/plugin/streamworks/config/streamworks-servers.json");
        try (InputStream inputStream = resource.getInputStream()) {
            List<StreamworksServerConfig> streamworksServerConfigs = mapper.readValue(inputStream, mapper.getTypeFactory().constructCollectionType(List.class, StreamworksServerConfig.class));
            streamworksServerConfigMap = streamworksServerConfigs.stream().collect(Collectors.toMap(StreamworksServerConfig::getEnvironment, Function.identity()));
        } catch (Exception e) {
            LOG.error("Could not initialize kubernetes servers.", e);
        }
    }

    public StreamworksServerConfig getStreamworksServerConfig(String environment) {
        return streamworksServerConfigMap.get(environment);
    }
}
