package ch.mobi.mobitor.plugin.streamworks;

/*-
 * §
 * mobitor-plugin-streamworks
 * --
 * Copyright (C) 2018 - 2019 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.streamworks.domain.StreamworksState;
import ch.mobi.mobitor.plugin.streamworks.domain.StreamworksStatusInformation;
import ch.mobi.mobitor.plugins.api.MobitorPluginLegendGenerator;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformationLegendWrapper;

import java.util.List;

import static java.util.Collections.singletonList;

public class StreamworksStatusLegendGenerator implements MobitorPluginLegendGenerator {

    @Override
    public List<ApplicationInformationLegendWrapper> createSuccessList() {
        StreamworksStatusInformation info = new StreamworksStatusInformation("SERVER", "env");
        info.setState(StreamworksState.COMPLETED);
        info.setVersion("47.1");

        return singletonList(new ApplicationInformationLegendWrapper("Successful Streamwork Status", info));
    }

    @Override
    public List<ApplicationInformationLegendWrapper> createErrorList() {
        StreamworksStatusInformation info = new StreamworksStatusInformation("SERVER", "env");
        info.setState(StreamworksState.ABNORMALLY_ENDED);
        info.setVersion("48.0");

        return singletonList(new ApplicationInformationLegendWrapper("Failed Streamworks Status", info));
    }

}
