package ch.mobi.mobitor.plugin.edwh.rule;

/*-
 * §
 * mobitor-plugin-edwh
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Pipeline;
import ch.mobi.mobitor.domain.screen.PipelineRule;
import ch.mobi.mobitor.domain.screen.RuleEvaluation;
import ch.mobi.mobitor.plugin.edwh.domain.EdwhDeploymentsInformation;
import ch.mobi.mobitor.plugin.test.rule.PipelineRuleTest;
import org.junit.Test;

import static ch.mobi.mobitor.plugin.edwh.domain.EdwhDeploymentsInformation.EDWH_DEPLOYMENT;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class EdwhDeploymentSuccessRuleTest extends PipelineRuleTest {

    @Test
    public void validateRuleCaresAboutCorrectType() {
        // arrange
        EdwhDeploymentSuccessRule rule = new EdwhDeploymentSuccessRule();

        // act
        boolean validatesType = rule.validatesType(EDWH_DEPLOYMENT);

        // assert
        assertThat(validatesType, is(true));
    }

    @Test
    public void validateRuleHasFailuresWhenDeploymentFailed() {
        // arrange
        EdwhDeploymentSuccessRule rule = new EdwhDeploymentSuccessRule();
        Pipeline pipeline = createPipeline();
        EdwhDeploymentsInformation edwhInfo = new EdwhDeploymentsInformation();
        edwhInfo.setApplicationName(APP_NAME);
        edwhInfo.setServerName(SERVER_NAME);
        edwhInfo.setEnvironment(ENV);
        edwhInfo.setState("faaalsch");

        pipeline.addInformation(ENV, APP_NAME, edwhInfo);
        RuleEvaluation newRuleEvaluation = createNewRuleEvaluation();

        // act
        rule.evaluateRule(pipeline, newRuleEvaluation);

        // assert
        assertThat(newRuleEvaluation.hasErrors(), is(true));
    }

    @Test
    public void validateRuleSuccessfulWhenDeploymentOk() {
        // arrange
        EdwhDeploymentSuccessRule rule = new EdwhDeploymentSuccessRule();
        Pipeline pipeline = createPipeline();
        EdwhDeploymentsInformation edwhInfo = new EdwhDeploymentsInformation();
        edwhInfo.setApplicationName(APP_NAME);
        edwhInfo.setServerName(SERVER_NAME);
        edwhInfo.setEnvironment(ENV);
        edwhInfo.setState("successful");

        pipeline.addInformation(ENV, APP_NAME, edwhInfo);
        RuleEvaluation newRuleEvaluation = createNewRuleEvaluation();

        // act
        rule.evaluateRule(pipeline, newRuleEvaluation);

        // assert
        assertThat(newRuleEvaluation.hasErrors(), is(false));
    }

    @Override
    protected PipelineRule createNewRule() {
        return new EdwhDeploymentSuccessRule();
    }
}
