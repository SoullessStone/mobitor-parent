package ch.mobi.mobitor.plugin.edwh.service.scheduling;

/*-
 * §
 * mobitor-plugin-edwh
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.edwh.domain.EdwhDeploymentsInformation;
import ch.mobi.mobitor.plugin.edwh.service.client.EdwhDeployment;
import ch.mobi.mobitor.plugin.edwh.service.client.EdwhDeploymentsService;
import ch.mobi.mobitor.plugins.api.model.ScreensModel;
import ch.mobi.mobitor.plugins.api.service.CollectorMetricService;
import ch.mobi.mobitor.plugins.api.service.RuleService;
import org.junit.Test;

import java.util.Date;
import java.util.List;

import static ch.mobi.mobitor.plugin.edwh.domain.EdwhDeploymentsInformation.EDWH_DEPLOYMENT;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class EdwhDeploymentsInformationCollectorTest {

    @Test
    public void collectEdwhDeploymentsInformation() {
        // arrange
        RuleService ruleService = mock(RuleService.class);
        ScreensModel screensModel = mock(ScreensModel.class);
        EdwhDeploymentsService edwhDeploymentsService = mock(EdwhDeploymentsService.class);
        Screen screen = mock(Screen.class);
        List<Screen> screensList = singletonList(screen);
        EdwhDeploymentsInformation info1 = new EdwhDeploymentsInformation();

        when(screensModel.getAvailableScreens()).thenReturn(screensList);
        when(screen.getMatchingInformation(eq(EDWH_DEPLOYMENT))).thenReturn(asList(info1));
        String version = "latest";
        String state = "successful";
        EdwhDeployment ed = new EdwhDeployment();
        ed.setRelease(version);
        ed.setState(state);
        when(edwhDeploymentsService.retrieveDeploymentInformation(any(), any(), any())).thenReturn(ed);

        CollectorMetricService collectorMetricService = mock(CollectorMetricService.class);

        EdwhDeploymentsInformationCollector collector = new EdwhDeploymentsInformationCollector(screensModel, ruleService, edwhDeploymentsService, collectorMetricService);

        // act
        collector.collectEdwhDeploymentsInformation();

        // assert
        verify(ruleService).updateRuleEvaluation(any(), eq(EDWH_DEPLOYMENT));
        verify(screen).setRefreshDate(eq(EDWH_DEPLOYMENT), any(Date.class));
        verify(collectorMetricService).updateLastRunCompleted(eq(EDWH_DEPLOYMENT));
        verify(collectorMetricService).submitCollectorDuration(anyString(), anyLong());
        assertThat(info1.getVersion()).isEqualTo(version);
        assertThat(info1.getState()).isEqualTo(state);
    }

}
