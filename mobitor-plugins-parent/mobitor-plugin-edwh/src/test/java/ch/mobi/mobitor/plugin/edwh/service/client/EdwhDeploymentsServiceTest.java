package ch.mobi.mobitor.plugin.edwh.service.client;

/*-
 * §
 * mobitor-plugin-edwh
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.edwh.service.client.dto.EdwhServerConfig;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import org.hamcrest.CoreMatchers;
import org.junit.Rule;
import org.junit.Test;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.givenThat;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class EdwhDeploymentsServiceTest {

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(options().dynamicPort());

    private final static String SERVICE_BASE_PATH = "/dwh/etl/rest";

    private void configureReleasesUrl() {
        givenThat(get(urlMatching(SERVICE_BASE_PATH + "/releases/.*"))
                .withHeader("Accept", equalTo("application/json"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("dwh-etl-releases-response.json")
                )
        );
    }


    @Test
    public void retrieveDeploymentInformation() {
        // arrange
        String baseUrl = "http://localhost:" + wireMockRule.port() + SERVICE_BASE_PATH;
        configureReleasesUrl();
        EdwhConfigurationService configService = mock(EdwhConfigurationService.class);
        EdwhServerConfig edwhServerConfig = new EdwhServerConfig();
        edwhServerConfig.setEnvironment("BB");
        edwhServerConfig.setUrl(baseUrl);
        when(configService.getEdwhServerConfig(anyString())).thenReturn(edwhServerConfig);
        EdwhDeploymentsService service = new EdwhDeploymentsService(configService);

        // act
        EdwhDeployment edwhDeployment = service.retrieveDeploymentInformation("EDWHC", "QUERYSURGE", "BB");

        // assert
        assertThat(edwhDeployment, is(not(nullValue())));
        assertThat(edwhDeployment.getRelease(), is(CoreMatchers.equalTo("4444.0")));
    }
}
