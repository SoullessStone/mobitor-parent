package ch.mobi.mobitor.plugin.edwh.service.client;

/*-
 * §
 * mobitor-plugin-edwh
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.edwh.service.client.dto.EdwhServerConfig;
import org.junit.Test;
import org.springframework.core.io.DefaultResourceLoader;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.*;

public class EdwhConfigurationServiceTest {

    @Test
    public void getEdwhServerConfig() {
        // arrange
        EdwhConfigurationService service = new EdwhConfigurationService(new DefaultResourceLoader());
        service.initializeEdwhServers();

        // act
        EdwhServerConfig serverConfig = service.getEdwhServerConfig("W");

        // assert
        assertThat(serverConfig, is(not(nullValue())));
        assertThat(serverConfig.getEnvironment(), is(not(nullValue())));
        assertThat(serverConfig.getUrl(), is(not(nullValue())));

    }
}
