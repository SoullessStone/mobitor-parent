package ch.mobi.mobitor.plugin.edwh;

/*-
 * §
 * mobitor-plugin-edwh
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.edwh.config.EdwhConfig;
import ch.mobi.mobitor.plugin.edwh.domain.EdwhDeploymentsInformation;
import ch.mobi.mobitor.plugins.api.domain.config.ExtendableScreenConfig;
import ch.mobi.mobitor.service.EnvironmentsConfigurationService;
import org.junit.Test;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class EdwhDeploymentsPluginTest {

    @Test
    public void createAndAssociateApplicationInformationBlocksShouldCreateEdwhInformation() {
        //given
        String serverName = "serverName";
        String applicationName = "applicationName";
        String env = "Prod";
        EnvironmentsConfigurationService environmentsConfigurationService = mock(EnvironmentsConfigurationService.class);
        EdwhDeploymentsPlugin plugin = new EdwhDeploymentsPlugin(environmentsConfigurationService);
        Screen screen = mock(Screen.class);
        ExtendableScreenConfig screenConfig = mock(ExtendableScreenConfig.class);
        EdwhConfig config = new EdwhConfig();
        config.setServerName(serverName);
        config.setApplicationNames(singletonList(applicationName));
        given(screen.getEnvironments()).willReturn(singletonList(env));
        given(environmentsConfigurationService.isNetworkReachable(anyString())).willReturn(true);

        //when
        plugin.createAndAssociateApplicationInformationBlocks(screen, screenConfig, singletonList(config));

        //then
        verify(screen).addInformation(eq(serverName), eq(applicationName), eq(env), any(EdwhDeploymentsInformation.class));
    }

    @Test
    public void createEdwhInformationShouldContainAllRequiredProperties() {
        //given
        EnvironmentsConfigurationService environmentsConfigurationService = mock(EnvironmentsConfigurationService.class);
        given(environmentsConfigurationService.isNetworkReachable(anyString())).willReturn(true);
        EdwhDeploymentsPlugin plugin = new EdwhDeploymentsPlugin(environmentsConfigurationService);
        String serverName = "junitServer";
        String applicationName = "junitApplication";
        String environment = "junitEnv";

        //when
        EdwhDeploymentsInformation information = plugin.createEdwhDeploymentsInformation(serverName, applicationName, environment);

        //then
        assertThat(information.getServerName()).isEqualTo(serverName);
        assertThat(information.getApplicationName()).isEqualTo(applicationName);
        assertThat(information.getEnvironment()).isEqualTo(environment);
    }

    @Test
    public void getConfigClass() {
        // arrange
        EnvironmentsConfigurationService environmentsConfigurationService = mock(EnvironmentsConfigurationService.class);
        given(environmentsConfigurationService.isNetworkReachable(anyString())).willReturn(true);
        EdwhDeploymentsPlugin plugin = new EdwhDeploymentsPlugin(environmentsConfigurationService);

        // act
        Class<EdwhConfig> configClass = plugin.getConfigClass();

        // assert
        assertThat(configClass).isEqualTo(EdwhConfig.class);
    }

}
