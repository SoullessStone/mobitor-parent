package ch.mobi.mobitor.plugin.edwh.service.scheduling;

/*-
 * §
 * mobitor-plugin-edwh
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.edwh.EdwhDeploymentsPlugin;
import ch.mobi.mobitor.plugin.edwh.domain.EdwhDeploymentsInformation;
import ch.mobi.mobitor.plugin.edwh.service.client.EdwhDeployment;
import ch.mobi.mobitor.plugin.edwh.service.client.EdwhDeploymentsService;
import ch.mobi.mobitor.plugins.api.model.ScreensModel;
import ch.mobi.mobitor.plugins.api.service.CollectorMetricService;
import ch.mobi.mobitor.plugins.api.service.RuleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

import static ch.mobi.mobitor.plugin.edwh.domain.EdwhDeploymentsInformation.EDWH_DEPLOYMENT;

@Component
@ConditionalOnBean(EdwhDeploymentsPlugin.class)
public class EdwhDeploymentsInformationCollector {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final ScreensModel screensModel;
    private final RuleService ruleService;
    private EdwhDeploymentsService edwhDeploymentsService;
    private final CollectorMetricService collectorMetricService;

    @Autowired
    public EdwhDeploymentsInformationCollector(ScreensModel screensModel,
                                               RuleService ruleService,
                                               EdwhDeploymentsService edwhDeploymentsService,
                                               CollectorMetricService collectorMetricService) {
        this.screensModel = screensModel;
        this.ruleService = ruleService;
        this.edwhDeploymentsService = edwhDeploymentsService;
        this.collectorMetricService = collectorMetricService;
    }

    @Scheduled(fixedDelayString = "${scheduling.pollingIntervalMs.edwhDeploymentInformationPollingInterval}", initialDelayString = "${scheduling.pollingInitialDelayMs.second}")
    public void collectEdwhDeploymentsInformation() {
        long start = System.currentTimeMillis();
        logger.info("Started retrieving EDWH deployments information...");
        this.screensModel.getAvailableScreens().parallelStream().forEach(this::populateEdwhDeploymentsInformation);
        long stop = System.currentTimeMillis();
        long duration = stop - start;
        logger.info("Reading EDWH deployment information took: " + duration + "ms");
        collectorMetricService.submitCollectorDuration("edwh.deployments", duration);
        collectorMetricService.updateLastRunCompleted(EDWH_DEPLOYMENT);
    }

    private void populateEdwhDeploymentsInformation(Screen screen) {
        List<EdwhDeploymentsInformation> edwhDeploymentsInformationList = screen.getMatchingInformation(EDWH_DEPLOYMENT);
        edwhDeploymentsInformationList.forEach(this::updateInformation);
        ruleService.updateRuleEvaluation(screen, EDWH_DEPLOYMENT);
        screen.setRefreshDate(EDWH_DEPLOYMENT, new Date());
    }

    private void updateInformation(EdwhDeploymentsInformation information) {
        EdwhDeployment edwhDeployment = edwhDeploymentsService.retrieveDeploymentInformation(information.getServerName(), information.getApplicationName(), information.getEnvironment());
        if (edwhDeployment != null) {
            information.setVersion(edwhDeployment.getRelease());
            information.setState(edwhDeployment.getState());
        }
    }

}
