package ch.mobi.mobitor.plugins.api;

/*-
 * §
 * mobitor-plugins-test
 * --
 * Copyright (C) 2018 - 2019 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformationLegendWrapper;
import org.junit.Test;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;

public abstract class AbstractMobitorLegendGeneratorTest {

    @Test
    public void testSuccessItemsExist() {
        // arrange
        MobitorPluginLegendGenerator generator = createLegendGenerator();

        // act
        List<ApplicationInformationLegendWrapper> successList = generator.createSuccessList();

        // assert
        assertThat(successList, hasSize(expectedSuccessListSize()));
    }

    @Test
    public void testErrorItemsExist() {
        // arrange
        MobitorPluginLegendGenerator generator = createLegendGenerator();

        // act
        List<ApplicationInformationLegendWrapper> errorList = generator.createErrorList();

        // assert
        assertThat(errorList, hasSize(expectedErrorListSize()));
    }

    @Test
    public void testProgressItemsMayExist() {
        // arrange
        MobitorPluginLegendGenerator generator = createLegendGenerator();

        // act
        List<ApplicationInformationLegendWrapper> progressList = generator.createProgressList();

        // assert
        assertThat(progressList, hasSize(expectedProgressListSize()));
    }

    protected int expectedProgressListSize() {
        return 0;
    }

    protected int expectedErrorListSize() {
        return 1;
    }

    protected int expectedSuccessListSize() {
        return 1;
    }

    public abstract MobitorPluginLegendGenerator createLegendGenerator();

}
