package ch.mobi.mobitor.plugin.test;

/*-
 * §
 * mobitor-plugins-test
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugins.api.MobitorPlugin;
import ch.mobi.mobitor.plugins.api.PluginConfig;
import ch.mobi.mobitor.plugins.api.domain.config.ExtendableScreenConfig;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformation;
import org.junit.Test;
import org.springframework.core.GenericTypeResolver;

import java.util.Objects;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public abstract class PluginTest<T extends MobitorPlugin<C>, C extends PluginConfig, I extends ApplicationInformation> {

    private final Class<T> classUnderTest;
    private final Class<C> configClass;
    private final Class<I> informationClass;

    @SuppressWarnings("unchecked")
    public PluginTest() {
        Class<?>[] genericTypes = Objects.requireNonNull(GenericTypeResolver.resolveTypeArguments(getClass(), PluginTest.class));
        this.classUnderTest = (Class<T>) genericTypes[0];
        this.configClass = (Class<C>) genericTypes[1];
        this.informationClass = (Class<I>) genericTypes[2];
    }

    public abstract String getConfigPropertyName();

    /**
     * This class can be overridden if the class to test has no default constructor
     * @return An instance of the class to test
     */
    protected T newClassUnderTestInstance() throws Exception {
        return classUnderTest.newInstance();
    }

    @Test
    public void getConfigPropertyNameShouldReturnCorrectName() throws Exception {
        //arrange
        T sut = newClassUnderTestInstance();

        //act
        String result = sut.getConfigPropertyName();

        //assert
        assertThat(result).isEqualTo(getConfigPropertyName());
    }

    @Test
    public void getConfigClassShouldReturnCorrectName() throws Exception {
        //arrange
        T sut = newClassUnderTestInstance();

        //act
        Class<?> result = sut.getConfigClass();

        //assert
        assertThat(result).isEqualTo(configClass);
    }

    @Test
    public void createAndAssociateApplicationInformationBlocksShouldAddInformationToScreen() throws Exception {
        //arrange
        T sut = newClassUnderTestInstance();
        Screen screen = mock(Screen.class);
        ExtendableScreenConfig screenConfig = mock(ExtendableScreenConfig.class);
        C config = configClass.newInstance();
        String serverName = "serverName";
        String applicationName = "appName";
        String environment = "env";
        config.setServerName(serverName);
        config.setApplicationName(applicationName);
        config.setEnvironment(environment);

        //act
        sut.createAndAssociateApplicationInformationBlocks(screen, screenConfig, singletonList(config));

        //assert
        verify(screen).addInformation(eq(serverName), eq(applicationName), eq(environment), any(informationClass));
    }

}
