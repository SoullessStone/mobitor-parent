package ch.mobi.mobitor.plugin.test.rule;

/*-
 * §
 * mobitor-plugins-test
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Pipeline;
import ch.mobi.mobitor.domain.screen.PipelineRule;
import ch.mobi.mobitor.domain.screen.RuleEvaluation;
import ch.mobi.mobitor.domain.screen.ServerContext;
import ch.mobi.mobitor.plugin.test.domain.TestPipeline;
import ch.mobi.mobitor.plugin.test.domain.TestRuleEvaluation;
import ch.mobi.mobitor.plugin.test.domain.TestServerContext;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

/**
 * Base Test Class for everyone implementing {@link PipelineRule}
 */
public abstract class PipelineRuleTest<T extends PipelineRule> implements PipelineRule {

    protected abstract T createNewRule();

    protected final String APP_NAME = "junit-app1";
    protected final String SERVER_NAME = "junit-serverName";
    protected final String SERVER_NAME_AGILE = "junit-serverName-agile";
    protected final String ENV = "J";

    @Override
    public void evaluateRule(Pipeline pipeline, RuleEvaluation newRuleEvaluation) {
        PipelineRule rule = createNewRule();
        rule.evaluateRule(pipeline, newRuleEvaluation);
    }

    @Override
    public boolean validatesType(String type) {
        PipelineRule rule = createNewRule();
        return rule.validatesType(type);
    }

    protected Pipeline createPipeline() {
        Map<String, ServerContext> serverContextMap = getServerContextMap();

        return new TestPipeline(SERVER_NAME, serverContextMap);
    }

    protected Pipeline createAgilePipeline() {
        Map<String, ServerContext> serverContextMap = getServerContextMap();

        return new TestPipeline(SERVER_NAME_AGILE, serverContextMap);
    }

    protected RuleEvaluation createNewRuleEvaluation() {
        return new TestRuleEvaluation();
    }

    @NotNull
    private Map<String, ServerContext> getServerContextMap() {
        Map<String, ServerContext> serverContextMap = new HashMap<>();
        ServerContext srvCtx = new TestServerContext();
        serverContextMap.put(ENV, srvCtx);

        return serverContextMap;
    }

}
