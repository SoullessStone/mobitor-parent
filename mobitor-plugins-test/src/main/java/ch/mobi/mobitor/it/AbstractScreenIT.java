package ch.mobi.mobitor.it;

/*-
 * §
 * mobitor-plugins-test
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugins.api.model.ScreensModel;
import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import javax.xml.xpath.XPathExpressionException;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.xpath;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
@ActiveProfiles("test")
@DirtiesContext
public abstract class AbstractScreenIT {

    @Autowired
    private WebApplicationContext webApplicationContext;

    private MockMvc mockMvc;

    @Autowired
    private ScreensModel screensModel;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @NotNull
    protected ResultActions performGetScreen(String screenKey) throws Exception {
        return this.mockMvc
                .perform(get("/screen?key=" + screenKey).accept("text/plain"))
                .andExpect(model().attributeExists("screenConfigKey"))
                .andExpect(model().attributeExists("nextScreenConfigKey"))
                .andExpect(model().attributeExists("rotate"))
                .andExpect(status().isOk());
    }

    @NotNull
    protected ResultActions performGet() throws Exception {
        return this.mockMvc
                .perform(get("/").accept("text/plain"))
                .andExpect(model().attributeExists("screens"))
                .andExpect(model().attributeExists("applicationVersion"))
                .andExpect(model().attributeExists("activeProfiles"))
                .andExpect(model().attributeExists("network"))
                .andExpect(status().isOk());
    }

    @NotNull
    protected Screen findScreen(String screenKey) {
        return screensModel.getAvailableScreens().stream()
                           .filter(s -> s.getConfigKey().equals(screenKey))
                           .reduce((a, b) -> {
                               throw new IllegalStateException("Multiple screens with key " + screenKey + " found: " + a + ", " + b);
                           })
                           .orElseThrow(() -> new IllegalStateException("No screen with key " + screenKey + " found"));
    }

    @NotNull
    protected ResultMatcher screenTitleContains(String title) throws XPathExpressionException {
        return xpath("/html/body/div[@id='header']/h1/text()[2]").string(containsString(title));
    }

    @NotNull
    protected ResultMatcher screenContainsEnvironmentColumn(String environment) throws XPathExpressionException {
        return xpath("/html/body/table/thead/tr/th[contains(@class, 'deliveryEnvironment')]/div/span[@title='" + environment + "']/@title").string(environment);
    }

    @NotNull
    protected ResultMatcher screenContainsNumberOfEnvironmentColumns(int numberOfColumns) throws XPathExpressionException {
        return xpath("/html/body/table/thead/tr/th[contains(@class, 'deliveryEnvironment')]/div/span[1]/@title").nodeCount(numberOfColumns);
    }

    @NotNull
    protected ResultMatcher screenContainsNumberOfApplicationVersionsForApplicationName(String applicationName, int numberOfColumns) throws XPathExpressionException {
        return xpath("/html/body/table/tbody/tr/td/div[contains(@class,'applicationInformationBlocks')]/div[contains(@class,'applicationVersion') and @title='" + applicationName + "']/../span[contains(@class,'informationBlock')]").nodeCount(numberOfColumns);
    }

    protected WebApplicationContext getWebApplicationContext() {
        return webApplicationContext;
    }

    protected ScreensModel getScreensModel() {
        return screensModel;
    }

}
