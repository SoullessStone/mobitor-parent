package ch.mobi.mobitor.domain.screen;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformation;
import ch.mobi.mobitor.plugins.api.domain.screen.information.TestApplicationInformation;
import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class DefaultServerContextTest {

    @Test
    public void addInformationShouldAddInformationIfItDoesNotExist() {
        //arrange
        DefaultServerContext sut = new DefaultServerContext();
        ApplicationInformation information = new TestApplicationInformation();
        String applicationName = "new";

        //act
        sut.addInformation(applicationName, information);

        //assert
        assertThat(sut.getMatchingInformationByApplicationName(applicationName)).hasSize(1)
                                                                                .containsExactly(information);
    }
    
    @Test
    public void getMatchingInformationShouldReturnInformationByType() {
        //arrange
        DefaultServerContext sut = new DefaultServerContext();
        ApplicationInformation information = new TestApplicationInformation();
        sut.addInformation("app", information);
        sut.addInformation("app", new TestApplicationInformation("other"));

        //act
        List<ApplicationInformation> result = sut.getMatchingInformation("test");

        //assert
        assertThat(result).containsExactly(information);
    }

}
