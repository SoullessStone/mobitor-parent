package ch.mobi.mobitor.domain.screen;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformation;
import ch.mobi.mobitor.plugins.api.domain.screen.information.TestApplicationInformation;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static ch.mobi.mobitor.domain.screen.RuleEvaluationSeverity.CONTAINS_ERRORS;
import static ch.mobi.mobitor.domain.screen.RuleEvaluationSeverity.TROPHY;
import static ch.mobi.mobitor.domain.screen.RuleEvaluationSeverity.WARNINGS_ONLY;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class DefaultPipelineTest {

    private DefaultServerContext serverContextT;

    @Test(expected = IllegalStateException.class)
    public void addInformationShouldThrowExceptionIfEnvironmentIsNotDeclaredInEnvironmentList() {
        //arrange
        DefaultPipeline sut = newPipelineWithServerContextMap();

        //act
        sut.addInformation("X", "app", new TestApplicationInformation());

        //assert
        // exception thrown
    }

    @Test
    public void addInformationShouldAddInformationToServerContext() {
        //arrange
        DefaultPipeline sut = newPipelineWithServerContextMap();
        String applicationName = "app";
        TestApplicationInformation information = new TestApplicationInformation();

        //act
        sut.addInformation("T", applicationName, information);

        //assert
        verify(serverContextT).addInformation(applicationName, information);
    }

    @Test
    public void getMatchingInformationShouldReturnMatchingInformation() {
        //arrange
        DefaultPipeline sut = newPipelineWithServerContextMap();
        String applicationName = "app";
        TestApplicationInformation information = new TestApplicationInformation();
        sut.addInformation("T", applicationName, information);
        when(serverContextT.getMatchingInformation("", applicationName)).thenReturn(singletonList(information));

        //act
        List<ApplicationInformation> result = sut.getMatchingInformation("", "T", applicationName);

        //assert
        assertThat(result).containsExactly(information);
    }

    @Test
    public void getRuleEvaluationSeverityShouldReturnTrophyIfPipelineContainsNoError() {
        //arrange
        DefaultPipeline pipeline = new DefaultPipeline("server", new HashMap<>());
        pipeline.updateRuleEvaluation("", okRuleEvaluation());

        //act
        RuleEvaluationSeverity result = pipeline.getRuleEvaluationSeverity();

        //assert
        assertThat(result).isEqualTo(TROPHY);
    }

    @Test
    public void getRuleEvaluationSeverityShouldReturnWarningIfPipelineContainsWarnings() {
        //arrange
        DefaultPipeline pipeline = new DefaultPipeline("server", new HashMap<>());
        pipeline.updateRuleEvaluation("", okRuleEvaluation());
        pipeline.updateRuleEvaluation("", warningRuleEvaluation());

        //act
        RuleEvaluationSeverity result = pipeline.getRuleEvaluationSeverity();

        //assert
        assertThat(result).isEqualTo(WARNINGS_ONLY);
    }

    @Test
    public void getRuleEvaluationSeverityShouldReturnErrorIfPipelineContainsErrors() {
        //arrange
        DefaultPipeline pipeline = new DefaultPipeline("server", new HashMap<>());
        pipeline.updateRuleEvaluation("", okRuleEvaluation());
        pipeline.updateRuleEvaluation("", warningRuleEvaluation());
        pipeline.updateRuleEvaluation("", errorRuleEvaluation());

        //act
        RuleEvaluationSeverity result = pipeline.getRuleEvaluationSeverity();

        //assert
        assertThat(result).isEqualTo(CONTAINS_ERRORS);
    }

    @Test
    public void getViolatedTypesShouldExtractInformationTypes() {
        //arrange
        DefaultPipeline pipeline = new DefaultPipeline("server", new HashMap<>());
        pipeline.updateRuleEvaluation("A", okRuleEvaluation());
        pipeline.updateRuleEvaluation("B", okRuleEvaluation());

        //act
        List<String> result = pipeline.getViolatedTypes();

        //assert
        assertThat(result).containsExactly("A", "B");
    }

    @NotNull
    private RuleEvaluation okRuleEvaluation() {
        RuleEvaluation ruleEvaluation = mock(RuleEvaluation.class);
        when(ruleEvaluation.hasWarnings()).thenReturn(false);
        when(ruleEvaluation.hasErrors()).thenReturn(false);
        return ruleEvaluation;
    }

    @NotNull
    private RuleEvaluation warningRuleEvaluation() {
        RuleEvaluation ruleEvaluation = mock(RuleEvaluation.class);
        when(ruleEvaluation.hasWarnings()).thenReturn(true);
        when(ruleEvaluation.hasErrors()).thenReturn(false);
        return ruleEvaluation;
    }

    @NotNull
    private RuleEvaluation errorRuleEvaluation() {
        RuleEvaluation ruleEvaluation = mock(RuleEvaluation.class);
        when(ruleEvaluation.hasWarnings()).thenReturn(false);
        when(ruleEvaluation.hasErrors()).thenReturn(true);
        return ruleEvaluation;
    }

    @NotNull
    private DefaultPipeline newPipelineWithServerContextMap() {
        Map<String, DefaultServerContext> serverContextMap = new HashMap<>();
        serverContextT = mock(DefaultServerContext.class);
        serverContextMap.put("T", serverContextT);
        serverContextMap.put("P", new DefaultServerContext());
        return new DefaultPipeline("server", serverContextMap);
    }
}
