package ch.mobi.mobitor.domain.screen;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import org.junit.Test;

import static java.util.Collections.emptyList;
import static org.assertj.core.api.Assertions.assertThat;

public class ScreenLabelComparatorTest {

    @Test
    public void compareShouldCompareScreenLabels1() {
        //arrange
        ScreenLabelComparator sut = new ScreenLabelComparator();
        Screen screen1 = newScreenWithLabel("label 1");
        Screen screen2 = newScreenWithLabel("label 2");

        //act
        int result = sut.compare(screen1, screen2);

        //assert
        assertThat(result).isNegative();
    }

    @Test
    public void compareShouldCompareScreenLabels2() {
        //arrange
        ScreenLabelComparator sut = new ScreenLabelComparator();
        Screen screen1 = newScreenWithLabel("label 2");
        Screen screen2 = newScreenWithLabel("label 1");

        //act
        int result = sut.compare(screen1, screen2);

        //assert
        assertThat(result).isPositive();
    }

    @Test
    public void compareShouldCompareScreenLabels3() {
        //arrange
        ScreenLabelComparator sut = new ScreenLabelComparator();
        Screen screen1 = newScreenWithLabel("label 1");
        Screen screen2 = newScreenWithLabel("label 1");

        //act
        int result = sut.compare(screen1, screen2);

        //assert
        assertThat(result).isZero();
    }

    @Test
    public void compareShouldReturnZeroIfAScreenIsNull1() {
        //arrange
        ScreenLabelComparator sut = new ScreenLabelComparator();
        Screen screen = newScreenWithLabel("label 1");

        //act
        int result = sut.compare(screen, null);

        //assert
        assertThat(result).isZero();
    }

    @Test
    public void compareShouldReturnZeroIfAScreenIsNull2() {
        //arrange
        ScreenLabelComparator sut = new ScreenLabelComparator();
        Screen screen = newScreenWithLabel("label 1");

        //act
        int result = sut.compare(null, screen);

        //assert
        assertThat(result).isZero();
    }

    private DefaultScreen newScreenWithLabel(String label) {
        return new DefaultScreen.Builder().label(label).environments(emptyList()).serverNames(emptyList()).build();
    }

}
