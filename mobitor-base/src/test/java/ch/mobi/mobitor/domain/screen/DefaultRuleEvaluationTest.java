package ch.mobi.mobitor.domain.screen;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.rest.domain.RestCallInformation;
import org.junit.Test;

import static ch.mobi.mobitor.domain.screen.RuleViolationSeverity.ERROR;
import static ch.mobi.mobitor.domain.screen.RuleViolationSeverity.WARNING;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class DefaultRuleEvaluationTest {

    @Test
    public void getAffectedEnvironments() throws Exception {
        //-- arrange
        DefaultRuleEvaluation sut = new DefaultRuleEvaluation();
        //-- act
        sut.addViolation("B", new RestCallInformation(), ERROR, "error1");
        sut.addViolation("Y", new RestCallInformation(), ERROR, "error2");
        //-- assert
        assertThat(sut.getAffectedEnvironments(), containsInAnyOrder("B", "Y"));
    }

    @Test
    public void getMessages() throws Exception {
        //-- arrange
        DefaultRuleEvaluation sut = new DefaultRuleEvaluation();
        //-- act
        sut.addViolation("B", new RestCallInformation(), ERROR, "error1");
        sut.addViolation("B", new RestCallInformation(), WARNING, "warning1");
        sut.addViolation("Y", new RestCallInformation(), WARNING, "warning2");
        //-- assert
        assertThat(sut.getMessages("B"), containsInAnyOrder(
                new RuleMessage(ERROR, "error1"),
                new RuleMessage(WARNING, "warning1")
        ));
    }

    @Test
    public void hasViolations_true() throws Exception {
        //-- arrange
        DefaultRuleEvaluation sut = new DefaultRuleEvaluation();
        //-- act
        sut.addViolation("B", new RestCallInformation(), ERROR, "error1");
        sut.addViolation("B", new RestCallInformation(), WARNING, "warning1");
        //-- assert
        assertThat(sut.hasErrors(), is(true));
    }

    @Test
    public void hasViolations_false() throws Exception {
        //-- arrange
        DefaultRuleEvaluation sut = new DefaultRuleEvaluation();
        //-- act
        sut.addViolation("B", new RestCallInformation(), WARNING, "warning1");
        //-- assert
        assertThat(sut.hasErrors(), is(false));
    }

    @Test
    public void hasWarnings_true() throws Exception {
        //-- arrange
        DefaultRuleEvaluation sut = new DefaultRuleEvaluation();
        //-- act
        sut.addViolation("B", new RestCallInformation(), ERROR, "error1");
        sut.addViolation("B", new RestCallInformation(), WARNING, "warning1");
        //-- assert
        assertThat(sut.hasWarnings(), is(true));
    }

    @Test
    public void hasWarnings_false() throws Exception {
        //-- arrange
        DefaultRuleEvaluation sut = new DefaultRuleEvaluation();
        //-- act
        sut.addViolation("B", new RestCallInformation(), ERROR, "error1");
        //-- assert
        assertThat(sut.hasWarnings(), is(false));
    }

    @Test
    public void hasViolationsOrWarnings_true() throws Exception {
        //-- arrange
        DefaultRuleEvaluation sut = new DefaultRuleEvaluation();
        //-- act
        sut.addViolation("B", new RestCallInformation(), ERROR, "error1");
        sut.addViolation("B", new RestCallInformation(), WARNING, "warning1");
        //-- assert
        assertThat(sut.hasViolationsOrWarnings(), is(true));
    }

    @Test
    public void hasViolationsOrWarnings_false() throws Exception {
        //-- arrange
        DefaultRuleEvaluation sut = new DefaultRuleEvaluation();
        //-- act/assert
        assertThat(sut.hasViolationsOrWarnings(), is(false));
    }

}
