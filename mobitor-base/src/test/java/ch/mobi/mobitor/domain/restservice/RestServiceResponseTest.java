package ch.mobi.mobitor.domain.restservice;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.rest.domain.RestServiceResponse;
import org.junit.Test;

import static org.assertj.core.util.Lists.emptyList;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.Assert.assertThat;

public class RestServiceResponseTest {

    @Test
    public void createErrorRestServiceResponseWithoutMessages() {
        // arrange
        // act
        RestServiceResponse errorRestServiceResponse = RestServiceResponse.createErrorRestServiceResponse("/path", 499);

        // assert
        assertThat(errorRestServiceResponse.getMessages(), is(emptyList()));
    }

    @Test
    public void createErrorRestServiceResponseWithNullMessage() {
        // arrange
        // act
        RestServiceResponse errorRestServiceResponse = RestServiceResponse.createErrorRestServiceResponse("/path", 499, null);

        // assert
        assertThat(errorRestServiceResponse.getMessages(), is(emptyList()));
    }

    @Test
    public void createErrorRestServiceResponseWithTwoMessages() {
        // arrange
        // act
        RestServiceResponse errorRestServiceResponse = RestServiceResponse.createErrorRestServiceResponse("/path", 499, "one", "two");

        // assert
        assertThat(errorRestServiceResponse.getMessages(), hasSize(2));
    }
}
