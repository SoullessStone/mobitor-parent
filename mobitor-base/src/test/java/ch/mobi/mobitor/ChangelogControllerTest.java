package ch.mobi.mobitor;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.service.config.ChangelogConfigurationService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static ch.mobi.mobitor.it.TestViewResolver.viewResolver;
import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ChangelogControllerTest {

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        ChangelogConfigurationService service = mock(ChangelogConfigurationService.class);
        ChangelogConfiguration configuration = mock(ChangelogConfiguration.class);
        this.mockMvc = MockMvcBuilders.standaloneSetup(new ChangelogController(service, configuration))
                                      .setViewResolvers(viewResolver())
                                      .build();
    }

    @Test
    public void requestShouldReturnChangelog() throws Exception {
        mockMvc.perform(get("/changelog"))
               .andDo(print())
               .andExpect(status().isOk())
               .andExpect(forwardedUrl("/templates/changelog.html"));
    }
}
