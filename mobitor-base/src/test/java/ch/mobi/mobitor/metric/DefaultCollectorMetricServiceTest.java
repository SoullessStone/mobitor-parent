package ch.mobi.mobitor.metric;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
import org.junit.Test;

import java.util.concurrent.atomic.AtomicLong;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

public class DefaultCollectorMetricServiceTest {

    @Test
    public void submitCollectorDuration() {
        // arrange
        MeterRegistry meterRegistrySpy = spy(SimpleMeterRegistry.class);
        DefaultCollectorMetricService collMetricService = new DefaultCollectorMetricService(meterRegistrySpy);

        AtomicLong durationMs = new AtomicLong(123456);

        // act
        collMetricService.submitCollectorDuration("test_label_val", durationMs.get());

        // assert
        verify(meterRegistrySpy).gauge(eq("gauge_collector_duration_ms"), anyList(), any(AtomicLong.class));
    }

}
