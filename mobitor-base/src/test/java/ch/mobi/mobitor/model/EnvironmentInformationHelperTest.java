package ch.mobi.mobitor.model;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.service.environment.EnvironmentTimestamps;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class EnvironmentInformationHelperTest {

    private EnvironmentTimestampModel model;
    private EnvironmentTimestamps timestamps;
    private EnvironmentInformationHelper sut;

    private static final String ENVIRONMENT = "env";

    @Before
    public void setUp() {
        timestamps = mock(EnvironmentTimestamps.class);
        model = mock(EnvironmentTimestampModel.class);
        when(model.getTimestamps(any())).thenReturn(timestamps);
        sut = new EnvironmentInformationHelper(model);
    }

    @Test
    public void hasInformationShouldDelegateToModel() {
        //arrange

        //act
        sut.hasInformation(ENVIRONMENT);

        //assert
        verify(model).hasTimestamps(ENVIRONMENT);
    }

    @Test
    public void getShiftedTimeShouldDelegateToModel() {
        //arrange
    
        //act
        sut.getShiftedTime(ENVIRONMENT);
        
        //assert
        verify(timestamps).getShiftedTime();
    }

    @Test
    public void getShiftedDaysShouldDelegateToModel() {
        //arrange

        //act
        sut.getShiftedDays(ENVIRONMENT);

        //assert
        verify(timestamps).getShiftedDays();
    }

}
