package ch.mobi.mobitor.model;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.DefaultScreen;
import ch.mobi.mobitor.domain.screen.Screen;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;

import java.util.List;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;

public class DefaultScreensModelTest {

    @Test
    public void getScreenShouldExtractCorrectScreen() {
        //arrange
        DefaultScreensModel model = newModelWithScreens();

        //act
        Screen result = model.getScreen("13");

        //assert
        assertThat(result.getLabel()).isEqualTo("my screen");
    }

    @Test
    public void hasScreenShouldReturnTrueIfScreenExists() {
        //arrange
        DefaultScreensModel model = newModelWithScreens();

        //act
        //assert
        assertThat(model.hasScreen("99")).isTrue();
    }

    @Test
    public void getAvailableScreensShouldReturnAllScreens() {
        //arrange
        DefaultScreensModel model = newModelWithScreens();

        //act
        List<Screen> result = model.getAvailableScreens();

        //assert
        assertThat(result).hasSize(2)
                          .extracting(Screen::getConfigKey)
                          .containsExactly("13", "99");
    }

    @NotNull
    private DefaultScreensModel newModelWithScreens() {
        DefaultScreen screen1 = new DefaultScreen.Builder().configKey("13")
                                                          .label("my screen")
                                                          .refreshInterval(42)
                                                          .environments(asList("B", "T", "P"))
                                                          .serverNames(asList("Server A", "Server B"))
                                                          .build();
        DefaultScreen screen2 = new DefaultScreen.Builder().configKey("99")
                                                          .label("new screen")
                                                          .refreshInterval(11)
                                                          .environments(asList("TEST", "PROD"))
                                                          .serverNames(asList("Server 1", "Server 2"))
                                                          .build();
        DefaultScreensModel model = new DefaultScreensModel();
        model.addScreen(screen1);
        model.addScreen(screen2);
        return model;
    }

}
