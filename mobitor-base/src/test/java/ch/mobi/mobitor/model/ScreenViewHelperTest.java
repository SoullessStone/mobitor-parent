package ch.mobi.mobitor.model;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.hpalm.domain.AlmInformation;
import ch.mobi.mobitor.plugin.hpalm.domain.TestRun;
import ch.mobi.mobitor.plugin.jira.domain.JiraInformation;
import ch.mobi.mobitor.plugin.kubernetes.domain.JobStatusPhase;
import ch.mobi.mobitor.plugin.kubernetes.domain.KubernetesBatchJobInformation;
import ch.mobi.mobitor.plugin.liima.domain.LiimaInformation;
import ch.mobi.mobitor.plugin.liima.service.ServersConfigurationService;
import ch.mobi.mobitor.plugin.nexusiq.config.Stage;
import ch.mobi.mobitor.plugin.nexusiq.domain.NexusIqInformation;
import ch.mobi.mobitor.plugin.nexusiq.service.scheduling.NexusIqConfigurationService;
import ch.mobi.mobitor.plugin.rest.domain.FlywayRestServiceResponse;
import ch.mobi.mobitor.plugin.rest.domain.ResponseInterpretation;
import ch.mobi.mobitor.plugin.rest.domain.RestCallInformation;
import ch.mobi.mobitor.plugin.rest.domain.RestServiceResponse;
import ch.mobi.mobitor.plugin.sonarqube.domain.SonarInformation;
import ch.mobi.mobitor.plugin.sonarqube.rule.SonarReportLimitsRule;
import ch.mobi.mobitor.plugin.swd.domain.SwdDeploymentInformation;
import ch.mobi.mobitor.plugin.teamcity.domain.TeamCityBuildInformation;
import ch.mobi.mobitor.plugin.teamcity.rule.TeamCityCoverageLimitsRule;
import ch.mobi.mobitor.service.EnvironmentsConfigurationService;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.springframework.core.io.DefaultResourceLoader;

import java.util.ArrayList;
import java.util.List;

import static ch.mobi.mobitor.domain.screen.information.SwdDeploymentInformationTest.aSwdDeploymentInformation;
import static ch.mobi.mobitor.plugin.kubernetes.domain.JobStatusPhase.FAILED;
import static ch.mobi.mobitor.plugin.kubernetes.domain.JobStatusPhase.PENDING;
import static ch.mobi.mobitor.plugin.kubernetes.domain.JobStatusPhase.RUNNING;
import static ch.mobi.mobitor.plugin.kubernetes.domain.JobStatusPhase.SUCCEEDED;
import static ch.mobi.mobitor.plugin.kubernetes.domain.JobStatusPhase.UNKNOWN;
import static java.util.Collections.emptyList;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ScreenViewHelperTest {

    private final ServersConfigurationService serversConfigurationService = mock(ServersConfigurationService.class);
    private final SonarReportLimitsRule sonarReportLimitsRule = new SonarReportLimitsRule();
    private final EnvironmentsConfigurationService environmentsConfigurationService = mock(EnvironmentsConfigurationService.class);
    private final NexusIqConfigurationService nexusIqConfigurationService = mock(NexusIqConfigurationService.class);
    private final TeamCityCoverageLimitsRule teamCityCoverageLimitsRule = mock(TeamCityCoverageLimitsRule.class);

    private ScreenViewHelper svh = new ScreenViewHelper(
            serversConfigurationService,
            sonarReportLimitsRule,
            environmentsConfigurationService,
            nexusIqConfigurationService,
            teamCityCoverageLimitsRule);

    private RestServiceResponse createResponse(int statusCode) {
        return new RestServiceResponse("http://path/", statusCode, ResponseInterpretation.SUCCESS, null, 0);
    }

    @Test
    public void getCssClassForServer() {
        mapServerStateToCssClass("success", "server-success");
        mapServerStateToCssClass("failed", "server-error");
        mapServerStateToCssClass("canceled", "server-abort");
        mapServerStateToCssClass("rejected", "server-abort");
        mapServerStateToCssClass("anythingReally", "server-working");
        mapServerStateToCssClass(null, "server-unknown");
    }

    private void mapServerStateToCssClass(String serverState, String expectedCssClass) {
        // arrange
        LiimaInformation amwInfo = new LiimaInformation("junit-serverName", "junit-applicationName", "Y");
        amwInfo.setState(serverState);

        // act
        String cssClassForServer = svh.getCssClassForServer(amwInfo);

        // assert
        assertEquals(expectedCssClass, cssClassForServer);
    }

    @Test
    public void getCssClassForBuild() {
        mapBuildStateToCssClass(null, null, "build-unknown");
        mapBuildStateToCssClass("running", "SUCCESS", "build-success build-running");
        mapBuildStateToCssClass("running", "FAILURE", "build-error build-running");
        mapBuildStateToCssClass("exploded", "SUCCESS", "build-success");
        mapBuildStateToCssClass("imploded", "FAILURE", "build-error");
        mapBuildStateToCssClass("imploded", "ROLLING", "");
        mapBuildStateToCssClass("running", "ROLLING", " build-running");
    }

    private void mapBuildStateToCssClass(String buildState, String buildStatus, String expectedCssClass) {
        // arrange
        TeamCityBuildInformation tcInformation = new TeamCityBuildInformation("junit-configIf", "junit-templ");
        tcInformation.setState(buildState);
        tcInformation.setStatus(buildStatus);

        // act
        String cssClass = svh.getCssClassForBuild(tcInformation);

        // assert
        assertEquals(expectedCssClass, cssClass);
    }

    @Test
    public void getApplicationScreenNameShouldReturnNameFoundInMappingFile() {
        //given
        ServersConfigurationService serversConfigurationService = mock(ServersConfigurationService.class);
        ScreenViewHelper sut =
                new ScreenViewHelper(serversConfigurationService, sonarReportLimitsRule, environmentsConfigurationService,
                        nexusIqConfigurationService, teamCityCoverageLimitsRule);
        when(serversConfigurationService.getApplicationScreenName(any())).thenReturn("name_in_mapping_file");

        //when
        String result = sut.getApplicationScreenName("ch_mobi_junit_amwApplicationName");

        //then
        Assertions.assertThat(result).isEqualTo("name_in_mapping_file");
    }

    @Test
    public void getApplicationScreenNameShouldCropPrefixIfItStartsWithChMobi() {
        //given
        ServersConfigurationService serversConfigurationService = mock(ServersConfigurationService.class);
        ScreenViewHelper sut =
                new ScreenViewHelper(serversConfigurationService, sonarReportLimitsRule, environmentsConfigurationService,
                        nexusIqConfigurationService, teamCityCoverageLimitsRule);

        //when
        String result = sut.getApplicationScreenName("ch_mobi_junit_amwApplicationName");

        //then
        Assertions.assertThat(result).isEqualTo("junit_amwApplicationName");
    }

    @Test
    public void getApplicationScreenNameShouldNotCropPrefixIfItNotStartsWithChMobi() {
        //given
        ServersConfigurationService serversConfigurationService = mock(ServersConfigurationService.class);
        ScreenViewHelper sut =
                new ScreenViewHelper(serversConfigurationService, sonarReportLimitsRule, environmentsConfigurationService,
                        nexusIqConfigurationService, teamCityCoverageLimitsRule);

        //when
        String result = sut.getApplicationScreenName("foo_junit_amwApplicationName");

        //then
        Assertions.assertThat(result).isEqualTo("foo_junit_amwApplicationName");
    }

    @Test
    public void getServerScreenName() {
        ServersConfigurationService serversConfigurationService = new ServersConfigurationService(new DefaultResourceLoader());
        serversConfigurationService.initializeAmwDeployments();

        ScreenViewHelper svHelper = new ScreenViewHelper(
                serversConfigurationService,
                sonarReportLimitsRule,
                environmentsConfigurationService,
                nexusIqConfigurationService,
                teamCityCoverageLimitsRule);

        String appScreenName = svHelper.getServerScreenName("fdt_v2");
        assertThat(appScreenName, equalTo("FDT v2"));
    }

    @Test
    public void getSonarCssClassStrict() {
        SonarInformation sonarInfo = new SonarInformation("junit-projectKey", true);

        String sonarCssClass = svh.getSonarCssClass(sonarInfo);

        assertThat(sonarCssClass, equalTo("sonarViolated"));
    }

    @Test
    public void getSonarCssClassNonStrict() {
        SonarInformation sonarInfo = new SonarInformation("junit-projectKey", false);

        String sonarCssClass = svh.getSonarCssClass(sonarInfo);

        assertThat(sonarCssClass, equalTo("sonarViolated"));
    }

    @Test
    public void getSonarCssClassNonStrictWithBlockers() {
        SonarInformation sonarInfo = new SonarInformation("junit-projectKey", false);
        sonarInfo.setBlockers(1);

        String sonarCssClass = svh.getSonarCssClass(sonarInfo);

        assertThat(sonarCssClass, equalTo("sonarViolated"));
    }

    @Test
    public void getSonarCssClassStrictWithAllGood() {
        SonarInformation sonarInfo = new SonarInformation("junit-projectKey", true);
        sonarInfo.setBlockers(0);
        sonarInfo.setCriticals(0);
        sonarInfo.setCoverage(99);

        String sonarCssClass = svh.getSonarCssClass(sonarInfo);

        assertThat(sonarCssClass, equalTo("sonarConform"));
    }

    @Test
    public void getRestCssClass() {
        RestCallInformation restInfo = new RestCallInformation();
        restInfo.setSwaggerUri("swagger.com");

        String restCssClass = svh.getRestCssClass(restInfo);

        assertThat(restCssClass, equalTo("rest-unknown"));
    }

    @Test
    public void getRestCssClassForErrors() {
        RestCallInformation restInfo = new RestCallInformation();
        restInfo.setSwaggerUri("swagger.com");
        restInfo.getRestServiceResponses().add(createResponse(404));

        String restCssClass = svh.getRestCssClass(restInfo);

        assertThat(restCssClass, equalTo("rest-error"));
    }

    @Test
    public void getRestCssClassForAllGood() {
        RestCallInformation restInfo = new RestCallInformation();
        restInfo.setSwaggerUri("swagger.com");
        restInfo.getRestServiceResponses().add(createResponse(299));

        String restCssClass = svh.getRestCssClass(restInfo);

        assertThat(restCssClass, equalTo("rest-success"));
    }

    @Test
    public void getCssClassForNexusIq() {
        NexusIqInformation nexusIqInfo = new NexusIqInformation("junit-publicId", Stage.BUILD);
        // no reset: nexusIqInfo.resetViolationsCount();
        String cssClass = svh.getCssClassForNexusIq(nexusIqInfo);

        assertThat(cssClass, equalTo("nexus-iq-unknown"));
    }

    @Test
    public void getCssClassForNexusIqWithAllGoodError() {
        NexusIqInformation nexusIqInfo = new NexusIqInformation("junit-publicId", Stage.RELEASE);
        nexusIqInfo.resetViolationsCount();

        String cssClass = svh.getCssClassForNexusIq(nexusIqInfo);

        assertThat(cssClass, equalTo("nexus-iq-ok"));
    }

    @Test
    public void getPolicySummary() {
        NexusIqConfigurationService nexIqConfService = new NexusIqConfigurationService(new DefaultResourceLoader());
        nexIqConfService.initializeNexusConfiguration();

        ScreenViewHelper svHelper = new ScreenViewHelper(serversConfigurationService, sonarReportLimitsRule,
                environmentsConfigurationService, nexIqConfService, teamCityCoverageLimitsRule);

        NexusIqInformation nexusInformation = new NexusIqInformation("junit-publicId", Stage.BUILD);
        nexusInformation.resetViolationsCount();
        nexusInformation.increaseViolationCount("eb17003085e84a8cad0fea7d312f3fbc");
        nexusInformation.increaseViolationCount("d89b440a4a65481b8109541bd1673910");

        String policySummary = svHelper.getPolicySummary(nexusInformation);

        assertThat(policySummary, containsString("Schwachstellen"));
        assertThat(policySummary, containsString("Lizenzen"));
    }

    @Test
    public void testJiraCssClassIssuesButNoCritical() {
        JiraInformation jiraInformation = new JiraInformation();
        jiraInformation.markAsUpdated();
        jiraInformation.setTotal(1);

        String cssClass = svh.getCssClassForJira(jiraInformation);
        assertThat(cssClass, equalTo("jira-info"));
    }

    @Test
    public void testJiraCssClassIssuesIncludingCritical() {
        JiraInformation jiraInformation = new JiraInformation();
        jiraInformation.markAsUpdated();
        jiraInformation.setTotal(1);
        jiraInformation.setCriticalIssues(1);

        String cssClass = svh.getCssClassForJira(jiraInformation);
        assertThat(cssClass, equalTo("jira-error"));
    }

    @Test
    public void testJiraCssClassIssuesIncludingMajor() {
        JiraInformation jiraInformation = new JiraInformation();
        jiraInformation.markAsUpdated();
        jiraInformation.setTotal(1);
        jiraInformation.setMajorIssues(1);

        String cssClass = svh.getCssClassForJira(jiraInformation);
        assertThat(cssClass, equalTo("jira-warning"));
    }

    @Test
    public void testJiraCssClassFilterNotUpdates() {
        JiraInformation jiraInformation = new JiraInformation();

        String cssClass = svh.getCssClassForJira(jiraInformation);
        assertThat(cssClass, equalTo("jira-pending"));
    }

    @Test
    public void testJiraCssClassFilterOk() {
        JiraInformation jiraInformation = new JiraInformation();
        jiraInformation.setTotal(0);
        jiraInformation.markAsUpdated();

        String cssClass = svh.getCssClassForJira(jiraInformation);
        assertThat(cssClass, equalTo("jira-ok"));
    }

    @Test
    public void getRestartCountClass() {
        assertThat(svh.getRestartCountClass(0), equalTo("restarts-ok"));
        assertThat(svh.getRestartCountClass(1), equalTo("restarts-ok"));
        assertThat(svh.getRestartCountClass(2), equalTo("restarts-warning"));
        assertThat(svh.getRestartCountClass(5), equalTo("restarts-error"));
        assertThat(svh.getRestartCountClass(6), equalTo("restarts-error"));
    }

    @Test
    public void testGetCssClassForDbOk() {
        FlywayRestServiceResponse fwRestResp =
                new FlywayRestServiceResponse("http://path/", 200, ResponseInterpretation.SUCCESS, emptyList(), 120);
        fwRestResp.setState("SUCCESS");

        String cssClassForDb = svh.getCssClassForDb(fwRestResp);

        assertThat(cssClassForDb, equalTo("flyway-ok"));
    }

    @Test
    public void testGetCssClassForDbFutureOk() {
        FlywayRestServiceResponse fwRestResp =
                new FlywayRestServiceResponse("http://path/", 200, ResponseInterpretation.SUCCESS, emptyList(), 120);
        fwRestResp.setState("FUTURE_SUCCESS");

        String cssClassForDb = svh.getCssClassForDb(fwRestResp);

        assertThat(cssClassForDb, equalTo("flyway-almost-ok"));
    }

    @Test
    public void testGetCssClassForDbUnknown() {
        FlywayRestServiceResponse fwRestResp =
                new FlywayRestServiceResponse("http://path/", 200, ResponseInterpretation.SUCCESS, emptyList(), 120);

        String cssClassForDb = svh.getCssClassForDb(fwRestResp);

        assertThat(cssClassForDb, equalTo("flyway-unknown"));
    }

    @Test
    public void testGetCssClassForDbError() {
        FlywayRestServiceResponse fwRestResp =
                new FlywayRestServiceResponse("http://path/", 200, ResponseInterpretation.SUCCESS, emptyList(), 120);
        fwRestResp.setState("ERROR");

        String cssClassForDb = svh.getCssClassForDb(fwRestResp);

        assertThat(cssClassForDb, equalTo("flyway-error"));
    }

    @Test
    public void testGetFlywayRestResponsesWithNone() {
        // arrange
        RestCallInformation restCallInform = new RestCallInformation();
        restCallInform.setSwaggerUri("http://junit.swagger.local/swagger.json");
        restCallInform.setRestServiceResponses(new ArrayList<>());

        // act
        FlywayRestServiceResponse flywayRestServiceResponse = svh.getFlywayRestServiceResponse(restCallInform);

        // assert
        assertThat(flywayRestServiceResponse, is(nullValue()));
    }

    @Test
    public void testGetFlywayRestResponsesWithExisting() {
        // arrange
        RestCallInformation restCallInform = new RestCallInformation();
        restCallInform.setSwaggerUri("http://junit.swagger.local/swagger.json");
        List<RestServiceResponse> restCalls = new ArrayList<>();
        restCalls.add(RestServiceResponse.createSuccessRestServiceResponse("http://path/", 200, 300));
        restCalls.add(RestServiceResponse.createErrorRestServiceResponse("http://path/", 400));
        restCalls.add(FlywayRestServiceResponse.createSuccessRestServiceResponse("http://path/", 200, 250));

        restCallInform.setRestServiceResponses(restCalls);

        // act
        FlywayRestServiceResponse flywayRestServiceResponse = svh.getFlywayRestServiceResponse(restCallInform);

        // assert
        assertThat(flywayRestServiceResponse, is(not((nullValue()))));
    }

    @Test
    public void testGetCssClassForAlmUnknown() {
        AlmInformation almInformation = new AlmInformation("domain", "project", "folder", "http://");
        List<TestRun> testRuns = new ArrayList<>();
        almInformation.updateTestRuns(testRuns);

        String cssClassForAlm = svh.getCssClassForAlm(almInformation);

        assertThat(cssClassForAlm, equalTo("alm-unknown"));
    }

    @Test
    public void testGetCssClassForAlmOk() {
        AlmInformation almInformation = new AlmInformation("domain", "project", "folder", "http://");
        List<TestRun> testRuns = new ArrayList<>();
        TestRun testRun = new TestRun(0, 0, "", "", 0, "", "", "", 1, "Finished", "Passed");
        testRuns.add(testRun);
        almInformation.updateTestRuns(testRuns);

        String cssClassForAlm = svh.getCssClassForAlm(almInformation);

        assertThat(cssClassForAlm, equalTo("alm-ok"));
    }

    @Test
    public void testGetCssClassForAlmError() {
        AlmInformation almInformation = new AlmInformation("domain", "project", "folder", "http://");
        List<TestRun> testRuns = new ArrayList<>();
        TestRun testRun = new TestRun(0, 0, "", "", 0, "", "", "", 1, "", "Failed");
        testRuns.add(testRun);
        almInformation.updateTestRuns(testRuns);

        String cssClassForAlm = svh.getCssClassForAlm(almInformation);

        assertThat(cssClassForAlm, equalTo("alm-error"));
    }

    @Test
    public void getKubernetesPodStatusCssClassShouldReturnCorrectCssClass() {
        assertThatStatusReturnsKubernetesPodStatusCssClass(PENDING, "statusRunning");
        assertThatStatusReturnsKubernetesPodStatusCssClass(RUNNING, "statusRunning");
        assertThatStatusReturnsKubernetesPodStatusCssClass(SUCCEEDED, "statusOk");
        assertThatStatusReturnsKubernetesPodStatusCssClass(FAILED, "statusError");
    }

    @Test
    public void getKubernetesPodStatusCssClassShouldReturnDefaultCssClassForUnknownStatus() {
        assertThatStatusReturnsKubernetesPodStatusCssClass(UNKNOWN, "statusUnknown");
        assertThatStatusReturnsKubernetesPodStatusCssClass(null, "statusUnknown");
    }

    @Test
    public void getKubernetesPodStatusIconCssClassShouldReturnCorrectCssClass() {
        assertThatStatusReturnsKubernetesPodStatusIconCssClass(PENDING, "fa-pause-circle");
        assertThatStatusReturnsKubernetesPodStatusIconCssClass(RUNNING, "fa-play-circle");
        assertThatStatusReturnsKubernetesPodStatusIconCssClass(SUCCEEDED, "fa-check-circle");
        assertThatStatusReturnsKubernetesPodStatusIconCssClass(FAILED, "fa-exclamation-circle");
    }

    @Test
    public void getKubernetesPodStatusCssClassShouldReturnDefaultIconCssClassForUnknownStatus() {
        assertThatStatusReturnsKubernetesPodStatusIconCssClass(UNKNOWN, "fa-question-circle");
        assertThatStatusReturnsKubernetesPodStatusIconCssClass(null, "fa-question-circle");
    }

    @Test
    public void getSwdDeploymentStatusCssClassShouldReturnCorrectCssClass() {
        assertThatRevisionReturnsSwdDeploymentStatusCssClass("1234.", "statusOk");
        assertThatRevisionReturnsSwdDeploymentStatusCssClass("", "statusError");
        assertThatRevisionReturnsSwdDeploymentStatusCssClass(null, "statusUnknown");
    }

    private void assertThatStatusReturnsKubernetesPodStatusCssClass(JobStatusPhase status, String expectedCssClass) {
        //given
        KubernetesBatchJobInformation information = mock(KubernetesBatchJobInformation.class);
        when(information.getStatus()).thenReturn(status);

        //when
        String result = svh.getKubernetesPodStatusCssClass(information);

        //then
        Assertions.assertThat(result).isEqualTo(expectedCssClass);
    }

    private void assertThatStatusReturnsKubernetesPodStatusIconCssClass(JobStatusPhase status, String expectedCssClass) {
        //given
        KubernetesBatchJobInformation information = mock(KubernetesBatchJobInformation.class);
        when(information.getStatus()).thenReturn(status);

        //when
        String result = svh.getKubernetesPodStatusIconCssClass(information);

        //then
        Assertions.assertThat(result).isEqualTo(expectedCssClass);
    }

    private void assertThatRevisionReturnsSwdDeploymentStatusCssClass(String revision, String expectedCssClass) {
        //given
        SwdDeploymentInformation information = aSwdDeploymentInformation();
        information.setRevision(revision);

        //when
        String result = svh.getSwdDeploymentStatusCssClass(information);

        //then
        Assertions.assertThat(result).isEqualTo(expectedCssClass);
    }

    @Test
    public void testRestErrorsPercentage() {
        String cssStyle = svh.getRestErrorsCssGradient(0, 0);
        assertThat(cssStyle, equalTo(""));

        cssStyle = svh.getRestErrorsCssGradient(10, 100);
        assertThat(cssStyle, containsString("10%"));

        cssStyle = svh.getRestErrorsCssGradient(20, 200);
        assertThat(cssStyle, containsString("10%"));

        cssStyle = svh.getRestErrorsCssGradient(75, 100);
        assertThat(cssStyle, containsString("75%"));

        cssStyle = svh.getRestErrorsCssGradient(77, 100);
        assertThat(cssStyle, containsString("77%"));
    }

}
