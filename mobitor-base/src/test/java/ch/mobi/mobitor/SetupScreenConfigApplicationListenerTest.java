package ch.mobi.mobitor;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.factory.ExtendableScreenFactory;
import ch.mobi.mobitor.domain.screen.DefaultScreen;
import ch.mobi.mobitor.plugins.api.domain.config.ExtendableScreenConfig;
import ch.mobi.mobitor.plugins.api.model.ScreensModel;
import ch.mobi.mobitor.service.config.ExtendableScreenConfigService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.boot.test.rule.OutputCapture;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SetupScreenConfigApplicationListenerTest {

    @Rule
    public OutputCapture capture = new OutputCapture();

    ScreensModel model;
    ExtendableScreenConfigService screenConfigService;
    ExtendableScreenFactory screenFactory;
    ApplicationContext applicationContext;

    @Before
    public void setUp() throws Exception {
        model = mock(ScreensModel.class);

        screenConfigService = mock(ExtendableScreenConfigService.class);
        ExtendableScreenConfig extendableScreenConfig = new ExtendableScreenConfig();
        extendableScreenConfig.setConfigKey("Generated");
        List<ExtendableScreenConfig> screenConfigs = new ArrayList<>();
        screenConfigs.add(extendableScreenConfig);
        when(screenConfigService.getConfigs()).thenReturn(screenConfigs);

        screenFactory = mock(ExtendableScreenFactory.class);
        DefaultScreen theScreen = mock(DefaultScreen.class);
        when(theScreen.getConfigKey()).thenReturn("Startup screen");
        when(screenFactory.initializeEmptyScreen(any())).thenReturn(theScreen);

        applicationContext = mock(ApplicationContext.class);
    }

    @Test
    public void onApplicationEventTheScreensShouldBeAdded() {
        ContextRefreshedEvent theEvent = new ContextRefreshedEvent(applicationContext);
        SetupScreenConfigApplicationListener listener = new SetupScreenConfigApplicationListener(model,screenConfigService,screenFactory);

        listener.onApplicationEvent(theEvent);
        assertThat(capture.toString(), containsString("Startup screen"));
        assertThat(capture.toString(), containsString("Empty screens have been initialized."));
    }
}
