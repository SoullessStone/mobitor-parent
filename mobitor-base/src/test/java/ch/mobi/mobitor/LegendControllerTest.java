package ch.mobi.mobitor;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.model.ScreenViewHelper;
import ch.mobi.mobitor.plugins.api.model.ScreensModel;
import ch.mobi.mobitor.service.plugins.MobitorPluginsRegistry;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static ch.mobi.mobitor.it.TestViewResolver.viewResolver;
import static org.mockito.Mockito.mock;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;

public class LegendControllerTest {

    private MockMvc mockMvc;

    @Before
    public void setUp() throws Exception {
        ScreensModel model = mock(ScreensModel.class);
        MobitorPluginsRegistry plugins = mock(MobitorPluginsRegistry.class);
        ScreenViewHelper helper = mock(ScreenViewHelper.class);

        this.mockMvc = MockMvcBuilders.standaloneSetup(new LegendController(plugins, helper))
                .setViewResolvers(viewResolver())
                .build();
    }

    @Test
    public void overviewShouldReturnALegend() throws Exception{
        mockMvc.perform(get("/legend")).andExpect(model().attributeExists("legend"));
    }
}
