package ch.mobi.mobitor;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 - 2019 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.model.EnvironmentInformationHelper;
import ch.mobi.mobitor.model.ScreenViewHelper;
import ch.mobi.mobitor.plugin.jira.JiraPlugin;
import ch.mobi.mobitor.plugin.jira.service.JiraPluginAttributeProvider;
import ch.mobi.mobitor.plugin.jira.service.client.JiraConfiguration;
import ch.mobi.mobitor.plugins.api.MobitorPlugin;
import ch.mobi.mobitor.plugins.api.model.ScreensModel;
import ch.mobi.mobitor.service.OnDutyService;
import ch.mobi.mobitor.service.plugins.MobitorPluginsRegistry;
import org.junit.Test;
import org.springframework.ui.ConcurrentModel;
import org.springframework.ui.Model;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

public class ScreenControllerPluginAttributesTest {

    @Test
    public void modelContainsPluginAttributes() {
        // arrange
        ScreensModel screensModel = mock(ScreensModel.class);
        ScreenViewHelper svh = mock(ScreenViewHelper.class);
        OnDutyService onDutyService = mock(OnDutyService.class);
        EnvironmentInformationHelper envInfoHelper = mock(EnvironmentInformationHelper.class);

        List<MobitorPlugin> pluginsList = new ArrayList<>();
        JiraConfiguration jiraConf = new JiraConfiguration();
        jiraConf.setBaseUrl("http://junit.local");
        JiraPlugin jiraPlugin = new JiraPlugin(new JiraPluginAttributeProvider(jiraConf));
        pluginsList.add(jiraPlugin);

        Optional<List<MobitorPlugin>> pluginsListOpt = Optional.of(pluginsList);
        MobitorPluginsRegistry pluginsRegistry = new MobitorPluginsRegistry(pluginsListOpt);
        pluginsRegistry.initialize();

        ScreenController screenController = new ScreenController(screensModel, svh, onDutyService, envInfoHelper, pluginsRegistry);

        Model model = new ConcurrentModel();

        // act
        screenController.overview("0", "", model);

        // assert
        assertTrue(model.containsAttribute("mobitor.plugins.jira.baseUrl"));
    }
}
