package ch.mobi.mobitor;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.model.EnvironmentInformationHelper;
import ch.mobi.mobitor.model.ScreenViewHelper;
import ch.mobi.mobitor.plugins.api.model.ScreensModel;
import ch.mobi.mobitor.service.OnDutyService;
import ch.mobi.mobitor.service.plugins.MobitorPluginsRegistry;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static ch.mobi.mobitor.it.TestViewResolver.viewResolver;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.startsWith;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class ScreenControllerTest {

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        ScreensModel model = mock(ScreensModel.class);
        when(model.hasScreen(startsWith("2"))).thenReturn(true);
        when(model.getScreen(eq("22"))).thenReturn(mock(Screen.class));
        ScreenViewHelper helper = mock(ScreenViewHelper.class);
        OnDutyService onDutyService = mock(OnDutyService.class);
        EnvironmentInformationHelper envHelper = mock(EnvironmentInformationHelper.class);
        MobitorPluginsRegistry pluginsRegistry = mock(MobitorPluginsRegistry.class);
        this.mockMvc = MockMvcBuilders.standaloneSetup(new ScreenController(model, helper, onDutyService, envHelper, pluginsRegistry))
                                      .setViewResolvers(viewResolver())
                                      .build();
    }

    @Test
    public void requestShouldReturnStatusOk() throws Exception {
        mockMvc.perform(get("/screen")).andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void requestShouldReturnInvalidScreenConfig() throws Exception {
        mockMvc.perform(get("/screen"))
               .andExpect(forwardedUrl("/templates/invalidscreenconfig.html"));
    }

    @Test
    public void requestShouldReturnNoDataForNonExistingKey() throws Exception {
        mockMvc.perform(get("/screen")
                .param("key", "2"))
               .andExpect(forwardedUrl("/templates/nodata.html"));
    }

    @Test
    public void requestShouldReturnScreenForExistingKey() throws Exception {
        mockMvc.perform(get("/screen")
                .param("key", "22"))
               .andExpect(forwardedUrl("/templates/screen.html"))
               .andExpect(model().attribute("nextScreenConfigKey", "22"))
               .andExpect(model().attributeExists("screen"));
    }

    @Test
    public void requestShouldSetNextScreenConfigKeyForRotationMode() throws Exception {
        mockMvc.perform(get("/screen")
                .param("key", "22")
                .param("rotate", "21,22"))
               .andExpect(forwardedUrl("/templates/screen.html"))
               .andExpect(model().attribute("nextScreenConfigKey", "21"))
               .andExpect(model().attributeExists("screen"));
    }
}
