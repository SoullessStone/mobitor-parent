package ch.mobi.mobitor;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Pipeline;
import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugins.api.model.ScreensModel;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static ch.mobi.mobitor.it.TestViewResolver.viewResolver;
import static java.util.Collections.singletonList;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.forwardedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class RulesControllerTest {

    private static final String SERVER = "jap-mobitor";

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        ScreensModel model = mock(ScreensModel.class);
        when(model.getScreen(eq("2"))).thenReturn(mock(Screen.class));
        Screen screenWithOnePipeline = mock(Screen.class);
        Pipeline pipeline = mock(Pipeline.class);
        when(pipeline.getAppServerName()).thenReturn(SERVER);
        when(screenWithOnePipeline.getPipelines()).thenReturn(singletonList(pipeline));
        when(model.getScreen(eq("3"))).thenReturn(screenWithOnePipeline);
        this.mockMvc = MockMvcBuilders.standaloneSetup(new RulesController(model))
                                      .setViewResolvers(viewResolver())
                                      .build();
    }

    @Test
    public void requestShouldReturnStatusOk() throws Exception {
        mockMvc.perform(get("/rules")
                .param("screen", "2")
                .param("server", SERVER))
               .andDo(print())
               .andExpect(status().isOk())
               .andExpect(forwardedUrl("/templates/rules.html"));
    }

    @Test
    public void requestShouldReturnStatusBadRequestForMissingScreenParameter() throws Exception {
        mockMvc.perform(get("/rules")).andExpect(status().isBadRequest());
    }

    @Test
    public void requestShouldNotAddAttributeIfScreenHasNoPipelines() throws Exception {
        mockMvc.perform(get("/rules")
                .param("screen", "2")
                .param("server", SERVER))
               .andExpect(model().attributeDoesNotExist("pipeline"));
    }

    @Test
    public void requestShouldAddAttributeIfScreenHasExactlyOnePipeline() throws Exception {
        mockMvc.perform(get("/rules")
                .param("screen", "3")
                .param("server", SERVER))
               .andExpect(model().attributeExists("pipeline"));
    }

}
