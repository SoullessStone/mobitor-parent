package ch.mobi.mobitor.service.config;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.config.ChangelogConfig;
import org.jetbrains.annotations.NotNull;
import org.junit.Test;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.ResourceLoader;

import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;

public class ChangelogConfigurationServiceTest {

    @Test
    public void initializeChangelogRepositoriesShouldInitializeConfigFromFile() {
        //arrange
        ResourceLoader resourceLoader = new DefaultResourceLoader();
        ChangelogConfigurationService sot = new ChangelogConfigurationService(resourceLoader);

        //act
        sot.initializeChangelogRepositories();

        //assert
        assertThat(sot.getAllChangelogConfigs()).size().isEqualTo(1);
        assertThat(sot.getAllChangelogConfigs().get(0)).isEqualToComparingFieldByField(expectedConfig());

    }

    @NotNull
    private ChangelogConfig expectedConfig() {
        ChangelogConfig config = new ChangelogConfig();
        config.setServerName("server");
        config.setApplicationName("application");
        config.setProject("PRO");
        config.setRepository("pro-repo");
        config.setEnvironments(asList("B", "T", "P"));
        return config;
    }
}
