package ch.mobi.mobitor.service;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugins.api.domain.config.OnDutyConfig;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

public class OnDutyServiceTest {

    private OnDutyService onDutyService;
    private OnDutyConfig onDuty;

    @Before
    public void setUp() throws Exception {

        onDutyService = spy(OnDutyService.class);

        onDuty = new OnDutyConfig();
        onDuty.setSprintDuration(2);
        onDuty.setSprintStart("2017-08-31");
        onDuty.setTeams(new ArrayList<>(Arrays.asList("A-Team" ,"Nordpol", "Houston", "G-One")));

        when(onDutyService.getNow()).thenReturn(new DateTime(DateTime.parse("2017-9-13")));
    }

    @Test
    public void TestCurrentOnDutyTeam() throws Exception {
        assertEquals("A-Team",onDutyService.getCurrentOnDutyTeam(onDuty));
    }

    @Test
    public void TestNextOnDutyTeam() throws Exception {
        assertEquals("Nordpol",onDutyService.getNextOnDutyTeam(onDuty));
    }

    @Test
    public void TestStringToDateTime() throws Exception {
        assertEquals(onDuty.getSprintStart(),onDutyService.stringToDateTime(onDuty.getSprintStart()).toString("yyyy-MM-dd"));
    }

    @Test
    public void TestGetWeeksUnitlNow() throws Exception {
        assertEquals(1,onDutyService.getWeeksUntilNow(onDuty.getSprintStart()));
    }

    @Test
    public void TestgetChangeDate(){
        assertEquals("13-09-2017", onDutyService.getChangeDate(onDuty));
    }


}
