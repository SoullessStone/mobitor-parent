package ch.mobi.mobitor.service.config.comparator;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.config.EnvironmentConfigProperties;
import ch.mobi.mobitor.plugin.rest.RestPluginConfiguration;
import ch.mobi.mobitor.plugin.rest.service.configservice.DefaultEnvironmentsConfigurationService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.core.io.DefaultResourceLoader;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

public class EnvironmentComparatorTest {

    private EnvironmentComparator sot;

    @Before
    public void setUp() {
        sot = new EnvironmentComparator();
    }

    @Test
    public void testNoDuplicates() {
        // arrange
        Set<String> envKeys = new HashSet<>(EnvironmentComparator.ENV_KEYS_ORDER);

        // act

        // assert
        assertEquals("same size, no duplicates", EnvironmentComparator.ENV_KEYS_ORDER.size(), envKeys.size());
    }

    @Test
    public void testConfiguredEnvironmentsMentionedInEnvConfigs() {
        // arrange
        DefaultEnvironmentsConfigurationService envConfService = new DefaultEnvironmentsConfigurationService(new DefaultResourceLoader(), new RestPluginConfiguration());
        envConfService.initializeEnvironments();

        // act
        List<String> envs = envConfService.getEnvironments().stream().map(EnvironmentConfigProperties::getEnvironment).collect(Collectors.toList());

        // assert
        envs.removeAll(EnvironmentComparator.ENV_KEYS_ORDER);
        assertThat(envs).isEmpty();
    }

    @Test
    public void testBuildBeforeB() {
        assertThat(sot.compare("build", "B")).isNegative();
    }

    @Test
    public void testTBeforeP() {
        assertThat(sot.compare("T", "P")).isNegative();
    }

    @Test
    public void testBAfterT() {
        assertThat(sot.compare("T", "B")).isPositive();
    }

    @Test
    public void testSamePosition() {
        assertThat(sot.compare("Y", "Y")).isZero();
    }

    @Test
    public void testNonExistingIsBeforeExisting() {
        assertThat(sot.compare("new", "B")).isNegative();
    }

    @Test
    public void testNullIsBeforeExisting() {
        assertThat(sot.compare(null, "B")).isNegative();
    }

    @Test
    public void testExistingIsAfterNonExisting() {
        assertThat(sot.compare("B", null)).isPositive();
    }
}
