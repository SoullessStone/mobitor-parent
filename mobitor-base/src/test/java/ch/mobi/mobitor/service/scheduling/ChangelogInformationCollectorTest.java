package ch.mobi.mobitor.service.scheduling;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.ChangelogConfiguration;
import ch.mobi.mobitor.domain.config.ChangelogConfig;
import ch.mobi.mobitor.domain.deployment.Deployment;
import ch.mobi.mobitor.plugin.bitbucket.service.client.BitBucketClient;
import ch.mobi.mobitor.plugin.bitbucket.service.client.domain.BitBucketCommitInfoResponse;
import ch.mobi.mobitor.plugin.jira.service.client.JiraClient;
import ch.mobi.mobitor.plugin.jira.service.client.domain.IssueFields;
import ch.mobi.mobitor.plugin.jira.service.client.domain.IssueResponse;
import ch.mobi.mobitor.service.DeploymentInformationService;
import ch.mobi.mobitor.service.EnvironmentsConfigurationService;
import ch.mobi.mobitor.service.config.ChangelogConfigurationService;
import org.junit.Test;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ChangelogInformationCollectorTest {

    @Test
    public void collectChangelogInformationShouldUpdateDeploymentDateField() {
        //arrange
        BitBucketCommitInfoResponse bitBucketCommitInfoResponse = new BitBucketCommitInfoResponse();
        Map<String, Object> properties = new HashMap<>();
        String jiraIssueKey = "MOB-1";
        properties.put("jira-key", singletonList(jiraIssueKey));
        bitBucketCommitInfoResponse.setProperties(properties);
        BitBucketClient bitBucketClient = mock(BitBucketClient.class);
        when(bitBucketClient.retrieveCommits(any(), any(), any())).thenReturn(singletonList(bitBucketCommitInfoResponse));

        JiraClient jiraClient = mock(JiraClient.class);
        IssueResponse issueResponse = new IssueResponse();
        IssueFields issueFields = new IssueFields();
        issueResponse.setFields(issueFields);
        when(jiraClient.retrieveIssue(any())).thenReturn(issueResponse);

        Deployment deployment = new Deployment();
        deployment.setState("success");
        deployment.setDeploymentDate(new Date().getTime());
        DeploymentInformationService deploymentInformationService = mock(DeploymentInformationService.class);
        when(deploymentInformationService.filterDeployments(any(), any())).thenReturn(singletonList(deployment));

        ChangelogConfigurationService changelogConfigurationService = mock(ChangelogConfigurationService.class);

        ChangelogConfig changelogConfig = new ChangelogConfig();
        changelogConfig.setEnvironments(singletonList("P"));
        when(changelogConfigurationService.getAllChangelogConfigs()).thenReturn(singletonList(changelogConfig));

        EnvironmentsConfigurationService environmentsConfigurationService = mock(EnvironmentsConfigurationService.class);
        when(environmentsConfigurationService.getName(any())).thenReturn("produktion-p");

        ChangelogConfiguration changelogConfiguration = new ChangelogConfiguration();
        String customFieldNameDeploymentDateProduction = "prod-deployment";
        changelogConfiguration.setCustomFieldNameDeploymentDateProduction(customFieldNameDeploymentDateProduction);
        String deploymentEnvironments = "PROD";
        changelogConfiguration.setCustomFieldNameDeploymentEnvironments(deploymentEnvironments);
        changelogConfiguration.setBlacklistedProjects(emptyList());

        ChangelogInformationCollector sot = new ChangelogInformationCollector(bitBucketClient, jiraClient, deploymentInformationService, changelogConfigurationService, environmentsConfigurationService, changelogConfiguration);

        //act
        sot.collectChangelogInformation();

        //assert
        verify(jiraClient).updateCustomField(eq(jiraIssueKey), eq(deploymentEnvironments), any());
        verify(jiraClient).updateCustomField(eq(jiraIssueKey), eq(customFieldNameDeploymentDateProduction), any());
    }
}
