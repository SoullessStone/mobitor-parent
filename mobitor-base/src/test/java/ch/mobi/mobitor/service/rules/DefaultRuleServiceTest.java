package ch.mobi.mobitor.service.rules;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.config.ScreenConfigTestFactory;
import ch.mobi.mobitor.domain.factory.ExtendableScreenFactory;
import ch.mobi.mobitor.domain.screen.PipelineRule;
import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.sonarqube.rule.SonarReportLimitsRule;
import ch.mobi.mobitor.plugins.api.MobitorPlugin;
import ch.mobi.mobitor.plugins.api.domain.config.ExtendableScreenConfig;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformationLegendWrapper;
import ch.mobi.mobitor.service.plugins.MobitorPluginsRegistry;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static ch.mobi.mobitor.plugin.sonarqube.domain.SonarInformation.SONAR;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static java.util.Optional.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

public class DefaultRuleServiceTest {

    @Test
    public void updateRuleEvaluation() {
        // arrange
        ExtendableScreenConfig minimalScreenConfig = ScreenConfigTestFactory.createMinimalScreenConfig();
        MobitorPlugin testPluginForMinimalScreenConfig = new MobitorPlugin<String>() {
            @Override
            public String getConfigPropertyName() {
                return "liimaServers";
            }

            @Override
            public Class<String> getConfigClass() {
                return String.class;
            }

            @Override
            public void createAndAssociateApplicationInformationBlocks(Screen screen, ExtendableScreenConfig screenConfig, List configs) {
            }

            @Override
            public List<ApplicationInformationLegendWrapper> getLegendApplicationInformationList() {
                return emptyList();
            }
        };
        List<MobitorPlugin> plugins = singletonList(testPluginForMinimalScreenConfig);
        MobitorPluginsRegistry pluginsRegistry = new MobitorPluginsRegistry(ofNullable(plugins));
        pluginsRegistry.initialize();
        ExtendableScreenFactory sf = new ExtendableScreenFactory(pluginsRegistry);
        Screen screen = sf.initializeEmptyScreen(minimalScreenConfig);

        List<PipelineRule> pipelineRules = new ArrayList<>();

        SonarReportLimitsRule ruleSpy = spy(new SonarReportLimitsRule());
        pipelineRules.add(ruleSpy);

        DefaultRuleService rs = new DefaultRuleService(pipelineRules);

        // act
        rs.updateRuleEvaluation(screen, SONAR);

        // assert
        // setup 3 environments with 2 servers, so amw rule needs to be checked 6 times:
        verify(ruleSpy, times(2)).validatesType(SONAR);
        verify(ruleSpy, times(2)).evaluateRule(any(), any());
    }

}
