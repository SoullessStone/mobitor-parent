package ch.mobi.mobitor.service.config;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.configgenerator.ScreenConfigGenerator;
import ch.mobi.mobitor.plugins.api.domain.config.ExtendableScreenConfig;
import ch.mobi.mobitor.service.plugins.MobitorPluginsRegistry;
import org.junit.Test;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ExtendableScreenConfigServiceTest {

    @Test
    public void initializeShouldInitializeConfig() {
        //arrange
        MobitorPluginsRegistry registry = mock(MobitorPluginsRegistry.class);
        ScreenConfigGenerator generator = mock(ScreenConfigGenerator.class);
        ExtendableScreenConfig generatedConfig = new ExtendableScreenConfig();
        generatedConfig.setLabel("Generated");
        when(generator.generateExtendableScreenConfig(any())).thenReturn(generatedConfig);
        ExtendableScreenConfigService sot = new ExtendableScreenConfigService(registry, singletonList(generator));

        //act
        sot.initialize();

        //assert
        assertThat(sot.getConfigs()).size().isEqualTo(2);
        assertThat(sot.getConfigs()).extracting(ExtendableScreenConfig::getLabel)
                                    .containsExactlyInAnyOrder("TEST Screen Konfiguration", "Generated");
    }

}
