package ch.mobi.mobitor;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@ConfigurationProperties(prefix = "scheduling")
public class SchedulingConfiguration {

	private final static Logger LOG = LoggerFactory.getLogger(SchedulingConfiguration.class);

	private PollingIntervalMs pollingIntervalMs;
	private PollingInitialDelayMs pollingInitialDelayMs;

    @PostConstruct
    public void logValues() {
        LOG.info("Polling interval Rest-Services:               " + pollingIntervalMs.restServicePollingInterval);
        LOG.info("Polling interval Liima:                       " + pollingIntervalMs.liimaInformationPollingInterval);
        LOG.info("Polling interval Nexus IQ:                    " + pollingIntervalMs.nexusIqInformationPollingInterval);
        LOG.info("Polling interval Sonar:                       " + pollingIntervalMs.sonarInformationPollingInterval);
        LOG.info("Polling interval TeamCity Build (Status):     " + pollingIntervalMs.teamCityBuildStatusInformationPollingInterval);
        LOG.info("Polling interval TeamCity Project (Status):   " + pollingIntervalMs.teamCityProjectStatusInformationPollingInterval);
        LOG.info("Polling interval TeamCity Coverage:           " + pollingIntervalMs.teamCityCoverageInformationPollingInterval);
        LOG.info("Polling interval Jira:                        " + pollingIntervalMs.jiraPollingInterval);
        LOG.info("Polling interval Kubernetes:                  " + pollingIntervalMs.kubernetesPodsInterval);
        LOG.info("Polling interval ALM:                         " + pollingIntervalMs.almPollingInterval);
        LOG.info("Polling interval BitBucket:                   " + pollingIntervalMs.bitBucketPollingInterval);
        LOG.info("Polling interval Kubernetes batch jobs:       " + pollingIntervalMs.kubernetesBatchJobInformationPollingInterval);
        LOG.info("Polling interval SWD deployments:             " + pollingIntervalMs.swdDeploymentInformationPollingInterval);
        LOG.info("Polling interval Zaproxy deployments:         " + pollingIntervalMs.zaproxyInformationPollingInterval);
        LOG.info("Polling interval EDWH deployments:            " + pollingIntervalMs.edwhDeploymentInformationPollingInterval);
        LOG.info("Polling interval DWH deployments:             " + pollingIntervalMs.dwhDeploymentInformationPollingInterval);
        LOG.info("Polling interval streamworks status:          " + pollingIntervalMs.streamworksStatusInformationPollingInterval);
        LOG.info("Polling initial first delay:  {}", pollingInitialDelayMs.first);
        LOG.info("Polling initial second delay: {}", pollingInitialDelayMs.second);
        LOG.info("Polling initial third delay:  {}", pollingInitialDelayMs.third);
    }

	public PollingIntervalMs getPollingIntervalMs() {
		return pollingIntervalMs;
	}

	public void setPollingIntervalMs(PollingIntervalMs pollingIntervalMs) {
		this.pollingIntervalMs = pollingIntervalMs;
	}

	public PollingInitialDelayMs getPollingInitialDelayMs() {
		return pollingInitialDelayMs;
	}

	public void setPollingInitialDelayMs(PollingInitialDelayMs pollingInitialDelayMs) {
		this.pollingInitialDelayMs = pollingInitialDelayMs;
	}

    public static class PollingIntervalMs {
        private long restServicePollingInterval;
        private long liimaInformationPollingInterval;
        private long nexusIqInformationPollingInterval;
        private long sonarInformationPollingInterval;
        private long teamCityBuildStatusInformationPollingInterval;
        private long teamCityProjectStatusInformationPollingInterval;
		private long teamCityCoverageInformationPollingInterval;
        private long jiraPollingInterval;
        private long kubernetesPodsInterval;
        private long almPollingInterval;
        private long bitBucketPollingInterval;
        private long kubernetesBatchJobInformationPollingInterval;
        private long swdDeploymentInformationPollingInterval;
        private long zaproxyInformationPollingInterval;
        private long edwhDeploymentInformationPollingInterval;
        private long dwhDeploymentInformationPollingInterval;
        private long streamworksStatusInformationPollingInterval;
        private long environmentTimestampInterval;

		public long getRestServicePollingInterval() {
			return restServicePollingInterval;
		}

		public void setRestServicePollingInterval(long restServicePollingInterval) {
			this.restServicePollingInterval = restServicePollingInterval;
		}

		public long getLiimaInformationPollingInterval() {
			return liimaInformationPollingInterval;
		}

		public void setLiimaInformationPollingInterval(long liimaInformationPollingInterval) {
			this.liimaInformationPollingInterval = liimaInformationPollingInterval;
		}

		public long getNexusIqInformationPollingInterval() {
			return nexusIqInformationPollingInterval;
		}

		public void setNexusIqInformationPollingInterval(long nexusIqInformationPollingInterval) {
			this.nexusIqInformationPollingInterval = nexusIqInformationPollingInterval;
		}

		public long getSonarInformationPollingInterval() {
			return sonarInformationPollingInterval;
		}

		public void setSonarInformationPollingInterval(long sonarInformationPollingInterval) {
			this.sonarInformationPollingInterval = sonarInformationPollingInterval;
		}

		public long getTeamCityBuildStatusInformationPollingInterval() {
			return teamCityBuildStatusInformationPollingInterval;
		}

		public void setTeamCityBuildStatusInformationPollingInterval(
				long teamCityBuildStatusInformationPollingInterval) {
			this.teamCityBuildStatusInformationPollingInterval = teamCityBuildStatusInformationPollingInterval;
		}

		public long getTeamCityProjectStatusInformationPollingInterval() {
			return teamCityProjectStatusInformationPollingInterval;
		}

		public void setTeamCityProjectStatusInformationPollingInterval(
				long teamCityProjectStatusInformationPollingInterval) {
			this.teamCityProjectStatusInformationPollingInterval = teamCityProjectStatusInformationPollingInterval;
		}

		public long getTeamCityCoverageInformationPollingInterval() {
			return teamCityCoverageInformationPollingInterval;
		}

		public void setTeamCityCoverageInformationPollingInterval(long teamCityCoverageInformationPollingInterval) {
			this.teamCityCoverageInformationPollingInterval = teamCityCoverageInformationPollingInterval;
		}

		public long getJiraPollingInterval() {
			return jiraPollingInterval;
		}

		public void setJiraPollingInterval(long jiraPollingInterval) {
			this.jiraPollingInterval = jiraPollingInterval;
		}

		public long getKubernetesPodsInterval() {
			return kubernetesPodsInterval;
		}

		public void setKubernetesPodsInterval(long kubernetesPodsInterval) {
			this.kubernetesPodsInterval = kubernetesPodsInterval;
		}

		public long getAlmPollingInterval() {
			return almPollingInterval;
		}

		public void setAlmPollingInterval(long almPollingInterval) {
			this.almPollingInterval = almPollingInterval;
		}

		public long getBitBucketPollingInterval() {
			return bitBucketPollingInterval;
		}

		public void setBitBucketPollingInterval(long bitBucketPollingInterval) {
			this.bitBucketPollingInterval = bitBucketPollingInterval;
		}

		public long getKubernetesBatchJobInformationPollingInterval() {
			return kubernetesBatchJobInformationPollingInterval;
		}

		public void setKubernetesBatchJobInformationPollingInterval(long kubernetesBatchJobInformationPollingInterval) {
			this.kubernetesBatchJobInformationPollingInterval = kubernetesBatchJobInformationPollingInterval;
		}

		public long getSwdDeploymentInformationPollingInterval() {
			return swdDeploymentInformationPollingInterval;
		}

		public void setSwdDeploymentInformationPollingInterval(long swdDeploymentInformationPollingInterval) {
			this.swdDeploymentInformationPollingInterval = swdDeploymentInformationPollingInterval;
		}

		public long getZaproxyInformationPollingInterval() {
			return zaproxyInformationPollingInterval;
		}

		public void setZaproxyInformationPollingInterval(long zaproxyInformationPollingInterval) {
			this.zaproxyInformationPollingInterval = zaproxyInformationPollingInterval;
		}

        public long getEdwhDeploymentInformationPollingInterval() {
            return edwhDeploymentInformationPollingInterval;
        }

        public void setEdwhDeploymentInformationPollingInterval(long edwhDeploymentInformationPollingInterval) {
            this.edwhDeploymentInformationPollingInterval = edwhDeploymentInformationPollingInterval;
        }

		public long getDwhDeploymentInformationPollingInterval() {
			return dwhDeploymentInformationPollingInterval;
		}

		public void setDwhDeploymentInformationPollingInterval(long dwhDeploymentInformationPollingInterval) {
			this.dwhDeploymentInformationPollingInterval = dwhDeploymentInformationPollingInterval;
		}

		public long getStreamworksStatusInformationPollingInterval() {
			return streamworksStatusInformationPollingInterval;
		}

		public void setStreamworksStatusInformationPollingInterval(long streamworksStatusInformationPollingInterval) {
			this.streamworksStatusInformationPollingInterval = streamworksStatusInformationPollingInterval;
		}

		public long getEnvironmentTimestampInterval() {
            return environmentTimestampInterval;
        }

		public void setEnvironmentTimestampInterval(long environmentTimestampInterval) {
			this.environmentTimestampInterval = environmentTimestampInterval;
		}
	}

	public static class PollingInitialDelayMs {
		private long first;
		private long second;
		private long third;

		public long getFirst() {
			return first;
		}

		public void setFirst(long first) {
			this.first = first;
		}

		public long getSecond() {
			return second;
		}

		public void setSecond(long second) {
			this.second = second;
		}

		public long getThird() {
			return third;
		}

		public void setThird(long third) {
			this.third = third;
		}
	}
}
