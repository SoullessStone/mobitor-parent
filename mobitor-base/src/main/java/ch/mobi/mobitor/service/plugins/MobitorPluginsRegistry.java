package ch.mobi.mobitor.service.plugins;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugins.api.MobitorPlugin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.*;

@Service
public class MobitorPluginsRegistry {

    private List<MobitorPlugin> configPlugins = new ArrayList<>();

    private Map<String, MobitorPlugin> confPluginsMap = new HashMap<>();

    @Autowired
    public MobitorPluginsRegistry(Optional<List<MobitorPlugin>> optConfigPlugins) {
        optConfigPlugins.ifPresent(plugins -> this.configPlugins = plugins);
    }

    @PostConstruct
    public void initialize() {
        configPlugins.forEach(p -> confPluginsMap.put(p.getConfigPropertyName(), p));
    }

    public List<MobitorPlugin> getConfigPlugins() {
        return configPlugins;
    }

    public MobitorPlugin getPlugin(String configPropertyName) {
        return this.confPluginsMap.get(configPropertyName);
    }

}
