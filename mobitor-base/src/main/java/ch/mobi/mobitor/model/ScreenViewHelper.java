package ch.mobi.mobitor.model;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.config.EnvironmentConfigProperties;
import ch.mobi.mobitor.config.EnvironmentPipeline;
import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.domain.screen.ServerContext;
import ch.mobi.mobitor.domain.screen.information.VersionInformation;
import ch.mobi.mobitor.plugin.dwh.domain.DwhDeploymentsInformation;
import ch.mobi.mobitor.plugin.edwh.domain.EdwhDeploymentsInformation;
import ch.mobi.mobitor.plugin.hpalm.domain.AlmInformation;
import ch.mobi.mobitor.plugin.jira.domain.JiraInformation;
import ch.mobi.mobitor.plugin.kubernetes.domain.KubernetesBatchJobInformation;
import ch.mobi.mobitor.plugin.liima.config.AppServerConfig;
import ch.mobi.mobitor.plugin.liima.domain.LiimaInformation;
import ch.mobi.mobitor.plugin.liima.service.ServersConfigurationService;
import ch.mobi.mobitor.plugin.nexusiq.domain.NexusIqInformation;
import ch.mobi.mobitor.plugin.nexusiq.service.scheduling.NexusIqConfigurationService;
import ch.mobi.mobitor.plugin.rest.domain.FlywayRestServiceResponse;
import ch.mobi.mobitor.plugin.rest.domain.RestCallInformation;
import ch.mobi.mobitor.plugin.sonarqube.domain.SonarInformation;
import ch.mobi.mobitor.plugin.sonarqube.rule.SonarReportLimitsRule;
import ch.mobi.mobitor.plugin.streamworks.domain.StreamworksStatusInformation;
import ch.mobi.mobitor.plugin.swd.domain.SwdDeploymentInformation;
import ch.mobi.mobitor.plugin.teamcity.domain.TeamCityBuildInformation;
import ch.mobi.mobitor.plugin.teamcity.domain.TeamCityCoverageInformation;
import ch.mobi.mobitor.plugin.teamcity.domain.TeamCityProjectInformation;
import ch.mobi.mobitor.plugin.teamcity.rule.TeamCityCoverageLimitsRule;
import ch.mobi.mobitor.plugin.zaproxy.domain.ZaproxyInformation;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformation;
import ch.mobi.mobitor.service.EnvironmentsConfigurationService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

import static ch.mobi.mobitor.plugin.rest.domain.RestCallInformation.REST;

@Component
public class ScreenViewHelper {

	private static final Logger LOG = LoggerFactory.getLogger(ScreenViewHelper.class);

	private final ServersConfigurationService serversConfigurationService;
	private final EnvironmentsConfigurationService environmentsConfigurationService;
	private final SonarReportLimitsRule sonarReportLimitsRule;
	private final NexusIqConfigurationService nexusIqConfigurationService;
	private final TeamCityCoverageLimitsRule teamCityCoverageLimitRules;

	@Autowired
	public ScreenViewHelper(ServersConfigurationService serversConfigurationService,
			SonarReportLimitsRule sonarReportLimitsRule,
			EnvironmentsConfigurationService environmentsConfigurationService,
			NexusIqConfigurationService nexusIqConfigurationService,
			TeamCityCoverageLimitsRule teamCityCoverageLimitRules) {
		this.serversConfigurationService = serversConfigurationService;
		this.sonarReportLimitsRule = sonarReportLimitsRule;
		this.environmentsConfigurationService = environmentsConfigurationService;
		this.nexusIqConfigurationService = nexusIqConfigurationService;
		this.teamCityCoverageLimitRules = teamCityCoverageLimitRules;
	}

	public String getCssClassForServer(LiimaInformation liimaInformation) {
		if (liimaInformation == null || liimaInformation.getState() == null) {
			return "server-unknown";
		}

		switch (liimaInformation.getState()) {
		// success
		case "success":
			return "server-success";
		// failed
		case "failed":
			return "server-error";
		// canceled
		// rejected
		case "canceled":
		case "rejected":
			return "server-abort";
		default:
			// delayed
			// PRE_DEPLOYMENT
			// progress
			// READY_FOR_DEPLOYMENT
			// requested
			// scheduled
			// simulating
			return "server-working";
		}
	}

	private boolean isRunning(String state) {
		return "running".equals(state);
	}

	private String getCssClassForBuild(String status, String state) {
		String cssClass = "";

		switch (status) {
		case "SUCCESS":
			cssClass += "build-success";
			break;
		case "FAILURE":
			cssClass += "build-error";
			break;
		default:
			cssClass += "";
		}

		// state(); // running,finished
		if (isRunning(state)) {
			cssClass += " build-running";
		}

		return cssClass;
	}

	public String getCssClassForProject(TeamCityProjectInformation teamCityProjectInformation) {
		if (teamCityProjectInformation == null || teamCityProjectInformation.getStatus() == null) {
			return "build-unknown";
		}

		return getCssClassForBuild(teamCityProjectInformation.getStatus(), teamCityProjectInformation.isRunning() ? "running" : "finished");
	}

	public String getCssClassForBuild(TeamCityBuildInformation tcInformation) {
		if (tcInformation == null || tcInformation.getStatus() == null) {
			return "build-unknown";
		}

		return getCssClassForBuild(tcInformation.getStatus(), tcInformation.getState());
	}

	public String getEnvironmentClass(String environment) {
		EnvironmentPipeline environmentPipeline = this.environmentsConfigurationService.getPipeline(environment);

		switch (environmentPipeline) {
		case AGILE:
			return "deliveryEnvironmentAgile";
		case CONVENTIONAL:
			return "deliveryEnvironmentConventional";
		default:
			return "deliveryEnvironment";
		}
	}

	public String getApplicationScreenName(String applicationName) {
		String screenName = this.serversConfigurationService.getApplicationScreenName(applicationName);
		if (!StringUtils.isBlank(screenName)) {
			return screenName;
		}
		if (applicationName.startsWith("ch_mobi_")) {
			return StringUtils.substringAfter(applicationName, "ch_mobi_");
		}
		return applicationName;
	}

	public String getServerScreenName(String serverName) {
		AppServerConfig appServerConfig = this.serversConfigurationService.getAppServerNameToConfigMap().get(serverName);

		return appServerConfig != null ? appServerConfig.getLabel() : serverName;
	}

	public String getVersionFromVersionInformation(ServerContext serverContext, String applicationName) {
		String version = "";
		List<ApplicationInformation> matchingInfoList = serverContext
				.getMatchingInformationByApplicationName(applicationName);
		if (CollectionUtils.isNotEmpty(matchingInfoList)) {
			for (ApplicationInformation appInfo : matchingInfoList) {
				if (appInfo instanceof VersionInformation) {
					VersionInformation vi = (VersionInformation) appInfo;
					version = vi.getVersion();
				}
			}

			return version;
		} else {
			LOG.debug("Could not determine version for application: " + applicationName);
			return version;
		}
	}

	public EnvironmentConfigProperties getEnvironmentConfig(String environment) {
		// TODO hackerish crap: environment config is used in the rest plugin only, so some classes are not part of the plugin api
		// since information to environments needs to be provided somehow, some EnvironmentInformationPlugin interface might solve this?
		// meanwhile....
		return environmentsConfigurationService.getEnvironmentConfig(environment);
	}

	public String getSonarCssClass(SonarInformation sonarInformation) {
		return sonarReportLimitsRule.ruleViolated(sonarInformation) ? "sonarViolated" : "sonarConform";
	}

	public String getCoverageCssClass(TeamCityCoverageInformation teamCityCoverageInformation) {
		return teamCityCoverageLimitRules.ruleViolated(teamCityCoverageInformation) ? "statusError" : "statusOk";
	}

	public String getRestCssClass(RestCallInformation restInformation) {
		if (restInformation.getRestServiceResponses().size() == 0) {
			return "rest-unknown";
		}

		if (restInformation.hasAnyErrors()) {
			return "rest-error";
		} else {
			return "rest-success";
		}
	}

    public long getNumberOfRestErrors(Screen screen) {
        List<RestCallInformation> restCallInfos = screen.getMatchingInformation(REST);
        long nbrOfErrors = restCallInfos.stream().mapToLong(RestCallInformation::getNumberOfErrors).sum();

		return nbrOfErrors;
	}

    public long getTotalNumberOfRestCalls(Screen screen) {
        List<RestCallInformation> restCallInfos = screen.getMatchingInformation(REST);
        long nbrOfRestCalls = restCallInfos.stream().mapToLong(RestCallInformation::getNumberOfRestCalls).sum();

		return nbrOfRestCalls;
	}

	public String getRestErrorsCssGradient(long errors, long max) {
		if (max == 0) {
			return "";
		}
		long percentage = errors * 100 / max;
		return String.format("background: linear-gradient(to right, red %1$d%%, green %1$d%%);", percentage);
	}

	public String getCssClassForNexusIq(NexusIqInformation nexusIqInformation) {
		if (nexusIqInformation == null || !nexusIqInformation.isInformationUpdated() || nexusIqInformation.hasError()) {
			return "nexus-iq-unknown";
		}

		if (nexusIqInformation.getNexusIqViolations() == 0) {
			return "nexus-iq-ok";
		} else {
			return "nexus-iq-error";
		}
	}

	public String getPolicySummary(NexusIqInformation nexusIqInformation) {
		StringBuilder message = new StringBuilder();
		if (nexusIqInformation == null) {
			message.append("No information");
		} else if (!nexusIqInformation.isInformationUpdated()) {
			message.append(nexusIqInformation.getPublicId()).append(" not current");
		} else if (nexusIqInformation.hasError()) {
			message.append(nexusIqInformation.getPublicId()).append(" not found");
		} else if (nexusIqInformation.getViolationsMap().size() == 0) {
			message.append(nexusIqInformation.getPublicId()).append(": congratulations!");
		} else {
			Map<String, Integer> violationsMap = nexusIqInformation.getViolationsMap();
			for (Map.Entry<String, Integer> entry : violationsMap.entrySet()) {
				String policyId = entry.getKey();
				Integer violations = entry.getValue();

				String label = nexusIqConfigurationService.getLabel(policyId);
				message.append(label).append(": ").append(violations).append(" ");
			}
		}

		return message.toString();
	}

	public String getCssClassForZaproxy(ZaproxyInformation zaproxyInformation) {
		if (zaproxyInformation == null || !zaproxyInformation.isInformationUpdated()) {
			return "zaproxy-unknown";
		}

		if (zaproxyInformation.getHighNumber() > 0) {
			return "zaproxy-high";
		}
		if (zaproxyInformation.getMediumNumber() > 0) {
			return "zaproxy-medium";
		}
		if (zaproxyInformation.getLowNumber() > 0) {
			return "zaproxy-low";
		}
		if (zaproxyInformation.getInfoNumber() > 0) {
			return "zaproxy-info";
		} else {
			return "zaproxy-unknown";
		}
	}

	public String getCssClassForJira(JiraInformation jiraInformation) {
		if (!jiraInformation.isInformationUpdated()) {
			return "jira-pending";
		} else if (jiraInformation.getTotal() == 0) {
			return "jira-ok";
		} else if (jiraInformation.getCriticalIssues() > 0) {
			return "jira-error";
		} else if (jiraInformation.getMajorIssues() > 0) {
			return "jira-warning";
		} else {
			return "jira-info";
		}
	}

	public String getRestartCountClass(long restartCount) {
		if (restartCount <= 1) {
			return "restarts-ok";
		} else if (restartCount < 5) {
			return "restarts-warning";
		} else {
			return "restarts-error";
		}
	}

	public String getCssClassForDb(FlywayRestServiceResponse flywayResponse) {
		if ("FUTURE_SUCCESS".equals(flywayResponse.getState())) {
			return "flyway-almost-ok";
		} else if ("SUCCESS".equals(flywayResponse.getState())) {
			return "flyway-ok";
		} else if (StringUtils.isBlank(flywayResponse.getState())) {
			return "flyway-unknown";
		} else {
			return "flyway-error";
		}
	}

	public FlywayRestServiceResponse getFlywayRestServiceResponse(RestCallInformation restCallInformation) {
		return FlywayRestServiceResponse.fromRestServiceResponses(restCallInformation.getRestServiceResponses());
	}

	public String getCssClassForAlm(AlmInformation almInformation) {
		if (almInformation.hasRuns() && almInformation.getNumberOfRuns() == almInformation.getPassedRuns()) {
			return "alm-ok";
		} else if (almInformation.hasRuns() && almInformation.getNumberOfRuns() != almInformation.getPassedRuns()) {
			return "alm-error";
		}

		return "alm-unknown";
	}

	public String getKubernetesPodStatusCssClass(KubernetesBatchJobInformation information) {
		if (information != null && information.getStatus() != null) {
			switch (information.getStatus()) {
			case PENDING:
				return "statusRunning";
			case RUNNING:
				return "statusRunning";
			case SUCCEEDED:
				return "statusOk";
			case FAILED:
				return "statusError";
			}
		}
		return "statusUnknown";
	}

	public String getKubernetesPodStatusIconCssClass(KubernetesBatchJobInformation information) {
		if (information != null && information.getStatus() != null) {
			switch (information.getStatus()) {
			case PENDING:
				return "fa-pause-circle";
			case RUNNING:
				return "fa-play-circle";
			case SUCCEEDED:
				return "fa-check-circle";
			case FAILED:
				return "fa-exclamation-circle";
			}
		}
		return "fa-question-circle";
	}

    public String getSwdDeploymentStatusCssClass(SwdDeploymentInformation information) {
        if (information != null && information.getRevision() != null) {
            if (!information.getRevision().isEmpty()) {
                return "statusOk";
            }
            return "statusError";
        }
        return "statusUnknown";
    }

    public String getEdwhDeploymentStatusCssClass(EdwhDeploymentsInformation information) {
        return information != null && information.isSuccessful() ? "statusOk" : "statusError";
    }

    public String getDwhDeploymentStatusCssClass(DwhDeploymentsInformation information) {
        return information != null && information.isSuccessful() ? "statusOk" : "statusError";
    }

    public String getStreamworksStatusCssClass(StreamworksStatusInformation info) {
        if (info == null || info.getState() == null) {
            return "statusUnknown";
        }
        switch (info.getState()) {
            case PREPARED:
            case RUNNING:
                return "statusRunning";
            case ABNORMALLY_ENDED:
                return "statusError";
            case COMPLETED:
                return "statusOk";
            default:
                return "statusUnknown";
        }
    }
}
